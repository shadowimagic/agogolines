import React, { createContext } from "react";
import { FirebaseConfig } from "../../config";
import app from "firebase/app";
import "firebase/database";
import "firebase/auth";
import "firebase/storage";
import { store } from "./store/store";

import {
  fetchUser,
  emailSignUp,
  mobileSignIn,
  signIn,
  facebookSignIn,
  appleSignIn,
  signOut,
  signOutM,
  updateProfile,
  clearLoginError,
  sendResetMail,
  updatePushToken,
  updateProfileImage,
  requestPhoneOtpDevice,
  deleteUser,
  validateReferer,
  checkUserExists,
  monitorProfileChanges,
} from "./actions/authactions";
import {
  addBooking,
  clearBooking,
  fetchBooking,
  fetchBookingAll,
  updateBooking,
} from "./actions/bookingactions";
import {
  fetchCancelReasons,
  fetchAllCancelReasons,
  addCancellationReason,
} from "./actions/cancelreasonactions";
import { fetchCarTypes, editCarType } from "./actions/cartypeactions";
import { getEstimate, clearEstimate } from "./actions/estimateactions";
import { fetchDriverEarnings } from "./actions/driverearningaction";
import { fetchEarningsReport } from "./actions/earningreportsaction";
import {
  fetchNotifications,
  editNotifications,
  sendNotification,
} from "./actions/notificationactions";
import { fetchPosition, watchDriverPosition } from "./actions/positionaction";
import { fetchPromos, editPromo, editPromos } from "./actions/promoactions";
import { fetchPoint } from "./actions/pointactions";
import {
  addUser,
  fetchUsers,
  fetchDrivers,
  editUser,
} from "./actions/usersactions";
import { fetchUserById } from "./actions/oneuseractions";
import {
  addCreditCard,
  fetchCreditCard,
  chooseCreditCard,
} from "./actions/creditcardactions";
import { addIban, fetchIban, getStripeAccountId } from "./actions/ibanactions";
import {
  fetchSettings,
  editSettings,
  clearSettingsViewError,
} from "./actions/settingsactions";
import {
  fetchPaymentMethods,
  addToWallet,
  updateWalletBalance,
  clearMessage,
} from "./actions/paymentactions";
import {
  fetchCommunication,
  addCommunication,
} from "./actions/communicationactions";

import {
  addBonPlan,
  fetchBonPlan,
  editBonPlan,
  deleteBonPlan,
} from "./actions/bonplanactions";

import {
  addNearby,
  fetchNearby,
  editNearby,
  deleteNearby,
} from "./actions/nearbyactions";

import {
  updateTripPickup,
  updateTripDrop,
  updateTripCar,
  updatSelPointType,
  clearTripPoints,
} from "./actions/tripactions";
import { fetchTasks, acceptTask, cancelTask } from "./actions/taskactions";
import {
  fetchBookingLocations,
  stopLocationFetch,
} from "./actions/locationactions";
import {
  fetchChatMessages,
  sendMessage,
  stopFetchMessages,
} from "./actions/chatactions";
import { fetchWithdraws, completeWithdraw } from "./actions/withdrawactions";

import { MinutesPassed, GetDateString } from "./other/DateFunctions";
import {
  fetchCoordsfromPlace,
  fetchAddressfromCoords,
  getDriveTime,
  getRouteDetails,
} from "./other/GoogleAPIFunctions";
import { GetDistance, GetTripDistance } from "./other/GeoFunctions";

import {
  addDriverInfos,
  deleteDriverInfos,
  fetchDriverInfos,
} from "./actions/driverinfosaction";

import {
  addMesVehicules,
  fetchMesVehicules,
  fetchDriverVehicules,
  updateSelectedVehicule,
} from "./actions/mesvehicules";
import {
  addTravel,
  fetchTravel,
  deleteTravel,
  fetchOneTravel,
  fetchHistorique,
  addTravelHistorique,
} from "./actions/travel";

import {
  addRiderRequest,
  fetchRiderRequest,
  deleteRiderRequest,
  fetchOneRiderRequest,
  updateRiderRequest,
} from "./actions/riderRequest";

import {
  fetchTravelData,
  updateTravel,
  deleteOneTravel,
} from "./actions/traveldata";
import { addBookings, fetchBookings } from "./actions/bookings";
import { addMesDocuments, fetchMesDocuments } from "./actions/mesdocuments";
import { addComment, fetchComment } from "./actions/commentactions";
import { createReservationCourse } from "./actions/reservationCourseActions";

const FirebaseContext = createContext(null);

const FirebaseProvider = ({ children }) => {
  let firebase = {
    app: null,
    database: null,
    auth: null,
    storage: null,
  };

  if (!app.apps.length) {
    app.initializeApp(FirebaseConfig);
    firebase = {
      app: app,
      database: app.database(),
      auth: app.auth(),
      storage: app.storage(),
      authRef: app.auth(),
      facebookProvider: new app.auth.FacebookAuthProvider(),
      googleProvider: new app.auth.GoogleAuthProvider(),
      appleProvider: new app.auth.OAuthProvider("apple.com"),
      phoneProvider: new app.auth.PhoneAuthProvider(),
      RecaptchaVerifier: app.auth.RecaptchaVerifier,
      captchaGenerator: (element) => new app.auth.RecaptchaVerifier(element),
      facebookCredential: (token) =>
        app.auth.FacebookAuthProvider.credential(token),
      googleCredential: (idToken) =>
        app.auth.GoogleAuthProvider.credential(idToken),
      mobileAuthCredential: (verificationId, code) =>
        app.auth.PhoneAuthProvider.credential(verificationId, code),
      usersRef: app.database().ref("users"),
      bookingRef: app.database().ref("bookings"),
      cancelreasonRef: (uid) => app.database().ref("cancelReason/" + uid),
      positionRef: (uid) => app.database().ref("position/" + uid),
      communicationOneRef: app.database().ref("communications/Promotion"),
      communicationRef: (id) => app.database().ref("communications/" + id),
      allCancelreasonRef: app.database().ref("cancelReason"),
      settingsRef: app.database().ref("settings"),
      carTypesRef: app.database().ref("rates/car_type"),
      promoRef: app.database().ref("offers"),
      pointRef: app.database().ref("points"),
      promoEditRef: (id) => app.database().ref("offers/" + id),
      notifyRef: app.database().ref("notifications/"),
      notifyEditRef: (id) => app.database().ref("notifications/" + id),
      singleUserRef: (uid) => app.database().ref("users/" + uid),
      creditCardRef: (uid) => app.database().ref("creditcard/" + uid),
      ibanRef: (uid) => app.database().ref("iban/" + uid),
      commentRef: (uid) => app.database().ref("comments/" + uid),
      commentallRef: app.database().ref("comments"),
      mesVehiculesRef: (uid) => app.database().ref("mesvehicules/" + uid),
      riderRequest: app.database().ref("riderRequest"),
      riderRequestRef: (uid) => app.database().ref("riderRequest/"+ uid),
      travelRef: (uid) => app.database().ref("travels/" + uid),
      travelHistoriqueRef: (uid) =>
        app.database().ref("travelshistorique/" + uid),
      bookingRef: (uid) => app.database().ref("bookings/" + uid),
      bookingallRef: app.database().ref("travelshistorique"),
      travelDataRef: () => app.database().ref("travels/"),
      BookingsRef: (uid) => app.database().ref("bookings/" + uid),
      bonplanRef: app.database().ref("bonplans"),
      nearbyRef: app.database().ref("nearbyPos"),
      singleNearbyRef: (uid) =>
        app.database().ref("nearbyPos/" + uid),
      mesDocumentsRef: (uid) => {
        return {
          database: app.database().ref("mesdocuments/" + uid),
          storage: app.storage().ref(`mesdocuments/${uid}/documents`),
        };
      },
      profileImageRef: (uid) => app.storage().ref(`users/${uid}/profileImage`),
      driverDocsStorageRef: (uid) => app.storage().ref("mesdocuments/" + uid),
      driverDocsRef: (uid) => app.database().ref("mesdocuments/" + uid),
      singleBookingRef: (bookingKey) =>
        app.database().ref("bookings/" + bookingKey),
      singleTravelRef: (travelKey) =>
        app.database().ref("travels/" + travelKey),
      requestedDriversRef: (bookingKey) =>
        app.database().ref("bookings/" + bookingKey + "/requestedDrivers"),
      walletBalRef: (uid) =>
        app.database().ref("users/" + uid + "/walletBalance"),
      walletHistoryRef: (uid) =>
        app.database().ref("users/" + uid + "/walletHistory"),
      referralIdRef: (referralId) =>
        app
          .database()
          .ref("users")
          .orderByChild("referralId")
          .equalTo(referralId),
      trackingRef: (bookingId) => app.database().ref("tracking/" + bookingId),
      tasksRef: () =>
        app.database().ref("bookings").orderByChild("status").equalTo("NEW"),
      singleTaskRef: (uid, bookingId) =>
        app
          .database()
          .ref("bookings/" + bookingId + "/requestedDrivers/" + uid),
      bookingListRef: (uid, role) =>
        role == "rider"
          ? app.database().ref("bookings").orderByChild("customer").equalTo(uid)
          : role == "driver"
          ? app.database().ref("bookings").orderByChild("driver").equalTo(uid)
          : role == "fleetadmin"
          ? app
              .database()
              .ref("bookings")
              .orderByChild("fleetadmin")
              .equalTo(uid)
          : app.database().ref("bookings"),
      chatRef: (bookingId) =>
        app.database().ref("chats/" + bookingId + "/messages"),
      withdrawRef: app.database().ref("withdraws/"),
      reservationCourseRef: (uid) =>
        app.database().ref("reservationcourse" + uid),

      api: {
        MinutesPassed: MinutesPassed,
        GetDateString: GetDateString,
        addReservationCourse: () =>
          app.database().ref("reservationcourse").push(),

        fetchCoordsfromPlace: fetchCoordsfromPlace,
        fetchAddressfromCoords: fetchAddressfromCoords,
        getDriveTime: getDriveTime,
        getRouteDetails: getRouteDetails,

        GetDistance: GetDistance,
        GetTripDistance: GetTripDistance,
        travelRemove: (uid) =>
          app
            .database()
            .ref("travels/" + uid)
            .remove(),
        getTravel: (uid) => app.database().ref("travels/" + uid),
        savePosition: (uid, location) =>
          app
            .database()
            .ref("position/" + uid + "/location")
            .set(location),
        saveTripLocation: (uid, location) =>
          app
            .database()
            .ref("travels/" + uid + "/location")
            .set(location),
        saveTripTime: (uid, data) =>
          app
            .database()
            .ref("travels/" + uid)
            .set(data),

        initData: (uid, data) =>
          app
            .database()
            .ref("travels/" + uid)
            .set(data),
        saveUserLocation: (uid, location) =>
          app
            .database()
            .ref("users/" + uid + "/location")
            .set(location),
        saveTracking: (bookingId, location) =>
          app
            .database()
            .ref("tracking/" + bookingId)
            .push(location),
        validateReferer: validateReferer,
        checkUserExists: checkUserExists,
        fetchBonPlan: () => (dispatch) => fetchBonPlan()(dispatch)(firebase),
        addBonPlan: (data) => (dispatch) =>
          addBonPlan(data)(dispatch)(firebase),
        editBonPlan: (id, data) => (dispatch) =>
          editBonPlan(id, data)(dispatch)(firebase),
        deleteBonPlan: (id) => (dispatch) =>
          deleteBonPlan(id)(dispatch)(firebase),

          fetchNearby: () => (dispatch) => fetchNearby()(dispatch)(firebase),
          addNearby: (data) => (dispatch) =>
            addNearby(data)(dispatch)(firebase),
          editNearby: (id, data) => (dispatch) =>
            editNearby(id, data)(dispatch)(firebase),
          deleteNearby: (id) => (dispatch) =>
            deleteNearby(id)(dispatch)(firebase),

        fetchUser: () => (dispatch) => fetchUser()(dispatch)(firebase),
        emailSignUp: (data) => emailSignUp(data)(firebase),
        mobileSignIn: (verficationId, code) => (dispatch) =>
          mobileSignIn(verficationId, code)(dispatch)(firebase),
        signIn: (email, password) => (dispatch) =>
          signIn(email, password)(dispatch)(firebase),
        facebookSignIn: (token) => (dispatch) =>
          facebookSignIn(token)(dispatch)(firebase),
        appleSignIn: (credentialData) => (dispatch) =>
          appleSignIn(credentialData)(dispatch)(firebase),
        signOut: () => (dispatch) => signOut()(dispatch)(firebase),
        signOutM: () => (dispatch) => signOutM()(dispatch)(firebase),
        updateProfile: (userAuthData, updateData) => (dispatch) =>
          updateProfile(userAuthData, updateData)(dispatch)(firebase),
        monitorProfileChanges: () => (dispatch) =>
          monitorProfileChanges()(dispatch)(firebase),
        clearLoginError: () => (dispatch) =>
          clearLoginError()(dispatch)(firebase),
        addBooking: (bookingData) => (dispatch) =>
          addBooking(bookingData)(dispatch)(firebase),
        clearBooking: () => (dispatch) => clearBooking()(dispatch)(firebase),
        updateBooking: (booking) => (dispatch) =>
          updateBooking(booking)(dispatch)(firebase),
        cancelBooking: (data) => (dispatch) =>
          cancelBooking(data)(dispatch)(firebase),
        fetchCancelReasons: () => (dispatch) =>
          fetchCancelReasons()(dispatch)(firebase),
        fetchAllCancelReasons: () => (dispatch) =>
          fetchAllCancelReasons()(dispatch)(firebase),
        addCancellationReason: (data) => (dispatch) =>
          addCancellationReason(data)(dispatch)(firebase),
        fetchCarTypes: () => (dispatch) => fetchCarTypes()(dispatch)(firebase),
        editCarType: (carTypes, method) => (dispatch) =>
          editCarType(carTypes, method)(dispatch)(firebase),
        getEstimate: (bookingData) => (dispatch) =>
          getEstimate(bookingData)(dispatch)(firebase),
        clearEstimate: () => (dispatch) => clearEstimate()(dispatch)(firebase),
        fetchSettings: () => (dispatch) => fetchSettings()(dispatch)(firebase),
        editSettings: (settings) => (dispatch) =>
          editSettings(settings)(dispatch)(firebase),
        clearSettingsViewError: () => (dispatch) =>
          clearSettingsViewError()(dispatch)(firebase),
        sendResetMail: (email) => (dispatch) =>
          sendResetMail(email)(dispatch)(firebase),
        fetchDriverEarnings: (uid, role) => (dispatch) =>
          fetchDriverEarnings(uid, role)(dispatch)(firebase),
        fetchEarningsReport: () => (dispatch) =>
          fetchEarningsReport()(dispatch)(firebase),
        fetchNotifications: (role) => (dispatch) =>
          fetchNotifications(role)(dispatch)(firebase),
        editNotifications: (notifications, method) => (dispatch) =>
          editNotifications(notifications, method)(dispatch)(firebase),
        sendNotification: (notification) => (dispatch) =>
          sendNotification(notification)(dispatch)(firebase),
        fetchPromos: () => (dispatch) => fetchPromos()(dispatch)(firebase),
        fetchPosition: (uid) => (dispatch) =>
          fetchPosition(uid)(dispatch)(firebase),
        fetchPoint: () => (dispatch) => fetchPoint()(dispatch)(firebase),
        editPromo: (promos, method) => (dispatch) =>
          editPromo(promos, method)(dispatch)(firebase),
        editPromos: (promos) => (dispatch) =>
          editPromos(promos)(dispatch)(firebase),
        fetchUsers: () => (dispatch) => fetchUsers()(dispatch)(firebase),
        addDriverInfos: (driverinfos) => (dispatch) =>
          addDriverInfos(driverinfos)(dispatch),
        deleteDriverInfos: () => (dispatch) => deleteDriverInfos()(dispatch),
        fetchDriverInfos: () => (dispatch) =>
          fetchDriverInfos()(dispatch)(firebase),
        fetchDrivers: () => (dispatch) => fetchDrivers()(dispatch)(firebase),
        addUser: (userdata) => (dispatch) =>
          addUser(userdata)(dispatch)(firebase),
        editUser: (id, user) => (dispatch) =>
          editUser(id, user)(dispatch)(firebase),
        updatePushToken: (userAuthData, token, platform) => (dispatch) =>
          updatePushToken(userAuthData, token, platform)(dispatch)(firebase),
        updateProfileImage: (userAuthData, imageBlob) => (dispatch) =>
          updateProfileImage(userAuthData, imageBlob)(dispatch)(firebase),
        requestPhoneOtpDevice: (phoneNumber, appVerifier) => (dispatch) =>
          requestPhoneOtpDevice(phoneNumber, appVerifier)(dispatch)(firebase),
        deleteUser: (uid) => (dispatch) => deleteUser(uid)(dispatch)(firebase),
        fetchPaymentMethods: () => (dispatch) =>
          fetchPaymentMethods()(dispatch)(firebase),
        addToWallet: (uid, amount) => (dispatch) =>
          addToWallet(uid, amount)(dispatch)(firebase),
        updateWalletBalance: (balance, details) => (dispatch) =>
          updateWalletBalance(balance, details)(dispatch)(firebase),
        clearMessage: () => (dispatch) => clearMessage()(dispatch)(firebase),
        updateTripPickup: (pickupAddress) => (dispatch) =>
          updateTripPickup(pickupAddress)(dispatch)(firebase),
        updateTripDrop: (dropAddress) => (dispatch) =>
          updateTripDrop(dropAddress)(dispatch)(firebase),
        updateTripCar: (selectedCar) => (dispatch) =>
          updateTripCar(selectedCar)(dispatch)(firebase),
        updatSelPointType: (selection) => (dispatch) =>
          updatSelPointType(selection)(dispatch)(firebase),
        clearTripPoints: () => (dispatch) =>
          clearTripPoints()(dispatch)(firebase),
        fetchTasks: () => (dispatch) => fetchTasks()(dispatch)(firebase),
        acceptTask: (userAuthData, task) => (dispatch) =>
          acceptTask(userAuthData, task)(dispatch)(firebase),
        cancelTask: (bookingId) => (dispatch) =>
          cancelTask(bookingId)(dispatch)(firebase),
        fetchBookingLocations: (bookingId) => (dispatch) =>
          fetchBookingLocations(bookingId)(dispatch)(firebase),
        stopLocationFetch: (bookingId) => (dispatch) =>
          stopLocationFetch(bookingId)(dispatch)(firebase),
        fetchChatMessages: (bookingId) => (dispatch) =>
          fetchChatMessages(bookingId)(dispatch)(firebase),
        sendMessage: (data) => (dispatch) =>
          sendMessage(data)(dispatch)(firebase),
        stopFetchMessages: (bookingId) => (dispatch) =>
          stopFetchMessages(bookingId)(dispatch)(firebase),
        fetchWithdraws: () => (dispatch) =>
          fetchWithdraws()(dispatch)(firebase),
        completeWithdraw: (entry) => (dispatch) =>
          completeWithdraw(entry)(dispatch)(firebase),

        addCreditCard: (data) => (dispatch) =>
          addCreditCard(data)(dispatch)(firebase),
        fetchCreditCard: (uid) => (dispatch) =>
          fetchCreditCard(uid)(dispatch)(firebase),
        chooseCreditCard: (cardInfo) => (dispatch) =>
          chooseCreditCard(cardInfo)(dispatch),
        addMesVehicules: (data) => (dispatch) =>
          addMesVehicules(data)(dispatch)(firebase),
        fetchMesVehicules: (uid) => (dispatch) =>
          fetchMesVehicules(uid)(dispatch)(firebase),
        fetchDriverVehicules: (uid) => (dispatch) =>
          fetchDriverVehicules(uid)(dispatch)(firebase),
        updateSelectedVehicule: (vehicule) => (dispatch) =>
          updateSelectedVehicule(vehicule)(dispatch),
        addTravel: (data) => (dispatch) => addTravel(data)(dispatch)(firebase),        
        addRiderRequest: (data) => (dispatch) => addRiderRequest(data)(dispatch)(firebase),
        fetchCommunication: () => (dispatch) =>
          fetchCommunication()(dispatch)(firebase),
        addCommunication: (data) => (dispatch) =>
          addCommunication(data)(dispatch)(firebase),
        fetchHistorique: () => (dispatch) =>
          fetchHistorique()(dispatch)(firebase),
        addTravelHistorique: (data) => (dispatch) =>
          addTravelHistorique(data)(dispatch)(firebase),
        deleteTravel: (data, action) => (dispatch) =>
          deleteTravel(data, action)(dispatch)(firebase),
        deleteRiderRequest: (riderId) => (dispatch) =>
          deleteRiderRequest(riderId)(dispatch)(firebase),
        deleteOneTravel: () => (dispatch) =>
          deleteOneTravel()(dispatch)(firebase),
        fetchTravel: () => (dispatch) => fetchTravel()(dispatch)(firebase),        
        fetchRiderRequest: () => (dispatch) => fetchRiderRequest()(dispatch)(firebase),
        fetchOneTravel: (driverid) => (dispatch) =>
          fetchOneTravel(driverid)(dispatch)(firebase), 
        fetchOneRiderRequest: (rideId) => (dispatch) =>
        fetchOneRiderRequest(rideId)(dispatch)(firebase),
        fetchTravelData: (donner) => (dispatch) =>
          fetchTravelData(donner)(dispatch)(firebase),
        createReservationCourse: (donner) => (dispatch) =>
          createReservationCourse(donner)(dispatch),
        updateTravel: (travel, from) => (dispatch) =>
          updateTravel(travel, from)(dispatch)(firebase),
        updateRiderRequest: (travel) => (dispatch) =>
          updateRiderRequest(travel)(dispatch)(firebase),
        updateBooking: (booking) => (dispatch) =>
          updateBooking(booking)(dispatch)(firebase),
        addBookings: (data) => (dispatch) =>
          addBookings(data)(dispatch)(firebase),
        fetchBookings: (uid) => (dispatch) =>
          fetchBookings(uid)(dispatch)(firebase),
        fetchBooking: () => (dispatch) => fetchBooking()(dispatch)(firebase),
        fetchBookingAll: (type) => (dispatch) =>
          fetchBookingAll(type)(dispatch)(firebase),
        addMesDocuments: (imageBlob) => (dispatch) =>
          addMesDocuments(imageBlob)(dispatch)(firebase),
        fetchMesDocuments: (uid) => (dispatch) =>
          fetchMesDocuments(uid)(dispatch)(firebase),
        addIban: (data) => (dispatch) => addIban(data)(dispatch)(firebase),
        fetchIban: (uid) => (dispatch) => fetchIban(uid)(dispatch)(firebase),
        getStripeAccountId: (uid) => (dispatch) =>
          getStripeAccountId(uid)(dispatch)(firebase),
        addComment: (data, idDriver) => (dispatch) =>
          addComment(data, idDriver)(dispatch)(firebase),
        fetchComment: (uid) => (dispatch) =>
          fetchComment(uid)(dispatch)(firebase),
        fetchUserById: (uid) => (dispatch) =>
          fetchUserById(uid)(dispatch)(firebase),
        watchDriverPosition: (uid) => (dispatch) =>
          watchDriverPosition(uid)(dispatch)(firebase),
      },
    };
  }
  return (
    <FirebaseContext.Provider value={firebase}>
      {children}
    </FirebaseContext.Provider>
  );
};
export { FirebaseContext, FirebaseProvider, store };
