import { TOGGLE_ALERT_DELETE_SCHOW_DEMAND, TOGGLE_ALERT_NO_ACTION } from '../store/types';

export const toggleProposeAlertDeleteSchowDemand = () => (dispatch) => {
	dispatch({
		type: TOGGLE_ALERT_DELETE_SCHOW_DEMAND,
	});
};

export const toggleProposeAlertNoAction= () => (dispatch) => {
	dispatch({
		type: TOGGLE_ALERT_NO_ACTION,
	});
};