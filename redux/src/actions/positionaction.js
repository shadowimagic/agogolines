import {
  FETCH_ALL_POSITION,
  FETCH_ALL_POSITION_SUCCESS,
  FETCH_ALL_POSITION_FAILED,
  WATCH_DRIVER_POSITION,
  WATCH_DRIVER_POSITION_FAILED,
  WATCH_DRIVER_POSITION_SUCCESS,
} from "../store/types";

export const fetchPosition = (uid) => (dispatch) => (firebase) => {
  const { positionRef } = firebase;

  dispatch({
    type: FETCH_ALL_POSITION,
    payload: null,
  });
  positionRef(uid).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: FETCH_ALL_POSITION_SUCCESS,
        payload: snapshot.val(),
      });
    } else {
      dispatch({
        type: FETCH_ALL_POSITION_FAILED,
        payload: "No position available.",
      });
    }
  });
};

export const watchDriverPosition = (uid) => (dispatch) => (firebase) => {
  const { positionRef } = firebase;
  dispatch({
    type: WATCH_DRIVER_POSITION,
    payload: null,
  });
  positionRef(uid).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: WATCH_DRIVER_POSITION_SUCCESS,
        payload: snapshot.val()?.location,
      });
    } else {
      dispatch({
        type: WATCH_DRIVER_POSITION_FAILED,
        payload: "No position available.",
      });
    }
  });
};
