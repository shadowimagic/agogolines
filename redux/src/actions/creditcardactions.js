import {
  FETCH_ALL_CREDITCARD,
  FETCH_ALL_CREDITCARD_SUCCESS,
  FETCH_ALL_CREDITCARD_FAILED,
  ADD_CREDITCARD,
  ADD_CREDITCARD_SUCCESS,
  ADD_CREDITCARD_FAILED,
  DELETE_CREDITCARD,
  DELETE_CREDITCARD_SUCCESS,
  DELETE_CREDITCARD_FAILED,
  CHOOSE_CREDIT_CARD,
} from "../store/types";

export const fetchCreditCard = () => (dispatch) => (firebase) => {
  const { auth, creditCardRef } = firebase;

  dispatch({
    type: FETCH_ALL_CREDITCARD,
    payload: null,
  });
  creditCardRef(auth.currentUser.uid).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: FETCH_ALL_CREDITCARD_SUCCESS,
        payload: snapshot.val(),
      });
    } else {
      dispatch({
        type: FETCH_ALL_CREDITCARD_FAILED,
        payload: "No creditcard available.",
      });
    }
  });
};

export const addCreditCard = (creditcarddata) => (dispatch) => (firebase) => {
  const { auth, creditCardRef } = firebase;

  dispatch({
    type: ADD_CREDITCARD,
    payload: creditcarddata,
  });
  //console.log("valeur de data1:"+JSON.stringify(creditcarddata))
  creditCardRef(auth.currentUser.uid)
    .set(creditcarddata)
    .then((data) => {
      // console.log("valeur de data2:"+JSON.stringify(data))
      dispatch({
        type: ADD_CREDITCARD_SUCCESS,
        payload: null,
      });
    })
    .catch((error) => {
      // console.log("valeur de error:3"+JSON.stringify(error))
      dispatch({
        type: ADD_CREDITCARD_FAILED,
        payload: error,
      });
    });
};

export const deleteCreditCard = (id) => (dispatch) => (firebase) => {
  const { auth, creditCardRef } = firebase;

  dispatch({
    type: DELETE_CREDITCARD,
    payload: id,
  });

  creditCardRef(auth.currentUser.uid)
    .remove()
    .then(() => {
      dispatch({
        type: DELETE_CREDITCARD_SUCCESS,
        payload: null,
      });
    })
    .catch((error) => {
      dispatch({
        type: DELETE_CREDITCARD_FAILED,
        payload: error,
      });
    });
};

export const chooseCreditCard = (cardInfo) => (dispatch) => {
  dispatch({
    type: CHOOSE_CREDIT_CARD,
    payload: cardInfo || null,
  });
};
