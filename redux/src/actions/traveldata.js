import {
  FETCH_ALL_TRAVELDATA,
  UPDATE_TRAVEL,
  FETCH_ALL_TRAVELDATA_SUCCESS,
  FETCH_ALL_TRAVELDATA_FAILED,
  FETCH_ONE_TRAVEL,
  FETCH_ONE_TRAVEL_SUCCESS,
  FETCH_ONE_TRAVEL_FAILED,
  DELETE_TRAVEL_ONE,
  DELETE_TRAVEL_SUCCESS_ONE,
  DELETE_TRAVEL_FAILED_ONE,
} from "../store/types";

import { language } from "../../../config";
import { RequestPushMsg } from "../other/NotificationFunctions";

import { GetVilleMin } from "../other/VilleFunctions";
import { GetDistance } from "../other/GeoFunctions";

export const fetchTravelData = (donner) => (dispatch) => async (firebase) => {
  const { travelDataRef } = firebase;
  if (donner && donner !== null) {
    //console.log("valeur de travel:" + JSON.stringify(donner));

    dispatch(
      {
        type: FETCH_ALL_TRAVELDATA,
        payload: null,
      },
      { allowMore: true }
    );
    await travelDataRef().on("value", (snapshot) => {
      if (snapshot.val()) {
        const data = snapshot.val();
        const arr = Object.keys(data)
          .filter(
            (i) =>
              data[i].status === "init" &&
              data[i].isFound === false &&
              GetDistance(
                data[i].lat_dep,
                data[i].long_dep,
                donner.lat_dep,
                donner.long_dep
              ) < 90 &&
              donner.passager_nb < data[i].max_passenger_nb &&
              data[i].direction === donner.direction
          )
          .map((i) => {
            data[i].id = i;
            return data[i];
          });
        dispatch(
          {
            type: FETCH_ALL_TRAVELDATA_SUCCESS,
            payload: arr,
          },
          { allowMore: true }
        );
      } else {
        dispatch(
          {
            type: FETCH_ALL_TRAVELDATA_FAILED,
            payload: "No travel available.",
          },
          { allowMore: true }
        );
      }
    });
  } else {
    const { travelDataRef } = firebase;

    dispatch(
      {
        type: FETCH_ALL_TRAVELDATA,
        payload: null,
      },
      { allowMore: true }
    );
    travelDataRef().on("value", (snapshot) => {
      if (snapshot.val()) {
        dispatch(
          {
            type: FETCH_ALL_TRAVELDATA_SUCCESS,
            payload: snapshot.val(),
          },
          { allowMore: true }
        );
      } else {
        dispatch(
          {
            type: FETCH_ALL_TRAVELDATA_FAILED,
            payload: "No travel available.",
          },
          { allowMore: true }
        );
      }
    });
  }
};

export const updateTravel = (travel, from) => (dispatch) => (firebase) => {
  const { singleTravelRef } = firebase;
  dispatch(
    {
      type: UPDATE_TRAVEL,
      payload: travel,
    },
    { allowMore: true }
  );
  if (travel?.driver_id) {
    singleTravelRef(travel.driver_id).update(travel);
  }
};

export const fetchOneTravel = (driver_id) => (dispatch) => (firebase) => {
  const { singleTravelRef } = firebase;

  dispatch(
    {
      type: FETCH_ONE_TRAVEL,
      payload: null,
    },
    { allowMore: true }
  );
  singleTravelRef(driver_id).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch(
        {
          type: FETCH_ONE_TRAVEL_SUCCESS,
          payload: snapshot.val(),
        },
        { allowMore: true }
      );
    } else {
      dispatch(
        {
          type: FETCH_ONE_TRAVEL_FAILED,
          payload: "No travel available.",
        },
        { allowMore: true }
      );
    }
  });
};

export const deleteOneTravel = () => (dispatch) => (firebase) => {
  const { auth, travelRef } = firebase;

  dispatch(
    {
      type: DELETE_TRAVEL_ONE,
      payload: null,
    },
    { allowMore: true }
  );
  travelRef(auth.currentUser.uid)
    .remove()
    .then(() => {
      dispatch(
        {
          type: DELETE_TRAVEL_SUCCESS_ONE,
          payload: null,
        },
        { allowMore: true }
      );
    })
    .catch((error) => {
      dispatch(
        {
          type: DELETE_TRAVEL_FAILED_ONE,
          payload: error,
        },
        { allowMore: true }
      );
    });
};
