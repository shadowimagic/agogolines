import {
	FETCH_ALL_BOOKINKS,
	FETCH_ALL_BOOKINKS_SUCCESS,
	FETCH_ALL_BOOKINKS_FAILED,
	ADD_BOOKINKS,
	ADD_BOOKINKS_SUCCESS,
	ADD_BOOKINKS_FAILED,
} from '../store/types';

export const fetchBookings = () => (dispatch) => (firebase) => {
	const { auth, BookingsRef } = firebase;

	dispatch({
		type: FETCH_ALL_BOOKINKS,
		payload: null,
	});
	BookingsRef(auth.currentUser.uid).on('value', (snapshot) => {
		if (snapshot.val()) {
			dispatch({
				type: FETCH_ALL_BOOKINKS_SUCCESS,
				payload: snapshot.val(),
			});
		} else {
			dispatch({
				type: FETCH_ALL_BOOKINKS_FAILED,
				payload: 'No bookings available.',
			});
		}
	});
};

export const addBookings = (bookingsdata) => (dispatch) => (firebase) => {
	const { auth, BookingsRef } = firebase;

	dispatch({
		type: ADD_BOOKINKS,
		payload: mesvehiculesdata,
	});
	//console.log("valeur de data1:"+JSON.stringify(mesvehiculesdata))
	BookingsRef(auth.currentUser.uid)
		.set(bookingsdata)
		.then((data) => {
			//console.log('valeur de data2:' + JSON.stringify(data));
			dispatch({
				type: ADD_BOOKINKS_SUCCESS,
				payload: null,
			});
		})
		.catch((error) => {
			//console.log('valeur de error:3' + JSON.stringify(error));
			dispatch({
				type: ADD_BOOKINKS_FAILED,
				payload: error,
			});
		});
};
