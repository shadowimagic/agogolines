import {
  FETCH_ALL_MESDOCUMENTS,
  FETCH_ALL_MESDOCUMENTS_SUCCESS,
  FETCH_ALL_MESDOCUMENTS_FAILED,
  ADD_MESDOCUMENTS,
  ADD_MESDOCUMENTS_SUCCESS,
  ADD_MESDOCUMENTS_FAILED,
} from "../store/types";

export const fetchMesDocuments = () => (dispatch) => (firebase) => {

  const {
    auth,
    mesDocumentsRef
  } = firebase; 

  dispatch({
    type: FETCH_ALL_MESDOCUMENTS,
    payload: null
  });
  mesDocumentsRef(auth.currentUser.uid).on("value", snapshot => {
    if (snapshot.val()) {
      dispatch({
        type: FETCH_ALL_MESDOCUMENTS_SUCCESS,
        payload: snapshot.val()
      });
    } else {
      dispatch({
        type: FETCH_ALL_MESDOCUMENTS_FAILED,
        payload: "No mesdocuments available."
      });
    }
  });
};



export const addMesDocuments = (imageBlob) => (dispatch) => (firebase) => {
 
  const {
    auth,
    mesDocumentsRef
  } = firebase;
  //console.log(imageBlob);

  dispatch({
    type: ADD_MESDOCUMENTS,
    payload: imageBlob
  });
  mesDocumentsRef(auth.currentUser.uid).storage.put(imageBlob[1]).then(() => {
    return mesDocumentsRef(auth.currentUser.uid).getDownloadURL()
  }).then(url => {
   // console.log(url);
    mesDocumentsRef(auth.currentUser.uid).set(url).then((data) => {
    // console.log("valeur de data2:"+JSON.stringify(data))
      dispatch({
        type: ADD_MESDOCUMENTS_SUCCESS,
        payload: null
      });
    }).catch((error) => {
    // console.log("valeur de error:3"+JSON.stringify(error))
      dispatch({
        type: ADD_MESDOCUMENTS_FAILED,
        payload: error
      });
    });
  })


}



// export const deleteMesDocuments = (id) => (dispatch) => (firebase) => {

//   const {
//     auth,
//     mesDocumentsRef
//   } = firebase;

//   dispatch({
//     type: DELETE_MESDOCUMENTS,
//     payload: id
//   });

//   mesDocumentsRef(auth.currentUser.uid).remove().then(() => {
//     dispatch({
//       type: DELETE_MESDOCUMENTS_SUCCESS,
//       payload: null
//     });
//   }).catch((error) => {
//     dispatch({
//       type: DELETE_MESDOCUMENTS_FAILED,
//       payload: error
//     });
//   });
// }