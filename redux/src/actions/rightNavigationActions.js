
import { TOGGLE_DRAWER, OPEN_RIGHT_DRAWER, RESET_RIGHT_DRAWER} from "../store/types";


export const toggleRightDrawer = () => (dispatch) => {
  dispatch({
    type: TOGGLE_DRAWER,
  });
};

export const resetRightDrawer = () => (dispatch) => {
  dispatch({
    type: RESET_RIGHT_DRAWER,
  });
};

export const openRightDrawer = () => (dispatch) => {
  dispatch({
    type: OPEN_RIGHT_DRAWER,
  });
};