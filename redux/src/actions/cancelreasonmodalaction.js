import {
	TOGGLE_CANCEL_REASON_MODAL,
	CLOSE_CANCEL_REASON_MODAL,
	OPEN_CANCEL_REASON_MODAL,
} from "../store/types";

export const toggleCancelReasonModal = () => (dispatch) => {
	dispatch({
		type: TOGGLE_CANCEL_REASON_MODAL,
	});
};

export const openCancelReasonModal = () => (dispatch) => {
	dispatch({
		type: OPEN_CANCEL_REASON_MODAL,
	});
};

export const closeCancelReasonModal = () => (dispatch) => {
	dispatch({
		type: CLOSE_CANCEL_REASON_MODAL,
	});
};
