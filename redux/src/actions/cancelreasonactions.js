import {
  FETCH_CANCEL_REASONS,
  FETCH_CANCEL_REASONS_SUCCESS,
  FETCH_CANCEL_REASONS_FAILED,
  FETCH_ALL_CANCEL_REASONS,
  FETCH_ALL_CANCEL_REASONS_SUCCESS,
  FETCH_ALL_CANCEL_REASONS_FAILED,
  ADD_CANCELLATION_REASON,
  ADD_CANCELLATION_REASON_SUCCESS,
  ADD_CANCELLATION_REASON_FAILED,
} from "../store/types";

import { language } from "../../../config";

export const fetchCancelReason = () => (dispatch) => (firebase) => {
  const { auth, cancelreasonRef } = firebase;

  dispatch({
    type: FETCH_CANCEL_REASONS,
    payload: null,
  });
  cancelreasonRef(auth.currentUser.uid).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: FETCH_CANCEL_REASONS_SUCCESS,
        payload: snapshot.val(),
      });
    } else {
      dispatch({
        type: FETCH_CANCEL_REASONS_FAILED,
        payload: language.no_cancel_reason,
      });
    }
  });
};

export const fetchAllCancelReasons = () => (dispatch) => (firebase) => {
  const { allCancelreasonRef } = firebase;

  dispatch({
    type: FETCH_ALL_CANCEL_REASONS,
    payload: null,
  });
  allCancelreasonRef.on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: FETCH_ALL_CANCEL_REASONS_SUCCESS,
        payload: snapshot.val(),
      });
    } else {
      dispatch({
        type: FETCH_ALL_CANCEL_REASONS_FAILED,
        payload: language.no_cancel_reason,
      });
    }
  });
};

export const addCancellationReason = (reasons) => (dispatch) => (firebase) => {
  const { auth, cancelreasonRef } = firebase;

  dispatch({
    type: ADD_CANCELLATION_REASON,
    payload: reasons,
  });
  cancelreasonRef(auth.currentUser.uid)
    .push(reasons)
    .then((data) => {
      dispatch({
        type: ADD_CANCELLATION_REASON_SUCCESS,
        payload: null,
      });
    })
    .catch((error) => {
      dispatch({
        type: ADD_CANCELLATION_REASON_FAILED,
        payload: error,
      });
    });
};
