import {
    FETCH_ALL_COMMENT,
    FETCH_ALL_COMMENT_SUCCESS,
    FETCH_ALL_COMMENT_FAILED,
    ADD_COMMENT,
    ADD_COMMENT_SUCCESS,
    ADD_COMMENT_FAILED,
} from "../store/types";


export const fetchComment = (idDriver) => (dispatch) => (firebase) => {
    const {
        commentallRef
    } = firebase;

    dispatch({
        type: FETCH_ALL_COMMENT,
        payload: null
    });
    
    commentallRef.on("value", snapshot => {


      if (snapshot.val()) {
				const data = snapshot.val();
				
				const arr = Object.keys(data)
					.filter(
						(i) =>
							data[i].idDriver=== idDriver 
					)
					.map((i) => {
						data[i].id = i;
						return data[i];
					});

				dispatch({
					type: FETCH_ALL_COMMENT_SUCCESS,
					payload: arr,
				}, { allowMore: true });
			} else {
        dispatch({
          type: FETCH_ALL_COMMENT_FAILED,
          payload: "No comment available"
        });
      }

      });
}

export const addComment = (commentData, idDriver) => (dispatch) => (firebase) => {

    const {
      auth,
      commentRef
    } = firebase;
  
    dispatch({
      type: ADD_COMMENT,
      payload: commentData
    });
    commentRef(idDriver).set(commentData).then((data) => {
      dispatch({
        type: ADD_COMMENT_SUCCESS,
        payload: null
      });
    }).catch((error) => {
      dispatch({
        type: ADD_COMMENT_FAILED,
        payload: error
      });
    });
  }