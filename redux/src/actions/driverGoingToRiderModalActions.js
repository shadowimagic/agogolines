import {
	TOGGLE_DRIVER_GOING_TO_RIDER,
	CLOSE_DRIVER_GOING_TO_RIDER,
	OPEN_DRIVER_GOING_TO_RIDER,
} from "../store/types";

export const toggleDriverGoingToRider = () => (dispatch) => {
	dispatch({
		type: TOGGLE_DRIVER_GOING_TO_RIDER,
	});
};

export const openDriverGoingToRider = () => (dispatch) => {
	dispatch({
		type: OPEN_DRIVER_GOING_TO_RIDER,
	});
};

export const closeDriverGoingToRider = () => (dispatch) => {
	dispatch({
		type: CLOSE_DRIVER_GOING_TO_RIDER,
	});
};
