import {
	TOGGLE_PICKUP_RIDER,
	CLOSE_PICKUP_RIDER,
	OPEN_PICKUP_RIDER,
} from "../store/types";

export const togglePickupRiderModal = () => (dispatch) => {
	dispatch({
		type: TOGGLE_PICKUP_RIDER,
	});
};

export const openPickupRiderModal = () => (dispatch) => {
	dispatch({
		type: OPEN_PICKUP_RIDER,
	});
};

export const closePickupRiderModal = () => (dispatch) => {
	dispatch({
		type: CLOSE_PICKUP_RIDER,
	});
};
