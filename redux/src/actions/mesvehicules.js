import {
  FETCH_ALL_MESVEHICULES,
  FETCH_ALL_MESVEHICULES_SUCCESS,
  FETCH_ALL_MESVEHICULES_FAILED,
  ADD_MESVEHICULES,
  ADD_MESVEHICULES_SUCCESS,
  ADD_MESVEHICULES_FAILED,
  FETCH_DRIVER_MESVEHICULES,
  FETCH_DRIVER_MESVEHICULES_SUCCESS,
  FETCH_DRIVER_MESVEHICULES_FAILED,
  UPDATE_SELECTED_VEHICULE,
} from "../store/types";

export const fetchMesVehicules = () => (dispatch) => (firebase) => {
  const { auth, mesVehiculesRef } = firebase;

  dispatch({
    type: FETCH_ALL_MESVEHICULES,
    payload: null,
  });
  mesVehiculesRef(auth.currentUser.uid).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: FETCH_ALL_MESVEHICULES_SUCCESS,
        payload: snapshot.val(),
      });
    } else {
      dispatch({
        type: FETCH_ALL_MESVEHICULES_FAILED,
        payload: "No mesvehicules available.",
      });
    }
  });
};

export const fetchDriverVehicules = (driverId) => (dispatch) => (firebase) => {
  const { mesVehiculesRef } = firebase;

  dispatch({
    type: FETCH_DRIVER_MESVEHICULES,
    payload: null,
  });
  mesVehiculesRef(driverId).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: FETCH_DRIVER_MESVEHICULES_SUCCESS,
        payload: snapshot.val(),
      });
    } else {
      console.log("something went wrong");
      dispatch({
        type: FETCH_DRIVER_MESVEHICULES_FAILED,
        payload: [],
      });
    }
  });
};

export const addMesVehicules =
  (mesvehiculesdata) => (dispatch) => (firebase) => {
    const { auth, mesVehiculesRef } = firebase;

    dispatch({
      type: ADD_MESVEHICULES,
      payload: mesvehiculesdata,
    });
    //console.log("valeur de data1:"+JSON.stringify(mesvehiculesdata))
    mesVehiculesRef(auth.currentUser.uid)
      .set(mesvehiculesdata)
      .then((data) => {
        console.log("valeur de data2:" + JSON.stringify(data));
        dispatch({
          type: ADD_MESVEHICULES_SUCCESS,
          payload: null,
        });
      })
      .catch((error) => {
        console.log("valeur de error:3" + JSON.stringify(error));
        dispatch({
          type: ADD_MESVEHICULES_FAILED,
          payload: error,
        });
      });
  };

export const updateSelectedVehicule = (vehicule) => (dispatch) => {
  dispatch({
    type: UPDATE_SELECTED_VEHICULE,
    payload: vehicule || null,
  });
};

// export const deleteMesVehicules = (id) => (dispatch) => (firebase) => {

//   const {
//     auth,
//     mesVehiculesRef
//   } = firebase;

//   dispatch({
//     type: DELETE_MESVEHICULES,
//     payload: id
//   });

//   mesVehiculesRef(auth.currentUser.uid).remove().then(() => {
//     dispatch({
//       type: DELETE_MESVEHICULES_SUCCESS,
//       payload: null
//     });
//   }).catch((error) => {
//     dispatch({
//       type: DELETE_MESVEHICULES_FAILED,
//       payload: error
//     });
//   });
// }
