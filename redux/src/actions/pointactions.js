import {
    FETCH_POINTS,
    FETCH_POINTS_SUCCESS,
    FETCH_POINTS_FAILED,
    EDIT_POINTS
  } from "../store/types";
  
  export const fetchPoint = () => (dispatch) => (firebase) => {
  
    const {
      pointRef
    } = firebase;
  
    dispatch({
      type: FETCH_POINTS,
      payload: null
    });
    pointRef.on("value", snapshot => {
      if (snapshot.val()) {
        const data = snapshot.val();
        const arr = Object.keys(data).map(i => {
          data[i].id = i;
          return data[i]
        });
        dispatch({
          type: FETCH_POINTS_SUCCESS,
          payload: arr
        });
      } else {
        dispatch({
          type: FETCH_POINTS_FAILED,
          payload: "No point available."
        });
      }
    });
  };