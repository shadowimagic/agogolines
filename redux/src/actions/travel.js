import {
  FETCH_ALL_TRAVEL,
  FETCH_ALL_TRAVEL_SUCCESS,
  FETCH_ALL_TRAVEL_FAILED,
  ADD_TRAVEL,
  ADD_TRAVEL_SUCCESS,
  ADD_TRAVEL_FAILED,
  ADD_TRAVEL_HIST,
  ADD_TRAVEL_SUCCESS_HIST,
  ADD_TRAVEL_FAILED_HIST,
  DELETE_TRAVEL,
  DELETE_TRAVEL_SUCCESS,
  DELETE_TRAVEL_FAILED,
  FETCH_ONE_TRAVEL,
  FETCH_ONE_TRAVEL_SUCCESS,
  FETCH_ONE_TRAVEL_FAILED,
  FETCH_ALL_CONFIRM_HIST,
  FETCH_ALL_CONFIRM_HIST_SUCCESS,
  FETCH_ALL_CONFIRM_HIST_FAILED,
} from "../store/types";

export const fetchTravel = () => (dispatch) => (firebase) => {
  const { auth, travelRef } = firebase;

  dispatch({
    type: FETCH_ALL_TRAVEL,
    payload: null,
  });
  travelRef(auth.currentUser.uid).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: FETCH_ALL_TRAVEL_SUCCESS,
        payload: snapshot.val(),
      });
    } else {
      dispatch({
        type: FETCH_ALL_TRAVEL_FAILED,
        payload: "No travel available.",
      });
    }
  });
};

export const fetchOneTravel = (driver_id) => (dispatch) => (firebase) => {
  const { travelRef } = firebase;

  dispatch({
    type: FETCH_ONE_TRAVEL,
    payload: null,
  });
  travelRef(driver_id).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: FETCH_ONE_TRAVEL_SUCCESS,
        payload: snapshot.val(),
      });
    } else {
      dispatch({
        type: FETCH_ONE_TRAVEL_FAILED,
        payload: "No travel available.",
      });
    }
  });
};

export const addTravel = (traveldata) => (dispatch) => (firebase) => {
  const { auth, travelRef } = firebase;

  dispatch({
    type: ADD_TRAVEL,
    payload: traveldata,
  });
  if (auth && auth.currentUser.uid && auth.currentUser.uid != undefined) {
    travelRef(traveldata.driver_id)
      .set(traveldata)
      .then((data) => {
        dispatch({
          type: ADD_TRAVEL_SUCCESS,
          payload: null,
        });
      })
      .catch((error) => {
        dispatch({
          type: ADD_TRAVEL_FAILED,
          payload: error,
        });
      });
  }
};

export const addTravelHistorique = (traveldata) => (dispatch) => (firebase) => {
  const { travelHistoriqueRef } = firebase;

  dispatch({
    type: ADD_TRAVEL_HIST,
    payload: traveldata,
  });
  // Push for driver
  travelHistoriqueRef(`${traveldata.driver_id}`)
    .push(traveldata)
    .then((data) => {
      dispatch({
        type: ADD_TRAVEL_SUCCESS_HIST,
        payload: null,
      });
    })
    .catch((error) => {
      dispatch({
        type: ADD_TRAVEL_FAILED_HIST,
        payload: error,
      });
    });
  // Push for rider
  travelHistoriqueRef(`${traveldata.propose_rider_id}`)
    .push(traveldata)
    .then((data) => {
      dispatch({
        type: ADD_TRAVEL_SUCCESS_HIST,
        payload: null,
      });
    })
    .catch((error) => {
      dispatch({
        type: ADD_TRAVEL_FAILED_HIST,
        payload: error,
      });
    });
};

export const fetchHistorique = () => (dispatch) => (firebase) => {
  const { auth, travelHistoriqueRef } = firebase;
  dispatch({
    type: FETCH_ALL_CONFIRM_HIST,
    payload: null,
  });
  travelHistoriqueRef(auth.currentUser.uid).on("value", (snapshot) => {
    if (snapshot.val()) {
      const data = snapshot.val();
      const arr = Object.keys(data).map((i) => {
        data[i].id = i;
        return data[i];
      });

      dispatch({
        type: FETCH_ALL_CONFIRM_HIST_SUCCESS,
        payload: arr,
      });
    } else {
      dispatch({
        type: FETCH_ALL_CONFIRM_HIST_FAILED,
        payload: "No bookings available.",
      });
    }
  });
};

export const deleteTravel =  (traveldata, action) => (dispatch) => (firebase) => {
    const { auth, travelRef } = firebase;

    dispatch({
      type: DELETE_TRAVEL,
      payload: traveldata,
    });
    if (action == "non") {
      travelRef(traveldata.driver_id)
        .update(traveldata)
        .then(() => {
          dispatch({
            type: DELETE_TRAVEL_SUCCESS,
            payload: null,
          });
        })
        .catch((error) => {
          dispatch({
            type: DELETE_TRAVEL_FAILED,
            payload: error,
          });
        });
    } else {
      travelRef(traveldata.driver_id)
        .set(null)
        .then(() => {
          dispatch({
            type: DELETE_TRAVEL_SUCCESS,
            payload: null,
          });
        })
        .catch((error) => {
          dispatch({
            type: DELETE_TRAVEL_FAILED,
            payload: error,
          });
        });
    }
  };
