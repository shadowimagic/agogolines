import {
    CONFIRM_BOOKING,
    CONFIRM_BOOKING_SUCCESS,
    CONFIRM_BOOKING_FAILED,
    CLEAR_BOOKING,
    UPDATE_BOOKING,
    FETCH_ALL_CONFIRM_BOOKING,
    FETCH_ALL_CONFIRM_BOOKING_SUCCESS,
    FETCH_ALL_CONFIRM_BOOKING_FAILED
} from "../store/types";

import {language} from "../../../config";
import {RequestPushMsg} from "../other/NotificationFunctions";

export const clearBooking = () => (dispatch) => (firebase) => {
    dispatch({
        type: CLEAR_BOOKING,
        payload: null,
    });
}

export const addBooking = (bookingData) => (dispatch) => (firebase) => {

    const {
        auth,
        bookingRef
    } = firebase;
    
    dispatch({
        type: CONFIRM_BOOKING,
        payload: bookingData
      });

      if (auth && auth.currentUser && auth.currentUser.uid){

        bookingRef(auth.currentUser.uid).set(bookingData).then((data) => {
   
          dispatch({
            type: CONFIRM_BOOKING_SUCCESS,
            payload: null
          });
      }).catch((error) => {
     
        dispatch({
          type: CONFIRM_BOOKING_FAILED,
          payload: error
        });
      });

      }

  
};


export const fetchBooking = () => (dispatch) => (firebase) => {
	const { auth, bookingRef } = firebase;

	dispatch({
		type: FETCH_ALL_CONFIRM_BOOKING,
		payload: null,
	});

  if (auth && auth.currentUser && auth.currentUser.uid){

    bookingRef(auth.currentUser.uid).on('value', (snapshot) => {
      if (snapshot.val()) {
  
        const data = snapshot.val();
        dispatch({
          type: FETCH_ALL_CONFIRM_BOOKING_SUCCESS,
          payload:data,
        });
      } else {
        dispatch({
          type: FETCH_ALL_CONFIRM_BOOKING_FAILED,
          payload: 'No bookings available.',
        });
      }
    });
  }
	
};



export const fetchBookingAll = (type) => (dispatch) => (firebase) => {
	const { auth,bookingallRef } = firebase;

	dispatch({
		type: FETCH_ALL_CONFIRM_BOOKING,
		payload: null,
	});
	bookingallRef.on('value', (snapshot) => {
		if (snapshot.val()) {

      const data = snapshot.val();

         if (type =='driver'){

             
          let arr = Object.keys(data)
          .filter(
            (i) =>
             auth && auth.currentUser && auth.currentUser.uid && data[i].driver_id=== auth.currentUser.uid
          )
          .map((i) => {
            data[i].id = i;
            return data[i];
          });
  
        dispatch({
          type: FETCH_ALL_CONFIRM_BOOKING_SUCCESS,
          payload: arr,
        });		

         }else{


          let arr = Object.keys(data)
          .filter(
            (i) =>
             auth && auth.currentUser && auth.currentUser.uid && data[i].id=== auth.currentUser.uid
          )
          .map((i) => {
            data[i].id = i;
            return data[i];
          });
  
        dispatch({
          type: FETCH_ALL_CONFIRM_BOOKING_SUCCESS,
          payload: arr,
        });		



         }        
				
     		
     
		
		} else {
			dispatch({
				type: FETCH_ALL_CONFIRM_BOOKING_FAILED,
				payload: 'No bookings available.',
			});
		}
	});
};

export const updateBooking = (booking) => (dispatch) => (firebase) => {
	const {bookingRef,auth} = firebase;

	dispatch({
		type: UPDATE_BOOKING,
		payload: booking,
	});
    

  if (auth && auth.currentUser && auth.currentUser.uid){


    bookingRef(auth.currentUser.uid).update(booking);
    if (booking.status=='arrived'){
       
      RequestPushMsg(
        booking.rider_pushToken,
        language.notification_title,
        "Votre conducteur est arrivé"
      );

    }
    if (booking.status=='cancelled'){
       
      RequestPushMsg(
        booking.rider_pushToken,
        language.notification_title,
        "Votre conducteur a annulé la course"
      );

    }

    if (booking.status=='started'){
       
      RequestPushMsg(
        booking.rider_pushToken,
        language.notification_title,
        "Votre conducteur a démarrer la course"
      );

    }

    if (booking.status=='ended'){
       
      RequestPushMsg(
        booking.rider_pushToken,
        language.notification_title,
        "Votre conducteur est arrivé à destination"
      );

    }

    
  }



	
	
	

};