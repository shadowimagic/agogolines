import {
  FETCH_ALL_IBAN,
  FETCH_ALL_IBAN_SUCCESS,
  FETCH_ALL_IBAN_FAILED,
  ADD_IBAN,
  ADD_IBAN_SUCCESS,
  ADD_IBAN_FAILED,
  GET_STRIPE_ACCOUNT_ID,
  GET_STRIPE_ACCOUNT_ID_SUCCESS,
  GET_STRIPE_ACCOUNT_ID_FAILURE,
} from "../store/types";

export const fetchIban = () => (dispatch) => (firebase) => {
  const { auth, ibanRef } = firebase;

  dispatch({
    type: FETCH_ALL_IBAN,
    payload: null,
  });

  ibanRef(auth.currentUser.uid).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: FETCH_ALL_IBAN_SUCCESS,
        payload: snapshot.val(),
      });
    } else {
      dispatch({
        type: FETCH_ALL_IBAN_FAILED,
        payload: "No iban available.",
      });
    }
  });
};

export const addIban = (ibanData) => (dispatch) => (firebase) => {
  const { auth, ibanRef } = firebase;
  dispatch({
    type: ADD_IBAN,
    payload: ibanData,
  });
  //console.log("valeur de data1:"+JSON.stringify(creditcarddata))
  ibanRef(auth.currentUser.uid)
    .set(ibanData)
    .then((data) => {
      // console.log("valeur de data2:"+JSON.stringify(data))
      dispatch({
        type: ADD_IBAN_SUCCESS,
        payload: null,
      });
    })
    .catch((error) => {
      //console.log("valeur de error:3"+JSON.stringify(error))
      dispatch({
        type: ADD_IBAN_FAILED,
        payload: error,
      });
    });
};

export const getStripeAccountId = (uid) => (dispatch) => (firebase) => {
  const { ibanRef } = firebase;

  dispatch({
    type: GET_STRIPE_ACCOUNT_ID,
    payload: null,
  });

  ibanRef(uid).on("value", (snapshot) => {
    if (snapshot.val()) {
      dispatch({
        type: GET_STRIPE_ACCOUNT_ID_SUCCESS,
        payload: snapshot.val()?.idStripeAccount || null,
      });
    } else {
      dispatch({
        type: GET_STRIPE_ACCOUNT_ID_FAILURE,
        payload: null,
      });
    }
  });
};
