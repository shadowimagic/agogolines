import {
    FETCH_NEARBY,
    FETCH_NEARBY_SUCCESS,
    FETCH_NEARBY_FAILED,
    EDIT_NEARBY,
    EDIT_NEARBY_SUCCESS,
    EDIT_NEARBY_FAILED,
    DELETE_NEARBY,
    DELETE_NEARBY_SUCCESS,
    DELETE_NEARBY_FAILED,
} from "../store/types";



export const fetchNearby = () => (dispatch) => (firebase) => {
    const {
        nearbyRef
      } = firebase;
    
      dispatch({
        type: FETCH_NEARBY,
        payload: null
      });

      nearbyRef.on("value", snapshot => {
        if (snapshot.val()) {
          const data = snapshot.val();
          const arr = Object.keys(data).map(i => {
            data[i].id = i
            return data[i]
          });
          dispatch({
            type: FETCH_NEARBY_SUCCESS,
            payload: arr
          });
        } else {
          dispatch({
            type: FETCH_NEARBY_FAILED,
            payload: "No driver nearby found."
          });
        }
      });
}

export const addNearby = (data) => (dispatch) => (firebase) => {
    const {
        nearbyRef
      } = firebase;
    
      dispatch({
        type: EDIT_NEARBY,
        payload: data
      });
    
      nearbyRef.push(data).then(() => {
        dispatch({
          type: EDIT_NEARBY_SUCCESS,
          payload: null
        });
      }).catch((error) => {
        dispatch({
          type: EDIT_NEARBY_FAILED,
          payload: error
        });
      });
}

export const editNearby = (id, data) => (dispatch) => (firebase) => {
  const {
    singleNearbyRef
  } = firebase;

  dispatch({
    type: EDIT_NEARBY,
    payload: id,
    allowMore: true 
  })

  if (data?.uid) {
    singleNearbyRef(data?.uid).update(data)
  }  
}

export const deleteNearby = (id) => (dispatch) => (firebase) => {
    const {
        singleNearbyRef
      } = firebase;
    
      dispatch({
        type: DELETE_NEARBY,
        payload: id
      });
    
      singleNearbyRef(id).remove().then(() => {
        dispatch({
          type: DELETE_NEARBY_SUCCESS,
          payload: null
        });
      }).catch((error) => {
        dispatch({
          type: DELETE_NEARBY_FAILED,
          payload: error
        });
      });
}