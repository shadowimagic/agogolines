import {
	TOGGLE_DROP_DOWN_RIDER,
	CLOSE_DROP_DOWN_RIDER,
	OPEN_DROP_DOWN_RIDER,
} from "../store/types";

export const toggleDropDownRiderModal = () => (dispatch) => {
	dispatch({
		type: TOGGLE_DROP_DOWN_RIDER,
	});
};

export const openDropDownRiderModal = () => (dispatch) => {
	dispatch({
		type: OPEN_DROP_DOWN_RIDER,
	});
};

export const closeDropDownRiderModal = () => (dispatch) => {
	dispatch({
		type: CLOSE_DROP_DOWN_RIDER,
	});
};
