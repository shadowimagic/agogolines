import { 
    ADD_RIDER_REQUEST,
    ADD_RIDER_REQUEST_FAILED, 
    ADD_RIDER_REQUEST_SUCCESS,
    DELETE_RIDER_REQUEST,
    DELETE_RIDER_REQUEST_FAILED,
    DELETE_RIDER_REQUEST_SUCCESS,
    FETCH_ALL_RIDER_REQUEST,
    FETCH_ALL_RIDER_REQUEST_FAILED,
    FETCH_ALL_RIDER_REQUEST_SUCCESS,
    FETCH_ONE_RIDER_REQUEST,
    FETCH_ONE_RIDER_REQUEST_FAILED,
    FETCH_ONE_RIDER_REQUEST_SUCCESS,
    UPDATE_RIDER_REQUEST,
} from "../store/types"



export const fetchRiderRequest = () => (dispatch) => (firebase) => {
    const { riderRequest, riderRequestRef } = firebase;

    dispatch({
        type: FETCH_ALL_RIDER_REQUEST,
        payload: null,
    });
    riderRequest
    .on("value", (snapshot) => {
        if (snapshot.val()) {
          const data = snapshot.val();
          const requestVal = [];
          const arr = Object.keys(data).filter((i) => i !== "default").map((i) => {
            riderRequestRef(i).on("value", (snapshot) => {
              if (snapshot.val()) {
                const data = snapshot.val();
                requestVal.push(data);
              } else {
                dispatch({
                  type: FETCH_ONE_RIDER_REQUEST_FAILED,
                  payload: "No rider available.",
                });
              }
            })
          })
          if (requestVal.length > 0) {
            dispatch({
              type: FETCH_ALL_RIDER_REQUEST_SUCCESS,
              payload: requestVal,
            });
          } else {
            dispatch({
              type: FETCH_ALL_RIDER_REQUEST_FAILED,
              payload: "No route available.",
            });
          }
        } else {
          dispatch({
            type: FETCH_ALL_RIDER_REQUEST_FAILED,
            payload: "No route available.",
          });
        }
      });
}

export const fetchOneRiderRequest = (rider_id)  => (dispatch) => (firebase) => {
    const { riderRequestRef } = firebase;

    dispatch({
        type: FETCH_ONE_RIDER_REQUEST,
        payload: null,
    });
    riderRequestRef(rider_id).on("value", (snapshot) => {
        if (snapshot.val()) {
          const data = snapshot.val();
          dispatch({
            type: FETCH_ONE_RIDER_REQUEST_SUCCESS,
            payload: data,
          });
        } else {
          dispatch({
            type: FETCH_ONE_RIDER_REQUEST_FAILED,
            payload: "No route available.",
          });
        }
    });

}

export const addRiderRequest = (data) => (dispatch) => (firebase) => {
    const { riderRequestRef } = firebase;

    dispatch({
        type: ADD_RIDER_REQUEST,
        payload: data,
    });
    
    riderRequestRef(data.rider_id).set(data)
    .then(() => {
      dispatch({
        type: ADD_RIDER_REQUEST_SUCCESS,
        payload: null,
      });
    })
    .catch((error) => {
      dispatch({
        type: ADD_RIDER_REQUEST_FAILED,
        payload: error,
      });
    });
}

export const deleteRiderRequest = (rider_id) => (dispatch) => (firebase) => {
    const { auth, riderRequestRef } = firebase;

    dispatch({
        type: DELETE_RIDER_REQUEST,
        payload: null,
    },{ allowMore: true });
    
    riderRequestRef(auth.currentUser.uid || rider_id)
    .remove()
    .then(() => {
      dispatch(
        {
          type: DELETE_RIDER_REQUEST_SUCCESS,
          payload: null,
        },
        { allowMore: true }
      );
    })
    .catch((error) => {
      dispatch(
        {
          type: DELETE_RIDER_REQUEST_FAILED,
          payload: error,
        },
        { allowMore: true }
      );
    });
}

export const updateRiderRequest = (rider) => (dispatch) => (firebase) => {
    const {riderRequestRef} = firebase;

    dispatch(
        {
          type: UPDATE_RIDER_REQUEST,
          payload: rider,
        },
        { allowMore: true }
      );
      if (rider?.rider_id) {
        riderRequestRef(rider.rider_id).update(rider);
      }
}