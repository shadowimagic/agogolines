import {
    FETCH_ALL_COMMUNICATION,
    FETCH_ALL_COMMUNICATION_SUCCESS,
    FETCH_ALL_COMMUNICATION_FAILED,
    ADD_COMMUNICATION,
    ADD_COMMUNICATION_SUCCESS,
    ADD_COMMUNICATION_FAILED
  } from "../store/types";
  
  export const fetchCommunication = () => (dispatch) => (firebase) => {
  
    const {
      communicationOneRef
    } = firebase;
  
    dispatch({
      type: FETCH_ALL_COMMUNICATION,
      payload: null
    });
    communicationOneRef.on("value", snapshot => {
      if (snapshot.val()) {
        const data = snapshot.val();
       
        dispatch({
          type: FETCH_ALL_COMMUNICATION_SUCCESS,
          payload: data
        });
      } else {
        dispatch({
          type: FETCH_ALL_COMMUNICATION_FAILED,
          payload: "No data available."
        });
      }
    });
  };
  


  export const addCommunication = (data) => (dispatch) => (firebase) => {
    const {
        communicationRef
      } = firebase;
      //  console.log("valeur de data dans action:",data);
    dispatch({
      type: ADD_COMMUNICATION,
      payload: null
    });
   
      //  console.log("valeur de data dans action:",data);
        communicationRef(data.type).set(data);
   
   
  }

  

  
  