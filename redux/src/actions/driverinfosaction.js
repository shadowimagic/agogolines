import {
	ADD_DRIVER_INFOS,
	DELETE_DRIVER_INFOS,
	FETCH_DRIVERS_INFOS,
  FETCH_DRIVERS_INFOS_SUCCESS,
  FETCH_DRIVERS_INFOS_FAILED
} from '../store/types';

export const addDriverInfos = (driverinfos) => (dispatch) => {
	dispatch({
		type: ADD_DRIVER_INFOS,
		payload: driverinfos,
	});
};

export const deleteDriverInfos = () => (dispatch) => {
	dispatch({
		type: DELETE_DRIVER_INFOS,
		payload: null,
	});
};

export const fetchDriverInfos = () => (dispatch) => (firebase) => {
	const { driverDocsRef, auth } = firebase;

  dispatch({
    type: FETCH_DRIVERS_INFOS,
    payload: null
  });

	//"auth.currentUser.uid"
	driverDocsRef(auth.currentUser.uid).on('value', (snapshot) => {
		if (snapshot.val()) {
			const data = snapshot.val();
      // console.log(data);

			dispatch({
				type: FETCH_DRIVERS_INFOS_SUCCESS,
				payload: data,
			});
		} else {
			dispatch({
				type: FETCH_DRIVERS_INFOS_FAILED,
				payload: 'No driver infos available.',
			});
		}
	});
};
