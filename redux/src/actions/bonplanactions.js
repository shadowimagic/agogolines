import {
    FETCH_BONPLAN,
    FETCH_BONPLAN_SUCCESS,
    FETCH_BONPLAN_FAILED,
    EDIT_BONPLAN,
    EDIT_BONPLAN_SUCCESS,
    EDIT_BONPLAN_FAILED,
    DELETE_BONPLAN,
    DELETE_BONPLAN_SUCCESS,
    DELETE_BONPLAN_FAILED,
  } from "../store/types";

  export const fetchBonPlan = () => (dispatch) => (firebase) => {
  
    const {
      bonplanRef
    } = firebase;
  
    dispatch({
      type: FETCH_BONPLAN,
      payload: null
    });
    bonplanRef.on("value", snapshot => {
      if (snapshot.val()) {
        const data = snapshot.val();
        const arr = Object.keys(data).map(i => {
          data[i].id = i;
          return data[i]
        });
        dispatch({
          type: FETCH_BONPLAN_SUCCESS,
          payload: arr
        });
      } else {
        dispatch({
          type: FETCH_BONPLAN_FAILED,
          payload: "No point available."
        });
      }
    });
  };


  export const addBonPlan = (data) => (dispatch) => (firebase) => {
    const {
      bonplanRef
    } = firebase;
  
    dispatch({
      type: EDIT_BONPLAN,
      payload: data
    });
  
    bonplanRef.push(data).then(() => {
      dispatch({
        type: EDIT_BONPLAN_SUCCESS,
        payload: null
      });
    }).catch((error) => {
      dispatch({
        type: EDIT_BONPLAN_FAILED,
        payload: error
      });
    });
  }


  export const editBonPlan = (id, bonplan) => (dispatch) => (firebase) => {

    const {
      singleBonPlanRef
    } = firebase;
  
    dispatch({
      type: EDIT_BONPLAN,
      payload: bonplan
    });
    let editedBonPlan = bonplan;
    delete editedBonPlan.id;
    singleBonPlanRef(id).set(editedBonPlan).then(() => {
      dispatch({
        type: EDIT_BONPLAN_SUCCESS,
        payload: null
      });
    }).catch((error) => {
      dispatch({
        type: EDIT_BONPLAN_FAILED,
        payload: error
      });
    });
  }


  export const deleteBonPlan = (id) => (dispatch) => (firebase) => {

    const {
      singleBonPlanRef
    } = firebase;
  
    dispatch({
      type: DELETE_BONPLAN,
      payload: id
    });
  
    singleBonPlanRef(id).remove().then(() => {
      dispatch({
        type: DELETE_BONPLAN_SUCCESS,
        payload: null
      });
    }).catch((error) => {
      dispatch({
        type: DELETE_BONPLAN_FAILED,
        payload: error
      });
    });
  }
  