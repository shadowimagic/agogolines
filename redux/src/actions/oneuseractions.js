import {
    FETCH_ONE_USER_FAILED,
    FETCH_ONE_USER_SUCCESS,
    FETCH_ONE_USER
  } from "../store/types";

export const fetchUserById = (id) => (dispatch) => (firebase) => {

    const {
      singleUserRef
    } = firebase;
  
    dispatch({
      type: FETCH_ONE_USER,
      payload: null
    });
  
    singleUserRef(id).once("value", snapshot => {
      if(snapshot.val()){
        dispatch({
          type: FETCH_ONE_USER_SUCCESS,
          payload: snapshot.val()
        })
      }else{
        dispatch({
          type: FETCH_ONE_USER_FAILED,
          payload: "No user available"
        })
      }
    })
  }