import { TOGGLE_MODAL, CLOSE_MODAL, OPEN_MODAL } from '../store/types';

export const toggleProposeModal = () => (dispatch) => {
	dispatch({
		type: TOGGLE_MODAL,
	});
};

export const openProposeModal = () => (dispatch) => {
	dispatch({
		type: OPEN_MODAL,
	});
};

export const closeProposeModal = () => (dispatch) => {
	dispatch({
		type: CLOSE_MODAL,
	});
};