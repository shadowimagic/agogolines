export const getStripeIdAccount = async (
  bankToken,
  accountToken,
  email,
  uid
) => {
  try {
    const url =
      "https://us-central1-agogolines-8bc39.cloudfunctions.net/urlcreatestripeaccount/createAccount";

    const rawResponse = await fetch(`${url}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        country: "FR",
        email: email,
        token: accountToken,
        ibantoken: bankToken,
        uid: uid,
      }),
    });

    const formattedResponse = await rawResponse.json();

    if (formattedResponse.statusCode === 400) {
      throw "request failed!";
    }

    return formattedResponse;
  } catch (e) {
    console.error("getStripeIdAccount", e);
    return null;
  }
};

export const getStripefile = async (filename, uid) => {
  const url =
    "https://us-central1-agogolines-8bc39.cloudfunctions.net/urlstripefile/stripefile";

  await fetch(`${url}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      filename: filename,
      userid: uid,
    }),
  })
    .then((response) => response.json())
    .then((responseData) => {
      console.log(
        "POST Response",
        "Response Body -> " + JSON.stringify(responseData)
      );
      return responseData.id;
    })
    .done();

  //  console.log("valeur de json file:",JSON.stringify(response))
};
