
export const GetVilleMin = (location) => {
    let re = /,/gi;
    let objetmap;
    let ville="";
    let lieu =location;
    lieu=lieu.toString();
    lieu =lieu.replace(re, "/");
    objetmap=lieu.split("/");
    if(objetmap.length ===2){

      ville =objetmap[0];


    }else if(objetmap.length ===1){

      ville=objetmap[0];



    }else if(objetmap.length===3){

      ville=objetmap[1];

    }else if(objetmap.length===4){

      ville=objetmap[2];

    }else if(objetmap.length===5){

      ville=objetmap[3];

    }else if(objetmap.length===6){

      ville=objetmap[4];

    }

    ville =ville.toLowerCase();
    ville= ville.replace(/ /g,"");
    ville =removeAccents(ville);
    console.log("valeur de ville:"+ville);
    return ville;


  }
  const removeAccents = (str) => {
 
    let accents = 'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    let accentsOut = "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    str = str.split('');
    str.forEach((letter, index) => {
      let i = accents.indexOf(letter);
      if (i != -1) {
        str[index] = accentsOut[i];
      }
    })
    return str.join('');
  }

