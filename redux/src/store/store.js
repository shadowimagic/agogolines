import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
//import logger from "redux-logger";

import {authreducer as auth} from "../reducers/authreducer";
import {cartypesreducer as cartypes} from "../reducers/cartypesreducer";
import {bookingslistreducer as bookinglistdata} from "../reducers/bookingslistreducer";
import {estimatereducer as estimatedata} from "../reducers/estimatereducer";
import {bookingreducer as bookingdata} from "../reducers/bookingreducer";
import {cancelreasonreducer as cancelreasondata} from "../reducers/cancelreasonreducer";
import {promoreducer as promodata} from "../reducers/promoreducer";
import {usersreducer as usersdata} from "../reducers/usersreducer";
import {notificationreducer as notificationdata} from "../reducers/notificationreducer";
import {driverearningreducer as driverearningdata} from "../reducers/driverearningreducer";
import {earningreportsreducer as earningreportsdata} from "../reducers/earningreportsreducer";
import {communicationreducer as communicationdata} from "../reducers/communicationreducer";
import {settingsreducer as settingsdata} from "../reducers/settingsreducer";
import {paymentreducer as paymentmethods} from "../reducers/paymentreducer";
import {tripreducer as tripdata} from "../reducers/tripreducer";
import {tasklistreducer as taskdata} from "../reducers/tasklistreducer";
import {locationreducer as locationdata} from "../reducers/locationreducer";
import {chatreducer as chatdata} from "../reducers/chatreducer";
import {withdrawreducer as withdrawdata} from "../reducers/withdrawreducer";
import {gpsreducer as gpsdata} from "../reducers/gpsreducer";
import {driverinfosreducer as driverdata} from "../reducers/driverinfosreducer";
import {creditcardreducer as creditcarddata} from "../reducers/creditcardreducer";
import {reducer as rightDrawerReducer} from "../reducers/rightDrawerReducer";
import {mesvehiculesreducer as mesvehicules} from "../reducers/mesvehiculesreducer";
import {travelreducer as travel} from "../reducers/travelreducer";
import {travelreducer as traveldata} from "../reducers/traveldatareducer";
import {bookingsreducer as bookings} from "../reducers/bookingsreducer";
import {positionreducer as positiondata} from "../reducers/positionreducer";


import {mesdocumentsreducer as mesdocuments} from "../reducers/mesdocumentsreducer";
import {ibanreducer as iban} from "../reducers/ibanreducer";

import {pointreducer as pointdata} from "../reducers/pointreducer";
import {commentreducer as comment} from "../reducers/commentreducer";
import {reducer as proposeModal} from "../reducers/proposeModalReducer";
import {reducer as proposeAlert} from "../reducers/proposeAlertReducer";
import {reducer as driverGoingToRiderModal} from "../reducers/driverGoingToRiderModalReducer";
import {reducer as cancelReasonModal} from "../reducers/cancelreasonmodalreducer";
import {reducer as pickUpRiderModal} from "../reducers/pickupridermodalreducer";
import {reducer as dropDownRiderModal} from "../reducers/dropdownridermodalreducer";
import {oneuserreducer as userdata} from "../reducers/oneuserreducer";
import { reservationCourseReducer as reservationCourse } from "../reducers/reservationReducer";
import { bonplanreducer as bonplandata } from "../reducers/bonplanreducer";
import { nearbyreducer as nearbydata } from "../reducers/nearbyreducer";
import { riderRequestReducer as requestReducerdata } from "../reducers/riderRequestReducer";


const reducers = combineReducers({
	auth,
	cartypes,
	bookinglistdata,
	positiondata,
	bonplandata,
	nearbydata,
	estimatedata,
	pointdata,
	bookingdata,
	cancelreasondata,
	communicationdata,
	promodata,
	usersdata,
	notificationdata,
	driverearningdata,
	earningreportsdata,
	settingsdata,
	paymentmethods,
	tripdata,
	taskdata,
	locationdata,
	chatdata,
	withdrawdata,
	gpsdata,
	driverdata,
	creditcarddata,
	rightDrawerReducer,
	mesvehicules,
	mesdocuments,
	iban,
	travel,
	requestReducerdata,
	bookings,
	proposeModal,
	proposeAlert,
	traveldata,
	comment,
	userdata,
	driverGoingToRiderModal,
	cancelReasonModal,
	pickUpRiderModal,
	dropDownRiderModal,
	reservationCourse
});

let middleware = [thunk];

export const store = createStore(reducers, {}, applyMiddleware(...middleware));
