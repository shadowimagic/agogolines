import {
	TOGGLE_DRIVER_GOING_TO_RIDER,
	CLOSE_DRIVER_GOING_TO_RIDER,
	OPEN_DRIVER_GOING_TO_RIDER,
} from "../store/types";

const INITIAL_STATE = {
	visible: false,
};

export function reducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case TOGGLE_DRIVER_GOING_TO_RIDER:
			return {...state, visible: !state.visible};
		case CLOSE_DRIVER_GOING_TO_RIDER:
			return {...state, visible: false};
		case OPEN_DRIVER_GOING_TO_RIDER:
			return {...state, visible: true};
		default:
			return state;
	}
}
