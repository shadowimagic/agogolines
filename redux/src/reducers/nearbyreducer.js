import { 
    FETCH_NEARBY,
    FETCH_NEARBY_SUCCESS,
    FETCH_NEARBY_FAILED
  } from "../store/types";

export const INITIAL_STATE = {
    nearby: null
};

export const nearbyreducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case FETCH_NEARBY:
            return {
                ...state,
              };
        case FETCH_NEARBY_SUCCESS:
            return {
                ...state,
                nearby: action.payload
            }
        case FETCH_NEARBY_FAILED:
            return {
                ...state,
                error:{
                    flag:true,
                    msg:action.payload
                  }
            }
        default:
            return state;
    }
};