import { TOGGLE_MODAL, CLOSE_MODAL, OPEN_MODAL } from "../store/types";

const INITIAL_STATE = {
  modalState: false,
};

export function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case TOGGLE_MODAL:
      return {modalState: !state.modalState};
      case CLOSE_MODAL:
      return {modalState: false};
      case OPEN_MODAL:
      return {modalState: true};
    default:
      return state;
  }
}