import { 
    FETCH_BONPLAN,
    FETCH_BONPLAN_SUCCESS,
    FETCH_BONPLAN_FAILED
  } from "../store/types";
  
  export const INITIAL_STATE = {
    bonplans:null,
    loading: false,
    error:{
      flag:false,
      msg: null
    }
  }
  
  export const bonplanreducer =  (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case FETCH_BONPLAN:
        return {
          ...state,
          loading:true
        };
      case FETCH_BONPLAN_SUCCESS:
        return {
          ...state,
          bonplans:action.payload,
          loading:false
        };
      case FETCH_BONPLAN_FAILED:
        return {
          ...state,
          bonplans:null,
          loading:false,
          error:{
            flag:true,
            msg:action.payload
          }
        };
      default:
        return state;
    }
  };