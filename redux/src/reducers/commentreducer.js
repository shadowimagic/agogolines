import {
    FETCH_ALL_COMMENT,
    FETCH_ALL_COMMENT_SUCCESS,
    FETCH_ALL_COMMENT_FAILED,
} from "../store/types";
    
    export const INITIAL_STATE = {
      comment:null
    }
    
    export const commentreducer =  (state = INITIAL_STATE, action) => {
      switch (action.type) {
        case FETCH_ALL_COMMENT:
          return {
            ...state
          };
        case FETCH_ALL_COMMENT_SUCCESS:
          return {
            ...state,
            comment:action.payload,
          };
        case FETCH_ALL_COMMENT_FAILED:
          return {
            ...state,
            comment:null,
          };
        default:
          return state;
      }
    };