import {
  FETCH_ALL_IBAN,
  FETCH_ALL_IBAN_SUCCESS,
  FETCH_ALL_IBAN_FAILED,
  GET_STRIPE_ACCOUNT_ID,
  GET_STRIPE_ACCOUNT_ID_SUCCESS,
  GET_STRIPE_ACCOUNT_ID_FAILURE,
} from "../store/types";

export const INITIAL_STATE = {
  iban: null,
  stripeAccountId: null,
};

export const ibanreducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_ALL_IBAN:
      return {
        ...state,
      };
    case FETCH_ALL_IBAN_SUCCESS:
      return {
        ...state,
        iban: action.payload,
      };
    case FETCH_ALL_IBAN_FAILED:
      return {
        ...state,
        iban: null,
      };
    case GET_STRIPE_ACCOUNT_ID:
      return {
        ...state,
        stripeAccountId: null,
      };
    case GET_STRIPE_ACCOUNT_ID_SUCCESS:
      return {
        ...state,
        stripeAccountId: action.payload,
      };
    case GET_STRIPE_ACCOUNT_ID_FAILURE:
      return {
        ...state,
        stripeAccountId: null,
      };
    default:
      return state;
  }
};
