import { 
    FETCH_ALL_COMMUNICATION,
    FETCH_ALL_COMMUNICATION_SUCCESS,
    FETCH_ALL_COMMUNICATION_FAILED,
    ADD_COMMUNICATION
  } from "../store/types";
  
  export const INITIAL_STATE = {
    communication:null,
    loading: false,
  }
  
  export const communicationreducer =  (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case FETCH_ALL_COMMUNICATION:
        return {
          ...state,
          loading:true
        };
      case FETCH_ALL_COMMUNICATION_SUCCESS:
        return {
          ...state,
          communication:action.payload,
          loading:false
        };
      case FETCH_ALL_COMMUNICATION_FAILED:
        return {
          ...state,
          users:null,
          loading:false,
        };
        case ADD_COMMUNICATION:
            return state;
      default:
        return state;
    }
  };