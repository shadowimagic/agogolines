import { 
    FETCH_POINTS,
    FETCH_POINTS_SUCCESS,
    FETCH_POINTS_FAILED,
    EDIT_POINTS
  } from "../store/types";
  
  export const INITIAL_STATE = {
    point:null,
    loading: false,
    error:{
      flag:false,
      msg: null
    }
  }
  
  export const pointreducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case FETCH_POINTS:
        return {
          ...state,
          loading:true
        };
      case FETCH_POINTS_SUCCESS:
        return {
          ...state,
          point:action.payload,
          loading:false
        };
      case FETCH_POINTS_FAILED:
        return {
          ...state,
          point:null,
          loading:false,
          error:{
            flag:true,
            msg:action.payload
          }
        };
      case EDIT_POINTS:
        return state;
      default:
        return state;
    }
  };