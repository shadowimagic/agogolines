import {
  UPDATE_GPS_LOCATION,
  UPDATE_BACKGROUND_LOCATION,
  UPDATE_FOREGROUND_LOCATION,
} from "../store/types";

const INITIAL_STATE = {
  location: null,
  backgroundPermission: null,
  foregroundPermission: null,
};
export const gpsreducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_GPS_LOCATION:
      return {
        ...state,
        location: action.payload,
      };
    case UPDATE_BACKGROUND_LOCATION: {
      return {
        ...state,
        backgroundPermission: action.payload,
      };
    }
    case UPDATE_FOREGROUND_LOCATION: {
      return {
        ...state,
        foregroundPermission: action.payload,
      };
    }
    default:
      return state;
  }
};
