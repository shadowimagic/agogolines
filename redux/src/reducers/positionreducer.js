import {
  FETCH_ALL_POSITION,
  FETCH_ALL_POSITION_SUCCESS,
  FETCH_ALL_POSITION_FAILED,
  WATCH_DRIVER_POSITION,
  WATCH_DRIVER_POSITION_FAILED,
  WATCH_DRIVER_POSITION_SUCCESS,
} from "../store/types";

export const INITIAL_STATE = {
  position: null,
  driverPosition: null,
};

export const positionreducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_ALL_POSITION:
      return {
        ...state,
      };
    case FETCH_ALL_POSITION_SUCCESS:
      return {
        ...state,
        position: action.payload,
      };
    case FETCH_ALL_POSITION_FAILED:
      return {
        ...state,
        position: null,
      };
    case WATCH_DRIVER_POSITION:
      return {
        ...state,
      };
    case WATCH_DRIVER_POSITION_SUCCESS:
      return {
        ...state,
        driverPosition: action.payload,
      };
    case WATCH_DRIVER_POSITION_FAILED:
      return {
        ...state,
        driverPosition: null,
      };
    default:
      return state;
  }
};
