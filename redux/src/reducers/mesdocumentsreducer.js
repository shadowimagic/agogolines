import {
  FETCH_ALL_MESDOCUMENTS,
  FETCH_ALL_MESDOCUMENTS_SUCCESS,
  FETCH_ALL_MESDOCUMENTS_FAILED,
} from "../store/types";
  
  export const INITIAL_STATE = {
    mesdocuments:null
  }
  
  export const mesdocumentsreducer =  (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case FETCH_ALL_MESDOCUMENTS:
        return {
          ...state
        };
      case FETCH_ALL_MESDOCUMENTS_SUCCESS:
        return {
          ...state,
          mesdocuments:action.payload,
        };
      case FETCH_ALL_MESDOCUMENTS_FAILED:
        return {
          ...state,
          mesdocuments:null,
        };
      default:
        return state;
    }
  };