import { TOGGLE_ALERT_DELETE_SCHOW_DEMAND, TOGGLE_ALERT_NO_ACTION } from "../store/types";

const INITIAL_STATE = {
  alertDeleteSchowDemandState: false,
  alertNoActionState: false,
};

export function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case TOGGLE_ALERT_DELETE_SCHOW_DEMAND:
      return {...state, alertDeleteSchowDemandState: !state.alertDeleteSchowDemandState};
      case TOGGLE_ALERT_NO_ACTION:
      return {...state, alertNoActionState: !state.alertNoActionState};
    default:
      return state;
  }
}
