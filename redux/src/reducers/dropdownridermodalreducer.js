import {
	TOGGLE_DROP_DOWN_RIDER,
	CLOSE_DROP_DOWN_RIDER,
	OPEN_DROP_DOWN_RIDER,
} from "../store/types";

const INITIAL_STATE = {
	dropDownRiderState: false,
};

export function reducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case TOGGLE_DROP_DOWN_RIDER:
			return {...state, dropDownRiderState: !state.dropDownRiderState};
		case CLOSE_DROP_DOWN_RIDER:
			return {...state, dropDownRiderState: false};
		case OPEN_DROP_DOWN_RIDER:
			return {...state, dropDownRiderState: true};
		default:
			return state;
	}
}
