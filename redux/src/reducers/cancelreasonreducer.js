import {
	FETCH_CANCEL_REASONS,
	FETCH_CANCEL_REASONS_SUCCESS,
	FETCH_CANCEL_REASONS_FAILED,
	FETCH_ALL_CANCEL_REASONS,
	FETCH_ALL_CANCEL_REASONS_SUCCESS,
	FETCH_ALL_CANCEL_REASONS_FAILED,
} from "../store/types";

const INITIAL_STATE = {
	reasons: null,
};

export const cancelreasonreducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case FETCH_CANCEL_REASONS:
			return {
				...state,
			};
		case FETCH_CANCEL_REASONS_SUCCESS:
			return {
				...state,
				reasons: action.payload,
			};
		case FETCH_CANCEL_REASONS_FAILED:
			return {
				...state,
				reasons: null,
			};
		case FETCH_ALL_CANCEL_REASONS:
			return {
				...state,
			};
		case FETCH_ALL_CANCEL_REASONS_SUCCESS:
			return {
				...state,
				reasons: action.payload,
			};
		case FETCH_ALL_CANCEL_REASONS_FAILED:
			return {
				...state,
				reasons: null,
			};
		default:
			return state;
	}
};
