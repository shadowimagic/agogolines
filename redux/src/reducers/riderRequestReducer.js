import {
    FETCH_ALL_RIDER_REQUEST,
    FETCH_ALL_RIDER_REQUEST_FAILED,
    FETCH_ALL_RIDER_REQUEST_SUCCESS,
    FETCH_ONE_RIDER_REQUEST,
    FETCH_ONE_RIDER_REQUEST_FAILED,
    FETCH_ONE_RIDER_REQUEST_SUCCESS,
} from "../store/types"

export const INITIAL_STATE = {
	ride: null,
};

export const riderRequestReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case FETCH_ALL_RIDER_REQUEST:
            return {
                ...state,
            }
        case FETCH_ALL_RIDER_REQUEST:
            return {
                ...state,
            }
        case FETCH_ALL_RIDER_REQUEST_FAILED:
            return {                
                ...state,
                ride: null,
            }
        case FETCH_ALL_RIDER_REQUEST_SUCCESS:
            return {
              ...state,
              ride: action.payload,
            };
        case FETCH_ONE_RIDER_REQUEST:
            return {
                ...state,
            }
        case FETCH_ONE_RIDER_REQUEST_FAILED:
            return {                
                ...state,
                ride: null,
            }
        case FETCH_ONE_RIDER_REQUEST_SUCCESS:
            return {
              ...state,
              ride: action.payload,
            };
        default:
            return state
    }
    
}