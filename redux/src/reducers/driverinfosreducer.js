import {
	FETCH_DRIVERS_INFOS,
	FETCH_DRIVERS_INFOS_SUCCESS,
	FETCH_DRIVERS_INFOS_FAILED,
} from '../store/types';

const INITIAL_STATE = {
	driverdata: null,
};

export const driverinfosreducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case FETCH_DRIVERS_INFOS:
			return { ...state };

		case FETCH_DRIVERS_INFOS_SUCCESS:
			return { ...state, driverdata: action.payload };
		case FETCH_DRIVERS_INFOS_FAILED:
			return { ...state, driverdata: null };

		default:
			return state;
	}
};
