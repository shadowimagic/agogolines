import {
	FETCH_ALL_BOOKINGS,
	FETCH_ALL_BOOKINGS_SUCCESS,
	FETCH_ALL_BOOKINGS_FAILED,
} from '../store/types';

export const INITIAL_STATE = {
	bookings: null,
};

export const bookingsreducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case FETCH_ALL_BOOKINGS:
			return {
				...state,
			};
		case FETCH_ALL_BOOKINGS_SUCCESS:
			return {
				...state,
				bookings: action.payload,
			};
		case FETCH_ALL_BOOKINGS_FAILED:
			return {
				...state,
				bookings: null,
			};
		default:
			return state;
	}
};
