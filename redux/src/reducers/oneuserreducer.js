import { 
    FETCH_ONE_USER_FAILED,
    FETCH_ONE_USER_SUCCESS,
    FETCH_ONE_USER
  } from "../store/types";
  
  export const INITIAL_STATE = {
    user:null,
  
  }
  
  export const oneuserreducer =  (state = INITIAL_STATE, action) => {
    switch (action.type) {
     
        case FETCH_ONE_USER:
        return {
          ...state,
        };
        case FETCH_ONE_USER_SUCCESS:
        return {
          ...state,
          user:action.payload,
        };
        case FETCH_ONE_USER_FAILED:
        return {
          ...state,
          user:null,
        };
      default:
        return state;
    }
  };