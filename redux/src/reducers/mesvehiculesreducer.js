import {
  FETCH_ALL_MESVEHICULES,
  FETCH_ALL_MESVEHICULES_SUCCESS,
  FETCH_ALL_MESVEHICULES_FAILED,
  FETCH_DRIVER_MESVEHICULES,
  FETCH_DRIVER_MESVEHICULES_SUCCESS,
  FETCH_DRIVER_MESVEHICULES_FAILED,
  UPDATE_SELECTED_VEHICULE,
} from "../store/types";

export const INITIAL_STATE = {
  mesvehicules: null,
  driverVehicules: [],
  selectedVehicule: null,
};

export const mesvehiculesreducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_ALL_MESVEHICULES:
      return {
        ...state,
      };
    case FETCH_ALL_MESVEHICULES_SUCCESS:
      return {
        ...state,
        mesvehicules: action.payload,
      };
    case FETCH_ALL_MESVEHICULES_FAILED:
      return {
        ...state,
        mesvehicules: null,
      };
    case FETCH_DRIVER_MESVEHICULES:
      return {
        ...state,
        driverVehicules: [],
      };
    case FETCH_DRIVER_MESVEHICULES_SUCCESS:
      return {
        ...state,
        driverVehicules: action.payload,
      };
    case FETCH_DRIVER_MESVEHICULES_FAILED:
      return {
        ...state,
        driverVehicules: [],
      };
    case UPDATE_SELECTED_VEHICULE:
      return {
        ...state,
        selectedVehicule: action.payload,
      };
    default:
      return state;
  }
};
