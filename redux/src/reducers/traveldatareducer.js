import {
	FETCH_ALL_TRAVELDATA,
	FETCH_ALL_TRAVELDATA_SUCCESS,
	UPDATE_TRAVEL,
	FETCH_ALL_TRAVELDATA_FAILED,
	FETCH_ONE_TRAVEL,
	FETCH_ONE_TRAVEL_SUCCESS,
	FETCH_ONE_TRAVEL_FAILED,
	DELETE_TRAVEL_ONE,
	DELETE_TRAVEL_SUCCESS_ONE,
	DELETE_TRAVEL_FAILED_ONE
} from '../store/types';

export const INITIAL_STATE = {
	travel: null,
};

export const travelreducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case FETCH_ALL_TRAVELDATA:
			return {
				...state,
			};
		case FETCH_ALL_TRAVELDATA_SUCCESS:
			return {
				...state,
				travel: action.payload,
			};
		case FETCH_ALL_TRAVELDATA_FAILED:
			return {
				...state,
				travel: null,
			};

		case DELETE_TRAVEL_ONE:
		
			return {
				travel: null,
			};
		case DELETE_TRAVEL_SUCCESS_ONE:
			
			return {
				travel: null,
			};
		case DELETE_TRAVEL_FAILED_ONE:
			
			return {
				travel: null,
			};


		case UPDATE_TRAVEL:
				return {
				  ...state
				} 
		case FETCH_ONE_TRAVEL:
			return Object.assign({}, state, {
                ...state
            });
		case FETCH_ONE_TRAVEL_SUCCESS:
			return Object.assign({}, state, {
                ...state,
				travel: action.payload,
            });
				
		case FETCH_ONE_TRAVEL_FAILED:
				return {
						...state,
						travel: null,
					};   
		default:
			return state;
	}
};
