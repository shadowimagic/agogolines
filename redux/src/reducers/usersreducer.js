import { 
    FETCH_ALL_USERS,
    FETCH_ALL_USERS_SUCCESS,
    FETCH_ALL_USERS_FAILED,
    FETCH_ONE_USER_FAILED,
    FETCH_ONE_USER_SUCCESS,
    FETCH_ONE_USER
  } from "../store/types";
  
  export const INITIAL_STATE = {
    users:null,
    loading: false,
    error:{
      flag:false,
      msg: null
    }
  }
  
  export const usersreducer =  (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case FETCH_ALL_USERS:
        return {
          ...state,
          loading:true
        };
      case FETCH_ALL_USERS_SUCCESS:
        return {
          ...state,
          users:action.payload,
          loading:false
        };
      case FETCH_ALL_USERS_FAILED:
        return {
          ...state,
          users:null,
          loading:false,
          error:{
            flag:true,
            msg:action.payload
          }
        };
        case FETCH_ONE_USER:
        return {
          ...state,
        };
        case FETCH_ONE_USER_SUCCESS:
        return {
          ...state,
          users:action.payload,
        };
        case FETCH_ONE_USER_FAILED:
        return {
          ...state,
          users:null,
        };
      default:
        return state;
    }
  };