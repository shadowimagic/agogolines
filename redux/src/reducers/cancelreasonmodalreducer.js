import {
	TOGGLE_CANCEL_REASON_MODAL,
	CLOSE_CANCEL_REASON_MODAL,
	OPEN_CANCEL_REASON_MODAL,
} from "../store/types";

const INITIAL_STATE = {
	cancelReasonState: false,
};

export function reducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case TOGGLE_CANCEL_REASON_MODAL:
			return {...state, cancelReasonState: !state.cancelReasonState};
		case CLOSE_CANCEL_REASON_MODAL:
			return {...state, cancelReasonState: false};
		case OPEN_CANCEL_REASON_MODAL:
			return {...state, cancelReasonState: true};
		default:
			return state;
	}
}
