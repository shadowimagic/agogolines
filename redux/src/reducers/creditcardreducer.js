import {
  FETCH_ALL_CREDITCARD,
  FETCH_ALL_CREDITCARD_SUCCESS,
  FETCH_ALL_CREDITCARD_FAILED,
  CHOOSE_CREDIT_CARD,
} from "../store/types";

export const INITIAL_STATE = {
  creditcards: null,
  cardToUse: null,
};

export const creditcardreducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_ALL_CREDITCARD:
      return {
        ...state,
      };
    case FETCH_ALL_CREDITCARD_SUCCESS:
      return {
        ...state,
        creditcards: action.payload,
      };
    case FETCH_ALL_CREDITCARD_FAILED:
      return {
        ...state,
        creditcards: null,
      };
    case CHOOSE_CREDIT_CARD:
      return {
        ...state,
        cardToUse: action.payload,
      };
    default:
      return state;
  }
};
