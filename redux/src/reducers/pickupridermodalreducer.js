import {
	TOGGLE_PICKUP_RIDER,
	CLOSE_PICKUP_RIDER,
	OPEN_PICKUP_RIDER,
} from "../store/types";

const INITIAL_STATE = {
	pickUpRiderState: false,
};

export function reducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case TOGGLE_PICKUP_RIDER:
			return {...state, pickUpRiderState: !state.pickUpRiderState};
		case CLOSE_PICKUP_RIDER:
			return {...state, pickUpRiderState: false};
		case OPEN_PICKUP_RIDER:
			return {...state, pickUpRiderState: true};
		default:
			return state;
	}
}
