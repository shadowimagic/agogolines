import {
  FETCH_ALL_TRAVEL,
  FETCH_ALL_TRAVEL_SUCCESS,
  FETCH_ALL_TRAVEL_FAILED,
  FETCH_ONE_TRAVEL,
  FETCH_ONE_TRAVEL_SUCCESS,
  FETCH_ONE_TRAVEL_FAILED,
  ADD_TRAVEL_HIST,
  ADD_TRAVEL_SUCCESS_HIST,
  ADD_TRAVEL_FAILED_HIST,
  FETCH_ALL_CONFIRM_HIST,
  FETCH_ALL_CONFIRM_HIST_SUCCESS,
  FETCH_ALL_CONFIRM_HIST_FAILED,
} from "../store/types";

export const INITIAL_STATE = {
  travel: null,
  histories: [],
  fetchingHistories: false,
};

export const travelreducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_ALL_TRAVEL:
      return {
        ...state,
      };
    case FETCH_ALL_TRAVEL_SUCCESS:
      return {
        ...state,
        travel: action.payload,
      };
    case FETCH_ALL_TRAVEL_FAILED:
      return {
        ...state,
        travel: null,
      };
    case FETCH_ONE_TRAVEL:
      return {
        ...state,
      };
    case FETCH_ONE_TRAVEL_SUCCESS:
      return {
        ...state,
        travel: action.payload,
      };
    case FETCH_ONE_TRAVEL_FAILED:
      return {
        ...state,
        travel: null,
      };

    case FETCH_ALL_CONFIRM_HIST:
      return {
        ...state,
        histories: [],
        fetchingHistories: true,
      };
    case FETCH_ALL_CONFIRM_HIST_SUCCESS:
      return {
        ...state,
        histories: action.payload,
        fetchingHistories: false,
      };
    case FETCH_ALL_CONFIRM_HIST_FAILED:
      return {
        ...state,
        fetchingHistories: false,
      };
    case ADD_TRAVEL_HIST:
      return {
        ...state,
      };
    case ADD_TRAVEL_SUCCESS_HIST:
      return {
        ...state,
        travel: action.payload,
      };
    case ADD_TRAVEL_FAILED_HIST:
      return {
        ...state,
        travel: null,
      };
    default:
      return state;
  }
};
