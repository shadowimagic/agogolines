import {
  CREATE_RESERVATION_COURSE,
  CREATE_RESERVATION_COURSE_FAILED,
  CREATE_RESERVATION_COURSE_SUCCESS,
} from "../store/types";

const INITIAL_STATE = {
  reservationCourse: null,
  loading: false,
};

export const reservationCourseReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CREATE_RESERVATION_COURSE:
      return {
        ...state,
        loading: true,
      };
    case CREATE_RESERVATION_COURSE_SUCCESS:
      return {
        ...state,
        reservationCourse: action.payload,
        loading: false,
      };
    case CREATE_RESERVATION_COURSE_FAILED:
      return {
        ...state,
        reservationCourse: null,
        loading: false,
        error: {
          flag: true,
          msg: action.payload,
        },
      };
    default:
      return state;
  }
};
