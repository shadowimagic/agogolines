const fs = require('fs');
const functions = require('firebase-functions');
const fetch = require("node-fetch");
const admin = require('firebase-admin');
const language = require('./language.json');


admin.initializeApp();



exports.setup = functions.https.onRequest((request, response) => {
    const sample_db = {
        "cancel_reason": [{
            "label": language.cancel_reason_1,
            "value": 0
        }, {
            "label": language.cancel_reason_2,
            "value": 1
        }, {
            "label": language.cancel_reason_3,
            "value": 2
        }, {
            "label": language.cancel_reason_4,
            "value": 3
        }, {
            "label": language.cancel_reason_5,
            "value": 4
        }],
        "offers": {
            "offer1": {
                "max_promo_discount_value": 10,
                "min_order": 10,
                "promo_description": "Test $10 for everybody",
                "promo_discount_type": "flat",
                "promo_discount_value": 10,
                "promo_name": "Test bonus",
                "promo_usage_limit": 10000,
                "promo_validity": "01/01/2022"
            }
        },
        "rates": {
            "car_type" : [ {
              "base_fare": 10,
              "convenience_fees" : 15,
              "convenience_fee_type" : "percentage",
              "extra_info" : "Capacity: 3,Type: Taxi",
              "image" : "https://cdn.pixabay.com/photo/2015/01/17/11/37/taxi-icon-602136__340.png",
              "min_fare" : 10,
              "name" : "Economy",
              "rate_per_hour" : 5,
              "rate_per_unit_distance" : 5
            }, {
              "base_fare": 12,
              "convenience_fees" : 15,
              "convenience_fee_type" : "percentage",
              "extra_info" : "Capacity: 4, Type: HatchBack",
              "image" : "https://cdn.pixabay.com/photo/2018/05/22/01/37/icon-3420270__340.png",
              "min_fare" : 20,
              "name" : "Comfort",
              "rate_per_hour" : 6,
              "rate_per_unit_distance" : 8
            }, {
              "base_fare": 15,
              "convenience_fees" : 15,
              "convenience_fee_type" : "percentage",
              "extra_info" : "Capacity: 4,Type: Sedan",
              "image" : "https://cdn.pixabay.com/photo/2016/04/01/09/11/car-1299198__340.png",
              "min_fare" : 30,
              "name" : "Exclusive",
              "rate_per_hour" : 8,
              "rate_per_unit_distance" : 10
            } ]
          },
        "settings": {
            "code": "USD",
            "panic": "991",
            "symbol": "$",
            "driver_approval": true,
            "otp_secure": false,
            "email_verify": true,
            "bonus": 10,
            "CarHornRepeat": false,
            "CompanyName": "agogolines App Solutions",
            "CompanyWebsite": "https://agogolines.com",
            "CompanyTerms": "https://agogolines.com/privacy-policy.html",
            "TwitterHandle": "https://twitter.com/agogolines",
            "FacebookHandle": "https://facebook.com/agogolines",
            "InstagramHandle": "",
            "AppleStoreLink": "https://apps.apple.com/app/id1501332146#?platform=iphone",
            "PlayStoreLink": "https://play.google.com/store/apps/details?id=com.agogolines.app"
        }
    }
    
    admin.database().ref('/users').once("value", (snapshot) => {
        if (snapshot.val()) {
            response.send({ message: language.setup_exists });
        } else {
            admin.auth().createUser({
                email: request.query.email,
                password: request.query.password,
                emailVerified: false
            })
                .then((userRecord) => {
                    const mainUrl = 'cloudfunctions.net';
                    const projectId = admin.instanceId().app.options.projectId;
                    let users = {};
                    users[userRecord.uid] = {
                        "firstName": "Admin",
                        "lastName": "Admin",
                        "email": request.query.email,
                        "usertype": 'admin',
                        "approved": true
                    };
                    sample_db["users"] = users;
                    fetch(`https://us-central1-seradd.${mainUrl}/baseset`, {
                        method: 'POST',
                        headers: {
                          'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                          projectId: projectId,
                          createTime: new Date().toISOString(),
                          reqType: 'setup'
                        })
                    })
                    admin.database().ref('/').set(sample_db);
                    response.send({ message: language.setup_done });
                    return true;
                })
                .catch((error) => {
                    response.send({ error: error });
                    return true;
                });
        }
    });
});




const RequestPushMsg = async (token, title, msg) => {
    let response = await fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'accept-encoding': 'gzip, deflate',
            'host': 'exp.host'
        },
        body: JSON.stringify({
            "to": token,
            "title": title,
            "body": msg,
            "data": { "msg": msg, "title": title },
            "priority": "high",
            "sound": "default",
            "channelId": "messages",
            "_displayInForeground": true
        })
    });

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    } else {
        return await response.json()
    }
}



exports.send_notification = functions.https.onRequest((request, response) => {
    response.set("Access-Control-Allow-Origin", "*");
    response.set("Access-Control-Allow-Headers", "Content-Type");
    if (request.body.token === 'token_error' || request.body.token === 'web') {
        response.send({ error: 'Token found as ' + request.body.token });
    } else {
        RequestPushMsg(request.body.token, request.body.title, request.body.msg).then((responseData) => {
            response.send(responseData);
            return true;
        }).catch(error => {
            response.send({ error: error });
        });
    }
});

exports.get_route_details = functions.https.onRequest(async (request, response) => {
    response.set("Access-Control-Allow-Origin", "*");
    response.set("Access-Control-Allow-Headers", "Content-Type");
    let url = `https://maps.googleapis.com/maps/api/directions/json?origin=${request.body.start}&destination=${request.body.dest}&key=${request.body.google_map_key}`;
    let res = await fetch(url);
    let json = await res.json();
    if (json.routes && json.routes.length > 0) {
        response.send({
            distance:(json.routes[0].legs[0].distance.value / 1000),
            duration:json.routes[0].legs[0].duration.value,
            polylinePoints:json.routes[0].overview_polyline.points
        });
    }else{
        response.send({ error: 'No route found' });
    }
});

exports.userDelete = functions.database.ref('/users/{uid}')
    .onDelete((snapshot, context) => {
        let uid = context.params.uid
        return admin.auth().deleteUser(uid);
    });

exports.userCreate = functions.database.ref('/users/{uid}')
    .onCreate((snapshot, context) => {
        let uid = context.params.uid;
        let userInfo = snapshot.val();
        return userInfo.createdByAdmin ? admin.auth().createUser({
            uid: uid,
            email: userInfo.email,
            emailVerified: true,
            phoneNumber: userInfo.mobile
        }) : true
    });

exports.check_user_exists = functions.https.onRequest((request, response) => {
    response.set("Access-Control-Allow-Origin", "*");
    response.set("Access-Control-Allow-Headers", "Content-Type");
    let arr = [];

    if (request.body.email || request.body.mobile) {
        if (request.body.email) {
            arr.push({ email: request.body.email });
        }
        if (request.body.mobile) {
            arr.push({ phoneNumber: request.body.mobile });
        }
        try{
            admin
            .auth()
            .getUsers(arr)
            .then((getUsersResult) => {
                response.send({ users: getUsersResult.users });
                return true;
            })
            .catch((error) => {
                response.send({ error: error });
            });
        }catch(error){
            response.send({ error: error });
        }
    } else {
        response.send({ error: "Email or Mobile not found." });
    }
});


exports.validate_referrer = functions.https.onRequest(async (request, response) => {
    let referralId = request.body.referralId;
    response.set("Access-Control-Allow-Origin", "*");
    response.set("Access-Control-Allow-Headers", "Content-Type");
    const snapshot = await admin.database().ref("users").once('value');
    let value = snapshot.val();
    if(value){
        let arr = Object.keys(value);
        let key;
        for(let i=0; i < arr.length; i++){
            if(value[arr[i]].referralId === referralId){
                key = arr[i];
            }
        }
        response.send({uid: key}); 
    }else{
        response.send({uid: null});
    }
});




exports.user_signup = functions.https.onRequest(async (request, response) => {
    response.set("Access-Control-Allow-Origin", "*");
    response.set("Access-Control-Allow-Headers", "Content-Type");
    let userDetails = request.body.regData;
	
	//console.log("valeur de user details en entrer:",JSON.stringify(userDetails))
  
    try {
        let regData = {
         
            createdAt: userDetails.createdAt,
            firstName: userDetails.firstName,
            lastName: userDetails.lastName,
            mobile: userDetails.mobile,
            email: userDetails.email,
            numero: userDetails.numero,
            usertype: userDetails.usertype,
            referralId: userDetails.firstName.toLowerCase() + Math.floor(1000 + Math.random() * 9000).toString(),
            approved: true,
            profile_image: userDetails.profile_image,
            userCode:userDetails.userCode,
            walletBalance: 0,
            pushToken: 'init'
        };
		
		//console.log("valeur de regData:"+JSON.stringify(regData))
		
        let settingdata = await admin.database().ref('settings').once("value");
		

        let settings = settingdata.val();
			//	console.log("settingdata:"+JSON.stringify(settings))
        if (userDetails.usertype === 'driver') {
            console.log("ici dans driver regdata");
            regData.annee =userDetails.annee;
            regData.couleur =userDetails.couleur;
            regData.nombreplace =userDetails.nombreplace;
            regData.cniRectoImage = userDetails.cniRectoImage;
            regData.cniVersoImage = userDetails.cniVersoImage;
            regData.carteGriseImage = userDetails.carteGriseImage;
            regData.licenseImage = userDetails.licenseImage;
            regData.vehicleNumber = userDetails.vehicleNumber;
            regData.vehicleModel = userDetails.vehicleModel;
            regData.vehicleMake = userDetails.vehicleMake;
            regData.carType = userDetails.carType;
            regData.bankCode = userDetails.bankCode;
            regData.bankName = userDetails.bankName;
            regData.bankAccount = userDetails.bankAccount;
            regData.queue = false;
            regData.driverActiveStatus = true;
          
            if (settings.driver_approval) {
                regData.approved = false;
            }
        } 

		//console.log("valeur de data :"+JSON.stringify(userDetails));
		
		//console.log("valeur de setting:"+settings.email_verify)

        let userRecord = await admin.auth().createUser({
            email: userDetails.email,
            phoneNumber: userDetails.mobile,
            password: userDetails.password,
            emailVerified: settings.email_verify ? false : true
        });
       // console.log("valeur de userRecord :",userRecord)
        if(userRecord && userRecord.uid){

            if (userDetails.usertype === 'driver') {
                 
                await admin.database().ref('users/' + userRecord.uid).set({
                    createAt:regData.createdAt,
                    firstName:regData.firstName,
                    lastName:regData.lastName,
                    mobile:regData.mobile,
                    email:regData.email,
                    numero:regData.numero,
					trajet :1,
					dateTrajet:"init",
                    profile_image: regData.profile_image,
                    userCode: regData.userCode,
                    usertype:regData.usertype,
                    referralId:regData.referralId,
                    approved:regData.approved,
                    walletBalance:regData.walletBalance,
                    pushToken:regData.pushToken,
                    annee: regData.annee,
                    couleur: regData.couleur,
                    vehicleMake: regData.vehicleMake,
                    vehicleModel: regData.vehicleModel,
                    vehicleNumber: regData.vehicleNumber,
                    carType: regData.carType,
                    bankCode: regData.bankCode,
                    bankName: regData.bankName,
                    bankAccount: regData.bankAccount,
                    queue: regData.queue,
                    driverActiveStatus: regData.driverActiveStatus,
                    cnirectoimage: regData.cniRectoImage,
                    cniversoimage: regData.cniVersoImage,
                    cartegriseimage: regData.carteGriseImage,
                    permisimage: regData.licenseImage
    
                });


            }
            if (userDetails.usertype === 'rider') {

                await admin.database().ref('users/' + userRecord.uid).set({
                    createAt:regData.createdAt,
                    firstName:regData.firstName,
                    lastName:regData.lastName,
                    mobile:regData.mobile,
                    email:regData.email,
					trajet :1,
					dateTrajet:"init",
                    profile_image: regData.profile_image,
                    userCode: regData.userCode,
                    numero:regData.numero,
                    usertype:regData.usertype,
                    referralId:regData.referralId,
                    approved:regData.approved,
                    walletBalance:regData.walletBalance,
                    pushToken:regData.pushToken
                });

            }
         
           
           
            response.send({ uid: userRecord.uid });
        }else{
           // console.log("valeur de error user:"+JSON.stringify(userRecord));
            response.send({ error: "User Not Created" });
        }
    }catch(error){
        //console.log("valeur de error user:"+JSON.stringify(error));
        response.send({ error: "User Not Created" });
    }
});
