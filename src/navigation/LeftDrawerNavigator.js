import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import LeftDrawerContent from './components/LeftDrawerContent';
import HomeDriver from '../screens/HomeDriver';
import HomeRider from '../screens/HomeRider';
import { colors } from '../common/theme';
import HomeHeader from './components//HomeHeader'
import { useSelector } from 'react-redux';

const Drawer = createDrawerNavigator();

function LeftDrawerNavigator() {
	const auth = useSelector((state) => state.auth);
	const userType = auth.info && auth.info.profile ? auth.info.profile.usertype: 'rider';


  React.useEffect(() => {

      console.log("--------------------USERTYPE----------------------");
      console.log(userType);
      console.log("--------------------------------------------------");

    
  }, [])

  
  return (
    <Drawer.Navigator
      initialRouteName={userType === 'rider' ? "HomeRiderScreen" : 'HomeDriverScreen'}
        drawerContent={(props) => <LeftDrawerContent {...props} />}
        screenOptions={{
          drawerStyle: {
            width: '100%',
            backgroundColor: colors.VIOLET
          },
          header: ({ navigation }) => (<HomeHeader navigation={navigation} />),
        }}
    >

      <Drawer.Screen name="HomeRiderScreen" component={HomeRider} />
      <Drawer.Screen name="HomeDriverScreen" component={HomeDriver} />
    </Drawer.Navigator>
  );
}


export default LeftDrawerNavigator;
