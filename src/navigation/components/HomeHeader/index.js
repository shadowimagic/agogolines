import React from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { SvgXml } from "react-native-svg";
import { Header } from "react-native-elements";
import { useSelector, useDispatch } from "react-redux";
import headertitle from "../../../../assets/svg/headerTitle";
import { toggleRightDrawer } from "../../../../redux/src/actions/rightNavigationActions";

export default function HomeHeader({ navigation }) {
  const dispatch = useDispatch();

  const openLeftMenu = () => {
    navigation.openDrawer();
  };

  const openRightMenu = () => {
    dispatch(toggleRightDrawer());
  };
  const auth = useSelector((state) => state.auth);

  const userPhoto =
    auth.info && auth.info.profile ? auth.info.profile.profile_image : null;
  return (
    <Header
      containerStyle={styles.headerContainer}
      leftComponent={
        <MaterialIcons
          name="menu"
          size={35}
          onPress={openLeftMenu}
          style={styles.headerIcon}
        />
      }
      centerComponent={<SvgXml xml={headertitle} />}
      rightComponent={
        <View style={styles.headerAvatar}>
          <TouchableOpacity onPress={openRightMenu}>
            <Image
              source={
                userPhoto
                  ? { uri: userPhoto }
                  : require("../../../../assets/images/profilePic.png")
              }
              style={styles.avatar}
            />
          </TouchableOpacity>
        </View>
      }
    />
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    height: 100,
    alignItems: "flex-end",
    backgroundColor: "#fff",
    shadowColor: "black",
    shadowOpacity: 0.26,
    shadowRadius: 6,
    shadowOffset: { height: 2, width: 0 },
    elevation: 3,
  },
  headerIcon: {
    position: "absolute",
    left: 20,
    bottom: 2,
  },
  headerAvatar: {
    position: "absolute",
    right: 20,
    bottom: 5,
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 35,
    backgroundColor: "gray",
    overflow: "hidden",
  },
});
