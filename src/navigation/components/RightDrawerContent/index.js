import React, { useState, useContext, useEffect } from "react";
import { View, StyleSheet, Text, ScrollView, Alert } from "react-native";
import { Icon, ListItem, Header, Image, Switch } from "react-native-elements";
import { useSelector, useDispatch } from "react-redux";
import { colors } from "../../../common/theme";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { language } from "../../../../config";
import pointBleue from "../../../../assets/svg/pointBleue";
import pointGris from "../../../../assets/svg/pointGris";
import triangle from "../../../../assets/svg/triangle";
import profile from "./../../../../assets/svg/profile";
import payement from "../../../../assets/svg/payement";
import parameter from "./../../../../assets/svg/parameter";
import logout from "./../../../../assets/svg/logout";
import document from "./../../../../assets/svg/document";
import carMenu from "./../../../../assets/svg/carMenu";
import profit from "./../../../../assets/svg/profit";
import { FirebaseContext } from "../../../../redux/src";
import { SvgXml } from "react-native-svg";

function RightDrawerContent(props) {
  const { api } = useContext(FirebaseContext);

  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const { fetchCommunication } = api || {};
  const [userPhoto, setUserPhoto] = useState(null);
  const [username, setUsername] = useState("");
  const [userType, setUserType] = useState("rider");
  const [activePromo, setActivePromo] = useState(false);
  const { deleteNearby } = api
  const resNearby = useSelector((state) => state.nearbydata);
  const nearby = resNearby.nearby

  const [switchState, setSwitchState] = useState(null);
  const [lastState, setLastState] = useState("");
  const [userTypeUpdated, setUserTypeUpdated] = useState(false);

  const [numberCompletedCourse, setNumberCompletedCourse] = useState(0);
  const [numberRemainingCourse, setNumberRemainingCourse] = useState(
    10 - numberCompletedCourse
  );

  let listPointForCompletedCourse = [];
  let listPointForRemainingCourse = [];

  const communicationdata = useSelector((state) => state.communicationdata);
  const communicationInformations = communicationdata.communication;


  useEffect(() => {
    if (auth.info && auth.info.profile) {
      let id = setInterval(() => {
        fetchCommunication && dispatch(fetchCommunication());
      }, 5000);
      return () => clearInterval(id);
    }
  }, [auth.info, fetchCommunication]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      if (communicationInformations && communicationInformations.montant) {
        setActivePromo(true);
      } else {
        setActivePromo(false);
      }
      if (auth.info.profile.dateTrajet === "init") {
        auth.info.profile.trajet = 0;
      }
      console.log(auth.info.profile)
      console.log("valeur de active promo:", activePromo);
    }
  }, [auth.info, communicationInformations, activePromo]);

  for (let i = 0; i < numberCompletedCourse; i++) {
    listPointForCompletedCourse.push(
      <View style={{ alignSelf: "center", marginLeft: 10 }} key={i}>
        <SvgXml xml={pointBleue} />
        <View
          style={{
            alignSelf: "center",
            alignItems: "center",
            height: 10,
          }}
        />
      </View>
    );
  }

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      //console.log("valeur de user type:",auth.info.profile.usertype)
      setNumberCompletedCourse(auth.info.profile.point);
      if (auth.info.profile.usertype === "rider") {
        setSwitchState(false);
        setLastState("rider");
      }

      if (auth.info.profile.usertype === "driver") {
        setSwitchState(true);
        setLastState("driver");
      }

      setNumberRemainingCourse(10 - auth.info.profile.point);
    }
  }, [auth.info]);


  const deleteOnSwitchProfile = async () => {
      if (nearby != null && nearby.length > 0) {
        const res = await nearby.filter(obj => obj.type == lastState && obj.uid == auth?.info?.uid)
        if (res.length > 0) {
         await res.map((obj) => { dispatch(deleteNearby(obj.id))});
        }
      }
  }

  useEffect(() => {
    if (auth?.info?.profile) {
      setNumberCompletedCourse(auth.info.profile.point);

      setNumberRemainingCourse(10 - auth.info.profile.point);

      setUserType(
        auth.info && auth.info.profile ? auth.info.profile.usertype : "rider"
      );
      setSwitchState(auth?.info?.profile?.usertype === "driver" ?? false);
      setUserPhoto(
        auth.info && auth.info.profile ? auth.info.profile.profile_image : null
      );
      setUsername(
        auth.info && auth.info.profile
          ? auth.info.profile.firstName + " " + auth.info.profile.lastName
          : ""
      );
    }
  }, [auth.info]);

  useEffect(() => {
    if (userTypeUpdated) {
      let updateData = {
        usertype: userType,
      };

      if (api) {
        dispatch(api.updateProfile(auth.info, updateData));
      }

      setUserTypeUpdated(false);
      Alert.alert(language.alert, language.profile_updated);
      props.navigation.navigate("AuthLoadingScreen", { isUpdateProfile: true });
    }
  }, [userTypeUpdated]);

  for (let i = 0; i < numberRemainingCourse; i++) {
    listPointForRemainingCourse.push(
      <View
        style={{
          alignSelf: "center",
          alignItems: "center",
          marginLeft: 10,
        }}
        key={i}
      >
        <SvgXml xml={pointGris} />
        {/**i == numberRemainingCourse - 1 ? (
					<View style={{alignSelf: "center", alignItems: "center"}}>
						<SvgXml xml={triangle} />
						<Text style={styles.drawerTextCourseGratuite}>1 course gratuite !</Text>
					</View>
				) : (
					<View
						style={{
							alignSelf: "center",
							alignItems: "center",
							height: 10,
						}}
					/>
					)**/}
      </View>
    );
  }

  const returnDate = () => {
    let now = new Date();
    let day = ("0" + now.getDate()).slice(-2);
    let month = ("0" + (now.getMonth() + 1)).slice(-2);
    let today = now.getFullYear() + "-" + month + "-" + day;

    return today;
  };

  const LogOut = () => {
    AsyncStorage.removeItem("firstRun");
    props.navigation.navigate("LoginScreen");
    if (api) {
      dispatch(api.signOut());
    }
  };

  const toggleSwitch = () => {
    setSwitchState((oldState) => !oldState);
    setUserType((oldType) => {
      return oldType === "rider" ? "driver" : "rider";
    });
    setUserTypeUpdated(true);
    deleteOnSwitchProfile();
  };

  const closeDrawer = () => {
    props.navigation.toggleDrawer();
  };

  const drawerListItem = [
    {
      title: "Mon Profil",
      icon: profile,
      screenName: "ProfileScreen",
      driver: true,
    },
    {
      title: "Mes Documents",
      icon: document,
      screenName: "DocumentScreen",
      driver: switchState,
    },
    {
      title: "Mes Vehicules",
      icon: carMenu,
      screenName: "CarScreen",
      driver: switchState,
    },
    {
      title: "Mes Gains",
      icon: profit,
      screenName: "BenefitScreen",
      driver: switchState,
    },
    {
      title: "Moyens de paiement",
      icon: payement,
      screenName: userType === "rider" ? "MyCartScreen" : "IbanScreen",
      driver: true,
    },
    {
      title: "Paramètres",
      icon: parameter,
      screenName: "SettingScreen",
      driver: true,
    },
    {
      title: "Déconnexion",
      icon: logout,
      screenName: "signout",
      driver: true,
    },
  ];

  if (!auth?.info?.profile) {
    return null;
  }

  return (
    <View style={{ flex: 1, position:'relative' }}>
      <Header
        containerStyle={styles.drawerHeader}
        rightComponent={
          <Icon
            name="close"
            color={colors.WHITE}
            size={30}
            onPress={closeDrawer}
            containerStyle={styles.drawerHeaderIcon}
          />
        }
        centerComponent={{
          text: "PROFIL",
          style: styles.drawerHeaderText,
        }}
        rightContainerStyle={styles.drawerHeaderContainerIcon}
        centerContainerStyle={styles.drawerHeaderContainerText}
      />

      <View style={styles.drawerBody}>
        <Text style={styles.drawerUserName}> {username} </Text>
        <View style={styles.drawerSwitchContainer}>
          <Text style={styles.drawerSwitchTextForContainer}>
            {" "}
            Choisissez votre mode:
          </Text>
          <View
            style={{
              //flex: 10.5,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              // paddingTop: 10,
              marginTop:10
            }}
          >
            <Text
              style={[
                styles.drawerSwitchTextVoyageur,
                {
                  paddingRight: 5,
                  color: !switchState ? colors.VIOLET : "#9C9C9C",
                },
              ]}
            >
              Voyageur
            </Text>
            <Switch
              trackColor={{
                false: "#767577",
                true: "#81b0ff",
              }}
              thumbColor={switchState ? "#f5dd4b" : "#f4f3f4"}
              ios_backgroundColor="#57F0A1"
              onValueChange={toggleSwitch}
              value={switchState}
            />
            <Text
              style={[
                styles.drawerSwitchTextChauffeur,
                {
                  paddingLeft: 5,
                  color: switchState ? colors.VIOLET : "#9C9C9C",
                },
              ]}
            >
              Chauffeur
            </Text>
          </View>
        </View>
        {!switchState && activePromo && (
          <View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
                paddingTop: 10,
              }}
            >
              <Text style={styles.drawerPointTextActive}>
                POINTS : {numberCompletedCourse}
              </Text>
              <Text style={styles.drawerPointTextInactive}>/10</Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                alignSelf: "center",
                //marginBottom: '10%',
              }}
            >
              {listPointForCompletedCourse}
              {listPointForRemainingCourse}
            </View>
          </View>
        )}
        {switchState && (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              paddingTop: 10,
              alignItems: "center",
              paddingHorizontal: 15,
              marginBottom: 10,
            }}
          >
            <Text style={styles.drawerPointTextInactive}> Aujourd'hui </Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={styles.drawerPointTextInactive}>
                {" "}
                Trajet(s) restant(s) :
              </Text>

              <Text style={styles.drawerPointTextActive}>
                {" "}
                {auth.info.profile.trajet}/2
              </Text>
            </View>
          </View>
        )}
        <ScrollView style={styles.drawerBodyElement}>
          {drawerListItem.map(
            (item, i) =>
              item.driver && (
                <ListItem
                  key={i}
                  topDivider
                  onPress={() => {
                    if (item.screenName !== "signout") {
                      props.navigation.navigate(item.screenName);
                    } else {
                      AsyncStorage.removeItem("firstRun");
                      props.navigation.navigate("LoginScreen");

                      if (api) {
                        dispatch(api.signOut());
                      }
                    }
                  }}
                >
                  <SvgXml xml={item.icon} />
                  <ListItem.Content>
                    <ListItem.Title style={styles.drawerBodyElementTitle}>
                      {item.title}
                    </ListItem.Title>
                  </ListItem.Content>
                  <ListItem.Chevron />
                </ListItem>
              )
          )}
        </ScrollView>
      </View>
      <View style={styles.drawerBodyLogo}>
        <Image
          containerStyle={styles.imageContainer}
          source={
            userPhoto == null
              ? require("../../../../assets/images/avatar_login.png")
              : { uri: userPhoto }
          }
          style={{ width: 100, height: 100 }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  drawerHeader: {
    height: "25%",
    alignItems: "center",
    backgroundColor: colors.VIOLET,
    borderBottomWidth: 0,
    zIndex: 0,
    elevation: 0,
  },
  drawerHeaderContainerIcon: {
    alignItems: "center",
    zIndex: 1,
  },
  drawerHeaderIcon: {
    color: colors.WHITE,
    zIndex: 1,
  },
  drawerHeaderContainerText: {
    alignItems: "center",
    justifyContent: "center",
  },
  drawerHeaderText: {
    fontFamily: "Montserrat-Bold",
    color: colors.WHITE,
    fontSize: 15,
    lineHeight: 18,
  },
  drawerUserName: {
    fontFamily: "Montserrat-Bold",
    fontSize: 18,
    lineHeight: 22,
    alignSelf: "center",
    color: colors.VIOLET,
    marginVertical: 15,
    zIndex: 3,
  },
  drawerTextCourseGratuite: {
    fontFamily: "Montserrat-Bold",
    color: "#5A48CB",
    fontSize: 10,
    position: "absolute",
    top: 10,
    width: 100,
  },
  drawerBody: {
    backgroundColor: colors.WHITE,
    position: "absolute",
    width: "100%",
    height: "80%",
    top: "25%",
    paddingTop: "12%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    zIndex: 0,
    elevation: 0,
  },
  drawerBodyLogo: {
    position: "absolute",
    top: "18.5%",
    alignSelf: "center",
    backgroundColor: "transparent",
    borderRadius:50,
    zIndex: 10,
    // elevation: 10,
  },
  imageContainer: {
    width: 100,
    height: 100,
    zIndex: 10,
    borderRadius: 50,
    elevation: 5,
  },
  drawerSwitchTextForContainer: {
    fontFamily: "Montserrat-Medium",
    fontSize: 16,
    lineHeight: 22,
    color: "#9C9C9C",
    marginTop:10
  },
  drawerSwitchContainer: {
    alignItems: "center",
    backgroundColor: "#F5F5F5",
    width: "85%",
    borderRadius: 10,
    alignSelf: "center",
    paddingBottom: "20%",
    height: "18%",
    marginBottom: 10,
  },
  drawerSwitchTextVoyageur: {
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
    lineHeight: 18,
  },
  drawerSwitchTextChauffeur: {
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
    lineHeight: 18,
  },
  drawerPointTextActive: {
    fontFamily: "Montserrat-Bold",
    color: colors.VIOLET,
    fontSize: 15,
  },
  drawerPointTextInactive: {
    fontFamily: "Montserrat-Bold",
    color: "#9C9C9C",
    fontSize: 15,
  },
  drawerBodyHeader: {
    alignItems: "center",
    width: "100%",
    height: "50%",
    marginBottom: 20,
    borderWidth: 2,
  },
  drawerBodyElement: {
    marginVertical: "10%",
    zIndex: 3,
  },
  drawerBodyElementTitle: {
    fontFamily: "Montserrat-Medium",
    fontSize: 15,
    color: "#5A48CB",
  },
});

export default RightDrawerContent;
