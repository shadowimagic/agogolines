import React, { useState, useContext } from "react";
import { View, StyleSheet, Text, ScrollView, Alert } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import car from "../../../../assets/svg/car";
import timer from "../../../../assets/svg/timer";
import bell from "../../../../assets/svg/bell";
import godFriend from "../../../../assets/svg/godFriend";
import goodDeal from "../../../../assets/svg/goodDeal";
import logout from "../../../../assets/svg/logout";
import cguCgv from "../../../../assets/svg/cguCgv";
import howItWork from "../../../../assets/svg/howItWork";
import proposeRoute from "../../../../assets/svg/proposeRoute";
import confidential from "../../../../assets/svg/confidential";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { FirebaseContext } from "../../../../redux/src";
import { toggleProposeModal } from "../../../../redux/src/actions/proposeModalAction";
import { Icon, ListItem, Header, Image } from "react-native-elements";
import { SvgXml } from "react-native-svg";
import logo_blanc from "../../../../assets/svg/logo_blanc";
import {
  toggleProposeAlertDeleteSchowDemand,
  toggleProposeAlertNoAction,
} from "./../../../../redux/src/actions/proposeAlertAction";

export default function LeftDrawerContent(props) {
  const { api } = useContext(FirebaseContext);
  const { fetchTravel, updateProfile } = api || {};
  const auth = useSelector((state) => state.auth);
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;
  const dispatch = useDispatch();

  const userType =
    auth.info && auth.info.profile ? auth.info.profile.usertype : "rider";
  let userRider = "";
  if (userType === "rider") {
    userRider = true;
  }

  const closeDrawer = async () => {
    props.navigation.toggleDrawer();
  };
  const drawerListRiderItem = [
    {
      title: userRider ? "Reserver une course" : "Proposer un trajet",
      icon: userRider ? car : proposeRoute,
      screenName: userRider ? "ProposeRouteScreen" : "ProposeRouteScreen",
    },
    {
      title: "Historique des trajets",
      icon: timer,
      screenName: "TravelHistoryScreen",
    },
    {
      title: "Notification",
      icon: bell,
      screenName: "NotificationScreen",
    },
    {
      title: "Parrainer des amis",
      icon: godFriend,
      screenName: "SponsorScreen",
    },
    {
      title: "Bons plans",
      icon: goodDeal,
      screenName: "GoodDealScreen",
    },
    {
      title: "CGU/CGV",
      icon: cguCgv,
      screenName: "CGUCGVScreen",
    },
    {
      title: "Comment ça marche",
      icon: howItWork,
      screenName: "HowItWorkScreen",
    },
    {
      title: "Politique de confidentialité",
      icon: confidential,
      screenName: "ConfidentialityScreen",
    },
    {
      title: "Déconnexion",
      icon: logout,
      screenName: "signout",
    },
  ];

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Header
          containerStyle={styles.drawerHeader}
          leftComponent={
            <Icon
              name="close"
              color="#fff"
              size={30}
              onPress={closeDrawer}
              containerStyle={styles.drawerHeaderIcon}
            />
          }
          centerComponent={{
            text: "MENU",
            style: styles.drawerHeaderText,
          }}
          leftContainerStyle={styles.drawerHeaderContainerIcon}
          centerContainerStyle={styles.drawerHeaderContainerText}
        />
      </View>
      <View style={styles.drawerBody}>
        <View style={styles.drawerBodyLogo}>
          <SvgXml xml={logo_blanc} />
        </View>
        <ScrollView style={styles.drawerBodyElement}>
          {drawerListRiderItem.map((item, i) => (
            <ListItem
              key={i}
              topDivider
              onPress={() => {
                if (item.screenName === "ProposeRouteScreen") {
                  props.navigation.closeDrawer();
                  if (travelInformations && travelInformations != null) {
                    if (travelInformations.status == "terminated") {
                      if (travelInformations.arrivee == false) {
                        const timeout = setTimeout(() => {
                          dispatch(toggleProposeModal());
                          clearTimeout(timeout);
                        }, 3000);
                      } else {
                        const timeout = setTimeout(() => {
                          dispatch(toggleProposeAlertNoAction());
                          clearTimeout(timeout);
                        }, 3000);
                      }
                    } else if (travelInformations.status == "init") {
                      const timeout = setTimeout(() => {
                        dispatch(toggleProposeAlertDeleteSchowDemand());
                        clearTimeout(timeout);
                      }, 3000);
                    } else if (travelInformations.status == "live") {
                      const timeout = setTimeout(() => {
                        dispatch(toggleProposeAlertNoAction());
                        clearTimeout(timeout);
                      }, 3000);
                    } else {
                      const timeout = setTimeout(() => {
                        dispatch(toggleProposeModal());
                        clearTimeout(timeout);
                      }, 3000);
                    }
                  } else {
                    const timeout = setTimeout(() => {
                      dispatch(toggleProposeModal());
                      clearTimeout(timeout);
                    }, 3000);
                  }
                } else if (item.screenName !== "signout") {
                  props.navigation.navigate(item.screenName);
                } else {
                  AsyncStorage.removeItem("firstRun");
                  props.navigation.navigate("LoginScreen");
                  if (api) {
                    dispatch(api.signOut());
                  }
                }
              }}
            >
              <SvgXml xml={item.icon} />
              <ListItem.Content>
                <ListItem.Title style={styles.drawerBodyElementTitle}>
                  {item.title}
                </ListItem.Title>
              </ListItem.Content>
              <ListItem.Chevron />
            </ListItem>
          ))}
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  drawerHeader: {
    height: "100%",
    backgroundColor: "transparent",
    borderBottomWidth: 0,
  },
  drawerHeaderContainerIcon: {
    alignItems: "center",
  },
  drawerHeaderIcon: {
    color: "#fff",
  },
  drawerHeaderContainerText: {
    alignItems: "center",
    justifyContent: "center",
  },
  drawerHeaderText: {
    fontFamily: "Montserrat-Bold",
    color: "#fff",
    fontSize: 15,
    lineHeight: 18,
  },
  header: {
    height: "25%",
  },
  drawerBody: {
    backgroundColor: "white",
    position: "absolute",
    top: "25%",
    width: "100%",
    height: "80%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  drawerBodyLogo: {
    alignItems: "center",
    marginBottom: 50,
    marginTop: 25,
    width: "100%",
    height: "22%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  drawerBodyElement: {
    marginBottom: 30,
    paddingHorizontal: "5%",
  },
  drawerBodyElementTitle: {
    fontFamily: "Montserrat-Medium",
    fontSize: 15,
    color: "#5A48CB",
  },
});
