import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import RightDrawerNavigator from "./RightDrawerNavigator";
import RegistrationStack from "./RegistrationStack";
import Login from "../screens/Login";
import AuthLoading from "../screens/AuthLoading";
import Register from "../screens/Register";
import TravelHistory from "../screens/RiderAndDriverLeftMenu/TravelHistory";
import Notification from "../screens/RiderAndDriverLeftMenu/Notification";
import Condition from "../screens/RiderAndDriverLeftMenu/Condition";
import Confidentiality from "../screens/RiderAndDriverLeftMenu/Confidentiality";
import HowItWork from "../screens/RiderAndDriverLeftMenu/HowItWork";
import Sponsor from "../screens/RiderAndDriverLeftMenu/Sponsor";
import GoodDeal from "../screens/RiderAndDriverLeftMenu/GoodDeal";
import CGUCGV from "../screens/RiderAndDriverLeftMenu/CGUCGV";
import Profile from "../screens/RiderAndDriverRightMenu/Profile";
import Iban from "../screens/RiderAndDriverRightMenu/Iban";
import Payment from "../screens/RiderAndDriverRightMenu/Payment";
import MyCart from "../screens/RiderAndDriverRightMenu/MyCart";
import Document from "../screens/DriverRightMenu/Document";
import Car from "../screens/DriverRightMenu/Car";
import CreateCar from "../screens/DriverRightMenu/CreateCar";
import BookCar from "../screens/RiderLeftMenu/BookCar";
import Search from "./../screens/SearchScreen";
import Setting from "../screens/RiderAndDriverRightMenu/Setting";
import Benefit from "../screens/DriverRightMenu/Benefit";
import EditCar from "../screens/DriverRightMenu/EditCar";
import ThreeDSecureStripe from "../screens/ThreeDSecureStripe";

const Stack = createNativeStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="AuthLoadingScreen"
        screenOptions={{
          headerShown: false,
        }}
      >
        {/* //element du menu principal */}
        <Stack.Screen name="AuthLoadingScreen" component={AuthLoading} />
        <Stack.Screen name="LoginScreen" component={Login} />
        <Stack.Screen name="RegisterScreen" component={RegistrationStack} />
        <Stack.Screen name="RiderRoot" component={RightDrawerNavigator} />
        <Stack.Screen name="DriverRoot" component={RightDrawerNavigator} />
        <Stack.Screen name="AdminRoot" component={RightDrawerNavigator} />

        {/* // elements communs du menu droit pour Rider et Driver */}
        <Stack.Screen name="ProfileScreen" component={Profile} />
        <Stack.Screen name="PaymentScreen" component={Payment} />
        <Stack.Screen name="MyCartScreen" component={MyCart} />
        <Stack.Screen name="SettingScreen" component={Setting} />

        {/* // element du menu droit pour Rider */}

        {/* // element du menu droit pour Driver */}
        <Stack.Screen name="DocumentScreen" component={Document} />
        <Stack.Screen name="CarScreen" component={Car} />
        <Stack.Screen name="CreateCarScreen" component={CreateCar} />
        <Stack.Screen name="EditCarScreen" component={EditCar} />
        <Stack.Screen name="BenefitScreen" component={Benefit} />
        <Stack.Screen name="IbanScreen" component={Iban} />

        {/* // elements communs du menu gauche pour Rider et Driver */}
        <Stack.Screen name="TravelHistoryScreen" component={TravelHistory} />
        <Stack.Screen name="NotificationScreen" component={Notification} />
        <Stack.Screen name="SponsorScreen" component={Sponsor} />
        <Stack.Screen name="ConditionScreen" component={Condition} />
        <Stack.Screen name="GoodDealScreen" component={GoodDeal} />
        <Stack.Screen name="CGUCGVScreen" component={CGUCGV} />
        <Stack.Screen name="HowItWorkScreen" component={HowItWork} />
        <Stack.Screen
          name="ConfidentialityScreen"
          component={Confidentiality}
        />

        {/* // element du menu gauche  pour Rider */}
        <Stack.Screen name="BookCarScreen" component={BookCar} />
        <Stack.Screen name="SearchScreen" component={Search} />

        {/* 3D Security */}
        <Stack.Screen
          name="ThreeDSecureScreen"
          component={ThreeDSecureStripe}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
