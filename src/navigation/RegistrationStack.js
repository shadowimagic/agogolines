import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import Register from "../screens/Register";
import PhoneMessage from "../screens/Register/PhoneMessage";
import CongratRegistration from "../screens/Register/CongratRegistration";

const Stack = createStackNavigator();
const RegistrationStack = () => {
	return (
		<Stack.Navigator
			initialRouteName="RegisterScreen"
			screenOptions={{
				headerShown: false,
			}}>
			<Stack.Screen name="RegisterScreen" component={Register} />
			<Stack.Screen name="PhoneMessageScreen" component={PhoneMessage} />
			<Stack.Screen name="CongratRegistrationScreen" component={CongratRegistration} />
		</Stack.Navigator>
	);
};

export default RegistrationStack;
