import React, { useEffect } from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { useDispatch, useSelector } from "react-redux";
import { DrawerActions } from "@react-navigation/native";
import RightDrawerContent from "./components/RightDrawerContent";
import LeftDrawerNavigator from "./LeftDrawerNavigator";
import {
  toggleRightDrawer,
  openRightDrawer,
  resetRightDrawer,
} from "../../redux/src/actions/rightNavigationActions";
import { colors } from "../common/theme";

const Drawer = createDrawerNavigator();

const RightDrawerNavigator = (props) => {
  const { rightDrawerState } = useSelector((state) => state.rightDrawerReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    if (rightDrawerState === "toggle") {
      props.navigation.dispatch(DrawerActions.openDrawer());
      dispatch(resetRightDrawer());
    }
  }, [rightDrawerState]);

  return (
    <Drawer.Navigator
      drawerContent={(props) => <RightDrawerContent {...props} />}
      screenOptions={{
        drawerPosition: "right",
        drawerStyle: {
          width: "100%",
          backgroundColor: colors.VIOLET,
        },
      }}
    >
      <Drawer.Screen
        name="RightDrawer"
        component={LeftDrawerNavigator}
        options={{ headerShown: false }}
      />
    </Drawer.Navigator>
  );
};

export default RightDrawerNavigator;
