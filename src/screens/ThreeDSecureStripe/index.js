import React, { useEffect } from "react";
import { WebView } from "react-native-webview";
import { stripekey } from "../../common/stripeKey";
import { Alert } from "react-native";

const ThreeDSecureStripe = ({ navigation, route }) => {
  const params = route.params || {};
  const { data: dataCaptureMe, callback, goBackCallBack } = params;
  const html = (dataCaptureMe) => {
    return `
      <html>
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </head>
        <body>

          <div style="
                height: 100%;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
             " >
             <div style="
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
             ">
                <h2>Waiting to processing...</h2>
             </div>

          </div>
          <!-- Load Stripe.js on your website. -->
          <script src="https://js.stripe.com/v3/"></script>
          <script type="text/javascript">
            window.onload = function() {
              try {
                var stripe = Stripe('${stripekey}', {stripeAccount: '${dataCaptureMe.stripeAccount}'})
                stripe.confirmCardPayment('${dataCaptureMe.clientSecret}').then(result => {
                 if(result.error && result.error.message){
                   const status = 'failed'
                   const message = result.error.message
                   const data = {status, message}
                   window.ReactNativeWebView.postMessage(JSON.stringify(data))
                   return
                 }
                 const status = result.paymentIntent.status
                 const message = ''
                 const data = {status, message}
                 window.ReactNativeWebView.postMessage(JSON.stringify(data))
                }).catch(e => {
                  const status = 'error'
                  const message = ''
                  const data = {status, message}
                  window.ReactNativeWebView.postMessage(JSON.stringify(data))
                })
              } catch(e) {
                window.ReactNativeWebView.postMessage(JSON.stringify(e))
              }

            }
          </script>
        </body>
       </html>
    `;
  };

  return (
    <WebView
      style={{ flex: 1 }}
      source={{
        baseUrl: "https://example.com", // DON'T REMOVE THIS LINE
        html: html(dataCaptureMe),
      }}
      javaScriptEnabled={true}
      bounces={false}
      scrollEnabled={false}
      sharedCookiesEnabled={true}
      onMessage={(event) => {
        if (event.nativeEvent.data) {
          const data = JSON.parse(event.nativeEvent.data);
          console.log("stripe.confirmCardPayment: ", { data });

          switch (data.status) {
            case "succeeded":
              navigation.goBack();
              callback();
              break;
            case "requires_capture":
              navigation.goBack();
              callback();
              break;
            case "failed":
              navigation.goBack();
              goBackCallBack();

              break;
            case "error":
              navigation.goBack();
              goBackCallBack();
              Alert.alert("", "Authentication failed");
              break;
            default:
              navigation.goBack();
          }
        }
      }}
    />
  );
};

export default ThreeDSecureStripe;
