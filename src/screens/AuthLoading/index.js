import React, { useContext, useEffect, useRef, useMemo, useState } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  ActivityIndicator,
  Text,
  Alert,
  Linking,
} from "react-native";
import { language } from "../../../config";
import { useSelector, useDispatch } from "react-redux";
import { FirebaseContext, store } from "../../../redux/src";
import { colors } from "../../common/theme";
import { CommonActions } from "@react-navigation/native";
import GetPushToken from "../../components/GetPushToken";
import * as permissions from "expo-permissions";
import * as Location from 'expo-location';

let showedLocationAlert = false;
export default function AuthLoadingScreen(props) {
  const { api } = useContext(FirebaseContext);
  const auth = useSelector((state) => state.auth);
  const gpsdata = useSelector((state) => state.gpsdata);
  const tokenFetched = useRef(false);

  const [checkUser, setCheckUser] = useState(false);
  const [isReady, setIsReady] = useState(false);

  const { isUpdateProfile } = props.route.params || {};

  // const { foregroundPermission } = gpsdata;

  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [foregroundPermission, setForegroundPermission] = useState(null);
 
  // const isReady = useMemo(
  //   () => foregroundPermission === "granted" && checkUser
  // );
  
  const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    }, [value]);
    return ref.current;
  };

  const { info, loading, error } = auth;

  const prevLoading = usePrevious(loading);
  const dispatch = useDispatch();

  const goTo = (navigation, routeName) => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{ name: routeName }],
      })
    );
  };

  const saveToken = async () => {
    let token = await GetPushToken();
    dispatch(
      api.updatePushToken(
        auth.info,
        token ? token : "token_error",
        Platform.OS == "ios" ? "IOS" : "ANDROID"
      )
    );
  };

  // useEffect(() => {
  //   if (foregroundPermission === "denied") {
  //     if (!showedLocationAlert) {
  //       showedLocationAlert = true;
  //       Alert.alert(
  //         language.disclaimer,
  //         language.disclaimer_text,
  //         [
  //           {
  //             text: language.ok,
  //             onPress: () => {
  //               store.dispatch({
  //                 type: "UPDATE_FOREGROUND_LOCATION",
  //                 payload: null,
  //               });
  //               const timeOut = setTimeout(() => {
  //                 // openSettings();
  //                 foregroundPermission = "granted";
  //                 clearTimeout(timeOut);
  //               }, 300);
  //             },
  //           },
  //         ],
  //         { cancelable: false }
  //       );
  //     }
  //   }
  // }, [foregroundPermission]);


  useEffect(() => {
    (async () => {
      
      let { status } = await Location.requestForegroundPermissionsAsync();

      setForegroundPermission(status);

      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      // console.log(foregroundPermission);
      setLocation(location);
    })();
  }, []);

  useEffect(() => {
    if (isUpdateProfile) {
      setCheckUser(true);
    }
  }, [isUpdateProfile]);

  useEffect(() => {
    if (prevLoading && !loading) {
      if (error?.flag) {
        dispatch(api.clearLoginError());
      }
      setCheckUser(true);
    }
  }, [loading, error]);


  useEffect(
    () => {
      
      setIsReady(foregroundPermission === "granted"/*  && checkUser */)

    });

  useEffect(() => {

    console.log("INFO--------------")
    console.log(info)
    console.log(foregroundPermission);
    if (isReady) 
    {
      if (!tokenFetched.current) {
        tokenFetched.current = true;
        saveToken();
      }
      if (info?.profile?.usertype) {
        let role = info?.profile?.usertype;
        if (info?.profile?.approved) {
          console.log("1 :" + role)
          switch (role) {
            case "rider":
              goTo(props.navigation, "RiderRoot");
              break;
            case "driver":
              goTo(props.navigation, "DriverRoot");
              break;
            case "admin":
            case "fleetadmin":
              goTo(props.navigation, "AdminRoot");
              break;
            default:
              Alert.alert(language.alert, language.not_valid_user_type);
              goTo(props.navigation, "LoginScreen");
              break;
          }
        }
      } else {
        goTo(props.navigation, "LoginScreen");
      }
    }
  }, [isReady]);

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../../assets/images/intro.jpg")}
        resizeMode="cover"
        style={styles.imagebg}
      >
        <ActivityIndicator size="large" color={colors.WHITE} />
        <Text style={{ paddingBottom: 100, color: colors.WHITE }}>
          {language.fetching_data}
        </Text>
      </ImageBackground>
      {/* <Text>TEST</Text> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#32296B",
  },
  imagebg: {
    position: "absolute",
    left: 0,
    top: 0,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    justifyContent: "flex-end",
    alignItems: "center",
  },
});
