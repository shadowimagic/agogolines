import React from "react";
import {View, Text, StyleSheet, StatusBar, Image, TouchableOpacity} from "react-native";
import {Input, Button} from "react-native-elements";

//colors
import {colors} from "../../../common/theme";

//icon congratRegister
import congratulationRegisterImage from "../../../../assets/images/congratulation_register.png";

const CongratRegistration = ({navigation}) => {
	return (
		<View style={styles.container}>
			<View style={styles.modalContainer}>
				<View style={styles.modalHeader}>
					<Image
						source={congratulationRegisterImage}
						containerStyle={styles.modalHeaderImage}
						resizeMode="contain"
					/>
					<Text style={styles.modalHeaderCongratulationText}>Félicitations</Text>
				</View>
				<View style={styles.modalBody}>
					<Text style={styles.modalBodyTextFirstLine}>
						Votre compte a été créé avec succès !
					</Text>
					<Text style={styles.modalBodyTextParagraph}>
						Nous venons de vous envoyer un e-mail
					</Text>
					<Text style={styles.modalBodyTextParagraph}>
						pour valider votre adresse de messagerie.
					</Text>
					<Text style={styles.modalBodyTextParagraph}>
						Cliquez sur le lien indiqué dans l’e-mail et
					</Text>
					<Text style={styles.modalBodyTextParagraph}>
						commencez à utiliser l’application.
					</Text>
				</View>
				<View style={styles.modalFooter}>
					<Button
						title="ok"
						titleStyle={styles.modalFooterButtonText}
						buttonStyle={styles.modalFooterButton}
						containerStyle={styles.modalFooterButtonContainer}
						onPress={() => navigation.navigate("LoginScreen")}
					/>
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: StatusBar.currentHeight,
		height: "100%",
		width: "100%",
		justifyContent: "center",
		alignItems: "center",
	},
	modalContainer: {
		width: "95%",
		height: "70%",
		paddingTop: "5%",
		paddingBottom: "5%",
		borderRadius: 13,
		backgroundColor: colors.WHITE,
		shadowColor: "rgba(197,197,197,0.5)",
		shadowOffset: {
			height: 0,
			width: 0,
		},
		shadowRadius: 8,
		elevation: 3,
	},
	modalHeader: {
		alignItems: "center",
		justifyContent: "center",
		alignContent: "center",
	},
	modalHeaderImage: {
		width: 150,
		height: 150,
	},
	modalHeaderCongratulationText: {
		color: colors.VIOLET,
		fontFamily: "Montserrat-Bold",
		textTransform: "uppercase",
		fontSize: 15,
		lineHeight: 18,
		marginTop: 10,
		marginBottom: "5%",
	},
	modalBody: {
		alignItems: "center",
		justifyContent: "center",
		alignContent: "center",
	},
	modalBodyTextFirstLine: {
		color: "#939393",
		fontFamily: "Montserrat-Medium",
		fontSize: 14,
		lineHeight: 17,
		marginBottom: "5%",
	},
	modalBodyTextParagraph: {
		color: "#939393",
		fontFamily: "Montserrat-Medium",
		fontSize: 14,
		lineHeight: 17,
	},
	modalFooter: {
		alignItems: "center",
		justifyContent: "center",
		alignContent: "center",
		marginTop: "5%",
	},
	modalFooterButtonText: {
		color: colors.WHITE,
		fontSize: 16,
		fontFamily: "Montserrat-Bold",
		textTransform: "uppercase",
		textAlign: "center",
	},
	modalFooterButton: {
		borderRadius: 13,
		height: 50,
		backgroundColor: colors.VIOLET,
	},
	modalFooterButtonContainer: {
		borderRadius: 13,
		width: "40%",
		// marginLeft: 15,
		// marginTop: '10%',
	},
});

export default CongratRegistration;
