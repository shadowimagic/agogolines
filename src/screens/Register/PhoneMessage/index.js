// import React, {useState, useRef, useEffect, useContext} from "react";
// import {View, StyleSheet, Text, StatusBar, SafeAreaView, TouchableOpacity} from "react-native";

// import {Input, Button} from "react-native-elements";
// import {useSelector, useDispatch} from "react-redux";

// import {language, countries, default_country_code, FirebaseConfig, features} from "../../../../config";
// import {FirebaseRecaptchaVerifierModal} from "expo-firebase-recaptcha";
// import {FirebaseContext} from "../../../../redux/src";

// //colors
// import {colors} from "../../../common/theme";

// //formik
// import {Formik} from "formik";

// //yup
// import * as yup from "yup";

// //verifSchema
// const verifCodeSchema = yup.object({
// 	verificationCode: yup.string().required().min(4),
// });

// const PhoneMessage = (props) => {
// 	const {api} = useContext(FirebaseContext);
// 	const {clearLoginError, requestPhoneOtpDevice, mobileSignIn, checkUserExists} = api;
// 	const auth = useSelector((state) => state.auth);
// 	const dispatch = useDispatch();
// 	const userInfos = props.route.params;
// 	const [state, setState] = useState({
// 		phoneNumber: userInfos.mobile,
// 		verificationId: null,
// 		verificationCode: null,
// 	});

// 	const pageActive = useRef(false);
// 	// const [loading, setLoading] = useState(false);
// 	const recaptchaVerifier = useRef(null);

// 	sendVerificationCode = async () => {
// 		// setLoading(true);
// 		// if (state.countryCode && state.countryCode !== language.select_country) {
// 			if (state.phoneNumber) {
// 				let formattedNum = state.phoneNumber.replace(/ /g, "");
// 				formattedNum = state.countryCode + formattedNum.replace(/-/g, "");
// 				if (formattedNum.length > 8) {
// 					checkUserExists({mobile: formattedNum}).then((res) => {
// 						if (res.users && res.users.length > 0) {
// 							pageActive.current = true;
// 							dispatch(
// 								requestPhoneOtpDevice(formattedNum, recaptchaVerifier.current)
// 							);
// 						} else {
// 							// setLoading(false);
// 							Alert.alert(language.alert, language.user_does_not_exists);
// 						}
// 					});
// 				} else {
// 					Alert.alert(language.alert, language.mobile_no_blank_error);
// 					// setLoading(false);
// 				}
// 			} else {
// 				Alert.alert(language.alert, language.mobile_no_blank_error);
// 				// setLoading(false);
// 			}
// 		// } else {
// 		// 	Alert.alert(language.alert, language.country_blank_error);
// 		// 	// setLoading(false);
// 		// }
// 	};
// 	useEffect(() => {
// 		console.log({'here': userInfos});
// 		sendVerificationCode();

// 		if (auth.info && pageActive.current) {
// 			pageActive.current = false;
// 			setLoading(false);
// 		}
// 		if (
// 			auth.error &&
// 			auth.error.msg &&
// 			pageActive.current &&
// 			auth.error.msg.message !== language.not_logged_in
// 		) {
// 			pageActive.current = false;
// 			//Alert.alert(language.alert, auth.error.msg.message);
// 			dispatch(clearLoginError());
// 			// setLoading(false);
// 			props.navigation.navigate("CongratRegistrationScreen");
// 		}
// 		if (auth.verificationId) {
// 			pageActive.current = false;
// 			setState({...state, verificationId: auth.verificationId});
// 			// setLoading(false);
// 		}
// 	}, [auth.info, auth.error, auth.error.msg, auth.verificationId]);

// 	finishRegistration = async () => {
// 		setTimeout(function () {
// 			// setLoading(true);
// 			pageActive.current = true;
// 			dispatch(mobileSignIn(state.verificationId, state.verificationCode));
// 		}, 2000);
// 	};

// 	return (
// 		<SafeAreaView style={styles.container}>
// 			<View style={styles.modalContainer}>
// 				<View style={styles.modalHeaderText}>
// 					<Text style={styles.modalHeaderText}>Entrez le code à 6 chiffres que </Text>
// 					<Text style={styles.modalHeaderText}>vous venez de recevoir par SMS</Text>
// 				</View>
// 				<Formik
// 					initialValues={{code: ""}}
// 					validationSchema={verifCodeSchema}
// 					onSubmit={(values, actions) => {
// 						finishRegistration();
// 						props.navigation.navigate("CongratRegistrationScreen");
// 						actions.resetForm();
// 					}}>
// 					{({handleChange, handleSubmit, values, errors}) => (
// 						<View>
// 							<Input
// 								autoCompleteType="tel"
// 								clearButtonMode="while-editing"
// 								editable={false}
// 								keyboardType="phone-pad"
// 								textContentType="telephoneNumber"
// 								label="Vous allez recevoir un code sur ce numéro *"
// 								labelStyle={styles.registerBodyInputLabelForm}
// 								inputStyle={styles.registerBodyInputForm}
// 								inputContainerStyle={styles.registerBodyInputContainerForm}
// 								value={state.phoneNumber}
// 							/>
// 							<Input
// 								autoCompleteType="postal-code"
// 								clearButtonMode="while-editing"
// 								editable={true}
// 								keyboardType="numeric"
// 								textContentType="postalCode"
// 								inputStyle={styles.modalInput}
// 								onChangeText={handleChange("verificationCode")}
// 								inputContainerStyle={styles.modalInputContainer}
// 							/>
// 							<Button
// 								title="suivant"
// 								titleStyle={styles.modalButtonText}
// 								buttonStyle={styles.modalButton}
// 								containerStyle={styles.modalButtonContainer}
// 								onPress={handleSubmit}
// 							/>
// 						</View>
// 					)}
// 				</Formik>
// 			</View>
// 		</SafeAreaView>
// 		// <View style={styles.container}>
// 		//   <View
// 		//     style={CustomStyle.modalContainer}
// 		//   >
// 		//     <View
// 		//       style={CustomStyle.modalHeaderText}
// 		//     >
// 		//       <Text>Entrez le code à 6 chiffres que </Text>
// 		//       <Text>vous venez de recevoir par SMS</Text>
// 		//     </View>

// 		//     <Formik
// 		//       initialValues={{code: ''}}
// 		//       validationSchema={verifCodeSchema}
// 		//       onSubmit={(values, actions) => {
// 		//         navigation.navigate('CongratScreen');
// 		//         actions.resetForm();
// 		//       }}
// 		//     >
// 		//       {({ handleChange, handleSubmit, values, errors}) => (
// 		//         <View>
// 		//           <View style={{alignItems: 'center'}}>
// 		//             <TextInput
// 		//               style={CustomStyle.textInputCode}
// 		//               value={values.code}
// 		//               onChangeText={handleChange('code')}
// 		//               keyboardType='numeric'
// 		//               />
// 		//             <Text style={CustomStyle.errorInputText}> {errors.code} </Text>
// 		//           </View>
// 		//           <TouchableOpacity onPress={handleSubmit} style={{
// 		//             alignItems: 'center',
// 		//             borderRadius: 21,
// 		//             paddingVertical: 15,
// 		//             borderRadius: 13,
// 		//             backgroundColor: colors.VIOLET,
// 		//             color: colors.WHITE,
// 		//           }}>
// 		//             <Text style={CustomStyle.textSuiv}>Suivant</Text>
// 		//           </TouchableOpacity>
// 		//         </View>
// 		//       )}
// 		//     </Formik>
// 		//   </View>
// 		// </View>
// 	);
// };

// const styles = StyleSheet.create({
// 	container: {
// 		flex: 1,
// 		marginTop: StatusBar.currentHeight,
// 		height: "100%",
// 		width: "100%",
// 		justifyContent: "center",
// 		alignItems: "center",
// 	},
// 	modalContainer: {
// 		width: "95%",
// 		height: 300,
// 		paddingTop: "10%",
// 		paddingBottom: "5%",
// 		borderRadius: 13,
// 		backgroundColor: colors.WHITE,
// 		shadowColor: "rgba(197,197,197,0.5)",
// 		shadowOffset: {
// 			height: 0,
// 			width: 0,
// 		},
// 		shadowRadius: 8,
// 		elevation: 3,
// 	},
// 	modalHeaderText: {
// 		fontSize: 15,
// 		lineHeight: 18,
// 		fontFamily: "Montserrat-Bold",
// 		alignItems: "center",
// 	},
// 	modalInput: {
// 		backgroundColor: colors.WHITE,
// 		borderWidth: 1,
// 		borderRadius: 13,
// 		shadowColor: "rgba(197,197,197,0.5)",
// 		shadowOffset: {
// 			height: 0,
// 			width: 0,
// 		},
// 		shadowRadius: 8,
// 		elevation: 3,
// 		padding: 10,
// 	},
// 	modalInputContainer: {
// 		width: "50%",
// 		borderBottomWidth: 0,
// 		marginTop: "10%",
// 		marginLeft: "25%",
// 	},
// 	modalButtonText: {
// 		color: colors.WHITE,
// 		fontSize: 16,
// 		fontFamily: "Montserrat-Bold",
// 		textTransform: "uppercase",
// 		//textAlign: 'center'
// 	},
// 	modalButton: {
// 		borderRadius: 13,
// 		height: 50,
// 		backgroundColor: colors.VIOLET,
// 	},
// 	modalButtonContainer: {
// 		borderRadius: 13,
// 		width: "90%",
// 		marginLeft: 15,
// 		marginTop: "10%",
// 	},
// });

// export default PhoneMessage;
import React, {useState, useRef, useEffect, useContext} from "react";
import {
	StyleSheet,
	View,
	ImageBackground,
	Dimensions,
	KeyboardAvoidingView,
	TouchableWithoutFeedback,
	Alert,
	TextInput,
	ActivityIndicator,
} from "react-native";
import {Icon, Button, Header, Input} from "react-native-elements";
import {useDispatch, useSelector} from "react-redux";
import {FirebaseContext} from "../../../../redux/src";
import {colors} from "../../../common/theme";
import {FirebaseRecaptchaVerifierModal} from "expo-firebase-recaptcha";
import RNPickerSelect from "react-native-picker-select";
import {
	language,
	countries,
	default_country_code,
	FirebaseConfig,
	features,
} from "../../../../config";
import {CustomStyle} from "../../../common/CustomStyle";

export default function PhoneMessage(props) {
	const userInfos = props.route.params;
	const {api} = useContext(FirebaseContext);
	const {clearLoginError, requestPhoneOtpDevice, mobileSignIn, checkUserExists} = api;
	const auth = useSelector((state) => state.auth);
	const dispatch = useDispatch();

	const formatCountries = () => {
		let arr = [];
		for (let i = 0; i < countries.length; i++) {
			arr.push({
				label: countries[i].label + " (+" + countries[i].phone + ")",
				value: "+" + countries[i].phone,
				key: countries[i].code,
			});
		}
		return arr;
	};
	const [state, setState] = useState({
		email: "",
		password: "",
		customStyleIndex: 0,
		phoneNumber: userInfos.numero,
		verificationId: null,
		verificationCode: null,
		countryCodeList: formatCountries(),
		countryCode: "+" + default_country_code.phone,
	});

	const pageActive = useRef(false);
	const [loading, setLoading] = useState(false);
	const recaptchaVerifier = useRef(null);

	useEffect(() => {
		if (auth.info && pageActive.current) {
			pageActive.current = false;
			setLoading(false);
		}
		if (
			auth.error &&
			auth.error.msg &&
			pageActive.current &&
			auth.error.msg.message !== language.not_logged_in
		) {
			pageActive.current = false;
			props.navigation.navigate("CongratRegistrationScreen");
			//Alert.alert(language.alert, auth.error.msg.message);
			dispatch(clearLoginError());
			setLoading(false);
		}
		if (auth.verificationId) {
			pageActive.current = false;
			setState({...state, verificationId: auth.verificationId});
			setLoading(false);
		}
	}, [auth.info, auth.error, auth.error.msg, auth.verificationId]);

	onPressLogin = async () => {
		setLoading(true);
		if (state.countryCode && state.countryCode !== language.select_country) {
			if (state.phoneNumber) {
				let formattedNum = state.phoneNumber.replace(/ /g, "");
				formattedNum = state.countryCode + formattedNum.replace(/-/g, "");
				if (formattedNum.length > 8) {
					checkUserExists({mobile: formattedNum}).then((res) => {
						if (res.users && res.users.length > 0) {
							pageActive.current = true;
							console.log('here 1');
							dispatch(requestPhoneOtpDevice(formattedNum, recaptchaVerifier.current));
							console.log('here 2')
						} else {
							setLoading(false);
							Alert.alert(language.alert, language.user_does_not_exists);
						}
					});
				} else {
					Alert.alert(language.alert, language.mobile_no_blank_error);
					setLoading(false);
				}
			} else {
				Alert.alert(language.alert, language.mobile_no_blank_error);
				setLoading(false);
			}
		} else {
			Alert.alert(language.alert, language.country_blank_error);
			setLoading(false);
		}
	};

	onSignIn = async () => {
		setLoading(true);
		pageActive.current = true;
		dispatch(mobileSignIn(state.verificationId, state.verificationCode));
		props.navigation.navigate("CongratRegistrationScreen");
	};

	return (
		<KeyboardAvoidingView behavior={"position"} style={styles.container}>
			<ImageBackground
				source={require("../../../../assets/images/arriereplan.png")}
				resizeMode="stretch"
				style={styles.imagebg}>
				<FirebaseRecaptchaVerifierModal
					ref={recaptchaVerifier}
					firebaseConfig={FirebaseConfig}
					attemptInvisibleVerification={false}
				/>
				<View style={styles.topBar}>
					<Header
						backgroundColor={colors.TRANSPARENT}
						leftComponent={{
							icon: "ios-arrow-back",
							type: "ionicon",
							color: colors.VIOLET,
							size: 35,
							component: TouchableWithoutFeedback,
							onPress: () => props.navigation.goBack(),
						}}
						containerStyle={styles.headerContainerStyle}
						innerContainerStyles={styles.headerInnerContainer}
					/>
				</View>

				<View style={styles.box1}>
					<RNPickerSelect
						placeholder={{
							label: language.select_country,
							value: language.select_country,
						}}
						value={state.countryCode}
						useNativeAndroidPickerStyle={true}
						style={{
							inputIOS: styles.pickerStyle,
							inputAndroid: styles.pickerStyle,
						}}
						onValueChange={(value) => setState({...state, countryCode: value})}
						items={state.countryCodeList}
						disabled={
							!!state.verificationId || !features.AllowCountrySelection ? true : false
						}
					/>
				</View>

				<View style={styles.box2}>
					<Input
						editable={true}
						autoCompleteType="tel"
						keyboardType="phone-pad"
						textContentType="telephoneNumber"
						value={state.phoneNumber}
						editable={!!state.verificationId ? false : true}
						keyboardType="phone-pad"
						inputStyle={styles.inputTextStyle}
						placeholder={language.mobile_no_placeholder}
						onChangeText={(value) => setState({...state, phoneNumber: value})}
						inputContainerStyle={styles.inputContainerStyle}
					/>
				</View>

				{state.verificationId ? null : (
					<View style={styles.MaterialButton}>
						<Button
							onPress={() => {onPressLogin()}}
							title={language.request_otp}
							titleStyle={styles.titleStyle}
							buttonStyle={styles.buttonStyle}
							containerStyle={styles.buttonContainerStyle}
						/>
					</View>
				)}
				{!!state.verificationId ? (
					<View style={styles.box2}>
						<Input
							placeholder={language.otp_here}
							onChangeText={(value) => setState({...state, verificationCode: value})}
							value={state.verificationCode}
							editable={true}
							keyboardType="phone-pad"
							inputStyle={styles.inputTextStyle}
							inputContainerStyle={styles.inputContainerStyle}
						/>
					</View>
				) : null}
				{!!state.verificationId ? (
					<View style={styles.MaterialButton}>
						<Button
							onPress={() => {onSignIn()}}
							title={language.authorize}
							titleStyle={styles.titleStyle}
							buttonStyle={styles.buttonStyle}
							containerStyle={styles.buttonContainerStyle}
						/>
					</View>
				) : null}

				{loading ? (
					<View style={CustomStyle.loading}>
						<ActivityIndicator color={colors.VIOLET} size="large" />
					</View>
				) : null}
			</ImageBackground>
		</KeyboardAvoidingView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	headerContainerStyle: {
		backgroundColor: colors.TRANSPARENT,
		borderBottomWidth: 0,
		marginTop: 0,
	},
	headerInnerContainer: {
		marginLeft: 10,
		marginRight: 10,
	},
	imagebg: {
		position: "absolute",
		left: 0,
		top: 0,
		width: Dimensions.get("window").width,
		height: Dimensions.get("window").height,
	},
	topBar: {
		marginTop: 0,
		marginLeft: 0,
		marginRight: 0,
		height: Dimensions.get("window").height * 0.35,
	},
	backButton: {
		height: 40,
		width: 40,
		marginLeft: 35,
		marginTop: 45,
	},
	backButtonImage: {
		height: 40,
		width: 40,
	},

	box1: {
		marginTop: 26,
		marginLeft: 35,
		marginRight: 35,
		justifyContent: "center",
	},
	box2: {
		marginTop: 12,
		marginLeft: 35,
		marginRight: 35,
		justifyContent: "center",
	},
	buttonContainerStyle:{
		borderRadius: 13,
		width: "85%",
	},
	textInput: {
		height: 35,
		color: colors.GREY.background,
		fontSize: 18,
		fontFamily: "Roboto-Regular",
		textAlign: "left",
		marginTop: 8,
		marginLeft: 5,
		paddingLeft: 5,
		borderRadius: 13,
		shadowColor: "rgba(197,197,197,0.5)",
		shadowOffset: {
			height: 0,
			width: 0,
		},
		shadowRadius: 8,
		shadowOpacity: 1,
		backgroundColor: "rgba(255,255,255,1)",
	},
	buttonStyle:{
		borderRadius: 13,
		backgroundColor: colors.VIOLET,
	},
	MaterialButton: {
		marginTop: 22,
		marginLeft: 35,
		marginRight: 35,
	},
	linkBar: {
		flexDirection: "row",
		marginTop: 30,
		alignSelf: "center",
	},
	barLinks: {
		marginLeft: 15,
		marginRight: 15,
		alignSelf: "center",
		fontSize: 18,
		fontWeight: "bold",
	},
	titleStyle:{
		color: colors.WHITE,
		fontSize: 16,
		fontFamily: "Montserrat-Bold",
		textTransform: "uppercase",
		textAlign: "center",
	},
	inputTextStyle:{
		backgroundColor: colors.WHITE,
		borderRadius: 13,
		shadowColor: "rgba(197,197,197,0.5)",
		shadowOffset: {
			height: 0,
			width: 0,
		},
		shadowRadius: 8,
		elevation: 3,
		padding: 10,
		borderLeftWidth:0,
	},
	inputContainerStyle: {
		width: "90%",
		borderBottomWidth: 0,
		left: 15,
	},
	linkText: {
		fontSize: 16,
		fontWeight: "bold",
		color: colors.WHITE,
		fontFamily: "Roboto-Bold",
	},
	pickerStyle: {
		color: colors.GREY.background,
		fontFamily: "Roboto-Regular",
		fontSize: 18,
		marginLeft: 5,
	},

	actionLine: {
		height: 20,
		flexDirection: "row",
		marginTop: 20,
		alignSelf: "center",
	},
	actionItem: {
		height: 20,
		marginLeft: 15,
		marginRight: 15,
		alignSelf: "center",
	},
	actionText: {
		fontSize: 15,
		fontFamily: "Roboto-Regular",
		fontWeight: "bold",
	},
});
