import React, { useState, useRef, useEffect, useContext } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  Modal,
  ActivityIndicator,
  Alert,
  SafeAreaView,
} from "react-native";
import { Input, Button, Image } from "react-native-elements";
import PictureModal from "../../components/PictureModal";
import * as firebase from "firebase";
import styles from "./style";
//colors
import { colors } from "../../common/theme";

//avatar_logo
import avatar_login from "../../../assets/images/avatar_login.png";

//add_photo
import addPhoto from "../../../assets/images/add_foto.png";
import app from "firebase/app";

//formik
// import {Formik} from "formik";

import { default_country_code, countries, language } from "../../../config";
import { FirebaseContext } from "../../../redux/src";
import "react-native-get-random-values";
import { v4 as uuidv4 } from "uuid";
import { CustomStyle } from "../../common/CustomStyle";
import { SvgXml } from "react-native-svg";
import logo_register from "../../../assets/svg/logo_register";
import add_photo_register from "../../../assets/svg/add_photo_register";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

//yup
// import * as yup from "yup";

// const verifSchema = yup.object({
// 	lastName: yup.string().required().min(4),
// 	firstName: yup.string().required().min(4),
// 	email: yup.string().email().required(),
// 	password: yup.string().required().min(4),
// 	confirmPassword: yup
// 		.string()
// 		.oneOf([yup.ref("password"), null], "Le mot de passe ne correspond pas"),
// 	referralId: yup.string().min(4),
// 	mobile: yup.string().required().min(9),
// });

function RegisterScreen(props) {
  const { api } = useContext(FirebaseContext);
  const { emailSignUp, validateReferer, checkUserExists } = api;
  var resp = null;
  const [userInfos, setUserInfos] = useState({
    usertype: "rider",
    firstName: "",
    lastName: "",
    profile_image: null,
    email: "",
    mobile: "",
    referralId: "",
    vehicleNumber: "",
    cniRectoImage: null,
    cniVersoImage: null,
    carteGriseImage: null,
    licenseImage: null,
    numero: "",
    vehicleMake: "",
    vehicleModel: "",
    aller: false,
    retour: false,
    dateTrajet: null,
    annee: "",
    couleur: "",
    vehicleNumber: "",
    nombreplace: "",
    countryCode: "+" + default_country_code.phone,
    carType: "",
    bankAccount: "",
    bankCode: "",
    bankName: "",
    verificationCode: "",
    verificationId: "",
    other_info: "",
    userCode: uuidv4().toString().substring(0, 5),
    password: "",
  });

  const [modalVisible, setModalVisible] = useState(false);
  const [capturedImage, setCapturedImage] = useState(null);
  const [confirmPassword, setConfirmPassword] = useState("");
  const [countryCode, setCountryCode] = useState(
    "+" + default_country_code.phone
  );
  const [mobileWithoutCountry, setMobileWithoutCountry] = useState("");
  const [loading, setLoading] = useState(false);
  const actionSheetRef = useRef(null);

  uploadImage = async (uri) => {
    const imageName = uuidv4();
    const response = await fetch(uri);
    const blob = await response.blob();
    var storageRef = firebase
      .storage()
      .ref()
      .child("images/" + imageName);
    const imageUrl = await storageRef.put(blob).then((data) => {
      data.ref.getDownloadURL().then((url) => {
        resp = url.toString();
        // console.log(resp);
        setUserInfos({ ...userInfos, profile_image: resp });
        return url;
      });
    });
    return imageUrl;
  };
  useEffect(() => {
    //console.log(userInfos);
    // userInfos.profile_image != null && onPressRegister();
  }, [userInfos.profile_image]);

  const formatCountries = () => {
    let arr = [];
    for (let i = 0; i < countries.length; i++) {
      arr.push({
        label: countries[i].label + " (+" + countries[i].phone + ")",
        value: "+" + countries[i].phone,
        key: countries[i].code,
      });
    }
    return arr;
  };

  const showActionSheet = () => {
    actionSheetRef.current?.setModalVisible(true);
  };

  const validateMobile = () => {
    if (mobileWithoutCountry.length < 9) {
      Alert.alert(language.alert, language.mobile_no_blank_error);
      return false;
    }
    return true;
  };

  const onPressRegister = () => {
    console.log(userInfos);
    if (validateMobile()) {
      setLoading(true);

      console.log("userInfos1");
      checkUserExists(userInfos).then((res) => {
        console.log("userInfos2");
        if (res.users && res.users.length > 0) {
          setLoading(false);
          console.log("userInfos3");
          Alert.alert(language.alert, language.user_exists);
        } else if (res.error) {
          setLoading(false);
          console.log("userInfos4");
          Alert.alert(language.alert, language.email_or_mobile_issue);
        } else {
          console.log("userInfos5");
          if (userInfos.referralId && userInfos.referralId.length > 0) {
            console.log("userInfos6");
            validateReferer(userInfos.referralId)
              .then((referralInfo) => {
                if (referralInfo.uid) {
                  emailSignUp({
                    ...userInfos,
                    signupViaReferral: referralInfo.uid,
                  }).then((res) => {
                    setLoading(false);
                    if (res.uid) {
                      props.navigation.navigate(
                        "PhoneMessageScreen",
                        userInfos
                      );
                    } else {
                      Alert.alert(language.alert, language.reg_error);
                    }
                  });
                } else {
                  setLoading(false);
                  Alert.alert(language.alert, language.referer_not_found);
                }
              })
              .catch((error) => {
                setLoading(false);
                Alert.alert(language.alert, language.referer_not_found);
              });
          } else {
            console.log("userInfos7");
            emailSignUp(userInfos).then((res) => {
              console.log("userInfos8");
              setLoading(false);
              if (res.uid) {
                props.navigation.navigate("PhoneMessageScreen", userInfos);
              } else {
                // console.log("userInfos ++ :"+ JSON.stringify(res));
                console.log("userInfos8");
                Alert.alert(language.alert, language.reg_error);
              }
            });
          }
        }
      });
    }
  };

  return (
    <KeyboardAwareScrollView
      extraHeight={200}
      scrollEnabled={false}
      contentContainerStyle={{ height: "100%" }}
    >
      <SafeAreaView style={styles.container}>
        <Modal visible={modalVisible} animationType="slide" transparent={true}>
          <PictureModal
            onClose={() => {
              setModalVisible(false);
            }}
            image={async (data) => {
              setLoading(true);
              const folderName = uuidv4().toString();
              const ppName =
                "pp" + uuidv4().toString().substring(0, 5) + ".png";
              await app
                .storage()
                .ref("photosprofiles/" + folderName)
                .child(ppName)
                .put(data);
              const imageUrl = await app
                .storage()
                .ref("photosprofiles/" + folderName)
                .child(ppName)
                .getDownloadURL();
              // console.log(imageUrl);
              setUserInfos({ ...userInfos, profile_image: imageUrl });
              setCapturedImage({ uri: imageUrl });
              setLoading(false);
            }}
          />
        </Modal>
        <ScrollView style={styles.registerScrollViewContainer}>
          {/* Header */}
          <View style={styles.registerHeader}>
            <Text style={styles.registerHeaderText}>Inscription</Text>
            {capturedImage && (
              <Image
                source={
                  capturedImage ? { uri: capturedImage.uri } : avatar_login
                }
                containerStyle={styles.registerHeaderImage}
                resizeMode="contain"
                PlaceholderContent={
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <ActivityIndicator size="large" color={colors.VIOLET} />
                  </View>
                }
                placeholderStyle={{
                  justifyContent: "center",
                  alignItems: "center",
                }}
              />
            )}
            {!capturedImage && (
              <View style={{ marginVertical: "5%" }}>
                <SvgXml xml={logo_register} />
              </View>
            )}

            <TouchableOpacity
              style={styles.registerHeaderButtonAddPhoto}
              onPress={() => {
                setModalVisible(true);
              }}
            >
              <SvgXml xml={add_photo_register} />
              <Text style={styles.registerHeaderTextAddPhoto}>
                ajouter une photo
              </Text>
            </TouchableOpacity>
          </View>
          {/** Header */}
          <View style={styles.registerBody}>
            <Input
              autoCompleteType="name"
              clearButtonMode="while-editing"
              editable={true}
              keyboardType="default"
              textContentType="familyName"
              label="Nom *"
              onChangeText={(value) => {
                setUserInfos({ ...userInfos, lastName: value });
              }}
              labelStyle={styles.registerBodyInputLabelForm}
              inputStyle={styles.registerBodyInputForm}
              inputContainerStyle={styles.registerBodyInputContainerForm}
              value={userInfos.lastName}
              returnKeyType="next"
            />
            <Input
              autoCompleteType="name"
              clearButtonMode="while-editing"
              editable={true}
              keyboardType="default"
              textContentType="name"
              label="Prenom *"
              onChangeText={(value) => {
                setUserInfos({ ...userInfos, firstName: value });
              }}
              labelStyle={styles.registerBodyInputLabelForm}
              inputStyle={styles.registerBodyInputForm}
              inputContainerStyle={styles.registerBodyInputContainerForm}
              value={userInfos.firstName}
              returnKeyType="next"
            />
            <Input
              autoCompleteType="email"
              clearButtonMode="while-editing"
              editable={true}
              keyboardType="email-address"
              textContentType="emailAddress"
              label="Email *"
              onChangeText={(value) => {
                setUserInfos({ ...userInfos, email: value });
              }}
              labelStyle={styles.registerBodyInputLabelForm}
              inputStyle={styles.registerBodyInputForm}
              inputContainerStyle={styles.registerBodyInputContainerForm}
              value={userInfos.email}
              returnKeyType="next"
            />
            <Input
              clearButtonMode="while-editing"
              editable={true}
              keyboardType="default"
              textContentType="newPassword"
              label="Mot de passe *"
              secureTextEntry={true}
              onChangeText={(value) => {
                setUserInfos({ ...userInfos, password: value });
              }}
              labelStyle={styles.registerBodyInputLabelForm}
              inputStyle={styles.registerBodyInputForm}
              inputContainerStyle={styles.registerBodyInputContainerForm}
              value={userInfos.password}
              returnKeyType="next"
            />
            <Input
              clearButtonMode="while-editing"
              editable={true}
              keyboardType="default"
              textContentType="password"
              label="Confirmation du mot de passe *"
              secureTextEntry={true}
              onChangeText={(value) => setConfirmPassword(value)}
              labelStyle={styles.registerBodyInputLabelForm}
              inputStyle={styles.registerBodyInputForm}
              inputContainerStyle={styles.registerBodyTelephoneContainerForm}
              value={confirmPassword}
              returnKeyType="next"
            />
            <Input
              autoCompleteType="email"
              clearButtonMode="while-editing"
              editable={true}
              keyboardType="numeric"
              textContentType="givenName"
              label="Code Parrainage"
              onChangeText={(value) => {
                setUserInfos({ ...userInfos, referralId: value });
              }}
              labelStyle={styles.registerBodyInputLabelForm}
              inputStyle={styles.registerBodyInputForm}
              inputContainerStyle={styles.registerBodyInputContainerForm}
              value={userInfos.referralId}
              returnKeyType="next"
            />
            <Input
              leftIcon={<Text style={styles.leftIconTelephone}> +33 </Text>}
              leftIconContainerStyle={styles.leftIconTelephoneContainer}
              autoCompleteType="tel"
              clearButtonMode="while-editing"
              editable={true}
              keyboardType="phone-pad"
              textContentType="telephoneNumber"
              label="Telephone *"
              onChangeText={(value) => {
                setMobileWithoutCountry(value);
                let formattedNum = value.replace(/ /g, "");
                let simpleNumber = formattedNum.replace(/-/g, "");
                formattedNum = countryCode + simpleNumber;
                setUserInfos({
                  ...userInfos,
                  mobile: formattedNum,
                  numero: simpleNumber,
                });
              }}
              labelStyle={styles.registerBodyInputLabelForm}
              inputStyle={styles.registerBodyTelephoneForm}
              inputContainerStyle={styles.registerBodyTelephoneContainerForm}
              value={mobileWithoutCountry}
              returnKeyType="send"
            />

            <Text style={styles.registerBodyTextForm}>
              * Champs Obligatoires
            </Text>

            <Button
              title="suivant"
              titleStyle={styles.registerBodyButtonTextForm}
              buttonStyle={styles.registerBodyButtonForm}
              containerStyle={styles.registerBodyButtonContainerForm}
              onPress={() => {
                setLoading(true);
                console.log("userInfos");
                // console.log(userInfos);
                console.log("userInfos");
                onPressRegister();
              }}
            />
          </View>
          {/* BodyForm
			<Formik 
				initialValues={{
				lastName: 'qwerty',
				firstName: '',
				email: '',
				password: '',
				confirmPassword: '',
				referralId: '',
				mobile: '',
				}}

				validationSchema={verifSchema}      

				onSubmit={(values, actions) => {
				console.log('debut submit');
				// setUserInfos({...userInfos, email:values.email });
				console.log('donnees enregistrees dans le state');
				console.log(userInfos);
				if(validateMobile()){
					// setLoading(true);
					console.log(userInfos);
					checkUserExists(userInfos).then((res)=>{
					if(res.users && res.users.length>0){
						// setLoading(false);
						Alert.alert(language.alert,language.user_exists);
					}
					else if(res.error){
						// setLoading(false);
						console.log(res.error);
						Alert.alert(language.alert,language.email_or_mobile_issue);
					}
					else{
						if (userInfos.referralId && userInfos.referralId.length > 0) {
						validateReferer(userInfos.referralId).then((referralInfo)=>{
							if (referralInfo.uid) {
							emailSignUp({...userInfos, signupViaReferral: referralInfo.uid}).then((res)=>{
								// setLoading(false);
								if(res.uid){
								props.navigation.navigate("PhoneScreen",userInfos);
								}
								else{
								Alert.alert(language.alert,language.reg_error);
								}
							})
							}
							else{
							setLoading(false);
							Alert.alert(language.alert,language.referer_not_found)
							}
						}).catch((error)=>{
							// setLoading(false);
							Alert.alert(language.alert,language.referer_not_found)
						});
						} 
						else {
						emailSignUp(userInfos).then((res)=>{
							// setLoading(false);
							if(res.uid){
							props.navigation.navigate("PhoneScreen",userInfos);
							}
							else{
							Alert.alert(language.alert,language.reg_error);
							}
						})
						}
					}
					})
				}
				console.log('enregistrement....');
				actions.resetForm();
				console.log('fin');

				}}>
				{({handleChange, handleSubmit, values, errors }) => (
				
				
				)}
			</Formik>  */}
          {/* <Formik 
			initialValues={{
				nom: '',
				prenom: '',
				email: '',
				motDePasse: '',
				confMotDePasse: '',
				codeParrainage: '',
				telephone: ''
			}}

			validationSchema={verifSchema}      

			onSubmit={(values, actions) => {
				setUserInfos(values); 
				navigation.navigate('PhoneScreen');
				actions.resetForm();
			}}>
			{({handleChange, handleSubmit, values, errors }) => (
				<View>
				<View>
					<Text style={CustomStyle.labelInputStyle}>Nom *</Text>
					<View style={CustomStyle.textInputContainerStyle}>
					<TextInput style={CustomStyle.inputTextStyle} 
						value={values.nom}
						onChangeText={handleChange('nom')}
					/>
					</View>
					<Text style={CustomStyle.errorInputText}>{ errors.nom }</Text>
				</View>

				<View>
					<Text style={CustomStyle.labelInputStyle}>Prenom *</Text>
					<View style={CustomStyle.textInputContainerStyle}>
					<TextInput style={CustomStyle.inputTextStyle} 
						value={values.prenom}
						onChangeText={handleChange('prenom')}
					/>
					</View>      
					<Text style={CustomStyle.errorInputText}>{ errors.prenom }</Text>
				</View>

				<View>
					<Text style={CustomStyle.labelInputStyle}>Email *</Text>
					<View style={CustomStyle.textInputContainerStyle}>
					<TextInput style={CustomStyle.inputTextStyle} 
						value={values.email}
						onChangeText={handleChange('email')}
					/>
					</View>      
					<Text style={CustomStyle.errorInputText}>{ errors.email }</Text>
				</View>

				<View>
					<Text style={CustomStyle.labelInputStyle}>Mot de passe *</Text>
					<View style={CustomStyle.textInputContainerStyle}>
					<TextInput style={CustomStyle.inputTextStyle} 
						value={values.motDePasse}
						onChangeText={handleChange('motDePasse')}
						secureTextEntry={true}
					/>
					</View>      
					<Text style={CustomStyle.errorInputText}>{ errors.motDePasse }</Text>
				</View>

				<View>
					<Text style={CustomStyle.labelInputStyle}>Confirmation du mot de passe *</Text>
					<View style={CustomStyle.textInputContainerStyle}>
					<TextInput style={CustomStyle.inputTextStyle} 
						value={values.confMotDePasse}
						onChangeText={handleChange('confMotDePasse')}
						secureTextEntry={true}
					/>
					</View>      
					<Text style={CustomStyle.errorInputText}>{ errors.confMotDePasse }</Text>
				</View>

				<View>
					<Text style={CustomStyle.labelInputStyle}>Code parrainage</Text>
					<View style={CustomStyle.textInputContainerStyle}>
					<TextInput style={CustomStyle.inputTextStyle} 
						value={values.codeParrainage}
						onChangeText={handleChange('codeParrainage')}
					/>
					</View>
					<Text style={CustomStyle.errorInputText}>{ errors.codeParrainage }</Text>
				</View>

				<Text style={CustomStyle.labelInputStyle}>Telephone *</Text>
				<TextInput style={CustomStyle.inputFr} keyboardType='numeric' />
				<TextInput 
					style={CustomStyle.inputTextStyle} 
					keyboardType='numeric' 
					value={values.telephone}
					onChangeText={handleChange('telephone')}
					/>
				<Text style={CustomStyle.errorInputText}>{ errors.telephone }</Text>

				<View style={{width: 180, height: 27, marginVertical: 5}}>
					<Text style={CustomStyle.champOb}>* Champs Obligatoires</Text>
				</View>

				<View style={{
					alignItems: 'center',
					borderRadius: 21,
					paddingVertical: 15,
					borderRadius: 13,
					backgroundColor: colors.VIOLET,
					color: colors.WHITE,}}>
					<TouchableOpacity onPress={handleSubmit}>
					<Text style={CustomStyle.textSuiv}>Suivant</Text>
					</TouchableOpacity>
				</View>
				</View>
				)}
			</Formik> */}
          {/** Form */}

          {/* Footer */}
          <View style={styles.registerFooter}>
            <Text style={styles.registerFooterText}>
              Vous avez déjà un compte ?
            </Text>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("LoginScreen")}
            >
              <Text style={styles.registerFooterTextConnect}>
                Connectez-vous !
              </Text>
            </TouchableOpacity>
          </View>
          {/** Footer */}

          {loading ? (
            <View style={styles.loading}>
              <ActivityIndicator color={colors.VIOLET} size="large" />
            </View>
          ) : null}
        </ScrollView>
      </SafeAreaView>
    </KeyboardAwareScrollView>
  );
}

export default RegisterScreen;
