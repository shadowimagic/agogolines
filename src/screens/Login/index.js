import React, { useState, useContext, useEffect, useRef } from "react";
import {
  View,
  Text,
  Image,
  Alert,
  ActivityIndicator,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  SafeAreaView,
  Modal,
} from "react-native";
import { Input, Button } from "react-native-elements";
import { useDispatch, useSelector } from "react-redux";
import { FirebaseContext } from "../../../redux/src";
// import logo from "../../../assets/logo.png";
import { useFormik } from "formik";
import * as yup from "yup";
import { language } from "../../../config";
import { colors } from "../../common/theme";
import styles from "./styles";
import { SvgXml } from "react-native-svg";
import logo_login from "../../../assets/svg/logo_login";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import ModalResetPassword from "../../components/ModalResetPassword";
import AgogoInput from "../../components/AgogoInput/AgogoInput";
import { DismissAvoidKeyboard } from "../../hooks/DismissAvoidKeyboard";
import { Container } from "./styles";
const Login = (props) => {
  const { api } = useContext(FirebaseContext);
  const firstTextInput = useRef();
  const secondTextInput = useRef();
  const auth = useSelector((state) => state.auth);
  const { signIn, fetchUser, clearLoginError } = api;
  const pageActive = useRef(false);
  const [loading, setLoading] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [displayModal, setDisplayModal] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (auth.info && pageActive.current) {
      pageActive.current = false;
      dispatch(fetchUser());
      props.navigation.navigate("AuthLoadingScreen");
      setLoading(false);
    }
    if (
      auth.error &&
      auth.error.msg &&
      auth.error.msg.message !== language.not_logged_in
    ) {
      pageActive.current = false;
      Alert.alert(language.alert, auth.error.msg.message);
      dispatch(clearLoginError());
      setLoading(false);
    }
  }, [auth.info, auth.error, auth.error.msg]);

  const schema = yup.object({
    userEmail: yup.string().email().required(),
    userPass: yup.string().min(6).required(),
  });

  const formik = useFormik({
    initialValues: {
      userEmail: "",
      userPass: "",
    },
    validationSchema: schema,
    onSubmit: (values, actions) => {
      setLoading(true);
      const email = formik.values.userEmail;
      const password = formik.values.userPass;
      // formik.resetForm();
      pageActive.current = true;
      if (api) {
        // console.log("LOGIN")
        dispatch(signIn(email, password));
      }
    },
    validateOnMount: true,
  });

  const onAction = async () => {
    if (formik.isValid) {
      formik.handleSubmit();
    } else {
      setBtnLoading(true);
      Alert.alert(language.alert, language.password_blank_messege, [
        {
          text: "OK",
          onPress: () => {
            setBtnLoading(false);
          },
        },
      ]);
    }
  };

  const onPressRegister = async () => {
    pageActive.current = false;
    props.navigation.navigate("RegisterScreen");
  };

  if (loading) {
    return (
      <View style={styles.loadingScreen}>
        <ActivityIndicator color={colors.VIOLET} size="large" />
      </View>
    );
  }

  return (
    <DismissAvoidKeyboard>
      <Container>
        <Modal visible={displayModal} transparent={true} animationType="fade">
          <ModalResetPassword closeModal={() => setDisplayModal(false)} />
        </Modal>
        <View style={styles.logo}>
          {/* <Image source={logo} /> */}
          <SvgXml xml={logo_login} />
          <Text style={styles.headerTitle}>{language.connectez_vous}</Text>
        </View>
        <View style={styles.inputWrapper}>
          <View style={styles.inputElement}>
            <Input
              onSubmitEditing={() => firstTextInput.current.focus()}
              editable
              label={language.email_placeholder}
              value={formik.values.userEmail}
              onChangeText={formik.handleChange("userEmail")}
              keyboardType="email-address"
              inputStyle={styles.inputStyle}
              inputContainerStyle={styles.inputContainerStyle}
              labelStyle={styles.labelInputStyle}
            />
            <Input
              ref={firstTextInput}
              editable
              label={language.password_placeholder}
              value={formik.values.userPass}
              onChangeText={formik.handleChange("userPass")}
              inputStyle={styles.inputStyle}
              inputContainerStyle={styles.inputContainerStyle}
              labelStyle={styles.labelInputStyle}
              secureTextEntry={true}
            />
            <TouchableOpacity onPress={() => setDisplayModal(true)}>
              <Text style={styles.forgotText}>
                {language.forgot_password_link}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.btnWrapper}>
          <View style={styles.btnContainer}>
            <Button
              title={language.login_button}
              titleStyle={styles.btnTitle}
              buttonStyle={styles.connectBtnStyle}
              onPress={onAction}
              loading={btnLoading}
            />
          </View>
          <Text style={styles.noAccText}>{language.pas_compte_link}</Text>
          <TouchableOpacity onPress={onPressRegister}>
            <Text style={styles.incrivezVousText}>Inscrivez-Vous !</Text>
          </TouchableOpacity>
        </View>
      </Container>
    </DismissAvoidKeyboard>
  );
};

export default Login;
