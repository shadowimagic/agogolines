import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../common/theme";
import styled from "styled-components/native";
const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

export const Container = styled.View`
  flex: 1;
  justify-content: space-around;
  align-items: center;
  height: ${height};
  width: ${width};
`;

export const ImageContainer = styled.View`
  height: 23%;
  /* width: 212.53px; */
  width: 51.5%;
  margin-top: 15%;
  border: 2px solid blue;
`;

export const TitleText = styled.Text`
  color: #5a48cb;
  font-family: montserrat-medium;
  font-size: 25px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 30px;
  text-align: center;
`;

export const TitleContainer = styled.View`
  height: 41px;
  width: 100%;
  /* color: #5a48cb; */
  /* font-family: Montserrat; */
  /* font-size: 25px; */
  /* font-weight: 500; */
  /* letter-spacing: 0; */
  /* line-height: 30px; */
  /* text-align: center; */
`;

export const InputContainer = styled.View`
  /* height: 76.18px; */
  height: 22%;
  /* width: 315.5px; */
  width: 85%;
`;
export const ForgotContainer = styled.TouchableOpacity`
  height: 27px;
  width: 40%;
`;

export const ForgotText = styled.Text`
  color: #000000;
  font-family: montserrat-medium;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 18px;
  text-align: center;
  text-decoration: underline;
`;

export const ButtonContainer = styled.View`
  height: 49px;
  width: 85%;
`;

export const ForgotTextContainer = styled.View`
  height: 27px;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

export const RegisterPreText = styled.Text`
  color: #5a48cb;
  font-family: montserrat-medium;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 18px;
  text-align: center;
`;
export const RegisterTextContainer = styled.TouchableOpacity`
  height: 27px;
  width: 100%;
`;

export const FooterContainer = styled.View`
  height: 8%;
  width: 70%;
  justify-content: space-between;
  flex-direction: column;
`;
export const RegisterText = styled.Text`
  color: #000000;
  font-family: montserrat-medium;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 18px;
  text-align: center;
  text-decoration: underline;
`;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    marginVertical: 10,
  },
  loadingScreen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 10,
  },
  logo: {
    alignItems: "center",
    justifyContent: "center",
  },
  headerTitle: {
    color: colors.VIOLET,
    fontSize: 24,
  },
  inputWrapper: {
    alignItems: "center",
    width: "100%",
  },
  inputElement: {
    width: "85%",
    alignItems: "center",
  },
  inputStyle: {
    height: 50,
    backgroundColor: colors.WHITE,
    borderRadius: 13,
    shadowColor: colors.BLACK,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    padding: 10,
    fontSize: 14,
    fontFamily: "Montserrat-Bold",
  },
  inputContainerStyle: {
    borderBottomWidth: 0,
  },
  labelInputStyle: {
    color: "black",
    fontSize: 13,
    fontFamily: "Montserrat-Bold",
    marginBottom: 14,
    marginLeft: 5,
  },
  btnWrapper: {
    alignItems: "center",
    width: "100%",
  },
  forgotText: {
    borderBottomColor: "black",
    color: colors.BLACK,
    textDecorationLine: "underline",
  },
  connectBtnStyle: {
    height: 50,
    borderRadius: 13,
    backgroundColor: "transparent",
    backgroundColor: colors.VIOLET,
    padding: 10,
    marginHorizontal: 15,
  },
  btnTitle: {
    backgroundColor: "transparent",
    color: colors.WHITE,
    fontSize: 16,
    fontFamily: "Montserrat-Bold",
    textTransform: "uppercase",
    marginHorizontal: 20,
  },
  btnContainer: {
    marginVertical: 10,
    width: "85%",
  },
  noAccText: {
    color: colors.VIOLET,
    marginTop: 15,
  },
  incrivezVousText: {
    marginTop: 10,
    color: colors.BLACK,
    textDecorationLine: "underline",
  },
});

export default styles;
