import React, { useState, useEffect, useRef, useContext, useCallback, useMemo } from "react";
import {
  Dimensions,
  StyleSheet,
  SafeAreaView,
  TextInput,
  Text,
  FlatList,
  View,
  TouchableOpacity,
  Alert,
  Image,
  Modal,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  Polyline,
  PROVIDER_DEFAULT,
} from "react-native-maps";
import { Google_Map_Key, language } from "../../../config";
import { colors } from "./../../common/theme";
import customMapStyle from "./../../common/customMapStyle";
import { SvgXml } from "react-native-svg";
import travel from "../../../assets/svg/travel";
import car from "../../../assets/images/car.png";
import src_dest from "../../../assets/images/src_dest.png";
import MapViewDirections from "react-native-maps-directions";
import Geolocation from "react-native-geolocation-service";
import "react-native-get-random-values";
import { v4 as uuidv4 } from "uuid";

//Modals
import ModalFollowLastResearch from "../../components/ModalFollowLastResearch";
import PaiementMod from "../../components/PaiementMod/index";
import SearchDriver from "../../components/SearchDriver";
import ValidateRoute from "../../components/ValidateRoute";
import TravelConfirm from "../../components/ModalTravelConfirmed";
import NoDriverFoundByRiderModal from "../../components/NoDriverFoundByRiderModal";
import RiderTravelGo from "../../components/RiderTravelGo";
import NegoPrixDriver from "../../components/NegoPrixDriver";
import RatingAfterTravel from "../../components/RatingAfterTravel";

import { FirebaseContext } from "../../../redux/src";
import { useSelector, useDispatch } from "react-redux";
import iconLocalisationStart from "./../../../assets/svg/iconLocalisationStart";
import iconLocalisationEnd from "./../../../assets/svg/iconLocalisationEnd";
import {
  fetchCoordsfromPlace,
  getDriverTime,
} from "../../../redux/src/other/GoogleAPIFunctions";
import DriverCancelTravel from "../../components/DriverCancelTravel";
import DriverWaitRiderNegoPrice from "../../components/DriverWaitRiderNegoPrice";
import ModalTimeBtwDriverRider from "../../components/ModalTimeBtwDriverRider";
import DriverPickUpRider from "../../components/DriverPickUpRider";
import TravelEnCours from "../../components/TravelEnCours";
import { EventRegister } from "react-native-event-listeners";

// Functions
import { getData, removeData, setData } from "../../functions/AsyncStorageFunctions";
import { fetchCreditCard } from "../../../redux/src/actions/creditcardactions";
import useDebounce from "../../hooks/useDebounce";
import { addNearby } from "../../../redux/src/actions/nearbyactions";

let checkTravelExist = false;

export default function HomeRider(props) {
  const [displayModal, setDisplayModal] = useState(false);
  const [displayDriverCancelTravel, setDisplayDriverCancelTravel] =
    useState(false);
  const [displayPaieMod, setDisplayPaieMod] = useState(false);
  const [travelCompleted, setTravelCompleted] = useState(false);
  const [displayResearchDriver, setDisplayResearchDriver] = useState(false);
  const [displayTravelConfirm, setDisplayTravelConfirm] = useState(false);
  const [displayWaitRiderNegoPrice, setDisplayWaitRiderNegoPrice] =
    useState(false);
  const [travelInit, setTravelInit] = useState(true);
  const [riderPriceDone, setRiderPriceDone] = useState(false);
  const [travelAccepted, setTravelAccepted] = useState(false);
  const [displayTimeBtwRD, setDisplayTimeBtwRD] = useState(false);
  const [displayDriverPURider, setDisplayDriverPURider] = useState(false);
  const [displayTravelEnCours, setTravelEnCours] = useState(false);
  const [travelTab, setTravelTab] = useState(null);
  const [displayRatingAfterTravel, setDisplayRatingAfterTravel] =
    useState(false);

  const [displayReservation, setDisplayReservation] = useState(true);
  const [searchKeyWordDepart, setSearchKeyWordDepart] = useState("");
  const [searchKeyWordDestination, setSearchKeyWordDestination] = useState("");
  const [isFocusDepart, setIsFocusDepart] = useState(false);
  const [isFocusDestination, setIsFocusDestination] = useState(false);
  const [searchResultsDepart, setSearchResultsDepart] = useState([]);
  const [searchResultsDestination, setSearchResultsDestination] = useState([]);
  const [isShowingResultsDepart, setIsShowingResultsDepart] = useState(false);
  const [RiderAndDriverDist, setRiderAndDriverDist] = useState(null);
  const [closeCancelModal, setCloseCancelModal] = useState(false);
  const [isShowingResultsDestination, setIsShowingResultsDestination] =
    useState(false);
  const [maDistance, setMaDistance] = useState(null);
  const [annuler, setAnnuler] = useState(false)
  const [pointValeur, setPointValeur] = useState(null);
  const [monTemps, setmonTemps] = useState(null);
  const refMap = useRef();
  const { api } = useContext(FirebaseContext);
  const [loadCal, setLoadCal] = useState(false);
  const dispatch = useDispatch();
  const {
    fetchTravelData,
    fetchOneTravel,
    fetchPoint,
    updateTravel,
    getRouteDetails,
    fetchBooking,
    signOutM,
    deleteNearby,
    fetchNearby,
    fetchRiderRequest,
    fetchOneRiderRequest,
    addRiderRequest,
    deleteRiderRequest,
    addNearby,
    watchDriverPosition,
    fetchCreditCard,
  } = api || {};
  const auth = useSelector((state) => state.auth);
  const travelData = useSelector((state) => state.traveldata);
  const positionReducer = useSelector((state) => state.positiondata);
  let bonusError = { text: "", id: uuidv4().toString().substring(0, 5) };
  const travelOneData = useSelector((state) => state.travel);

  const travelInformations = useMemo(() => travelOneData.travel, [travelOneData?.travel]);

  let travelList = travelData.travel;

  const { driverPosition } = positionReducer;

  const latitudeDelta = 0.0222;
  const longitudeDelta = 0.0321;

  const pointdata = useSelector((state) => state.pointdata);
  const resNearby = useSelector((state) => state.nearbydata);
  const riderData = useSelector((state) => state.requestReducerdata)
  const riderRequest = riderData.ride

  
  const nearby = resNearby.nearby
  const pointInformations = pointdata.point;

  const [paymentData, setPaymentData] = useState(null);

  const direction = (dep, dest) => {
    let direction = "";
    let difx = dep.latitude - dest.latitude;
    let dify = dep.longitude - dest.longitude;
    let angle = (Math.atan2(difx, dify) * 180) / Math.PI;
    if (angle > -22.5 && angle < 22.5) {
      direction = "N";
    }
    if (angle > 22.5 && angle < 67.5) {
      //direction = 'NE';
      direction = "N";
    }
    if (angle > 67.5 && angle < 112.5) {
      direction = "E";
    }
    if (angle > 112.5 && angle < 157.5) {
      //direction = 'SE';
      direction = "S";
    }
    if (angle < -157.5 || angle > 157.5) {
      direction = "S";
    }
    if (angle < -112.5 && angle > -157.5) {
      //direction ='SW';
      direction = "S";
    }
    if (angle < -67.5 && angle > -112.5) {
      direction = "W";
    }
    if (angle < -22.5 && angle > -67.5) {
      //direction = 'NW';
      direction = "N";
    }
    return direction;
  };

  // new google place function

  const searchLocation = async (text, type) => {
    if (type === "depart") {
      setSearchKeyWordDepart(text);
    } 
    if (type === "destination") {
      setSearchKeyWordDestination(text);
    }
  };

  useDebounce(
    async () => {
      if (isFocusDepart) {
        const response = await fetch(
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=" +
            Google_Map_Key +
            "&input=" +
            searchKeyWordDepart
        );
        const json = await response.json();

        if (json.predictions) {
          setSearchResultsDepart(json.predictions);
          setIsShowingResultsDepart(true);
          setDisplayReservation(false);
        }
      }
    },
    500,
    [searchKeyWordDepart]
  );

  useDebounce(
    async () => {
      if (isFocusDestination) {
        const response = await fetch(
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=" +
            Google_Map_Key +
            "&input=" +
            searchKeyWordDestination
        );
        const json = await response.json();

        if (json.predictions) {
          setSearchResultsDestination(json.predictions);
          setIsShowingResultsDestination(true);
          setDisplayReservation(false);
        }
      }
    },
    500,
    [searchKeyWordDestination]
  );

  
  const [nearbyPosition, setNearbyPosition] = useState({...Rider, longitude: null, latitude: null, date: null})
  const [carPosition, setCarPosition] = useState({
    latitude: 0,
    longitude: 0,
    error: null,
  });
  const [depLocation, setDepLocation] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    address_name: null,
    default: true,
  });
  const [destLocation, setDestLocation] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    address_name: null,
    default: true,
  });
  const [initialRegion, setInitialRegion] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: latitudeDelta,
    longitudeDelta: longitudeDelta,
  });
  const [distance, setDistance] = useState({
    value: null,
    text: "",
  });
  const [byRoadDuration, setByRoadDuration] = useState({
    value: null,
    text: "",
  });
  const [modalVisible, setModalVisible] = useState(false);
  const [pickLocationType, setPickLocationType] = useState(null);
  const [margin, setMargin] = useState(0);
  const [sendData, setSendData] = useState({
    rider_id: null,
    rider_notification: null,
    lat_dep: null,
    long_dep: null,
    addr_dep: null,
    lat_dest: null,
    long_dest: null,
    addr_dest: null,
    direction: null,
    distance: null,
    durer: null,
    prix: 0,
    prix_total: 0,
    prix_affiner: 0,
    passager_nb: null,
    create_date:
      new Date().getDate() +
      "/" +
      new Date().getMonth() +
      "/" +
      new Date().getFullYear(),
    create_time: new Date().getTime(),
    profile_img_rider: null,
    mode_paiement: "especes",
  });

  const [rateDetails, setRateDetails] = useState({
    rate_per_unit_distance: 5,
    min_distance: 5,
    min_rate_unit_distance: 0.5,
    free_doc: 2.5,
  });

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      dispatch(fetchBooking());
      dispatch(fetchPoint());
      dispatch(fetchNearby());

      if (pointInformations && pointInformations[0].point) {
        console.log(
          "valeur de le point information:",
          pointInformations[0].point
        );
        let val = Math.ceil(
          (pointInformations[0].montant * auth.info.profile.point) /
            pointInformations[0].point
        );
        setPointValeur(val);
        console.log("valeur de valeur:", val);
      } else {
        setPointValeur(0);
      }
      console.log("platform:", auth.info.profile.userPlatform);
    }
  }, [auth.info, fetchBooking, fetchPoint]);

  useEffect(() => {
    if (auth.info && auth.info.profile && auth.info.profile.login) {
      const interval = setInterval(() => {
        let next = auth.info.profile.login;
        //console.log("valeur de next:",next);
        AsyncStorage.getItem("loginplus", (err, result) => {
          //	console.log("valeur de result:",result);
          if (next != result) {
            dispatch(signOutM());
            props.navigation.navigate("LoginScreen");
          }
        });
      }, 7000);
      return () => clearInterval(interval);
    }
  }, [auth.info]);

  useEffect(() => {
    if (
      destLocation.latitude != null &&
      destLocation.longitude != null &&
      !destLocation.default
    ) {
      let startLoc =
        '"' + depLocation.latitude + "," + depLocation.longitude + '"';
      let destLoc =
        '"' + destLocation.latitude + "," + destLocation.longitude + '"';

      let distance = 0;

      if (
        depLocation.latitude == destLocation.latitude &&
        depLocation.longitude == destLocation.longitude
      ) {
        distance = 0;
      } else {
        var radlat1 = (Math.PI * depLocation.latitude) / 180;
        var radlat2 = (Math.PI * destLocation.latitude) / 180;
        var theta = depLocation.longitude - destLocation.longitude;
        var radtheta = (Math.PI * theta) / 180;
        var dist =
          Math.sin(radlat1) * Math.sin(radlat2) +
          Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
          dist = 1;
        }
        dist = Math.acos(dist);
        dist = (dist * 180) / Math.PI;
        dist = dist * 60 * 1.1515;
        distance = dist * 1.609344;
      }

      let prixTrajet = 0;

      if (distance <= rateDetails.min_distance) {
        prixTrajet = rateDetails.rate_per_unit_distance;
      } else {
        let diffDistance = distance - rateDetails.min_distance;

        prixTrajet = Math.round(
          diffDistance * rateDetails.min_rate_unit_distance +
            rateDetails.rate_per_unit_distance
        );
      }

      getRouteDetails(startLoc, destLoc).then((res) => {
        if (res) {
          checkTravelExist && setTravelTab(null);
          setSendData({
            rider_id: auth.info.uid,
            rider_notification: auth.info.pushToken,
            lat_dep: depLocation.latitude,
            long_dep: depLocation.longitude,
            addr_dep: depLocation.address_name,
            lat_dest: destLocation.latitude,
            long_dest: destLocation.longitude,
            addr_dest: destLocation.address_name,
            direction: direction(depLocation, destLocation),
            distance: res.distance,
            durer: parseFloat(res.duration / 60).toFixed(0),
            prix: prixTrajet,
            passager_nb: 1,
            prix_total: prixTrajet,
            prix_affiner: prixTrajet,
            create_date:
              new Date().getDate() +
              "/" +
              new Date().getMonth() +
              "/" +
              new Date().getFullYear(),
            create_time: new Date().getTime(),
            profile_img_rider: auth.info.profile.profile_image,
            mode_paiement: "especes",
          });
        }
        
      });
    }
  }, [destLocation]);

  useEffect(() => {
    if (paymentData != null && displayResearchDriver == true) {
      dispatch(addRiderRequest({
        ...sendData,
        addr_dep: depLocation.address_name,
        addr_dest: destLocation.address_name,
        passager_nb: 1,
        create_date:
          new Date().getDate() +
          "/" +
          new Date().getMonth() +
          "/" +
          new Date().getFullYear(),
        create_time: new Date().getTime(),
        lat_dep: depLocation.latitude,
        long_dep: depLocation.longitude,
        lat_dest: destLocation.latitude,
        long_dest: destLocation.longitude,
        mode_paiement: sendData.mode_paiement || "especes",
        rider_notification: null, 
        profile_img_rider: null, 
        driverDist: RiderAndDriverDist,
        status: "init"
      }))
    }
    if (annuler == true && riderRequest.etat == "refuser") {
      console.log("")      
      dispatch(deleteRiderRequest());
      setAnnuler(false);
    }
  }, [displayResearchDriver, annuler])

  const initFetch = useCallback(() => {
    if (auth?.info?.profile) {
      fetchOneRiderRequest && dispatch(fetchOneRiderRequest(auth.info.uid));
      fetchOneTravel && dispatch(fetchOneTravel(riderRequest?.driver_id));
      fetchRiderRequest && dispatch(fetchRiderRequest());
    }
  }, [dispatch, auth?.info?.profile]);

  useEffect(() => {
    initFetch()
  }, [])

  useEffect(() => {
    if (riderRequest != null && displayResearchDriver) {
      if (riderRequest.etat == "accepter") {
        startRouteIfOK();
      }
    }
  })

  const startRouteIfOK = () => {
    setDisplayResearchDriver(false);
  }

  useEffect(() => {
    findCoordinates();
  }, []);

  const Rider = {
    uid: auth?.info?.uid,
    type: auth?.info?.profile?.usertype,
  }

  const findCoordinates = async () => {
    Geolocation.getCurrentPosition(
      (position) => {
        setDepLocation({
          ...depLocation,
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          default: false,
        });
        setInitialRegion({
          ...initialRegion,
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: latitudeDelta,
          longitudeDelta: longitudeDelta,
        });
        refMap?.current?.animateToRegion({
          ...initialRegion,
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
        let merge = {...Rider, longitude: position.coords.longitude, latitude: position.coords.latitude, date: new Date().getTime()}
        setNearbyPosition(merge);
      },
      (error) => {
        console.log({ error });
      },
      {
        enableHighAccuracy: true,
        timeout: 15000,
      }
    );
  };


  useEffect(() => {
    retrieveInfoNearby();
  }, [nearbyPosition])

  const retrieveInfoNearby = () => {
    if (nearby == null ) {
      if (nearbyPosition.uid) {
        dispatch(addNearby(nearbyPosition))
      } 
      
    } else {
      const res = nearby.filter(obj => obj.type == 'rider' && obj.uid == auth.info.uid)

      if (res.length == 0) { dispatch(addNearby(nearbyPosition)); }
      if (res) {
        for (let i= 0; i < res.length; i++) {
          const secondeSeparation = Math.abs((new Date().getTime() - res[i].date) / 1000);
          if (secondeSeparation >= 900) {
            dispatch(deleteNearby(res[i].id))
          }
        }
      }
    }
  }
 //GET ALL DRIVER NEARBY AND SORT

  const onPressBackModePaiement = async (donner) => {
    setSendData({ ...sendData, mode_paiement: donner });
    setLoadCal(true);
  };

  onPressBackCalcul = async (donner, montantpromo, code) => {
    if (auth.info && auth.info.profile && loadCal === true) {
      console.log("valeur de donner  dans calcul:", donner);
      setPaymentData({ donner, montantpromo, code });
      dispatch(fetchTravelData(sendData));

      if (destLocation.latitude != null && destLocation.longitude != null) {
        //  console.log("sendData data"+JSON.stringify(sendData));
      }
    }
  };

  const onEnRoute = () => {
    setDisplayDriverPURider(false);
    setTravelEnCours(true);
    dispatch(
      updateTravel({
        is_rider_picked_up: true,
        driver_id: travelTab?.driver_id,
      })
    );
  };

  const onReadyToTravel = () => {
    setTravelAccepted(false);
    setDisplayTimeBtwRD(true);
    travelInformations?.driver_id &&
      dispatch(watchDriverPosition(travelInformations?.driver_id));
  };

  const getTravelData = async () => {
    const selectedTravel = travelList.find(
      (travelItem) => !travelItem?.isFound && travelItem?.status === "init"
    );
    if (selectedTravel) {
      let result = await getDistanceTime(selectedTravel);
      let { donner, montantpromo, code } = paymentData || {};

      setMaDistance(result.distance_in_km);
      setmonTemps(result.time_in_text);
      if (montantpromo === undefined) montantpromo = 0;
      if (code === undefined) code = "0";
      setTravelTab(selectedTravel);
      setTravelCompleted(true);
      let iddriver = selectedTravel.id + "";
      await AsyncStorage.setItem("idDriver", iddriver);
      dispatch(
        updateTravel(
          {
            propose_dep_rider: sendData.addr_dep,
            propose_dest_rider: sendData.addr_dest,
            propose_email_rider: auth.info.profile.email,
            propose_firstname_rider: auth.info.profile.firstName,
            rider_pushToken: auth.info.profile.pushToken,
            propose_lastname_rider: auth.info.profile.lastName,
            propose_nbPlace_rider: sendData.passager_nb,
            propose_negoPrice_rider: sendData.prix_affiner,
            propose_negoPrice: sendData.prix_affiner,
            propose_tarif: sendData.prix_total,
            rider_trip_direction: "S",
            rider_trip_distance: sendData.distance,
            rider_trip_time: sendData.durer,
            rider_trip_propose_date_time: new Date().getTime(),
            propose_phone_rider: auth.info.profile.mobile,
            propose_profile_img_rider:
              auth?.info?.profile?.profile_image || null,
            mode_paiement: sendData.mode_paiement || "especes",
            isFound: true,
            isAffiner_rider: false,
            isAffiner_driver: false,
            prix_promo: montantpromo,
            code_promo: code,
            prix_point: pointValeur || 0,
            rider_lat_dep: sendData.lat_dep,
            rider_long_dep: sendData.long_dep,
            rider_lat_dest: sendData.lat_dest,
            rider_long_dest: sendData.long_dest,
            started_at: null,
            status: "init",
            arrived_at: null,
            accepted_at: null,
            paided_at: null,
            ended_at: null,
            driver_pushToken: selectedTravel.driver_pushToken,
            propose_rider_id: auth.info.uid,
            driver_id: selectedTravel.driver_id,
            propose_dst_btw_rider_driver: result.distance_in_km,
            propose_tmp_btn_rider_driver: result.time_in_text,
            is_rider_picked_up: false,
          },
          "init getTravelData"
        )
      );
    }
  };


  const onCheckAlreadyHaveTravel = async () => {
    checkTravelExist = true;
    const travelId = await getData("travelId");
    if (travelId) {
      !travelInformations && dispatch(fetchOneTravel(travelId));
    } else {
      await removeData("travelId");
      await removeData("cardToUse");
    }
  };

  const attachOldTravelLocations = () => {
    setSearchKeyWordDepart(travelInformations?.propose_dep_rider);
    setSearchKeyWordDestination(travelInformations.propose_dest_rider);

    travelInformations?.rider_lat_dep &&
      travelInformations?.rider_long_dep &&
      setDepLocation({
        latitude: travelInformations?.rider_lat_dep,
        longitude: travelInformations?.rider_long_dep,
        address_name: travelInformations?.propose_dep_rider,
        default: false,
      });
    travelInformations.rider_lat_dest &&
      travelInformations.rider_long_dest &&
      setDestLocation({
        latitude: travelInformations.rider_lat_dest,
        longitude: travelInformations.rider_long_dest,
        address_name: travelInformations.propose_dest_rider,
        default: false,
      });
  };

  useEffect(() => {
    if (!checkTravelExist) {
      return;
    }
    if (
      travelInformations &&
      travelInformations?.status === "accepted" &&
      travelInformations?.propose_rider_id === auth.info.uid
    ) {
      dispatch(fetchCreditCard());
      setSendData({ ...travelInformations });
      setTravelTab({ ...travelInformations });
      setTravelInit(false);
      attachOldTravelLocations();
      !travelAccepted && setTravelAccepted(true);
    }
    checkTravelExist = false;
  }, [travelInformations]);

  useEffect(() => {
    if (travelList?.length > 0 && !travelTab) {
      getTravelData();
    }
  }, [travelList, travelTab]);

  useEffect(() => {
    const listener = EventRegister.addEventListener("initrider", (data) => {
      if (data) {
        travelInformations?.driver_id &&
          dispatch(
            updateTravel(
              {
                isAffiner_rider: false,
                isFound: false,
                status: "init",
                driver_id: travelInformations?.driver_id,
              },
              "initrider"
            )
          );
        setDisplayDriverCancelTravel(false);
        setDisplayPaieMod(false);
        setTravelCompleted(false);
        setDisplayResearchDriver(false);
        setDisplayTravelConfirm(false);
        setDisplayWaitRiderNegoPrice(false);
        setRiderPriceDone(false);
        setTravelAccepted(false);
        setDisplayTimeBtwRD(false);
        setDisplayDriverPURider(false);
        setTravelEnCours(false);
        setDisplayRatingAfterTravel(false);
        setMaDistance(null);
        setPointValeur(null);
        setmonTemps(null);

        setSearchKeyWordDepart("");
        setSearchKeyWordDestination("");
        setDisplayModal(false);
        setTravelInit(true);
        setTravelTab(null);
        setSendData({
          rider_id: null,
          rider_notification: null,
          lat_dep: null,
          long_dep: null,
          addr_dep: null,
          lat_dest: null,
          long_dest: null,
          addr_dest: null,
          direction: null,
          distance: null,
          durer: null,
          prix: 0,
          prix_total: 0,
          prix_affiner: 0,
          passager_nb: null,
          create_date:
            new Date().getDate() +
            "/" +
            new Date().getMonth() +
            "/" +
            new Date().getFullYear(),
          create_time: new Date().getTime(),
          profile_img_rider: null,
          mode_paiement: "bonus",
        });
        setDepLocation({
          latitude: 37.78825,
          longitude: -122.4324,
          address_name: null,
          default: true,
        });
        setDestLocation({
          latitude: 37.78825,
          longitude: -122.4324,
          address_name: null,
          default: true,
        });
      }
    });
    return () => {
      EventRegister.removeEventListener(listener);
    };
  }, [travelInformations?.driver_id]);

  useEffect(() => {
    onCheckAlreadyHaveTravel();
  }, []);

  useEffect(() => {
    if (driverPosition?.lat && driverPosition?.lng) {
      setCarPosition({
        longitude: driverPosition.lng,
        latitude: driverPosition.lat,
      });
      refMap?.current?.animateToRegion({
        ...initialRegion,
        latitude: driverPosition.lat,
        longitude: driverPosition.lng,
      });
    }
  }, [driverPosition?.lat && driverPosition?.lng]);

  const calculPosition = async (id, name, trajet) => {
    let lnlg = await fetchCoordsfromPlace(id);
    //console.log("Value of LnLG "+lnlg);
    setInitialRegion((prevState) => {
      return {
        ...prevState,
        latitude: lnlg.lat,
        longitude: lnlg.lng,
        latitudeDelta: latitudeDelta,
        longitudeDelta: longitudeDelta,
      };
    });

    if (trajet === "destination") {
      setDestLocation({
        latitude: lnlg.lat,
        longitude: lnlg.lng,
        address_name: name,
        default: false,
      });
      refMap?.current?.animateToRegion({
        ...initialRegion,
        latitude: lnlg.lat,
        longitude: lnlg.lng,
      });
    } else {
      setDepLocation({
        latitude: lnlg.lat,
        longitude: lnlg.lng,
        address_name: name,
        default: false,
      });

      refMap?.current?.animateToRegion({
        ...initialRegion,
        latitude: lnlg.lat,
        longitude: lnlg.lng,
      });
    }
  };

  onPressBackDataRider = async (donner) => {
    if (donner.prixAffiner != 0) {
      setSendData({
        ...sendData,
        passager_nb: donner.nbrepassager,
        prix_affiner: donner.prixAffiner,
      });
      //	console.log("valeur de donner:", donner);
    }
    setLoadCal(true);
  };

  useEffect(() => {
    if (loadCal === true) {
      dispatch(fetchTravelData(riderRequest))
      //console.log("Je suis dans le useEffect travel");
    }
  }, [riderRequest]);

  const getDistanceTime = async (travelDriver) => {
    //console.log("call getDistanceTime homeRider");

    return await getDriverTime(travelDriver.addr_dep, sendData.addr_dep);
  };

  return (
    <View style={{ flex: 1, alignItems: "center" }}>
      <Modal visible={displayModal} animationType="slide" transparent={true}>
        {sendData.addr_dep !== null ? (
          <ModalFollowLastResearch
            data={sendData}
            setDisplayModal={setDisplayModal}
            onFollowTravelsModal={() => {
              setDisplayModal(false);
              setDisplayPaieMod(true);
            }}
          />
        ) : null}
      </Modal>

      <Modal visible={displayPaieMod} animationType="slide" transparent={true}>
        {sendData.addr_dep !== null ? (
          <PaiementMod
            onPressRecherche={(donner, montantpromo, code) =>
              onPressBackCalcul(donner, montantpromo, code)
            }
            onPressNext={(donner) => onPressBackModePaiement(donner)}
            data={sendData}
            setDisplayPaieMod={setDisplayPaieMod}
            onSave={() => {
              setTravelInit(false);
              setDisplayPaieMod(false);
              setDisplayResearchDriver(true);
            }}
          />
        ) : null}
      </Modal>

      <Modal
        visible={displayResearchDriver}
        animationType="slide"
        transparent={true}
      >
        {sendData.addr_dep !== null ? (
          <SearchDriver
            setDisplayResearchDriver={setDisplayResearchDriver}
            setTravelCompleted={setTravelCompleted}
            setTravelInit={setTravelInit}
            setAnnuler={setAnnuler}
          />
        ) : null}
      </Modal>

      {travelTab !== null ? (
        <Modal
          visible={travelCompleted}
          animationType="slide"
          transparent={true}
        >
          <ValidateRoute
            datarider={sendData}
            datadriver={travelTab}
            laDistance={maDistance}
            leTemps={monTemps}
            riderPriceDone={riderPriceDone}
            setTravelAccepted={setTravelAccepted}
            setDisplayTravelConfirm={setDisplayTravelConfirm}
            setTravelCompleted={setTravelCompleted}
            setTravelInit={setTravelInit}
          />
        </Modal>
      ) : (
        <Modal
          visible={travelCompleted}
          animationType="slide"
          transparent={true}
        >
          {
            <NoDriverFoundByRiderModal
              onRetry={() => {
                //onPressRecherche(modePaiement);
                //onPressBackCalcul();
                setTravelCompleted(false);
                setDisplayResearchDriver(true);
              }}
            />
          }
        </Modal>
      )}

      <Modal
        visible={displayTravelConfirm}
        animationType="slide"
        transparent={true}
      >
        {sendData.addr_dep !== null ? (
          <NegoPrixDriver
            datarider={sendData}
            modepaiement={paymentData}
            datadriver={travelTab}
            laDistance={maDistance}
            leTemps={monTemps}
            setDisplayTravelConfirm={setDisplayTravelConfirm}
            setDisplayWaitRiderNegoPrice={setDisplayWaitRiderNegoPrice}
          />
        ) : null}
      </Modal>

      <Modal
        visible={displayWaitRiderNegoPrice}
        animationType="slide"
        transparent={true}
      >
        {sendData.addr_dep !== null ? (
          <DriverWaitRiderNegoPrice
            datarider={sendData}
            datadriver={travelTab}
            laDistance={maDistance}
            leTemps={monTemps}
            setDisplayWaitRiderNegoPrice={setDisplayWaitRiderNegoPrice}
            setDisplayDriverCancelTravel={setDisplayDriverCancelTravel}
            setTravelCompleted={setTravelCompleted}
            setRiderPriceDone={setRiderPriceDone}
            setTravelInit={setTravelInit}
          />
        ) : null}
      </Modal>

      <Modal
        visible={displayDriverCancelTravel}
        animationType="slide"
        transparent={true}
      >
        {sendData.addr_dep !== null ? (
          <DriverCancelTravel
            datarider={sendData}
            datadriver={travelTab}
            setDisplayDriverCancelTravel={setDisplayDriverCancelTravel}
          />
        ) : null}
      </Modal>

      <Modal visible={travelAccepted} animationType="slide" transparent={true}>
        {sendData.addr_dep !== null ? (
          <TravelConfirm
            datadriver={travelTab}
            setDisplayDriverCancelTravel={setDisplayDriverCancelTravel}
            setTravelAccepted={setTravelAccepted}
            onCloseModal={onReadyToTravel}
          />
        ) : null}
      </Modal>

      <Modal
        visible={displayTimeBtwRD}
        animationType="slide"
        transparent={true}
      >
        {sendData.addr_dep !== null ? (
          <ModalTimeBtwDriverRider
            datarider={sendData}
            datadriver={travelTab}
            laDistance={maDistance}
            leTemps={monTemps}
            setDisplayDriverCancelTravel={setDisplayDriverCancelTravel}
            setDisplayTimeBtwRD={setDisplayTimeBtwRD}
            setDisplayDriverPURider={setDisplayDriverPURider}
          />
        ) : null}
      </Modal>

      <Modal
        visible={displayDriverPURider}
        animationType="slide"
        transparent={true}
      >
        {sendData.addr_dep !== null ? (
          <DriverPickUpRider
            datarider={sendData}
            datadriver={travelTab}
            laDistance={maDistance}
            leTemps={monTemps}
            setDisplayDriverCancelTravel={setDisplayDriverCancelTravel}
            setDisplayDriverPURider={setDisplayDriverPURider}
            validateModal={onEnRoute}
          />
        ) : null}
      </Modal>

      <Modal
        visible={displayTravelEnCours}
        animationType="slide"
        transparent={true}
      >
        {sendData.addr_dep !== null ? (
          <TravelEnCours
            datarider={sendData}
            datadriver={travelTab}
            setDisplayTravelEnCours={setTravelEnCours}
            setDisplayDriverCancelTravel={setDisplayDriverCancelTravel}
            setDisplayRatingAfterTravel={setDisplayRatingAfterTravel}
            navigation={props.navigation}
          />
        ) : null}
      </Modal>

      <Modal
        visible={displayRatingAfterTravel}
        animationType="slide"
        transparent={true}
      >
        {sendData.addr_dep !== null ? (
          <RatingAfterTravel
            datarider={sendData}
            datadriver={travelTab}
            validateModal={() => {
              setDisplayRatingAfterTravel(false);
              setTravelInit(true);
            }}
            setDisplayRatingAfterTravel={setDisplayRatingAfterTravel}
            setTravelInit={setTravelInit}
          />
        ) : null}
      </Modal>

      <MapView
        ref={refMap}
        tracksViewChanges={false}
        style={[styles.map, { marginBottom: margin }]}
        customMapStyle={customMapStyle}
        showsUserLocation={true}
        showsMyLocationButton={true}
        liteMode={true}
        loadingEnabled={true}
        loadingIndicatorColor={colors.VIOLET}
        initialRegion={initialRegion}
        provider={PROVIDER_GOOGLE}
        onMapReady={() => setMargin(1)}
      >
        {destLocation.latitude && destLocation.longitude && (
          <MapViewDirections
            lineDashPattern={[0]}
            apikey={Google_Map_Key}
            origin={depLocation}
            destination={destLocation}
            timePrecision="now"
            mode="DRIVING"
            language="fr"
            strokeWidth={4}
            strokeColor={colors.VIOLET}
            onStart={(params) => {
              //	console.log("Started routing.... /n/n" + params);
            }}
            onError={(error) => {
              console.log(error);
            }}
            onReady={(result) => {
              setDistance({
                text: `${result.distance} km`,
                value: result.distance,
              });
              //console.log(`Duration: ${result.duration} min.`);
            }}
          />
        )}
        <Marker coordinate={depLocation}>
          <View>
            <SvgXml
              xml={iconLocalisationStart}
              style={{ height: 35, width: 35 }}
            />
          </View>
        </Marker>

        {/* TODO: add polyline */}
        {/* <Polyline
          lineDashPattern={[1]}
          coordinates={carPosition.routeCoordinates}
          strokeWidth={2}
        /> */}

        {!!carPosition.latitude && !!carPosition.longitude && (
          <Marker.Animated coordinate={carPosition}>
            <Image source={car} />
          </Marker.Animated>
        )}

        {destLocation.latitude && destLocation.longitude && (
          <Marker coordinate={destLocation}>
            <View>
              <SvgXml
                xml={iconLocalisationEnd}
                style={{ height: 35, width: 35 }}
              />
            </View>
          </Marker>
        )}
      </MapView>

      <View style={styles.searchContainer}>
        
        <View style={{display:'flex', flexDirection:'column', alignItems:"center"}}>
          <Image source={src_dest} />
          <Text style={{
                      fontFamily: "Montserrat-Medium",
                      fontWeight: "bold",
                      fontSize: 18,
                      marginTop: 10,
                      textAlign:"center"
                    }}> Indiquez votre trajet</Text>
        </View>
        
        

        <View
          style={{
            display:'flex', flexDirection:'row',
            marginTop:15
          }}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >

            <SvgXml xml={travel} style={{ zIndex: 2, height: 100, width: 25 }} />
          </View>
          <SafeAreaView style={styles.searchBodyButtonsWrapper}>
            <View style={[styles.searchBodyContentButtonView]}>
              <Text style={styles.searchBodyContentTitleButton}> DEPART </Text>
              <TextInput
                placeholder="Où etes-vous ?"
                returnKeyType="search"
                style={{
                  color: "#848484",
                }}
                placeholderTextColor="#848484"
                onChangeText={(text) => searchLocation(text, "depart")}
                value={searchKeyWordDepart}
                onFocus={() => setIsFocusDepart(true)}
                onBlur={() => setIsFocusDepart(false)}
              />
            </View>

            <View
              style={{
                zIndex: 2,
                height: 1,
                width: 228,
                marginVertical:12,
                backgroundColor: "#ECECEC",
              }}
            />

            <View style={[styles.searchBodyContentButtonView]}>
              <Text style={styles.searchBodyContentTitleButton}> ARRIVEE </Text>
              <TextInput
                placeholder="Où allez-vous ?"
                returnKeyType="search"
                style={{
                  color: "#848484",
                }}
                placeholderTextColor="#848484"
                onChangeText={(text) => searchLocation(text, "destination")}
                value={searchKeyWordDestination}
                onFocus={() => setIsFocusDestination(true)}
                onBlur={() => setIsFocusDestination(false)}
              />
            </View>
          </SafeAreaView>
        </View>
      </View>
      <View
        style={{
          alignSelf: "center",
          position: "absolute",
          bottom: '20%',
        }}
      >
        {isShowingResultsDepart && (
          <FlatList
            data={searchResultsDepart}
            renderItem={({ item, index }) => {
              return (
                <TouchableOpacity
                  key={item.place_id}
                  style={[
                    {
                      width: "100%",
                      justifyContent: "center",
                      borderBottomColor: "#ECECEC",
                      paddingLeft: 15,
                    },
                    index === searchResultsDepart.length - 1
                      ? { borderBottomWidth: 0 }
                      : { borderBottomWidth: 1 },
                  ]}
                  onPress={() => {
                    console.log("depart");
                    setSearchKeyWordDepart(item.description);
                    setIsShowingResultsDepart(false);
                    setDisplayReservation(true);
                    calculPosition(item.place_id, item.description, "depart");
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Montserrat-Medium",
                      color: "#848484",
                      fontWeight: "bold",
                      fontSize: 14,
                      paddingBottom: 20,
                      paddingTop: 10,
                    }}
                  >
                    {item.description}
                  </Text>
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item, index) => {
              return item.place_id;
            }}
            style={styles.searchResultsContainer}
          />
        )}
        {isShowingResultsDestination && (
          <FlatList
            data={searchResultsDestination}
            renderItem={({ item, index }) => {
              return (
                <TouchableOpacity
                  key={item.place_id}
                  style={[
                    {
                      width: "100%",
                      justifyContent: "center",
                      borderBottomColor: "#ECECEC",
                      paddingLeft: 15,
                    },
                    index === searchResultsDestination.length - 1
                      ? { borderBottomWidth: 0 }
                      : { borderBottomWidth: 1 },
                  ]}
                  onPress={() => {
                    setSearchKeyWordDestination(item.description);
                    setIsShowingResultsDestination(false);
                    setDisplayReservation(true);
                    calculPosition(
                      item.place_id,
                      item.description,
                      "destination"
                    );
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Montserrat-Medium",
                      color: "#848484",
                      fontWeight: "bold",
                      fontSize: 14,
                      paddingBottom: 20,
                      paddingTop: 10,
                    }}
                  >
                    {item.description}
                  </Text>
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item, index) => {
              return item.place_id;
            }}
            style={styles.searchResultsContainer}
          />
        )}
      </View>

      {travelInit && (
        <View
          style={{
            alignSelf: "center",
            position: "absolute",
            bottom: 0,
          }}
        >
          {displayReservation === true ? (
            <RiderTravelGo
              onPressDataRider={(donner) => onPressBackDataRider(donner)}
              data={sendData}
              openModal={() => setDisplayModal(true)}
              nearby={nearby}
              setRiderAndDriverDist={setRiderAndDriverDist}
            />
          ) : null}
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  map: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  searchContainer: {
    backgroundColor: "white",
    flexDirection: "column",
    height: 280,
    width: "80%",
    position: "absolute",
    top: 20,
    alignSelf: "center",
    // justifyContent: "center",
    paddingVertical:10,
    alignItems: "center",
    borderRadius: 13,
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 2,
    shadowRadius: 6,
    elevation: 3,
  },
  searchBodyIconWrapper: {
    backgroundColor: "red",
  },
  searchBodyButtonsWrapper: {
    height: "100%",
    width: "80%",
  },
  searchBodyButton: {
    height: 40,
    width: "100%",
    flexDirection: "column",
  },
  searchBodyContentButtonView: {
    // height: 40,
    width: "100%",
    flexDirection: "column",
  },
  searchBodyContentTitleButton: {
    // paddingBottom: 8,
    fontSize: 12,
    // lineHeight: 15,
    fontWeight:'bold',
    color: colors.BLACK,
    fontFamily: "Montserrat-Bold",
  },
  searchBodyContentLocationButton: {
    fontSize: 14,
    lineHeight: 18,
    fontFamily: "Montserrat-Medium",
    color: "rgba(151,151,151,1)",
  },
  resultItem: {
    width: "100%",
    justifyContent: "center",
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    paddingLeft: 15,
    height: 40,
    color: "rgba(151,151,151,1)",
    fontFamily: "Montserrat-Medium",
    fontSize: 14,
  },
  searchResultsContainer: {
    width: 340,
    height: 200,
    backgroundColor: "#FFFFFF",
    color: "rgba(151,151,151,1)",

    borderBottomRightRadius: 13,
    borderBottomLeftRadius: 13,
    top: 124,
    borderRadius: 13,
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 2,
    shadowRadius: 6,
    elevation: 3,
    zIndex: 20,
  },
});
