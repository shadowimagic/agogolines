import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const BookCar = (props) => {
	return (
		<View style={styles.screen}>
			<Text>BookCar</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
});

export default BookCar;
