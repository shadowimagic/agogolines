import { StyleSheet, Dimensions } from 'react-native';
const { height, width } = Dimensions.get('window');
import { colors } from '../../../common/theme';

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		backgroundColor: colors.VIOLET,
	},
	header: {
		height: '25%',
		alignItems: 'center',
		justifyContent: 'space-between',
		flexDirection: 'row',
		flexBasis: "30%",
		paddingHorizontal: "8%",
	},
	headerText: {
		color: colors.WHITE,
		fontFamily: "Montserrat-Bold",
		fontSize: 15,
		textTransform: "uppercase",
		lineHeight: 18,
	},
	headerIcon: {

	},
	body: {
		position: 'absolute',
		top: '25%',
		height: '80%',
		width: '100%',
		backgroundColor: 'white',
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
	},
	bodyHeaderContainer: {
		marginHorizontal: 16,
		marginVertical: 10,
		padding: 16,
	},
	bodyHeaderText: {
		color: colors.BLACK,
		textAlign: 'center',
		fontSize: 18,
		fontWeight: 'bold',
	},
	nationalIdContainer: {
		marginHorizontal: 16,
		borderBottomWidth: 1,
        borderBottomColor: colors.GREY.secondary,
	},
	textLabelText: {
		color: colors.GREY.secondary,
		fontSize: 14,
		fontWeight: 'bold',
		textTransform: 'uppercase',
	},
	textLabelContainer: {
		marginVertical: 16,
	},
	btnStyle: {
		borderRadius: 13,
		backgroundColor: colors.GREY.primary,
		height: height / 6,
	},
	image: {
		borderRadius: 13,
		height: height / 6,
	}
	,
	inputElementContainer: {
		marginBottom: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
	},
	btnWrapper: {
		width: '45%',
	},
	icon: {
		color: colors.GREY.iconPrimary,
	},
	textInputWrapper: {
		alignItems: 'center',
		marginVertical: 10,
	},
	textInputText: {
		color: colors.GREY.default,
		fontWeight: 'bold',
	},
  saveBtnWrapper: {
			marginBottom: 40,
			marginTop: 10,
			width: '100%',
			alignItems: 'center',
	},
	btnContainer: {
		width: '85%',
	},
	saveBtnStyle: {
		textAlign: 'center',
		borderRadius: 13,
		backgroundColor: colors.VIOLET,
		paddingVertical: 10,
		paddingHorizontal: 20,
	},
	saveBtnTitleStyle: {
		textTransform: 'uppercase',
	},
});

export default styles;
