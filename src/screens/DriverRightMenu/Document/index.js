import React, { useState, useContext, useEffect } from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert,
} from "react-native";
import Modal from "react-native-modal";
import { Button } from "react-native-elements";
import styles from "./styles";
import { Ionicons } from "@expo/vector-icons";
import { FirebaseContext } from "../../../../redux/src";
import PictureModal from "../../../components/PictureModal";
import { useDispatch, useSelector } from "react-redux";
import { getStripefile } from "../../../../redux/src/other/StripeFunctions";
import "react-native-get-random-values";
import { v4 as uuidv4 } from "uuid";
import app from "firebase/app";

const Document = (props) => {
  const { api } = useContext(FirebaseContext);
  const { fetchDriverInfos } = api;
  const driverdata = useSelector((state) => state.driverdata);
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const [modalVisible, setModalVisible] = useState(false);
  const [imageNationalIdRecto, setImageNationalIdRecto] = useState(null);
  const [imageNationalIdVerso, setImageNationalIdVerso] = useState(null);
  const [imageDrivingLicense, setImageDrivingLicense] = useState(null);
  const [imageCarLicense, setImageCarLicense] = useState(null);
  const [imageType, setImageType] = useState("");
  const [idCniVerso, setIdCniVerso] = useState(null);
  const [idCniRecto, setIdCniRecto] = useState(null);

  useEffect(() => {
    if (driverdata.driverdata) {
      setImageCarLicense(driverdata.driverdata.carteGrisse);
      setImageNationalIdRecto(driverdata.driverdata.cniRecto);
      setImageNationalIdVerso(driverdata.driverdata.cniVerso);
      setImageDrivingLicense(driverdata.driverdata.permis);
    }
  }, [driverdata]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      dispatch(fetchDriverInfos());
    }
  }, [auth.info]);

  const onSave = async () => {
    if (
      !imageNationalIdRecto ||
      !imageDrivingLicense ||
      !imageCarLicense ||
      !imageNationalIdVerso
    ) {
      Alert.alert(
        "Alerte!",
        `Merci de d'importer les documents suivants (obligatoire)`
      );
    } else {
      await app
        .database()
        .ref("mesdocuments/" + auth.info.uid)
        .set({
          cniRecto: imageNationalIdRecto,
          cniVerso: imageNationalIdVerso,
          carteGrisse: imageCarLicense,
          permis: imageDrivingLicense,
          idcnirecto: idCniRecto,
          idcniverso: idCniVerso,
        });
      Alert.alert("", "Documents enregistrées avec success!", [
        {
          text: "Ok",
          onPress: () => {
            setImageCarLicense(null);
            setImageDrivingLicense(null);
            setImageNationalIdRecto(null);
            setImageNationalIdVerso(null);
            props.navigation.goBack();
          },
        },
      ]);
    }
  };

  return (
    <View style={styles.screen}>
      <Modal isVisible={modalVisible} animationType="slide" transparent={true}>
        <PictureModal
          onClose={() => {
            setModalVisible(false);
          }}
          image={async (data) => {
            if (imageType === "nationalIdRecto") {
              await app
                .storage()
                .ref("mesdocuments/" + auth.info.uid)
                .child("cniRecto.png")
                .put(data);
              const imageUrl = await app
                .storage()
                .ref("mesdocuments/" + auth.info.uid)
                .child("cniRecto.png")
                .getDownloadURL();

              setImageNationalIdRecto(imageUrl);

              const url =
                "https://us-central1-agogolines-8bc39.cloudfunctions.net/urlstripefile/stripefile";

              await fetch(`${url}`, {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  filename: "cniRecto.png",
                  userid: auth.info.uid,
                }),
              })
                .then((response) => response.json())
                .then((responseData) => {
                  /* console.log(
									 
									 "Response Body recto-> " + JSON.stringify(responseData)
								 )*/
                  setIdCniRecto(responseData.id);
                });
            } else if (imageType === "nationalIdVerso") {
              await app
                .storage()
                .ref("mesdocuments/" + auth.info.uid)
                .child("cniVerso.png")
                .put(data);
              const imageUrl = await app
                .storage()
                .ref("mesdocuments/" + auth.info.uid)
                .child("cniVerso.png")
                .getDownloadURL();

              setImageNationalIdVerso(imageUrl);

              const url =
                "https://us-central1-agogolines-8bc39.cloudfunctions.net/urlstripefile/stripefile";

              await fetch(`${url}`, {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  filename: "cniVerso.png",
                  userid: auth.info.uid,
                }),
              })
                .then((response) => response.json())
                .then((responseData) => {
                  console.log(
                    "Response Body verso-> " + JSON.stringify(responseData)
                  );
                  setIdCniVerso(responseData.id);
                });
            } else if (imageType === "drivingLicense") {
              await app
                .storage()
                .ref("mesdocuments/" + auth.info.uid)
                .child("permis.png")
                .put(data);
              const imageUrl = await app
                .storage()
                .ref("mesdocuments/" + auth.info.uid)
                .child("permis.png")
                .getDownloadURL();

              setImageDrivingLicense(imageUrl);
            } else if (imageType === "carLicense") {
              await app
                .storage()
                .ref("mesdocuments/" + auth.info.uid)
                .child("carteGrisse.png")
                .put(data);
              const imageUrl = await app
                .storage()
                .ref("mesdocuments/" + auth.info.uid)
                .child("carteGrisse.png")
                .getDownloadURL();

              setImageCarLicense(imageUrl);
            }
          }}
        />
      </Modal>
      <View style={styles.header}>
        <Ionicons
          name="chevron-back"
          size={30}
          color="white"
          onPress={() => {
            props.navigation.goBack();
          }}
        />
        <Text style={styles.headerText}>Mes documents</Text>
        <View></View>
      </View>
      <View style={styles.body}>
        <View style={styles.bodyHeaderContainer}>
          <Text style={styles.bodyHeaderText}>
            Merci d'importer les documents suivants (obligatoire)
          </Text>
        </View>
        <ScrollView>
          <View style={styles.nationalIdContainer}>
            <View style={styles.textLabelContainer}>
              <Text style={styles.textLabelText}>
                piece nationale d'identite
              </Text>
            </View>
            <View style={styles.inputElementContainer}>
              <View style={styles.btnWrapper}>
                {!imageNationalIdRecto ? (
                  <Button
                    buttonStyle={styles.btnStyle}
                    icon={
                      <Ionicons
                        name="add-outline"
                        style={styles.icon}
                        size={80}
                      />
                    }
                    onPress={() => {
                      setImageType("nationalIdRecto");
                      setModalVisible(true);
                    }}
                  />
                ) : (
                  <TouchableOpacity
                    onPress={() => {
                      setImageType("nationalIdRecto");
                      setModalVisible(true);
                    }}
                  >
                    <Image
                      style={styles.image}
                      source={{
                        uri: imageNationalIdRecto,
                      }}
                    />
                  </TouchableOpacity>
                )}
                <View style={styles.textInputWrapper}>
                  <Text style={styles.textInputText}>Recto</Text>
                </View>
              </View>
              <View style={styles.btnWrapper}>
                {!imageNationalIdVerso ? (
                  <Button
                    buttonStyle={styles.btnStyle}
                    icon={
                      <Ionicons
                        name="add-outline"
                        style={styles.icon}
                        size={80}
                      />
                    }
                    onPress={() => {
                      setImageType("nationalIdVerso");
                      setModalVisible(true);
                    }}
                  />
                ) : (
                  <TouchableOpacity
                    onPress={() => {
                      setImageType("nationalIdVerso");
                      setModalVisible(true);
                    }}
                  >
                    <Image
                      style={styles.image}
                      source={{
                        uri: imageNationalIdVerso,
                      }}
                    />
                  </TouchableOpacity>
                )}
                <View style={styles.textInputWrapper}>
                  <Text style={styles.textInputText}>Verso</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.nationalIdContainer}>
            <View style={styles.textLabelContainer}>
              <Text style={styles.textLabelText}>permis de conduire</Text>
            </View>
            <View style={styles.inputElementContainer}>
              <View style={styles.btnWrapper}>
                {!imageDrivingLicense ? (
                  <Button
                    buttonStyle={styles.btnStyle}
                    icon={
                      <Ionicons
                        name="add-outline"
                        style={styles.icon}
                        size={80}
                      />
                    }
                    onPress={() => {
                      setImageType("drivingLicense");
                      setModalVisible(true);
                    }}
                  />
                ) : (
                  <TouchableOpacity
                    onPress={() => {
                      setImageType("drivingLicense");
                      setModalVisible(true);
                    }}
                  >
                    <Image
                      style={styles.image}
                      source={{
                        uri: imageDrivingLicense,
                      }}
                    />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
          <View style={styles.nationalIdContainer}>
            <View style={styles.textLabelContainer}>
              <Text style={styles.textLabelText}>carte grise du vehicule</Text>
            </View>
            <View style={styles.inputElementContainer}>
              <View style={styles.btnWrapper}>
                {!imageCarLicense ? (
                  <Button
                    buttonStyle={styles.btnStyle}
                    icon={
                      <Ionicons
                        name="add-outline"
                        style={styles.icon}
                        size={80}
                      />
                    }
                    onPress={() => {
                      setImageType("carLicense");
                      setModalVisible(true);
                    }}
                  />
                ) : (
                  <TouchableOpacity
                    onPress={() => {
                      setImageType("carLicense");
                      setModalVisible(true);
                    }}
                  >
                    <Image
                      style={styles.image}
                      source={{
                        uri: imageCarLicense,
                      }}
                    />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.saveBtnWrapper}>
          <View style={styles.btnContainer}>
            <Button
              title={"enregistrer"}
              buttonStyle={styles.saveBtnStyle}
              titleStyle={styles.saveBtnTitleStyle}
              onPress={onSave}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default Document;
