import { StyleSheet } from 'react-native';
import { colors } from '../../../common/theme';

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		backgroundColor: colors.VIOLET,
	},
	header: {
		height: '20%',
		alignItems: 'center',
		justifyContent: 'space-between',
		flexDirection: 'row',
		paddingHorizontal: "8%",
	},
	headerText: {
		color: colors.WHITE,
		fontFamily: "Montserrat-Bold",
		fontSize: 15,
		textTransform: "uppercase",
	},
	body: {
		height: '80%',
		width: '100%',
		backgroundColor: 'white',
		borderTopLeftRadius: 25,
		borderTopRightRadius: 25,
	},
	bodyHeaderContainer: {
		marginHorizontal: 16,
		marginVertical: 10,
	},
	bodyHeaderText: {
		color: colors.GREY.secondary,
		fontFamily: 'Montserrat-Bold',
	},
	elContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginHorizontal: 8,
	},
	elValue: {
		fontFamily: 'Montserrat-Bold',
	},
	elKey: {
		color: colors.GREY.secondary,
		fontFamily: 'Montserrat-Bold',
		marginVertical: 2,
	},
	item: {
		marginVertical: 10,
		marginHorizontal: 5,
		flexDirection: 'row',
		justifyContent: 'space-evenly',
	},
	itemNoCar: {
		marginVertical: 10,
		marginHorizontal: 5,
		alignItems: 'center',
		justifyContent: 'center',
		height: '20%'
	},
	noCar: {
		fontFamily: 'Montserrat-Bold',
		fontSize: 18,
		textTransform: 'uppercase'
	},
	itemCard: {
		borderRadius: 8,
		backgroundColor: 'transparent',
		shadowColor: 'black',
		shadowOffset: {
			height: 0,
			width: 2,
		},
		shadowRadius: 6,
		shadowOpacity: 0.26,
		elevation: 3,
		padding: 10,
		width: '80%',
	},
	itemElHeaderContainer: {
		marginHorizontal: 8,
		marginBottom: 10,
		marginTop: 6,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	itemElHeaderContainerRow: {
		flexDirection: 'row',
	},
	itemElHeaderText: {
		fontFamily: 'Montserrat-Bold',
		textTransform: 'uppercase',
	},
	carSvg: {
		marginRight: 6,
	},
	editSvg: {
		marginRight: 16,
	},
	btnWrapper: {
		marginVertical: 20,
		width: '100%',
		alignItems: 'center',
	},
	btnContainer: {
		width: '85%',
	},
	btnElStyle: {
		textAlign: 'center',
		borderRadius: 13,
		backgroundColor: colors.VIOLET,
		paddingVertical: 10,
		paddingHorizontal: 20,
	},
	btnTitleStyle: {
		textTransform: 'uppercase',
	},
	icon: {
		marginRight: 15,
	},
});

export default styles;
