import React, { useEffect, useContext, useState } from "react";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import { Button } from "react-native-elements";
import styles from "./styles";
import { SvgXml } from "react-native-svg";
import car from "../../../../assets/svg/car";
import tick from "../../../../assets/svg/tick";
import edit from "../../../../assets/svg/edit";
import garbage from "../../../../assets/svg/garbage";
import { Ionicons } from "@expo/vector-icons";
import { FirebaseContext } from "../../../../redux/src";
import { useDispatch, useSelector } from "react-redux";

const Car = (props) => {
  const { api } = useContext(FirebaseContext);
  const { fetchMesVehicules, addMesVehicules } = api || {};

  const dispatch = useDispatch();
  const mesvehicules = useSelector((state) => state.mesvehicules);
  const auth = useSelector((state) => state.auth);
  const vehicules = mesvehicules.mesvehicules;

  const [cars, setCars] = useState([]);

  const deleteCar = (key) => {
    const filterCar = cars.filter((el) => el.carKey !== key);
    dispatch(addMesVehicules(filterCar));
  };

  const CarList = (item) => {
    const {
      carMake,
      carColor,
      carYear,
      carNumber,
      carModel,
      carSitNum,
      carKey,
      carDefault,
    } = item.item;
    return (
      <View style={styles.item}>
        <View style={styles.itemCard}>
          <View style={styles.itemElHeaderContainer}>
            <View style={styles.itemElHeaderContainerRow}>
              <View style={styles.carSvg}>
                <SvgXml xml={car} />
              </View>
              <Text style={styles.itemElHeaderText}>
                {carMake} - {carModel}
              </Text>
            </View>
            <View style={styles.itemElHeaderContainerRow}>
              <TouchableOpacity
                onPress={() => {
                  props.navigation.navigate({
                    name: "EditCarScreen",
                    params: {
                      carMake,
                      carColor,
                      carYear,
                      carNumber,
                      carModel,
                      carSitNum,
                      carKey,
                    },
                  });
                }}
              >
                <View style={styles.editSvg}>
                  <SvgXml xml={edit} />
                </View>
              </TouchableOpacity>
              {/* <View>
								<SvgXml xml={tick} />
							</View> */}
            </View>
          </View>
          <View style={styles.elContainer}>
            <Text style={styles.elKey}>ANNÉE</Text>
            <Text style={styles.elValue}>{carYear}</Text>
          </View>
          <View style={styles.elContainer}>
            <Text style={styles.elKey}>IMMATRICULATION</Text>
            <Text style={styles.elValue}>{carNumber}</Text>
          </View>
          <View style={styles.elContainer}>
            <Text style={styles.elKey}>COULEUR</Text>
            <Text style={styles.elValue}>{carColor}</Text>
          </View>
          <View style={styles.elContainer}>
            <Text style={styles.elKey}>PLACES PASSAGERS</Text>
            <Text style={styles.elValue}>{carSitNum}</Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            deleteCar(carKey);
          }}
        >
          <SvgXml xml={garbage} />
        </TouchableOpacity>
      </View>
    );
  };

  useEffect(() => {
    if (vehicules && vehicules.length !== 0) {
      setCars([]);
      vehicules.map((el) => {
        setCars((prevState) => {
          return [el, ...prevState];
        });
      });
    }
    if (!vehicules) {
      setCars([]);
    }
  }, [vehicules]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      dispatch(fetchMesVehicules());
    }
  }, [auth.info]);

  return (
    <View style={styles.screen}>
      <View style={styles.header}>
        <Ionicons
          name="chevron-back"
          size={30}
          color="white"
          onPress={() => {
            props.navigation.goBack();
          }}
        />
        <Text style={styles.headerText}>Mes Véhicules</Text>
        <View></View>
      </View>
      <View style={styles.body}>
        <View style={styles.bodyHeaderContainer}>
          <Text style={styles.bodyHeaderText}>Par defaut:</Text>
        </View>
        {cars.length !== 0 ? (
          <FlatList
            data={cars}
            keyExtractor={(item) => item.carKey}
            renderItem={CarList}
          />
        ) : (
          <View style={styles.itemNoCar}>
            <Text style={styles.noCar}>Aucun vehicules enregistrer!</Text>
          </View>
        )}
        <View style={styles.btnWrapper}>
          <TouchableOpacity style={styles.btnContainer}>
            <Button
              icon={
                <Ionicons
                  name="add-outline"
                  size={24}
                  color="white"
                  style={styles.icon}
                />
              }
              title={"ajoute un vehicule"}
              buttonStyle={styles.btnElStyle}
              titleStyle={styles.btnTitleStyle}
              onPress={() => {
                props.navigation.navigate("CreateCarScreen");
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Car;
