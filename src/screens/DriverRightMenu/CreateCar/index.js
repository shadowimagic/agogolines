import React, {useContext, useState} from "react";
import {View, Text, ScrollView, Alert} from "react-native";
import {Button, Input} from "react-native-elements";
import styles from "./styles";
import {Ionicons} from "@expo/vector-icons";
import InputSpinner from "react-native-input-spinner";
import {colors} from "../../../common/theme";
import {useDispatch, useSelector} from "react-redux";
import {FirebaseContext} from "../../../../redux/src";
import "react-native-get-random-values";
import {useFormik} from "formik";
import * as yup from "yup";
import {v4 as uuidv4} from "uuid";

const CreateCarScreen = (props) => {
	const [loading, setLoading] = useState(false);

	const auth = useSelector((state) => state.auth);
	const mesvehicules = useSelector((state) => state.mesvehicules);

	const dispatch = useDispatch();
	const {api} = useContext(FirebaseContext);
	const {addMesVehicules} = api;

	const schema = yup.object({
		mark: yup.string().required(),
		model: yup.string().required(),
		year: yup.number().required(),
		matriculationNum: yup.string().required(),
		color: yup.string().required(),
		sitNum: yup.number().max(7).min(1).required(),
	});

	const formik = useFormik({
		initialValues: {
			mark: "",
			model: "",
			year: "",
			matriculationNum: "",
			color: "",
			sitNum: 2,
		},
		validationSchema: schema,
		onSubmit: (values, actions) => {
			let carData;
			if (mesvehicules.mesvehicules && mesvehicules.mesvehicules.length !== 0) {
				carData = [
					...mesvehicules.mesvehicules,
					{
						carKey: uuidv4(),
						carModel: values.model,
						carYear: values.year,
						carColor: values.color,
						carNumber: values.matriculationNum,
						carSitNum: values.sitNum,
						carMake: values.mark,
						carDefault: false
					},
				];
			} else {
				carData = [
					{
						carKey: uuidv4(),
						carModel: values.model,
						carYear: values.year,
						carColor: values.color,
						carNumber: values.matriculationNum,
						carSitNum: values.sitNum,
						carMake: values.mark,
						carDefault: true
					},
				];
			}
			dispatch(addMesVehicules(carData));
			setLoading(false);
			Alert.alert("", "Votre véhicule a été enregistrer", [
				{
					text: "Ok",
					onPress: () => {
						actions.resetForm();
						props.navigation.goBack();
					},
				},
			]);
		},
		validateOnMount: true,
	});

	const onSave = async () => {
		if (formik.isValid) {
			setLoading(true);
			formik.handleSubmit();
		} else {
			setLoading(true);
			Alert.alert(
				"",
				"Merci de renseigner les informations obligatoires sur votre véhicule",
				[
					{
						text: "Ok",
						onPress: () => {
							setLoading(false);
						},
					},
				]
			);
		}
	};

	return (
		<View style={styles.screen}>
			<View style={styles.header}>
				<Ionicons
					name="chevron-back"
					size={24}
					color="white"
					onPress={() => {
						props.navigation.goBack();
					}}
				/>
				<Text style={styles.headerText}>Mes Véhicules</Text>
				<View></View>
			</View>
			<View style={styles.body}>
				<ScrollView contentContainerStyle={styles.inputWrapper}>
					<View style={styles.bodyHeaderContainer}>
						<Text style={styles.bodyHeaderText}>
							Merci de rensigner les informations sur votre vehicule (obligatoire)
						</Text>
					</View>
					<View style={styles.inputElement}>
						<Input
							editable
							label="marque véhicule"
							inputStyle={styles.inputStyle}
							value={formik.values.mark}
							onChangeText={formik.handleChange("mark")}
							inputContainerStyle={styles.inputContainerStyle}
							labelStyle={styles.labelInputStyle}
						/>
						<Input
							editable
							label="modèle"
							inputStyle={styles.inputStyle}
							value={formik.values.model}
							onChangeText={formik.handleChange("model")}
							inputContainerStyle={styles.inputContainerStyle}
							labelStyle={styles.labelInputStyle}
						/>
						<Input
							editable
							label="année"
							inputStyle={styles.inputStyle}
							value={formik.values.year}
							keyboardType="numeric"
							onChangeText={formik.handleChange("year")}
							inputContainerStyle={styles.inputContainerStyle}
							labelStyle={styles.labelInputStyle}
						/>
						<Input
							editable
							label="numéro d'immatriculation"
							inputStyle={styles.inputStyle}
							value={formik.values.matriculationNum}
							onChangeText={formik.handleChange("matriculationNum")}
							inputContainerStyle={styles.inputContainerStyle}
							labelStyle={styles.labelInputStyle}
						/>
						<Input
							editable
							label="couleur"
							inputStyle={styles.inputStyle}
							value={formik.values.color}
							onChangeText={formik.handleChange("color")}
							inputContainerStyle={styles.inputContainerStyle}
							labelStyle={styles.labelInputStyle}
						/>
						<View style={styles.counterContainer}>
							<Text style={styles.labelInputStyle}>nombre de places passagers</Text>
							<InputSpinner
								max={7}
								min={1}
								step={1}
								skin={"square"}
								showBorder={true}
								colorLeft={"transparent"}
								colorRight={"transparent"}
								editable={false}
								colorMax={"#f04048"}
								colorMin={"#40c5f4"}
								onChange={(num) => {
									formik.setFieldValue("sitNum", num);
								}}
								value={formik.values.sitNum}
								inputStyle={styles.inputCounterStyle}
								style={styles.counterStyle}
								buttonStyle={styles.counterBtn}
								buttonTextColor={colors.GREYD}
								buttonPressTextStyle={{color: colors.GREYD}}
								colorPress={colors.GREY.primary}
							/>
						</View>
					</View>
				</ScrollView>
				<View style={styles.btnWrapper}>
					<View style={styles.btnContainer}>
						<Button
							title={"enregistrer"}
							buttonStyle={styles.btnElStyle}
							titleStyle={styles.btnTitleStyle}
							onPress={onSave}
							loading={loading}
						/>
					</View>
				</View>
			</View>
		</View>
	);
};

export default CreateCarScreen;
