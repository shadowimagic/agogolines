import { StyleSheet } from 'react-native';
import { colors } from '../../../common/theme';

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		backgroundColor: colors.VIOLET,
	},
	header: {
		height: '20%',
		alignItems: 'center',
		justifyContent: 'space-between',
		flexDirection: 'row',
		paddingHorizontal: '10%',
	},
	headerText: {
		color: 'white',
		textTransform: 'uppercase',
		fontFamily: 'Montserrat-Bold',
	},
	body: {
		height: '80%',
		backgroundColor: 'white',
		borderTopLeftRadius: 25,
		borderTopRightRadius: 25,
	},
	bodyHeaderContainer: {
		marginHorizontal: 16,
		marginVertical: 10,
		padding: 16,
	},
	bodyHeaderText: {
		color: colors.BLACK,
		textAlign: 'center',
		fontSize: 16,
		fontFamily: 'Montserrat-Bold',
	},
	btnWrapper: {
		marginVertical: 20,
		width: '100%',
		alignItems: 'center',
	},
	btnContainer: {
		width: '85%',
	},
	btnElStyle: {
		textAlign: 'center',
		borderRadius: 13,
		backgroundColor: colors.VIOLET,
		paddingVertical: 10,
		paddingHorizontal: 20,
	},
	btnTitleStyle: {
		textTransform: 'uppercase',
	},
	inputWrapper: {
		alignItems: 'center',
		width: '100%',
	},
	inputElement: {
		width: '85%',
	},
	inputStyle: {
		backgroundColor: 'white',
		borderRadius: 13,
		shadowColor: 'black',
		shadowOffset: {
			height: 0,
			width: 2,
		},
		shadowRadius: 6,
		shadowOpacity: 0.26,
		elevation: 6,
		padding: 10,
	},
	counterContainer: {
		backgroundColor: 'white',
		padding: 10,
	},
	inputCounterStyle: {
		borderColor: colors.GREY.border,
	},
	counterStyle: {
		borderColor: colors.GREY.border,
		backgroundColor: 'white',
		marginVertical: 10,
		width: '40%',
		elevation: 0
	},
	counterBtn: {
		width: '25%',
	},
	inputContainerStyle: {
		borderBottomWidth: 0,
	},
	inputContainerCounterStyle: {
		borderBottomWidth: 0,
		width: '35%',
	},
	labelInputStyle: {
		color: colors.GREY.secondary,
		fontSize: 14,
		textTransform: 'uppercase',
		fontFamily: 'Montserrat-Bold',
		padding: 5,
	},
});

export default styles;