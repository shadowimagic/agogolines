
import React, { useState, useContext, useEffect} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons'
import {  Card, Divider } from 'react-native-elements';

//colors
import { colors } from '../../../common/theme'

//Constants
import Constants from 'expo-constants'

import travel from '../../../../assets/svg/travel'
import { SvgXml } from 'react-native-svg';

import { ListItem } from 'react-native-elements';

import { FirebaseContext } from '../../../../redux/src';
import { useSelector, useDispatch } from 'react-redux';
import { Dimensions } from "react-native";
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";

const Benefit = (props) => {


    const [dailygross,setDailygross] = useState(0);

    const [pregross,setPregross] = useState(0);
    const [weekgrossesp,setWeekgrossespeces] = useState(0);
    const [weekgrosscb,setWeekgrosscb] = useState(0);
    const [somgross,setSomPregross] = useState(0);

    const [monthlygross,setMonthlygross] = useState(0);
    const [totalgross,setTotalgross] = useState(0);

    const [cmtEspece,setCmtEspece] = useState([0]);
    const [cmtCarte,setCmtCarte] = useState([0]);


   
    const {api} = useContext(FirebaseContext);
    const { fetchBookingAll } = api;
    const bookinglistdata = useSelector((state) => state.bookingdata);
	const auth = useSelector((state) => state.auth);
	const dispatch = useDispatch();
    const screenWidth = Dimensions.get("window").width;

 

    useEffect(() => {
        if(auth.info && auth.info.profile){
            dispatch(fetchBookingAll("driver"));
        }
    }, [auth.info]);

    const returnDate = () => {
		let now = new Date();
		let day = ("0" + now.getDate()).slice(-2);
		let month = ("0" + (now.getMonth() + 1)).slice(-2);
		let today = (now.getFullYear())+ "-" + (month) + "-" + (day);
     //   console.log("valeur de la date du jour:",today);
		return today;
  
	   }



       const timeConverter = (UNIX_timestamp) => {
       
        let now = new Date(UNIX_timestamp);
		let day = ("0" + now.getDate()).slice(-2);
		let month = ("0" + (now.getMonth() + 1)).slice(-2);
		let today = (now.getFullYear())+ "-" + (month) + "-" + (day);
       // console.log("valeur de date de timestamp:",today);
        return today;
      }

    useEffect(()=>{
        if(bookinglistdata && bookinglistdata.booking){
            let som =0;
            let somday=0;
            let somMonth=0;
            let somweek =0;
            let i =0;
            let j=0;
            let somweekespeces =0;
            let somweekbancaire =0;
            let somprev =0;
            let now = new Date();
            let instancedate2 = new Date(returnDate())
            let startDay = 1; //0=sunday, 1=monday etc.
            let d = now.getDay(); //get the current day
            let weekStart = new Date(now.valueOf() - (d<=0 ? 7-startDay:d-startDay)*86400000); //rewind to start day
            let weekEnd = new Date(weekStart.valueOf() + 6*86400000); //add 6 days to get last day
            let lastDayOfMonth = new Date(now.getFullYear(), now.getMonth(), 0);
            setCmtEspece([0]);
            setCmtCarte([0]);
            const element =  bookinglistdata.booking;
           
            
              element.forEach(elt => {
                
                
              
                som =som + elt.propose_negoPrice;
                let datte =timeConverter(elt.create_time)
                let tDate = new Date(elt.create_time);
                let instancedate1 = new Date(datte);

                if (elt.mode_paiement ==='especes' && instancedate1.valueOf() >= weekStart.valueOf() && instancedate1.valueOf() <= weekEnd.valueOf()){
                    somweekespeces=somweekespeces+elt.propose_negoPrice;
                     i=i+1;
                   //  console.log("valeur de especes-----::",somweekespeces);

                     setCmtEspece(old => [...old, i]);
                   

                   
                

                }

                if (elt.mode_paiement ==='cartecredit' && instancedate1.valueOf() >= weekStart.valueOf() && instancedate1.valueOf() <= weekEnd.valueOf()){
                    somweekbancaire=somweekbancaire+elt.propose_negoPrice;
                     j=j+1;
                    // console.log("valeur de cartecredit-----::",somweekbancaire);

                    setCmtCarte(old => [...old, j]);
                }

                

                if(tDate.getMonth() === now.getMonth() && tDate.getFullYear() === now.getFullYear()){
                    
                    somMonth=somMonth+elt.propose_negoPrice;
                }

            
                if (instancedate1.valueOf() === instancedate2.valueOf()){

                   somday=somday+elt.propose_negoPrice;
                }

                
                if (instancedate1.valueOf() <= lastDayOfMonth.valueOf()){
                    somprev=somprev+elt.propose_negoPrice;
                }
                  
              });
            
           let somme = somweekespeces+somweekbancaire;
           setSomPregross(somme);
            setWeekgrossespeces(somweekespeces);
            setWeekgrosscb(somweekbancaire);
            setMonthlygross(somMonth);
            setDailygross(somday);
            setTotalgross(som);
            setPregross(somprev);
            
        }
    },[bookinglistdata]);

    return (
    <View style={{ flex: 1 }}>
        <View style={styles.headerDocsContainer}>
        <TouchableOpacity onPress={() => props.navigation.goBack() }>
            <Ionicons 
                name="ios-chevron-back"
                size={30}
                color={colors.WHITE}
            />
        </TouchableOpacity>
        <View>
            <Text style={styles.headerText}>MES GAINS</Text>
        </View>
        <View>
            <Text></Text>
        </View>
        </View>
        <ScrollView style={styles.scrollViewContainer}>


        <View style={{flexDirection:'row',marginVertical:1,alignItems: 'center', justifyContent:'space-between'}}>
					
                    <View style={{
                        backgroundColor:colors.WHITE,
                        width:47,
                        height:47,
                        borderRadius:11,
                        alignItems: 'center',
                        justifyContent:'center',
                        shadowColor: '#000',
                        shadowOffset: { width: 0, height: 3 },
                        shadowOpacity: 0.5,
                        shadowRadius: 5, 
                    }}>
                    <Ionicons 
                        name="ios-chevron-back"
                        size={30}
                        color={colors.GREYD}
                    />
                    </View>
                    <Text style={styles.notesmid}>Cette semaine</Text>
					<Text style={styles.notesnext}>
                    <View style={{
                        backgroundColor:colors.WHITE,
                        width:47,
                        height:47,
                        borderRadius:11,
                        alignItems: 'center',
                        justifyContent:'center',
                        shadowColor: '#000',
                        shadowOffset: { width: 0, height: 3 },
                        shadowOpacity: 0.5,
                        shadowRadius: 5, 
                    }}>
                    <Ionicons 
                        name="ios-chevron-forward"
                        size={30}
                        color={colors.GREYD}
                    />
                    </View>
                    </Text>
		</View> 

        <View style={styles.scrollViewChart}>
     
  
        <LineChart
					bezier
					withHorizontalLabels={false}
					withVerticalLabels={true}
					data={{
						labels: ["LUN", "MAR", "MER", "JEU", "VEN", "SAM", "DIM"],
						datasets: [
							{
								data: cmtEspece,
								strokeWidth: 2,
								color: (opacity = 1) => `#5BE4C7`, // optional
							},
							{
								data: cmtCarte,
								strokeWidth: 2,
								color: (opacity = 1) => `#5A48CB`, // optional
							}
						],
						legend: [weekgrossesp+'€', weekgrosscb+'€'],
					}}
                  
					width={Dimensions.get('window').width - 10}
					height={165}
                  
					chartConfig={{
						backgroundColor: "#ffffff",
                        backgroundGradientFrom: "#ffffff",
                        backgroundGradientTo: "#ffffff",
                        
                        useShadowColorFromDataset: true, // optional
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `#838383`,
                       
                         style: {
							borderRadius: 16,
                           
						},
                        propsForDots: {
                            r: "6",
                            strokeWidth: "2",
                            stroke: "#ffffff"
                          }
					}}
					style={{
						marginVertical: 3,
						
					}}
				/>
        </View>
      
        <Card containerStyle={styles.stylecard}>
               <View style={{flexDirection:'row', justifyContent:'space-between'}}>
					<Text style={styles.notes1}>Espèces</Text>
					<Text style={styles.notes2}>{weekgrossesp} €</Text>
				</View>
                
                <Divider style={{ backgroundColor: '#dfe6e9', marginVertical:2}} />
				
                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
					<Text style={styles.notes11}>Carte bancaire</Text>
					<Text style={styles.notes22}>{weekgrosscb} €</Text>
				</View>

              

                 <View style={{flexDirection:'row',marginVertical:2, justifyContent:'space-between'}}>
					<Text style={styles.notes111}>TOTAL</Text>
					<Text style={styles.notes222}>{somgross} €</Text>
				</View> 

                 
        
        </Card>
        <Card containerStyle={styles.stylecard1}>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
					<Text style={styles.notes1111}>NET</Text>
					<Text style={styles.notes2222}>{somgross} €*</Text>
				</View>
        </Card>

        <View style={{flexDirection:'row',alignItems: 'center',marginVertical:15, justifyContent:'center'}}>
					<Text style={styles.noteback}>
                        Total commissions : 5,25€
                        * Ce chiffre d'affaires ne tiennent pas compte d'éventuels ajustements.
                    </Text>
		</View> 
		
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({


  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Constants.statusBarHeight,
    backgroundColor: colors.TRANSPARENT,
  },
  rectangle: {
    width: 50,
    backgroundColor: "yellow",
    margin: 0,
    justifyContent: "center",
    alignItems: "center",
    height: 52,
    borderColor:"black"

},
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '25%',
    paddingHorizontal: '8%',
    flexBasis: '25%'
  },
 
  stylecard1:{       
    backgroundColor: colors.VIOLET, 
    borderRadius:15,
    marginTop:-16,
    paddingTop :20,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5, 
   
},
  stylecard:{
           
            backgroundColor:"white",
            borderRadius:15,
            elevation:10,
            zIndex:20,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 3 },
            shadowOpacity: 0.5,
            shadowRadius: 5,
            
        },

        noteback: {
            fontSize: 15,
            color:'#838383',
            fontFamily: 'Montserrat-Bold'
        },
       
       
        notes1: {
            fontSize: 20,
            color:'#5BE4C7',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },

        notes2: {
            fontSize: 25,
            color:'#5BE4C7',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },

        notes11: {
            fontSize: 20,
            color:'#5A48CB',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },
        notes22: {
            fontSize: 25,
            color:'#5A48CB',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },


        notes111: {
            fontSize: 20,
            color:'#000000',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },
        notes222: {
            fontSize: 25,
            color:'#000000',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },
        
        notes1111: {
            fontSize: 20,
            color:'#ffffff',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },
        notes2222: {
            fontSize: 25,
            color:'#ffffff',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },


        notesprev: {
            fontSize: 20,
            color:'#000000',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },
        notesmid: {
            fontSize: 20,
            color:'#000000',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },
        notesnext: {
            fontSize: 20,
            color:'#000000',
            fontFamily: 'Montserrat-Bold',
            textTransform:'capitalize'
        },




  headerText: {
    height: 22,
    color: colors.WHITE,
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    textTransform: 'uppercase',
    lineHeight: 18,
  },


 

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    position: 'absolute',
    width: '100%',
    height: '80%',
    top: '19%',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,    
    paddingHorizontal: '5%',
    paddingTop: 20,
    /* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
  },
  scrollViewChart:{
    backgroundColor: colors.WHITE,
    paddingTop: 10,
    flexDirection:'column',
    justifyContent:'center',
    alignItems:'center'
    
   
      
    
  },

    originDestination: {
        marginBottom: 12,
    },

    titleOr: {
        width: '70%',
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        lineHeight: 15,
        color: '#848484',
    },
    titleDes: {
        width: '70%',
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        lineHeight: 15,
        color: '#848484'
    },

    date: {
        width: 110,
        height: 30,
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        lineHeight: 15,
        color: '#848484',
        textAlign: 'right'
    },

    price: {
        color: colors.VIOLET,
        width: 110,
        height: 30,
        fontFamily: 'Montserrat-Bold',
        fontSize: 14,
        lineHeight: 18,
        textAlign: 'right',
    },  
    
})

export default Benefit;

