import { SvgXml } from "react-native-svg";
import customMapStyle from "./../../common/customMapStyle";
import iconLocalisationStart from "../../../assets/svg/iconLocalisationStart";
import iconLocalisationEnd from "../../../assets/svg/iconLocalisationEnd";
import React, {
  useEffect,
  useRef,
  useCallback,
  useContext,
  useMemo,
  Component
} from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  Alert,
  SafeAreaView,
} from "react-native";
import MapView, {
  PROVIDER_DEFAULT,
  Marker,
  Polyline,
  PROVIDER_GOOGLE,
} from "react-native-maps";
import { Google_Map_Key } from "../../../config";
import { colors } from "./../../common/theme";
import MapViewDirections from "react-native-maps-directions";
import { useDispatch, useSelector } from "react-redux";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Geolocation from "react-native-geolocation-service";
import RouteConfirm from "./../../components/RouteConfirm/index";
import ProposeTravel from "./../../components/ProposeTravel/index";
import Modal from "react-native-modal";
import PlacesAutoComplete from "./../../components/PlacesAutoComplete/index";
import {
  fetchAddressfromCoords,
  fetchCoordsfromPlace,
  getDriverTime,
} from "../../../redux/src/other/GoogleAPIFunctions";
import useState from "react-usestateref";
import "react-native-get-random-values";
import { v4 as uuidv4 } from "uuid";
import { FirebaseContext } from "../../../redux/src";
import CustomToast from "../../components/CustomToast";
import DriverGoingToRider from "../../components/DriverGoingToRider";
import ConfirmPickUpRider from "../../components/ConfirmPickUpRider";
import DriverTravelInProcess from "../../components/DriverTravelInProcess";
import DriverCancelTravelInProcess from "../../components/DriverCancelTravelInProcess/index";
import DropDownRiderByDriver from "../../components/ConfirmDropDownRider";
import DriverValidatePaymentMethod from "../../components/DriverValidatePaymentMethod";
import enroutevers from "../../../assets/svg/enroutevers";
import ValidateRouteRider from "../../components/ValidateRouteRider";
import {
  calculateCrowFlies,
  findDirection,
} from "../../functions/MapFunctions";
// import { json } from "express";
import { setData } from "../../functions/AsyncStorageFunctions";
import { editNearby } from "../../../redux/src/actions/nearbyactions";


const MIN_DISTANCE_TO_CALL_API = 10;

const latitudeDelta = 0.0922;
const longitudeDelta = 0.0421;

let driverDocumentserror = {
  text: "",
  id: uuidv4().toString().substring(0, 5),
};
let departLocationError = {
  text: "",
  id: uuidv4().toString().substring(0, 5),
};

let vehiculeError = { text: "", id: uuidv4().toString().substring(0, 5) };
let trajetError = { text: "", id: uuidv4().toString().substring(0, 5) };

let startWatchForeground = false;

let firstTimeCheck = true;

export default function HomeDriver(props) {
  const dispatch = useDispatch();
  const { api } = useContext(FirebaseContext);
  const {
    addTravel,
    signOutM,
    deleteTravel,
    fetchMesVehicules,
    getRouteDetails,
    fetchDriverInfos,
    fetchOneTravel,
    addNearby,
    deleteNearby,
    fetchNearby,
    fetchRiderRequest,    
    fetchOneRiderRequest,
    getStripeAccountId,
    updateRiderRequest,
  } = api || {};

  const auth = useSelector((state) => state.auth);
  const mesvehicules = useSelector((state) => state.mesvehicules);
  const driverdata = useSelector((state) => state.driverdata);
  const travels = useSelector((state) => state.travel);
  const resNearby = useSelector((state) => state.nearbydata);
  const riderData = useSelector((state) => state.requestReducerdata);
  const rider = riderData.ride

  const vehicules = mesvehicules.mesvehicules;
  const driverDocuments = driverdata.driverdata;
  const nearby = resNearby.nearby

  const travelInformations = useMemo(() => travels.travel, [travels?.travel]);

  const { travelType, travelId, isLastTravel } = useMemo(() => {
    return {
      travelType: travelInformations ? "arrivee" : "depart",
      travelId: travelInformations ? 2 : 1,
      isLastTravel: !!travelInformations,
    };
  }, [travelInformations]);

  const [errors, setErrors] = useState([]);

  const refMap = useRef();

  const [depLocation, setDepLocation] = useState({
    latitude: 48.856614,
    longitude: 2.3522219,
    address_name: null,
    default: true,
  });
  const [destLocation, setDestLocation] = useState({
    latitude: 48.856614,
    longitude: 2.3522219,
    address_name: null,
    default: true,
  });

  const isHaveDesAndDep = useMemo(() => {
    return (
      depLocation.address_name &&
      !depLocation.default &&
      !destLocation.default &&
      destLocation.address_name
    );
  }, [
    depLocation?.address_name,
    depLocation?.default,
    destLocation?.default,
    destLocation?.address_name,
  ]);

  const [nearbyPosition, setNearbyPosition] = useState({...Driver, longitude: null, latitude: null, date: null});
  const [dataRider, setDataRider] = useState(null);
  
  const [initialRegion, setInitialRegion] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: latitudeDelta,
    longitudeDelta: longitudeDelta,
  });
  const [carPosition, setCarPosition] = useState({
    latitude: 0,
    longitude: 0,
    error: null,
  });

  const [routeCoordinates, setRouteCoordinates] = useState([]);

  const [placeAutoCompleteVisible, setPlaceAutoCompleteVisible] =
    useState(false);
  const [proposeTravelVisible, setProposeTravelVisible] = useState(true);
  const [routeConfirmVisible, setRouteConfirmVisible] = useState(false);
  const [driverGoingToRider, setDriverGoingRider] = useState(false);
  const [confirmPickUpRiderVisible, setConfirmPickUpRiderVisible] =
    useState(false);
  const [driverCancelTravelInProcess, setDriverCancelTravelInProcess] =
    useState(false);
  const [driverTravelInProcess, setDriverTravelInProcess] = useState(false);
  const [dropDownRiderByDriver, setDropDownRiderByDriver] = useState(false);
  const [driverValidatePaymentMethod, setDriverValidatePaymentMethod] =
    useState(false);

  const [pickLocationType, setPickLocationType] = useState(null);
  const [carid, setCarId] = useState(null);
  const [margin, setMargin] = useState(0);
  const [sendData, setSendData] = useState(null);
  const [loadingStartTravel, setLoadingStartTravel] = useState(false);
  const [proposeRider, setProposeRider ] = useState(false);

  // start toggle functions

  /* proposeTravel */
  const openProposeTravel = () => setProposeTravelVisible(true);
  const closeProposeTravel = () => setProposeTravelVisible(false);

  /* routeConfirm */
  const openRouteConfirm = () => setRouteConfirmVisible(true);
  const closeRouteConfirm = () => setRouteConfirmVisible(false);

  /* driverGoingToRider */
  const openDriverGoingToRider = () => setDriverGoingRider(true);
  const closeDriverGoingToRider = () => setDriverGoingRider(false);

  /* driverGoingToRider */
  const openConfirmPickUpRider = () => setConfirmPickUpRiderVisible(true);
  const closeConfirmPickUpRider = () => setConfirmPickUpRiderVisible(false);

  /* DriverTravelInProcess */
  const openDriverTravelInProcess = () => setDriverTravelInProcess(true);
  const closeDriverTravelInProcess = () => setDriverTravelInProcess(false);

  /* DriverValidatePaymentMethod */
  const openDriverValidatePaymentMethod = () =>
    setDriverValidatePaymentMethod(true);
  const closeDriverValidatePaymentMethod = () =>
    setDriverValidatePaymentMethod(false);

  /* DriverTravelInProcess */
  const openDropDownRiderByDriver = () => setDropDownRiderByDriver(true);
  const closeDropDownRiderByDriver = () => setDropDownRiderByDriver(false);

  /* PlaceAutoComplete */
  const openPlaceAutoComplete = () => setPlaceAutoCompleteVisible(true);
  const closePlaceAutoComplete = () => setPlaceAutoCompleteVisible(false);

  /* DriverCancelTravelInProcess */
  const openDriverCancelTravelInProcess = () =>
    setDriverCancelTravelInProcess(true);
  const closeDriverCancelTravelInProcess = () =>
    setDriverCancelTravelInProcess(false);

  const closeAllModalsWithoutProposal = () => {
    routeConfirmVisible && closeRouteConfirm();
    driverGoingToRider && closeDriverGoingToRider();
    confirmPickUpRiderVisible && closeConfirmPickUpRider();
    driverTravelInProcess && closeDriverTravelInProcess();
    driverValidatePaymentMethod && closeDriverValidatePaymentMethod();
    dropDownRiderByDriver && closeDropDownRiderByDriver();
    placeAutoCompleteVisible && closePlaceAutoComplete();
    driverCancelTravelInProcess && closeDriverCancelTravelInProcess();
  };


  const getDistanceTime = async (currentCoordinate) => {
    if (travelInformations != null) {
      const latlng = `${currentCoordinate.latitude},${currentCoordinate.longitude}`;
      const addresseConducteur = await fetchAddressfromCoords(latlng);
      let resultat = await getDriverTime(
        addresseConducteur,
        destLocation.address_name
      );

      api.savePosition(auth.info.uid, {
        lat: currentCoordinate.latitude,
        lng: currentCoordinate.longitude,
        duration: resultat.distance_in_km || 5,
        distance: resultat.time_in_secs || 2,
      });
    }
  };

  useEffect(() => {
    if (!travelInformations) {
      return openProposeTravel();
    }

    if (travelInformations?.status === "init") {
      let times1 = parseInt(travelInformations?.create_time, 10);
      let times2 = parseInt(new Date().getTime(), 10);
      let date1 = new Date(times1);
      let date2 = new Date(times2);

      let diff = (date2 - date1) / 1000;
      diff /= 60;
      let datadiff = 0;
      datadiff = Math.abs(Math.round(diff));
      if (datadiff < 120 && travelInformations?.status == "init") {
        !routeConfirmVisible && openRouteConfirm();
      }
      if (datadiff > 120 && travelInformations?.status == "init") {
        routeConfirmVisible && closeRouteConfirm(false);
        dispatch(deleteTravel(travelInformations, "oui"));
      }
      firstTimeCheck = false;
    }

    if (
      travelInformations?.status === "init" &&
      travelInformations?.isFound &&
      travelInformations?.isAffiner_driver &&
      travelInformations?.isAffiner_rider &&
      firstTimeCheck
    ) {
      firstTimeCheck = false;
      if (
        Math.round(
          Math.abs(new Date() - new Date(travelInformations.create_time)) /
            (1000 * 60)
        ) > 120
      ) {
        travelInformations.isFound = false;
        travelInformations.isAffiner_rider = false;
        travelInformations.isAffiner_driver = false;
        travelInformations.status = "init";
        //return dispatch(addTravel(travelInformations));
        // Travel time over 2 hours !!!! Cancelling It
      }
      if (travelInformations?.recuperer_rider) {
        // To minutes)
        return openDriverTravelInProcess();
      }

      if (!travelInformations?.arriver_vers_passager) {
        return !routeConfirmVisible && openDriverGoingToRider();
      }
    }
    console.log("Travel info: "+JSON.stringify(travelInformations))
  }, [travelInformations]);

  useEffect(() => {
    if (auth.info && auth.info.profile && auth.info.profile.login) {
      const interval = setInterval(() => {
        let next = auth.info.profile.login;
        AsyncStorage.getItem("loginplus", (err, result) => {
          if (next != result) {
            dispatch(signOutM());
            props.navigation.navigate("LoginScreen");
          }
        });
      }, 7000);
      return () => clearInterval(interval);
    }
  }, [auth.info]);


  useEffect(() => {
    if (auth?.info?.profile) {
      if (
        travelInformations != null &&
        travelInformations.status != undefined
      ) {
        depLocation.default &&
          setDepLocation({
            latitude: travelInformations.rider_lat_dep,
            longitude: travelInformations.rider_long_dep,
            address_name: travelInformations.propose_dep_rider,
            default: false,
          });

        destLocation.default &&
          travelInformations.rider_lat_dest &&
          travelInformations.rider_long_dest &&
          setDestLocation({
            latitude: travelInformations.rider_lat_dest,
            longitude: travelInformations.rider_long_dest,
            address_name: travelInformations.propose_dest_rider,
            default: false,
          });
      }
    }
  }, [auth?.info?.profile, travelInformations]);

  const StartForegroundGeolocation = async () => {
    Geolocation.watchPosition(
      (location) => {
        const { latitude, longitude } = location.coords;
        const newCoordinate = { latitude, longitude };
        dispatch(editNearby({...Driver, longitude: newCoordinate.longitude, latitude: newCoordinate.latitude}))
        !startWatchForeground && getDistanceTime(newCoordinate);
        api.savePosition(auth.info.uid, {
          lat: newCoordinate.latitude,
          lng: newCoordinate.longitude,
        });
        setCarPosition(newCoordinate);
        refMap?.current?.animateToRegion({
          ...initialRegion,
          latitude: newCoordinate.latitude,
          longitude: newCoordinate.longitude,
        });
        startWatchForeground = true;
      },
      (error) => {
        console.error({ error });
      },
      { maximumAge: 0, distanceFilter: 10, enableHighAccuracy: true }
    );
  };

  useEffect(() => {
    let car_id = "";
    if (vehicules && vehicules.length !== 0) {
      vehicules.forEach((el) => {
        if (el.carDefault) {
          car_id = el.carKey;
          setCarId(car_id);
          return;
        }
      });
    }
  }, [vehicules]);


  useEffect(() => {
    if(JSON.stringify(rider) !== JSON.stringify(null)) {
      // positionInRiderRequest(rider)
    }
  }, [rider])

  const positionInRiderRequest = (rider) => {
    for (let i = 0; i < rider.length; i++) {
      rider[i].driverDist.map((ix) => {
        if (ix.driver_id == auth.info.uid) {
          setDataRider(rider[i]);
          validation();
        }
      })
    }
  }

  const initFetch = useCallback(() => {
    if (auth?.info?.profile) {
      fetchMesVehicules && dispatch(fetchMesVehicules());
      fetchDriverInfos && dispatch(fetchDriverInfos());
      fetchOneTravel && dispatch(fetchOneTravel(auth?.info?.uid));
      getStripeAccountId && dispatch(getStripeAccountId(auth?.info?.uid));
      fetchNearby && dispatch(fetchNearby());
      fetchRiderRequest && dispatch(fetchRiderRequest());
    }
  }, [dispatch, auth?.info?.profile]);

  useEffect(() => {
    findCoordinates(Driver);
    initFetch();    
  }, []);

  const reInitTravel = (isDeleteTravel = false) => {
    setDepLocation({
      latitude: 37.78825,
      longitude: -122.4324,
      address_name: null,
      default: true,
    });
    setDestLocation({
      latitude: 37.78825,
      longitude: -122.4324,
      address_name: null,
      default: true,
    });
    isDeleteTravel && dispatch(deleteTravel(travelInformations, "oui"));
    closeAllModalsWithoutProposal();
    openProposeTravel();
  };

  const Driver = {
      id: null,
      uid: auth?.info?.uid,
      type: auth?.info?.profile?.usertype,
  }

  const findCoordinates = async (Driver = null) => {
    Geolocation.getCurrentPosition(
      (position) => {
        setDepLocation({
          ...depLocation,
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
        setInitialRegion({
          ...initialRegion,
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: latitudeDelta,
          longitudeDelta: longitudeDelta,
        });
        refMap?.current?.animateToRegion({
          ...initialRegion,
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
        let merge = {...Driver, longitude: position.coords.longitude, latitude: position.coords.latitude, date: new Date().getTime()};
        setNearbyPosition(merge)
      },
      (error) => {
        console.log({ error });
      },
      {
        enableHighAccuracy: true,
        timeout: 15000,
      }
    );
    !startWatchForeground && StartForegroundGeolocation();
  };

  useEffect(() => {
    RetreiveInfoNearby();
  }, [nearbyPosition])

  const RetreiveInfoNearby = () => {
    if (nearby == null ) {
      if (nearbyPosition.uid) {
        dispatch(addNearby(nearbyPosition))
      } 
    } else {
      const res = nearby.filter(obj => obj.type == 'driver' && obj.uid == auth.info.uid)

      if (res.length == 0) { dispatch(addNearby(nearbyPosition)); }
      if (res) {
        for (let i= 0; i < res.length; i++) {
          const secondeSeparation = Math.abs((new Date().getTime() - res[i].date) / 1000);
          if (secondeSeparation/1000 >= 900) {
            // dispatch(deleteNearby(res[i].id))
          }
        }
      }
    }
  }

  const returnDate = () => {
    let now = new Date();
    let day = ("0" + now.getDate()).slice(-2);
    let month = ("0" + (now.getMonth() + 1)).slice(-2);
    let today = now.getFullYear() + "-" + month + "-" + day;

    return today;
  };

  const saveData = async (destLat, destLong, destAddresName) => {
    const dest = {
      latitude: destLat,
      longitude: destLong,
      address_name: destAddresName,
    };
    //console.log("je suis ici dans fonction saveData"+ JSON.stringify(dest));
    let startLoc =
      '"' + depLocation.latitude + "," + depLocation.longitude + '"';
    let destLoc = '"' + dest.latitude + "," + dest.longitude + '"';

    await getRouteDetails(startLoc, destLoc).then((res) => {
      if (res) {
        if (auth.info && auth.info.profile) {
          setSendData({
            driver_id: auth.info.uid,
            driver_firstName: auth.info.profile.firstName,
            driver_lastName: auth.info.profile.lastName,
            driver_mobile: auth.info.profile.mobile,
            driver_pushToken: auth.info.profile.pushToken,
            driver_profile_img:
              auth &&
              auth.info &&
              auth.info.profile &&
              auth.info.profile.profile_image
                ? auth.info.profile.profile_image
                : null,
            driver_email: auth.info.profile.email,
            lat_dep: depLocation.latitude,
            long_dep: depLocation.longitude,
            addr_dep: depLocation.address_name,
            lat_dest: dest.latitude,
            long_dest: dest.longitude,
            addr_dest: dest.address_name,
            status: "init",
            direction: findDirection(depLocation, dest),
            max_passenger_nb: 3,
            passager_nb: 0,
            prix_point: 0,
            prix_promo: 0,
            arriver_vers_passager: false,
            arriver_vers_passager_at: new Date().getTime(),
            arriver_dest_passager: false,
            arriver_dest_passager_at: new Date().getTime(),
            car_id: carid,
            travel_distanceInKm: res.distance,
            travel_roadDurationInSecond: parseFloat(res.duration / 60).toFixed(
              0
            ),
            travel_roadDurationText: parseFloat(res.duration / 60).toFixed(0),
            create_date: new Date(),
            create_time: new Date().getTime(),
            mode_paiement: "especes",
            isAffiner_rider: false,
            isAffiner_driver: false,
            isFound: false,
            travel_id: travelId,
            travel_type: travelType,
            depart: true,
            recuperer_rider: true,
            arrivee: isLastTravel,
            isPaid: false,
          });
        }
      }
    });
    setLoadingStartTravel(false);
  };

  useEffect(() => {
    if (sendData != null) {
      closeProposeTravel();
      dispatch(addTravel(sendData));
    }
  }, [sendData]);


  const calculPosition = async (id, name, trajet) => {
    const lnlg = await fetchCoordsfromPlace(id);

    setInitialRegion((prevState) => {
      return {
        ...prevState,
        latitude: lnlg.lat,
        longitude: lnlg.lng,
      };
    });

    if (trajet == "destination") {
      setDestLocation({
        latitude: lnlg.lat,
        longitude: lnlg.lng,
        address_name: name,
      });
      refMap?.current?.animateToRegion({
        ...initialRegion,
        latitude: lnlg.lat,
        longitude: lnlg.lng,
      });
    } else {
      setDepLocation({
        latitude: lnlg.lat,
        longitude: lnlg.lng,
        address_name: name,
        default: false,
      });

      refMap?.current?.animateToRegion({
        ...initialRegion,
        latitude: lnlg.lat,
        longitude: lnlg.lng,
      });
    }

    closePlaceAutoComplete();
  };

  const onDepPress = useCallback(() => {
    if (vehicules && vehicules.length < 1) {
      vehiculeError.text = "Bien vouloir enregistrer votre véhicule.";
    }

    if (!driverDocuments) {
      driverDocumentserror.text = "Bien vouloir enregistrer vos documents. ";
    }

    if (vehiculeError.text || driverDocumentserror.text) {
      setErrors([...errors, vehiculeError, driverDocumentserror]);
    } else {
      vehiculeError.text = "";
      driverDocumentserror.text = "";
      setPickLocationType("depart");
      openPlaceAutoComplete();
    }
  }, [vehicules, driverDocuments, errors]);

  const validation = async () => {
    if (rider) {
      for (let x=0; x < rider.filter((i) => i !== "default").length; x++) {
        if (rider[x].status == "init" && rider[x].driverDist[0].driver_id == auth.info.uid && !rider[x]?.etat) {
            const dest = {
              latitude: rider[x].lat_dest,
              longitude: rider[x].long_dest,
              address_name: rider[x].addr_dest,
            };
            const dep = {
              latitude:rider[x].lat_dep,
              longitude:rider[x].long_dep,
              address_name: rider[x].addr_dep,
            }
            let startLoc = '"' + dep.latitude + "," + dep.longitude + '"';
            let destLoc = '"' + dest.latitude + "," + dest.longitude + '"';

          await getRouteDetails(startLoc, destLoc).then((res) => {
            if (res) {
                const travelInfo = {
                ...rider[x],
                  etat: "accepter",
                  addr_dep: dep.address_name,
                  addr_dest: dest.address_name,
                  lat_dep: dep.latitude,
                  lat_dest: dest.latitude,
                  long_dep: dep.longitude,
                  long_dest: dest.longitude,
                  driver_id: auth.info.uid,
                  id: auth.info.uid,
                  driver_firstName: auth.info.profile.firstName,
                  driver_lastName: auth.info.profile.lastName,
                  driver_mobile: auth.info.profile.mobile,
                  driver_pushToken: auth.info.profile.pushToken,
                  driver_email: auth.info.profile.email,
                  status: "init",
                  driver_profile_img:
                    auth &&
                    auth.info &&
                    auth.info.profile &&
                    auth.info.profile.profile_image
                      ? auth.info.profile.profile_image
                      : null,
                  driver_email: auth.info.profile.email,
                  max_passenger_nb: 3,
                  prix_point: 0,
                  prix_promo: 0,
                  arriver_vers_passager: false,
                  arriver_vers_passager_at: new Date().getTime(),
                  arriver_dest_passager: false,
                  arriver_dest_passager_at: new Date().getTime(),
                  car_id: carid,
                  direction: findDirection(dep, dest),
                  code_promo: 0,
                  travel_distanceInKm: res.distance,
                  travel_roadDurationInSecond: parseFloat(res.duration / 60).toFixed(0),
                  travel_roadDurationText: parseFloat(res.duration / 60).toFixed(0),
                  create_date: new Date(),
                  create_time: new Date().getTime(),
                  isAffiner_driver: false,
                  isAffiner_rider: false,
                  isFound: false,
                  travel_id: travelId,
                  travel_type: travelType,
                  depart: true,
                  recuperer_rider: true,
                  arrivee: isLastTravel,
                  isPaid: false,
                }
                updateRiderRequest && dispatch(updateRiderRequest(travelInfo));
                addTravel && dispatch(addTravel(travelInfo));
              }
          })
          }
        }
    } else {
      return null
    }
  }

  const onDestPress = useCallback(() => {
    if (auth.info && auth.info.profile) {
      const instancedate1 = new Date(auth.info.profile.dateTrajet);
      const instancedate2 = new Date(returnDate());


        // if (true) {
        if (vehicules && vehicules.length >= 0) {
        } else {
          vehiculeError.text =
            " Pas de véhicules enregistrés!! Bien vouloir enregistrer un... ";
        }
        if (driverDocuments && driverDocuments != null) {
        } else {
          driverDocumentserror.text =
            " Aucun documents enregistrés!! Bien vouloir enregistrer vos documents... ";
        }
        if (!(vehiculeError.text == "" && driverDocumentserror.text == "")) {
          setErrors([...errors, vehiculeError, driverDocumentserror]);
        }
    }
  }, [errors, auth?.info?.profile, driverDocuments,  vehicules]);

  const onPressProposeGo = useCallback(() => {

    console.log("Proposing...")
    setLoadingStartTravel(true);
    saveData(
      destLocation.latitude,
      destLocation.longitude,
      destLocation.address_name
    );
  }, [destLocation]);

  const renderProposeTravel = useMemo(() => {
    return (
      <Modal
        isVisible={proposeTravelVisible}
        backdropOpacity={0}
        coverScreen={false}
      >
        <ProposeTravel
          depLocation={depLocation}
          destLocation={destLocation}
          onDepPress={onDepPress}
          onDestPress={onDestPress}
          onPressGo={onPressProposeGo}
          isHaveDesAndDep={isHaveDesAndDep}
          loading={loadingStartTravel}
        />
      </Modal>
    );
  }, [
    travelInformations,
    vehicules,
    driverDocuments,
    auth?.info?.profile,
    proposeTravelVisible,
    depLocation,
    destLocation,
    loadingStartTravel
  ]);

  return (
    <SafeAreaView style={{ flex: 1, width: "100%", height: "100%" }}>
      <MapView
        ref={refMap}
        style={[styles.map, { marginBottom: margin }]}
        customMapStyle={customMapStyle}
        showsMyLocationButton={true}
        loadingEnabled={true}
        loadingIndicatorColor={colors.VIOLET}
        initialRegion={initialRegion}
        provider={PROVIDER_GOOGLE}
        onMapReady={() => setMargin(1)}
      >
        {!!depLocation.address_name && !!destLocation.address_name && (
          <MapViewDirections
            lineDashPattern={[0]}
            apikey={Google_Map_Key}
            origin={depLocation}
            destination={destLocation}
            timePrecision="now"
            mode="DRIVING"
            language="fr"
            strokeWidth={4}
            strokeColor={colors.VIOLET}
            onError={(error) => {
              console.log(error);
            }}
          />
        )}

        {depLocation.address_name != null && (
          <Marker coordinate={depLocation}>
            <View>
              <SvgXml
                xml={iconLocalisationStart}
                style={{ height: 35, width: 35 }}
              />
            </View>
          </Marker>
        )}

        {destLocation.address_name != null && (
          <Marker coordinate={destLocation}>
            <View>
              <SvgXml
                xml={iconLocalisationEnd}
                style={{ height: 35, width: 35 }}
              />
            </View>
          </Marker>
        )}

        <Polyline
          lineDashPattern={[1]}
          coordinates={routeCoordinates}
          strokeWidth={2}
        />

        {!!carPosition.longitude && !!carPosition.latitude && (
          <Marker.Animated coordinate={carPosition}>
            <SvgXml xml={enroutevers} />
          </Marker.Animated>
        )}
      </MapView>

      {renderProposeTravel}

      <Modal
        isVisible={placeAutoCompleteVisible}
        backdropColor="white"
        backdropOpacity={1}
        coverScreen={false}
        onBackButtonPress={closePlaceAutoComplete}
      >
        <PlacesAutoComplete
          depLocation={depLocation}
          destLocation={destLocation}
          onIconPress={closePlaceAutoComplete}
          onCalculPosition={calculPosition}
          locationType={pickLocationType}
        />
      </Modal>

      <RouteConfirm
        isVisible={routeConfirmVisible}
        closeRouteConfirm={closeRouteConfirm}
        openProposeTravel={openProposeTravel}
        openDriverGoingToRider={openDriverGoingToRider}
        reInitTravel={reInitTravel}
        onDestPress={onDestPress}
      />

      <Modal
        isVisible={driverGoingToRider}
        animationType="slide"
        transparent={true}
        backdropOpacity={0}
        coverScreen={false}
        style={{ margin: 0 }}
        backdropColor="white"
      >
        <DriverGoingToRider
          closeDriverGoingToRider={closeDriverGoingToRider}
          openRouteConfirm={openRouteConfirm}
          openConfirmPickUpRider={openConfirmPickUpRider}
          openDriverCancelTravelInProcess={openDriverCancelTravelInProcess}
        />
      </Modal>

      <Modal
        isVisible={driverCancelTravelInProcess}
        transparent={true}
        coverScreen={false}
        animationType="slide"
        style={{ margin: 0 }}
      >
        <DriverCancelTravelInProcess
          closeDriverCancelTravelInProcess={closeDriverCancelTravelInProcess}
          openRouteConfirm={openRouteConfirm}
        />
      </Modal>

      <Modal
        isVisible={confirmPickUpRiderVisible}
        animationType="slide"
        transparent={true}
        coverScreen={false}
        onBackButtonPress={closeConfirmPickUpRider}
        style={{ margin: 0 }}
      >
        <ConfirmPickUpRider
          closeConfirmPickUpRider={closeConfirmPickUpRider}
          openRouteConfirm={openRouteConfirm}
          openDriverTravelInProcess={openDriverTravelInProcess}
        />
      </Modal>

      <Modal
        isVisible={driverTravelInProcess}
        animationType="slide"
        onBackButtonPress={closeDriverTravelInProcess}
        transparent={true}
        coverScreen={false}
        style={{ margin: 0 }}
      >
        <DriverTravelInProcess
          closeDriverTravelInProcess={closeDriverTravelInProcess}
          openDriverValidatePaymentMethod={openDriverValidatePaymentMethod}
        />
      </Modal>

      <Modal
        isVisible={driverValidatePaymentMethod}
        animationType="slide"
        transparent={true}
        coverScreen={false}
        style={{ margin: 0 }}
      >
        <DriverValidatePaymentMethod
          closeDriverValidatePaymentMethod={closeDriverValidatePaymentMethod}
          openDropDownRiderByDriver={openDropDownRiderByDriver}
        />
      </Modal>

      <Modal
        isVisible={dropDownRiderByDriver}
        animationType="slide"
        coverScreen={false}
        onBackButtonPress={closeDropDownRiderByDriver}
        transparent={true}
        style={{ margin: 0 }}
      >
        <DropDownRiderByDriver
          closeDropDownRiderByDriver={closeDropDownRiderByDriver}
          reInitTravel={reInitTravel}
        />
      </Modal>

      {/* <DeniedTravelByRiderModal /> */}

      <View
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          zIndex: 10,
        }}
      >
        {errors.map((error) =>
          error.text != "" ? (
            <CustomToast
              key={error.id}
              message={error.text}
              onHide={() => {
                departLocationError.text = "";
                vehiculeError.text = "";
                driverDocumentserror.text = "";
                setErrors((errors) =>
                  errors.filter(
                    (currentError) => currentError.text !== error.text
                  )
                );
              }}
            />
          ) : null
        )}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  map: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  searchContainer: {
    backgroundColor: "white",
    flexDirection: "row",
    width: "80%",
    height: "22%",
    position: "absolute",
    top: 20,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 13,
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 3,
  },
  proposeContainer: {
    width: "80%",
    height: "60%",
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 13,
    // elevation: 3,
  },
  searchBodyIconWrapper: {
    height: "100%",
    width: "20%",
    justifyContent: "center",
    alignItems: "center",
  },
  searchBodyButtonsWrapper: {
    height: "100%",
    width: "80%",
    justifyContent: "space-around",
  },
  searchBodyButton: {
    height: 40,
    width: "100%",
    flexDirection: "column",
  },
  searchBodyContentButtonView: {
    height: 40,
    width: "100%",
    flexDirection: "column",
  },
  searchBodyContentTitleButton: {
    fontSize: 12,
    lineHeight: 15,
    color: colors.BLACK,
    fontFamily: "Montserrat-Bold",
  },
  searchBodyContentLocationButton: {
    fontSize: 14,
    lineHeight: 18,
    fontFamily: "Montserrat-Medium",
    color: "#848484",
  },
});
