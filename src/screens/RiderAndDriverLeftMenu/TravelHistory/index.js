import React, { useContext, useEffect, useMemo } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

//colors
import { colors } from "../../../common/theme";

//Constants
import Constants from "expo-constants";

import travel from "../../../../assets/svg/travel";
import { SvgXml } from "react-native-svg";

import { ListItem } from "react-native-elements";

import { FirebaseContext } from "../../../../redux/src";
import { useSelector, useDispatch } from "react-redux";

function HistoryTravelStack({ navigation }) {
  const { api } = useContext(FirebaseContext);
  const { fetchHistorique } = api || {};

  const auth = useSelector((state) => state.auth);

  const travelState = useSelector((state) => state.travel);

  const { histories, fetchingHistories } = travelState;

  const dispatch = useDispatch();

  useEffect(() => {
    if (auth?.info?.uid) {
      fetchHistorique && dispatch(fetchHistorique(auth?.info?.uid));
    }
  }, []);

  const timeConverter = (UNIX_timestamp) => {
    let now = new Date(UNIX_timestamp);
    let day = ("0" + now.getDate()).slice(-2);
    let month = ("0" + (now.getMonth() + 1)).slice(-2);
    let today = now.getFullYear() + "-" + month + "-" + day;
    return today;
  };

  const formattedHistories = useMemo(() => {
    if (!histories || !histories.length) {
      return [];
    }

    let now = new Date();
    let startDay = 1; //0=sunday, 1=monday etc.
    let d = now.getDay(); //get the current day
    let weekStart = new Date(
      now.valueOf() - (d <= 0 ? 7 - startDay : d - startDay) * 86400000
    ); //rewind to start day
    let weekEnd = new Date(weekStart.valueOf() + 6 * 86400000); //add 6 days to get last day
    const newHistories = [];
    histories.forEach((item) => {
      let datte = timeConverter(item.create_time);
      let instancedate1 = new Date(datte);

      if (
        instancedate1.valueOf() >= weekStart.valueOf() &&
        instancedate1.valueOf() <= weekEnd.valueOf()
      ) {
        newHistories.push(item);
      }
    });
    return newHistories;
  }, [histories]);

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.headerDocsContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons name="ios-chevron-back" size={30} color={colors.WHITE} />
        </TouchableOpacity>
        <View>
          <Text style={styles.headerText}>Historique</Text>
        </View>
        <View>
          <Text></Text>
        </View>
      </View>
      <ScrollView style={styles.scrollViewContainer}>
        {formattedHistories && formattedHistories.length > 0 ? (
          formattedHistories.map((item, i) => (
            <ListItem
              key={i}
              style={[
                {
                  borderBottomColor: "#CFCFCF",
                  borderBottomWidth: 1,
                },
                i === formattedHistories.length - 1 && {
                  paddingBottom: 50,
                  borderBottomWidth: 0,
                },
              ]}
              containerStyle={{ padding: 0 }}
            >
              <SvgXml xml={travel} style={{ width: 50, height: "100%" }} />
              <ListItem.Content>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-around",
                  }}
                >
                  <View
                    style={{
                      flexDirection: "column",
                      width: "80%",
                      justifyContent: "space-between",
                    }}
                  >
                    <View style={styles.originDestination}>
                      <ListItem.Title style={styles.titleOr} numberOfLines={3}>
                        {item.propose_dep_rider}
                      </ListItem.Title>
                    </View>
                    <View style={styles.finalDestination}>
                      <ListItem.Title style={styles.titleDes} numberOfLines={3}>
                        {item.propose_dest_rider}
                      </ListItem.Title>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "space-between",
                    }}
                  >
                    <ListItem.Title style={styles.date}>
                      {JSON.stringify(
                        new Date(item.rider_trip_propose_date_time)
                      ).substring(1, 11)}
                    </ListItem.Title>
                    <ListItem.Title style={styles.price}>
                      {item.propose_negoPrice}€
                    </ListItem.Title>
                  </View>
                </View>
              </ListItem.Content>
            </ListItem>
          ))
        ) : (
          <View style={{ alignItems: "center" }}>
            {fetchingHistories && (
              <ActivityIndicator size="large" color="#5A48CB" />
            )}

            {!fetchingHistories && (
              <Text>Vous n'avez pas encore de réservation</Text>
            )}
          </View>
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: Constants.statusBarHeight,
    backgroundColor: colors.TRANSPARENT,
  },
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: "25%",
    paddingHorizontal: "8%",
    flexBasis: "30%",
  },

  headerText: {
    height: 22,
    color: colors.WHITE,
    fontFamily: "Montserrat-Bold",
    fontSize: 15,
    textTransform: "uppercase",
    lineHeight: 18,
  },

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    position: "absolute",
    width: "100%",
    height: "80%",
    top: "25%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: "5%",
    paddingTop: 20,
  },

  originDestination: {
    marginBottom: 12,
  },

  titleOr: {
    width: "70%",
    fontFamily: "Montserrat-Medium",
    fontSize: 13,
    lineHeight: 15,
    color: "#848484",
  },
  titleDes: {
    width: "70%",
    fontFamily: "Montserrat-Medium",
    fontSize: 13,
    lineHeight: 15,
    color: "#848484",
  },

  date: {
    width: 100,
    height: 30,
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    lineHeight: 15,
    color: "#848484",
    textAlign: "right",
  },

  price: {
    color: colors.VIOLET,
    width: 100,
    height: 30,
    fontFamily: "Montserrat-Bold",
    fontSize: 14,
    lineHeight: 18,
    textAlign: "right",
  },
});

export default HistoryTravelStack;
