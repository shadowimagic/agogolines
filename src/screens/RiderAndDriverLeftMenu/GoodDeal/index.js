import React, {useState, useEffect, useRef, useContext, useCallback} from "react";
import {
	View,
	Text,
	StyleSheet,
	Image,
	ScrollView,
	TouchableOpacity,
	FlatList,
	TextInput,
	Share,
} from "react-native";

import {Ionicons} from "@expo/vector-icons";

//colors
import {colors} from "../../../common/theme";

//images
import rectangle from "../../../../assets/images/Rectangle.png";
import taxi from "../../../../assets/images/Taxi.png";

import { FirebaseContext } from '../../../../redux/src';
import { useSelector, useDispatch } from 'react-redux';

//Constants
import Constants from "expo-constants";

function SponsorScreen({navigation}) {
	const [msg, setMsg] = useState("ZV617F");
	const [isMsgSent, setIsMsgSent] = useState(false);

	const { api } = useContext(FirebaseContext);
	const {
	  editBonPlan, 
	  fetchBonPlan,
	  deleteBonPlan
	} = api;
	

	const [data, setData] = useState([]);
	const bonplandata = useSelector(state => state.bonplandata);
	const auth = useSelector((state) => state.auth);
	const dispatch = useDispatch();

	

	  useEffect(() => {
		if (auth.info && auth.info.profile) {
            
			dispatch(fetchBonPlan())
		
		}
	}, [auth.info]);

  
	useEffect(()=>{
	  if(bonplandata.bonplans){
		const indexOfObject = bonplandata.bonplans.findIndex(object => {
			return object.id === '-MuRSIWLEsYhUnTFi-_L';
		  });
		
		bonplandata.bonplans.splice(indexOfObject, 1);
		setData(bonplandata.bonplans);
	  }else{
		setData([]);
	  }
	},[bonplandata.bonplans]);

	const ShareMessage = (messages) => {
		//Here is the Share API
		Share.share({message: messages, title: "envoyé via Agogolines mobile"})
			//after successful share return result
			.then((result) => {
				//console.log(result);
				setIsMsgSent(true);
				//console.log(isMsgSent);
			})
			//If any thing goes wrong it comes here
			.catch((errorMsg) => {
				console.log(errorMsg);
				//console.log(isMsgSent);
			});
	};
	return (
		<View style={{flex: 1}}>
			<View style={styles.headerDocsContainer}>
				<TouchableOpacity onPress={() => navigation.goBack()}>
					<Ionicons name="ios-chevron-back" size={30} color={colors.WHITE} />
				</TouchableOpacity>
				<View>
					<Text style={styles.headerText}>bon plans</Text>
				</View>
				<View>
					<Text></Text>
				</View>
			</View>
			<ScrollView style={styles.scrollViewContainer}>
				{/**<View
					style={{
						alignItems: "center",
						marginBottom: 20,
					}}>
					<Text style={styles.bigTitle}>10% DE REMISE !</Text>
					<Text style={styles.bigTitle}>LOREM IMPSUM AT VERO EOS</Text>
				</View>

				<View
					style={{
						alignItems: "center",
						marginBottom: 20,
					}}>
					<Image source={rectangle} />
				</View>

				<View
					style={{
						alignItems: "center",
						marginBottom: 30,
					}}>
					<Text style={styles.textLabel}>CODE PROMO</Text>
					<View style={{justifyContent: "center", width: 250, height: 65, marginTop: 10}}>
						<TextInput editable={false} style={styles.input} value="ZV617F" />
					</View>
				</View>

				<View
					style={{
						alignItems: "center",
						marginBottom: 20,
					}}>
					<Text style={styles.textContent}>
						At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
						praesentium voluptatum deleniti atque corrupti quos dolores et quas
						molestias excepturi sint occaecati cupiditate non provident, similique sunt
						in culpa qui officia deserunt mollitia animi, idum f labora dolor Ettum f
						labora. rerum facilis.
					</Text>
				</View>**/}


				





				{bonplandata && (
					<FlatList
						data={data}
						renderItem={({item, index}) => {
							return (
							
			     	<View style={styles.card}>
					<View style={styles.cardHeader}>
						<Text style={styles.textCardHeader}>{item.stitre}</Text>
					</View>
					<View style={styles.cardContent}>
					
				    	<Image
									source={
										item.urlImage
											? {uri: item.urlImage}
											: taxi
									}
									style={{width: 296, height: 185,borderRadius:9}}
									
								/>
						<Text style={styles.textCardContent}>{item.titre}</Text>
						<View style={{alignItems: "center", justifyContent: "center"}}>
							<Text
								style={[
									styles.textContent,
									{marginTop: 10, textAlign: "justify", width: 280},
								]}>
								{item.message}
							</Text>
						</View>
					</View>
					<View style={styles.cardFooter}>
						<TouchableOpacity style={styles.btnInvit} onPress={() => ShareMessage(item.url)}>
							<Text style={styles.textInvit}>J’EN PROFITE !</Text>
						</TouchableOpacity>
						
					</View>
				    	
				      </View>
							);
						}}
						keyExtractor={(item, index) => {
							return item.id;
						}}
						
						
					/>
				)}
			</ScrollView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		marginTop: Constants.statusBarHeight,
		backgroundColor: colors.TRANSPARENT,
	},
	headerDocsContainer: {
		backgroundColor: colors.VIOLET,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		height: "25%",
		paddingHorizontal: "8%",
		flexBasis: "30%",
	},

	headerText: {
		height: 22,
		color: colors.WHITE,
		fontFamily: "Montserrat-Bold",
		fontSize: 15,
		textTransform: "uppercase",
		lineHeight: 18,
	},

	scrollViewContainer: {
		backgroundColor: colors.WHITE,
		position: "absolute",
		width: "100%",
		height: "80%",
		top: "25%",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingHorizontal: "8%",
		paddingTop: 20,
		/* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
	},

	bigTitle: {
		textAlign: "center",
		color: colors.VIOLET,
		fontFamily: "Montserrat-Bold",
		fontSize: 20,
		lineHeight: 25,
	},

	input: {
		borderRadius: 10,
		color: colors.BLACK,
		borderWidth: 1,
		paddingVertical: 10,
		paddingHorizontal: 20,
		textAlign: "center",
		fontFamily: "Montserrat-Bold",
		fontSize: 30,
		lineHeight: 35,

		borderColor: "rgba(255,255,255,0.5)",
		backgroundColor: "white",
		shadowColor: "rgba(255,255,255,0.5)",
		elevation: 2,
	},

	textInvit: {
		fontSize: 18,
		fontFamily: "Montserrat-Bold",
		lineHeight: 22,
		textAlign: "center",
		color: colors.WHITE,
		paddingHorizontal: 55,
		paddingVertical: 15,
	},

	btnInvit: {
		justifyContent: "center",
		alignItems: "center",
		width: "100%",
		height: 50,
		marginTop: 5,
		borderRadius: 13,
		backgroundColor: colors.VIOLET,
	},
	textLabel: {
		fontSize: 18,
		lineHeight: 22,
		textAlign: "center",
		color: "#979797",
		fontFamily: "Montserrat-Bold",
	},
	textContent: {
		fontFamily: "Montserrat-Medium",
		fontSize: 12,
		textAlign: "justify",
		lineHeight: 15,
		color: "#848484",
	},
	separator: {
		height: 1,
		backgroundColor: "#979797",
		marginVertical: 15,
	},

	card: {
		height: 555,
		borderRadius: 12,
		backgroundColor: colors.WHITE,
		alignItems: "center",
		marginBottom: 60,

		borderColor: "rgba(255,255,255,0.5)",
		backgroundColor: "white",
		shadowColor: "rgba(255,255,255,0.5)",
		elevation: 2,
	},

	cardHeader: {
		width: "100%",
		height: 60,
		backgroundColor: colors.VIOLET,
		borderTopLeftRadius: 12,
		borderTopRightRadius: 12,
		alignItems: "center",
		justifyContent: "center",
	},

	textCardHeader: {
		fontSize: 20,
		fontFamily: "Montserrat-Bold",
		color: colors.WHITE,
		textAlign: "center",
		lineHeight: 25,
	},

	textCardContent: {
		textAlign: "center",
		color: colors.VIOLET,
		fontSize: 20,
		fontFamily: "Montserrat-Bold",
		marginTop: 30,
	},

	cardContent: {
		marginTop: 35,
		paddingHorizontal: 5,
		alignItems: "center",
	},

	cardFooter: {
		marginTop: 10,
		width: "80%",
		alignSelf: "center",
		alignItems: "center",
		justifyContent:"center",
	},
});

export default SponsorScreen;
