import React, {useState} from "react";
import {View, Text, StyleSheet, Image, ScrollView, TouchableOpacity} from "react-native";
import {Ionicons} from "@expo/vector-icons";

import {Entypo} from "@expo/vector-icons";

//colors
import {colors} from "../../../common/theme";

//Constants
import Constants from "expo-constants";

function Confidentiality({navigation}) {
	return (
		<View style={{flex: 1}}>
			<View style={styles.headerDocsContainer}>
				<TouchableOpacity onPress={() => navigation.goBack()}>
					<Ionicons name="ios-chevron-back" size={30} color={colors.WHITE} />
				</TouchableOpacity>
				<View>
					<Text style={styles.headerText}>Politique de confidentialité</Text>
				</View>
				<View>
					<Text></Text>
				</View>
			</View>
			<ScrollView style={styles.scrollViewContainer}>
				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>1. Généralités</Text>
				</View>
				<View style={styles.textContentContainer}>
					<Text style={styles.textContent}>
						All Moves Trades dont le siège social au 58, avenue Wagram 75017 Paris, en
						sa qualité de responsable du traitement, attache une grande importance à la
						protection et au respect de votre vie privée. La présente politique vise à
						vous informer de nos pratiques concernant la collecte, l’utilisation et le
						partage des informations que vous êtes amenés à nous fournir par le biais de
						notre plateforme (la « Plateforme ») accessible depuis le site internet ou
						nos applications mobiles Agogolines.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						Nous vous invitons à lire attentivement le présent document pour connaître
						et comprendre nos pratiques quant aux traitements de vos données
						personnelles que nous mettons en œuvre.
					</Text>
				</View>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>2. Informations recueillis</Text>
				</View>
				<View style={styles.textContentContainer}>
					<Text style={styles.textContent}>
						Nous sommes susceptibles de recueillir et de traiter les données suivantes :
					</Text>

					<Text
						style={[
							styles.textContent,
							{marginTop: 10, fontSize: 18, color: colors.VIOLET},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							2.1.{" "}
						</Text>
						Les informations que vous nous transmettez directement
					</Text>

					<Text style={[styles.textContent, {marginTop: 10}]}>
						En utilisant nos plateformes, vous êtes amenés à nous transmettre des
						informations, dont certaines sont de nature à vous identifier (« Données
						Personnelles »). C’est notamment le cas lorsque vous remplissez des
						formulaires (comme par exemple le formulaire d’inscription), Ces
						informations contiennent notamment les données suivantes :
					</Text>

					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.1.1.{" "}
						</Text>
						Les données nécessaires à l’inscription au service que nous fournissons sur
						nos plateformes ou à l’accès à tout autre service fournis par nous. Ces
						données sont notamment vos nom et prénom, adresse e-mail, date de naissance,
						sexe, numéro de téléphone et mot de passe. Ces informations sont
						obligatoires. A défaut, Agogolines ne sera pas en mesure de vous fournir les
						services proposés par nos plateformes et vous ne serez pas autorisés à créer
						de compte sur notre plateforme ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.1.2.{" "}
						</Text>
						Une copie de l’ensemble des réservations ou des publications effectuées sur
						nos plateformes ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.1.3.{" "}
						</Text>
						Le détail des opérations financières ou comptables effectuées sur nos
						plateformes ou par tout autre moyen, contenant notamment les informations
						relatives à votre carte de paiement, vos données bancaires, les informations
						relatives aux trajets réservés ou pour lesquels vous avez publié une annonce
						sur nos plateformes ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.1.4.{" "}
						</Text>
						Le détail de vos visites sur nos Plateformes et des contenus auxquels vous
						avez accédé ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.1.5.{" "}
						</Text>
						Le cas échéant, vos réponses à nos sondages et questionnaires et les avis
						que vous avez laissés pour évaluer un trajet effectué avec un autre membre
						de notre communauté, ces informations étant notamment susceptibles d’être
						utilisées dans le cadre de recherches et d’analyses du comportement
						utilisateur ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.1.6.{" "}
						</Text>
						Les données que nous pouvons vous demander de fournir lorsque vous nous
						signalez un problème relatif à nos plateformes ou à nos services.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.1.7.{" "}
						</Text>
						Les données liées à votre localisation lorsque vous avez accepté que nous
						collections et traitions ces données ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.1.8.{" "}
						</Text>
						Une copie de votre passeport, votre permis de conduite, votre carte
						d’identité ou tout autre document similaire que vous avez accepté de nous
						fournir ;
					</Text>

					<Text
						style={[
							styles.textContent,
							{marginTop: 10, fontSize: 18, color: colors.VIOLET},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							2.2.{" "}
						</Text>
						Les données que nous recueillons automatiquement
					</Text>

					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.2.1.{" "}
						</Text>
						Dans le cas où vous vous connectez à nos services en utilisant les
						fonctionnalités de réseaux sociaux mises à votre disposition, Agogolines
						aura accès à certaines des données (notamment, vos prénom, nom de famille,
						photographie, adresse e-mail et nombre d’amis Facebook) de votre compte sur
						ledit réseau social conformément aux conditions générales d’utilisation du
						réseau social concerné.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.2.2.{" "}
						</Text>
						Lors de chacune de vos visites, nous sommes susceptibles de recueillir,
						conformément à la législation applicable et avec votre accord, le cas
						échéant, des informations relatives aux appareils sur lesquels vous utilisez
						nos services ou aux réseaux depuis lesquels vous accédez à nos services.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						Parmi les technologies utilisées pour recueillir ces informations, nous
						avons notamment recours aux cookies (pour en savoir plus à ce sujet,
						veuillez vous référer à notre Charte sur les Cookies.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							2.2.3.{" "}
						</Text>
						Nous recueillons également des informations sur votre utilisation de notre
						plateforme (telles que le nombre d’annonces publiées, votre taux de réponse
						aux messages, votre date d’inscription, votre moyenne d’avis reçus, etc.)
						qui sont susceptibles d’être affichées sur votre profil public.
					</Text>
				</View>

				<View style={styles.textContentContainer}>
					<Text
						style={[
							styles.textContent,
							{marginTop: 10, fontSize: 18, color: colors.VIOLET},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							2.3.{" "}
						</Text>
						Durée de conservation de vos données
					</Text>

					<Text style={[styles.textContent, {marginTop: 10}]}>
						Agogolines ne conserve vos données que pour la durée nécessaire aux
						opérations pour lesquelles elles ont été collectées et ceci dans le respect
						de la législation en vigueur.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Entypo name="dot-single" size={24} color="black" />
						Les données financières (par exemple les paiements, remboursements, etc.)
						sont conservées pour la durée requise par les lois applicables en matière
						fiscale et comptable ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Entypo name="dot-single" size={24} color="black" />
						Les contenus créés par vous sur nos Plateformes (tels que les commentaires,
						avis et notation) sont rendus anonymes mais demeurent visibles sur nos
						Plateformes.
					</Text>
				</View>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>3. Utilisations des données recueillis</Text>
				</View>
				<Text style={[styles.textContent, {marginTop: 10}]}>
					Nous utilisons les données que nous recueillons afin de :
				</Text>

				<View style={styles.textContentContainer}>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.1.{" "}
						</Text>
						exécuter les contrats conclus entre vous et nous ou entre nous et nos
						partenaires commerciaux (par exemple en matière d’assurance) et vous fournir
						les informations et services demandés ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.2.{" "}
						</Text>
						vous envoyer des renseignements sur nos services (comme par exemple, les
						confirmations de réservation) par e-mail, SMS ou tout autre moyen de
						communication ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.3.{" "}
						</Text>
						percevoir vos paiements ou vous transmettre les sommes collectées pour votre
						compte ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.4.{" "}
						</Text>
						vous permettre de personnaliser votre profil sur nos plateformes ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.5.{" "}
						</Text>
						vous permettre de communiquer et d’échanger avec les autres membres de notre
						communauté sur la plateforme, notamment en ce qui concerne nos services ou
						le(s) trajets(s) déjà effectués ou que vous prévoyez d’effectuer ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.6.{" "}
						</Text>
						vous donner accès et vous permettre de communiquer avec notre service de
						relation ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.7.{" "}
						</Text>
						nous assurer du respect de la législation applicable, nos conditions
						générales d’utilisation notre politique de confidentialité. En cas de
						manquements de votre part, nous pourrons être amenés à suspendre votre
						compte sur nos plateformes ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.8.{" "}
						</Text>
						vous envoyer, conformément aux dispositions légales applicables et avec
						votre accord lorsque la législation l’exige, des messages marketing,
						publicitaires et promotionnels et des informations relatives à l’utilisation
						de nos services, aux modalités de réservation, ou vous suggérer et vous
						conseiller des biens ou des services susceptibles de vous intéresser. Nous
						sommes également susceptibles d’utiliser vos données pour vous adresser des
						messages publicitaires susceptibles de vous intéresser sur les plateformes
						de réseaux sociaux ou sites de tiers.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.9.{" "}
						</Text>
						afin de vous informer des modifications apportées à nos services ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.10.{" "}
						</Text>
						vérifier les informations contenues dans votre passeport, permis de
						conduire, carte d’identité ou tout autre document d’identité que nous
						aurions collecté lors de votre inscription ou votre utilisation de nos
						plateformes ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.11.{" "}
						</Text>
						gérer nos plateformes et effectuer des opérations techniques internes dans
						le cadre de résolution de problèmes, analyse de données, de tests,
						recherches, analyses, études et sondages ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.12.{" "}
						</Text>
						améliorer et optimiser nos plateformes, notamment pour nous assurer de ce
						que l’affichage de nos contenus est adapté à votre appareil ;
					</Text>
				</View>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>
						4. Destinataires des informations des recueillis et les conditions de leur
						transmission
					</Text>
				</View>
				<View style={styles.textContentContainer}>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.1.{" "}
						</Text>
						Dans le cadre de l’utilisation de nos services, certaines de vos
						informations sont transmises aux autres membres.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.2.{" "}
						</Text>
						Nous sommes également susceptibles de partager des informations vous
						concernant, notamment des données personnelles, avec d’autres entités dans
						le cadre prévu par la présente politique.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.3.{" "}
						</Text>
						Nous travaillons également en étroite collaboration avec des entreprises
						tierces qui peuvent avoir accès à vos données personnelles, et notamment
						avec :
					</Text>

					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Entypo name="dot-single" size={24} color="black" />
						les plateformes de réseaux sociaux qui peuvent vous proposer des
						fonctionnalités vous permettant d’intégrer dans votre profil Agogolines des
						informations issues de votre compte sur leur propre plateforme ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Entypo name="dot-single" size={24} color="black" />
						les partenaires commerciaux qui font la promotion de leurs services sur nos
						plateformes et auxquels vous pouvez décider de souscrire. Ces services
						peuvent notamment consister en des services d’assurance, des services
						bancaires, des services de location de véhicules, etc. ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Entypo name="dot-single" size={24} color="black" />
						les partenaires assureurs ou intermédiaires en assurance afin de confirmer
						votre éligibilité aux programmes d’assurance, vous communiquer un prix,
						permettre votre souscription aux contrats d’assurance proposés et la gestion
						de vos sinistres le cas échéant et répondre à leurs obligations légales et
						réglementaires ;
					</Text>

					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.{" "}
						</Text>
						Nous ne partageons vos données avec les tiers mentionnés au 4.3. Ci-dessus
						que dans les cas suivants :
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.1.{" "}
						</Text>
						Lorsque nous faisons appel à un prestataire de services dans le cadre de
						l’exécution de tout contrat conclu entre vous et nous ou afin de fournir ou
						améliorer nos services (par exemple dans le cadre des paiements effectués
						via nos plateformes) ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.2.{" "}
						</Text>
						Dans le cadre du processus de réservation, nous sommes amenés, afin de
						fournir les services demandés, à afficher sur nos plateformes ou à
						transmettre à un autre membre certaines de vos données personnelles telles
						que vos prénom, photographie, numéro de téléphone portable ou adresse e-mail
						;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.3.{" "}
						</Text>
						Nous publions les avis que vous rédigez dans le cadre de notre système
						d’avis sur nos plateformes. Ces avis, qui contiennent votre prénom et la
						première lettre de votre nom ainsi que votre photographie sont visibles par
						tous les visiteurs de nos plateformes ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.4.{" "}
						</Text>
						Lorsque nous faisons appel à des fournisseurs de moteurs de recherche et de
						solutions analytiques pour améliorer et optimiser nos plateformes ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.5.{" "}
						</Text>
						Lorsque vous en faites la demande expresse (par exemple lorsque vous
						utilisez les méthodes d’authentification fournies par les réseaux sociaux),
						souscrivez à un service fourni par un de nos partenaires, demandez à
						bénéficier d’une offre ou d’un bon plan d’un de nos partenaires) ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.6.{" "}
						</Text>
						Lorsque nous avons l’obligation légale de le faire ou si nous pensons de
						bonne foi que cela est nécessaire pour répondre à toute réclamation à
						l’encontre de Agogolines. Se conformer à toute demande judiciaire, faire
						exécuter tout contrat conclu avec nos membres, tel que les Conditions
						Générales d’Utilisation et la présente Politique de Confidentialité.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.7.{" "}
						</Text>
						Dans l’hypothèse où nous vendrions ou acquérions une entreprise ou des
						actifs, auquel cas nous nous réservons la possibilité de partager vos
						Données Personnelles avec le potentiel vendeur ou acheteur de cette
						entreprise ou de ces actifs
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.8.{" "}
						</Text>
						Si Agogolines ou tout ou partie de ses actifs sont rachetés par un tiers,
						les données en notre possession seront, le cas échéant, transférées au
						nouveau propriétaire.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.9.{" "}
						</Text>
						Conformément à la législation applicable et avec votre consentement
						lorsqu’il est requis, nous pouvons agréger des données qui vous concernent
						et que nous recevons ou envoyons à nos partenaires commerciaux, notamment
						tout ou partie de vos données personnelles et les informations collectées
						par l’intermédiaire de cookies. Ces informations agrégées ne seront
						utilisées que pour les finalités décrites ci-dessus.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.4.10.{" "}
						</Text>
						Nous attirons votre attention sur le fait que si vous décidez de nous
						laisser accéder à certaines de vos informations, notamment à vos données
						personnelles, par l’intermédiaire de services de connexion mis à disposition
						par nos partenaires commerciaux, leurs politiques de confidentialité vous
						est également opposable. Nous n’avons aucun contrôle sur la collecte ou le
						traitement de vos données mis en œuvre par nos partenaires commerciaux sur
						leur propre plateforme.
					</Text>
				</View>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>5. Utilisations de vos messages</Text>
				</View>

				<View style={styles.textContentContainer}>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							5.1.{" "}
						</Text>
						Nous pouvons prendre connaissance des messages que vous échangez avec
						d’autres membres de notre communauté via nos plateformes notamment à des
						fins de prévention des fraudes, d’amélioration de nos services, d’assistance
						utilisateur, de vérification du respect par nos membres des contrats conclus
						avec nous et notamment de nos Conditions Générales d’Utilisation.
					</Text>
				</View>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>6. Publicité ciblée, e-mails et SMS</Text>
				</View>

				<View style={styles.textContentContainer}>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						Conformément à la législation applicable et avec votre consentement
						lorsqu’il est requis, nous pourrons utiliser les données que vous nous
						fournissez sur nos plateformes à des fins de prospection commerciale (par
						exemple pour vous adresser nos newsletters, vous envoyer des invitations à
						nos événements ou toute autre communication susceptible de vous intéresser
						et afficher des publicités ciblées sur les plateformes de médias sociaux ou
						sites tiers).
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							6.1.{" "}
						</Text>
						Vous pouvez à tout moment retirer votre consentement en décochant la case
						afférente dans votre compte, cliquant sur le lien de désinscription fourni
						dans chacune de nos communications ou en nous contactant selon les modalités
						décrites ci-dessous.
					</Text>
				</View>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>7. Transfères</Text>
				</View>
				<Text style={[styles.textContent, {marginTop: 10}]}>
					Les données personnelles sont conservées au sein de l’Union Européenne. Sur
					simple demande formulée de votre, nous pouvons vous fournir davantage
					d’informations quant à ces transferts sur www.agogolines.com (notamment les
					clauses contractuelles types de la Commission Européenne).
				</Text>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>8. Vos droits sur vos données personnelles</Text>
				</View>

				<View style={styles.textContentContainer}>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							8.1.{" "}
						</Text>
						Vous pouvez également demander l’effacement de vos données personnelles
						ainsi que la rectification des données personnelles erronées ou obsolètes («
						droit à l’oubli et droit de rectification »). Veuillez noter que nous sommes
						susceptibles de conserver certaines informations vous concernant lorsque la
						loi nous l’impose ou lorsque nous avons un motif légitime de le faire. C’est
						par exemple le cas, si nous estimons que vous avez commis une fraude ou
						violé nos Conditions Générales d’Utilisation et que nous souhaitons éviter
						que vous contourniez les règles applicables à notre communauté.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							8.2.{" "}
						</Text>
						Vous disposez également du droit de vous opposer à tout moment pour des
						raisons tenant à sa situation particulière, au traitement de vos données
						Personnelles. (« droit d’opposition »).
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							8.3.{" "}
						</Text>
						Du droit à la limitation du traitement de vos données à caractère personnel
						si vous considérez que le traitement est illicite ou excessif (« droit à la
						limitation du traitement »). Ce droit n’est pas absolu,
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							8.4.{" "}
						</Text>
						Vous disposez du droit d’introduire une réclamation auprès de l’autorité de
						contrôle compétente ou d’obtenir réparation auprès des tribunaux compétents
						si vous considérez nous n’avons pas respecté vos droits.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							8.5.{" "}
						</Text>
						Vous disposez également du droit à la portabilité de vos données,
						c’est-à-dire au droit de recevoir les données personnelles que vous nous
						avez fournis dans un format structuré, couramment utilisé et lisible par la
						machine et le droit de transmettre ces données à un autre responsable du
						traitement (« droit à la portabilité »).
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							8.6.{" "}
						</Text>
						Vous disposez également du droit de définir des directives relatives au sort
						de vos données personnelles après votre mort.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							8.7.{" "}
						</Text>
						Pour exercer ces droits, vous pouvez nous contacter à l’adresse suivante :
						contact@agogolines.com
					</Text>
				</View>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>9. Cookies et technologies semblables</Text>
				</View>
				<Text style={[styles.textContent, {marginTop: 10}]}>
					Pour en savoir plus, consultez notre charte des Cookies
				</Text>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>10. Confidentialité de votre mot de passe</Text>
				</View>
				<Text style={[styles.textContent, {marginTop: 10}]}>
					Vous êtes responsable de la confidentialité du mot de passe que nous vous avez
					choisi pour accéder à votre compte sur nos plateformes.
				</Text>
				<Text style={[styles.textContent, {marginTop: 10}]}>
					Vous vous engagez à conserver ce mot de passe secret et à ne le communiquer à
					personne.
				</Text>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>
						11. Liens vers d’autres sites internet et réseaux sociaux
					</Text>
				</View>
				<Text style={[styles.textContent, {marginTop: 10}]}>
					Nos plateformes peuvent occasionnellement contenir des liens vers les sites
					internet de nos partenaires ou de sociétés tierces. Veuillez noter que ces sites
					internet ont leur propre politique de confidentialité et que nous déclinons
					toute responsabilité quant à l’utilisation faite par ces sites des informations
					collectées lorsque vous cliquez sur ces liens.
				</Text>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>
						12. Modification de notre politique de confidentialité
					</Text>
				</View>
				<Text style={[styles.textContent, {marginTop: 10}]}>
					Nous pouvons être amené à modifier occasionnellement la présente politique de
					confidentialité. Lorsque cela est nécessaire, nous vous en informerons et/ou
					solliciterons votre accord. Nous vous conseillons de consulter régulièrement
					cette page pour prendre connaissance des éventuelles modifications ou mises à
					jour apportées à notre politique de confidentialité.
				</Text>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>12. Contact</Text>
				</View>
				<Text style={[styles.textContent, {marginTop: 10, marginBottom: 50}]}>
					Pour toute question relative à la présente politique de confidentialité ou pour
					toute demande relative à vos données personnelles, vous pouvez nous contacter en
					nous adressant un email à l’adresse contact@agogolines.com ou en nous adressant
					un courrier à l’adresse suivante : All Moves Trades 58, Avenue de Wagram75017
					Paris.
				</Text>
			</ScrollView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		marginTop: Constants.statusBarHeight,
		backgroundColor: colors.TRANSPARENT,
	},
	headerDocsContainer: {
		backgroundColor: colors.VIOLET,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		height: "25%",
		paddingHorizontal: "8%",
		flexBasis: "30%",
	},

	headerText: {
		height: 22,
		color: colors.WHITE,
		fontFamily: "Montserrat-Bold",
		fontSize: 15,
		textTransform: "uppercase",
		lineHeight: 18,
	},

	scrollViewContainer: {
		backgroundColor: colors.WHITE,
		position: "absolute",
		width: "100%",
		height: "80%",
		top: "25%",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingHorizontal: "8%",
		paddingTop: 20,
		/* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
	},

	fistTitleContainer: {
		marginTop: 10,
	},

	fistTitle: {
		fontSize: 20,
		fontFamily: "Montserrat-Bold",
		color: colors.VIOLET,
		lineHeight: 24,
	},

	textContentContainer: {
		marginTop: 10,
	},

	textContent: {
		fontFamily: "Montserrat-Medium",
		fontSize: 15,
		textAlign: "justify",
		lineHeight: 15,
		color: "#000",
	},
	videoContainer: {
		alignItems: "center",
		justifyContent: "center",
		marginTop: 20,
		backgroundColor: colors.BLACK,
		height: 215,
	},
	imgPlay: {
		color: colors.WHITE,
	},
	subTitle: {
		fontSize: 15,
		lineHeight: 20,
		textAlign: "left",
		color: colors.BLACK,
		fontFamily: "Montserrat-Bold",
		marginTop: 30,
	},
});

export default Confidentiality;
