import { Ionicons } from "@expo/vector-icons";
//Constants
import Constants from "expo-constants";
import React from "react";
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
//colors
import { colors } from "../../../common/theme";



function CGUCGV({navigation}) {
	return (
		<View style={{flex: 1}}>
			<View style={styles.headerDocsContainer}>
				<TouchableOpacity onPress={() => navigation.goBack()}>
					<Ionicons name="ios-chevron-back" size={30} color={colors.WHITE} />
				</TouchableOpacity>
				<Text style={styles.headerText}>CGU / CGV</Text>
				<View>
					<Text></Text>
				</View>
			</View>
			<ScrollView style={styles.scrollViewContainer}>
				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>1. Objet</Text>
				</View>
				<View style={styles.textContentContainer}>
					<Text style={styles.textContent}>
						Le présent contrat constitue un contrat d’adhésion au service de la
						plateforme All Moves Trade (ci-après, « Agogolines »)
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						La société All Moves Trade domiciliée au 58, avenue de Wagram 75017 Paris
						avec un capital de 1600 euros est une plateforme de covoiturage urbain et
						interurbain accessible sur un site internet notamment à l’adresse
						www.agogolines.com ou sous forme d’application mobile et destinée à mettre
						en relation des conducteurs se rendant à une destination donnée pour leur
						propre compte avec des passagers allant dans la même direction afin de leur
						permettre de partager le trajet.
					</Text>
				</View>

				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>2. Définitions</Text>
				</View>
				<View style={styles.textContentContainer}>
					<Text style={styles.textContent}>Dans les présentes,</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Annonce » :{" "}
						</Text>
						désigne indistinctement une annonce de covoiturage urbain ou une annonce de
						covoiturage interurbain ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							«Annonce de Covoiturage» :{" "}
						</Text>
						désigne une annonce concernant un trajet posté sur la plateforme par un
						conducteur ;
					</Text>

					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« All Moves Trade » :{" "}
						</Text>
						à la signification qui lui est donnée à l’article 1 ci-dessus ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« CGU » :{" "}
						</Text>
						désigne les présentes Conditions Générales d’Utilisation ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Compte » :{" "}
						</Text>
						désigne le compte qui doit être créé pour pouvoir devenir Membre et accéder
						à certains services proposés par la plateforme ;
					</Text>

					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Compte Facebook » :{" "}
						</Text>
						à la signification qui lui est donné à l’article 3.2 ci-dessous ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Confirmation de Réservation » :{" "}
						</Text>
						à la signification qui lui est donnée à l’article 4.2.1 ci- dessous ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Contenu Membre » :{" "}
						</Text>
						à la signification qui lui est donnée à l’article 11.2 ci-dessous ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Frais de Service » :{" "}
						</Text>
						à la signification qui lui est donnée à l’article 5.2 ci-dessous ;
					</Text>

					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Membre » :{" "}
						</Text>
						désigne toute personne physique ayant créé un compte sur la plateforme ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Passager » :{" "}
						</Text>
						désigne le membre ayant accepté la proposition d’être transporté par le
						Conducteur ou, le cas échéant, la personne pour le compte de laquelle un
						Membre a réservé une Place ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Participation aux Frais » :{" "}
						</Text>
						désigne, pour un trajet en covoiturage donné, la somme d’argent demandée par
						le Conducteur et acceptée par le Passager au titre de sa participation aux
						frais de déplacement ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Place » :{" "}
						</Text>
						désigne la place réservée par un passager à bord du véhicule d’un conducteur
						;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Plateforme » :{" "}
						</Text>
						à le sens qui lui est donné à l’article 1, ci-dessus ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Réservation » :{" "}
						</Text>
						à le sens qui lui est donné à l’article 4.2.1 ci-dessous ;
					</Text>

					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Services » :{" "}
						</Text>
						désigne l’ensemble des services rendus par « Agogolines » par
						l’intermédiaire de la plateforme ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Site » :{" "}
						</Text>
						désigne le site internet accessible à l’adresse www.agogolines.com;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Sous-Trajet » :{" "}
						</Text>
						à le sens qui lui est donné à l’article 4.1 ci-dessous ;
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Trajet » :{" "}
						</Text>
						désigne indistinctement un trajet en covoiturage
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							« Covoiturage » :{" "}
						</Text>
						désigne le trajet faisant l’objet d’une annonce faite par le conducteur et
						acceptée par le passager ;
					</Text>
				</View>
				<View style={styles.fistTitleContainer}>
					<Text style={styles.fistTitle}>
						3. Inscription à la Plateforme et création de Compte
					</Text>
				</View>
				<Text
					style={[
						styles.textContent,
						{marginTop: 10, fontSize: 18, color: colors.VIOLET},
					]}>
					<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>3.1. </Text>
					Conditions d’inscription à la Plateforme
				</Text>
				<View style={styles.textContentContainer}>
					<Text style={styles.textContent}>
						L’utilisation de plateforme est réservée aux personnes physiques âgées de 18
						ans ou plus. Toute inscription sur la Plateforme par une personne mineure
						est strictement interdite. En accédant, utilisant ou vous inscrivant sur la
						plateforme, vous déclarez et garantissez avoir 18 ans ou plus.
					</Text>
				</View>
				<Text
					style={[
						styles.textContent,
						{marginTop: 10, fontSize: 18, color: colors.VIOLET},
					]}>
					<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>3.2. </Text>
					Création de Compte
				</Text>
				<View style={styles.textContentContainer}>
					<Text style={styles.textContent}>
						Pour créer votre Compte, vous devez remplir l’ensemble des champs
						obligatoires figurant sur le formulaire d’inscription et avoir lu et
						accepter les présentes CGU.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						A l’occasion de la création de votre compte, vous vous engagez à fournir des
						informations personnelles exactes et conformes à la réalité et à les mettre
						à jour, par l’intermédiaire de votre profil ou en en avertissant Agogolines,
						afin d’en garantir la pertinence et l’exactitude tout au long de votre
						relation contractuelle avec Agogolines.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						Vous vous engagez à ne pas partager votre compte avec un tiers. A ce titre,
						vous êtes seul responsable de l’utilisation faite de votre Compte par un
						tiers, tant que vous n’avez pas expressément notifié Agogolines
						l’utilisation frauduleuse par un tiers de votre compte.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						Vous vous engagez à ne pas créer ou utiliser, sous votre propre identité ou
						celle d’un tiers, d’autres comptes que celui initialement créé.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						Agogolines, à des fins de transparence, d'amélioration de la confiance, ou
						de préservation ou détection des fraudes, se réserve le droit de demander à
						ses membres l'envoi d'une photo de pièce d’identité (ou de mettre en place
						un système de vérification de certaines des informations que les membres
						fournissent sur leur profil tels que le numéro de téléphone ou une pièce
						d'identité).
					</Text>

					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							3.3.{" "}
						</Text>
						Vérification
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Agogolines peut, à des fins de transparence, d’amélioration de la
							confiance, ou de prévention ou détection des fraudes, mettre en place un
							système de vérification de certaines des informations que vous
							fournissez sur votre profil.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Vous reconnaissez et acceptez que toute référence sur la plateforme ou
							les Services à des informations dites « vérifiées » ou tout terme
							similaire, signifie uniquement qu’un membre a réussi avec succès la
							procédure de vérification existante sur la plateforme ou les services
							afin de vous fournir davantage d’informations sur le membre avec lequel
							vous envisagez de voyager. « Agogolines » ne garantit ni la véracité, ni
							la fiabilité, ni la validité de l’information ayant fait l’objet de la
							procédure de vérification.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>4. Utilisation des Services</Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.1.{" "}
						</Text>
						Publication des Annonces de Covoiturage
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							En tant que membre, vous pouvez créer et publier des Annonces de
							covoiturage urbain et interurbain sur la plateforme en indiquant des
							informations quant au trajet en covoiturage que vous comptez effectuer
							(dates/heures et lieux de départ et d’arrivée, nombre de places
							offertes, options proposées, montant de la participation aux Frais,
							etc.).
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Lors de la publication de votre annonce de covoiturage interurbain, vous
							pouvez indiquer des villes étapes, dans lesquelles vous acceptez de vous
							arrêter pour prendre ou déposer des passagers. Les tronçons du trajet en
							covoiturage entre ces villes étapes ou entre l’une de ces villes étapes
							et le point de départ ou d’arrivée du trajet en covoiturage constituent
							des « Sous-trajets ».
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Vous n’êtes autorisé à publier une annonce de covoiturage que si vous
							remplissez l’ensemble des conditions suivantes :
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								a){" "}
							</Text>
							vous êtes titulaire d’un permis de conduire valide d’au moins 2 ans
						</Text>
						<Text style={[styles.textContent]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								b){" "}
							</Text>
							vous ne proposez des annonces de covoiturage urbain et interurbain que
							pour des véhicules dont vous êtes le propriétaire ou que vous utilisez
							avec l’autorisation expresse du propriétaire;
						</Text>
						<Text style={[styles.textContent]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								c){" "}
							</Text>
							vous êtes et demeurez le conducteur principal du véhicule, objet de
							l’annonce de covoiturage ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								d){" "}
							</Text>
							le véhicule bénéficie d’une assurance au tiers valide ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								e){" "}
							</Text>
							vous n’avez aucune contre-indication ou incapacité médicale à conduire ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								f){" "}
							</Text>
							le véhicule que vous comptez utiliser pour le trajet est une voiture de
							tourisme à 4 roues, disposant d’un maximum de 7 places assises, à
							l’exclusion des voitures dites « sans permis » ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								g){" "}
							</Text>
							vous ne comptez pas publier une autre annonce pour le même trajet en
							Covoiturage sur la plateforme ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								h){" "}
							</Text>
							toutes les places offertes ont une ceinture de sécurité, et ce même si
							le véhicule est homologué en présence de sièges dépourvus de ceinture de
							sécurité ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								i){" "}
							</Text>
							vous utilisez un véhicule en parfait état de fonctionnement et conforme
							aux usages et dispositions légales applicables, notamment avec un
							contrôle technique à jour ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								j){" "}
							</Text>
							vous êtes un consommateur et n’agissez pas à titre professionnel.
						</Text>
					</View>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						Vous êtes seul responsable du contenu de l’Annonce de covoiturage que vous
						publiez sur la plateforme. En conséquence, vous déclarez et garantissez
						l’exactitude et la véracité de toute information contenue dans votre annonce
						de covoiturage et vous engagez à effectuer le trajet en covoiturage selon
						les modalités décrites dans votre annonce de covoiturage.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						Agogolines se réserve la possibilité, à sa seule discrétion et sans préavis,
						de ne pas publier ou retirer, à tout moment, toute annonce de covoiturage
						qui ne serait pas conforme aux CGU ou qu’elle considérerait comme
						préjudiciable à son image, celle de la Plateforme ou celle des Services.
					</Text>

					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.2.{" "}
						</Text>
						Réservation d’une Place
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Nous mettons en ligne un système de réservation de places la «
							Réservation ».
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Lorsqu’un Passager est intéressé par une annonce de covoiturage, il peut
							effectuer une demande de réservation en ligne. Au moment de la
							réservation, le passager procède au paiement en ligne du montant de la
							participation aux frais et des frais de service afférents, le cas
							échéant. Après vérification du paiement par Agogolines et validation de
							la demande de Réservation par le conducteur, le cas échéant, le Passager
							reçoit une confirmation de réservation la Confirmation de Réservation.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Si vous êtes un Conducteur et que vous avez choisi de gérer vous-mêmes
							les demandes de Réservation lors de la publication de votre annonce de
							covoiturage, vous êtes tenu de répondre à toute demande de réservation
							dans un certain délai. A défaut, la demande de réservation expire
							automatiquement et le passager est remboursé de l’intégralité des sommes
							versées au moment de la demande de Réservation, le cas échéant
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							A compter de la confirmation de la réservation, Agogolines vous transmet
							les coordonnées téléphoniques du conducteur (si vous êtes passager) ou
							du passager (si vous êtes conducteur), dans le cas où le membre a donné
							son accord à la divulgation de son numéro de téléphone. Vous êtes
							désormais seuls responsables de l’exécution du contrat vous liant à
							l’autre membre.
						</Text>
					</View>

					<Text style={[styles.textContent, {marginTop: 10, color: colors.VIOLET}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							4.2.2.{" "}
						</Text>
						Réservations
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Le conducteur comme le passager doit correspondre à l’identité
							communiquée à Agogolines et aux autres membres participant au trajet de
							covoiturage urbain et interurbain ;
						</Text>
						<Text style={styles.textContent}>
							Toutefois, Agogolines permet à ses membres de réserver une ou plusieurs
							places pour le compte d’un tiers. Dans ce cas, vous vous engagez à
							indiquer avec exactitude au conducteur, au moment de la réservation, les
							prénoms, âge et numéro de téléphone de la personne pour le compte de
							laquelle vous réservez une place. Dans le cas où vous réservez une place
							pour un trajet en covoiturage pour un mineur voyageant seul âgé de plus
							de 13 ans, vous vous engagez à demander l’accord préalable du conducteur
							et à lui fournir une autorisation des représentants légaux dûment
							remplie et signée.
						</Text>
						<Text style={styles.textContent}>
							Par ailleurs, il est interdit de publier une annonce de covoiturage pour
							un conducteur autre que vous-même.
						</Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							4.3.{" "}
						</Text>
						Système d’avis
					</Text>
					<Text style={[styles.textContent, {marginTop: 10, color: colors.VIOLET}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							4.3.1.{" "}
						</Text>
						Fonctionnement
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Votre avis, ainsi que celui laissé par un autre membre à votre égard, le
							cas échéant, ne sont visibles et publiés sur la plateforme qu’après le
							plus court des délais suivants : immédiatement après que vous ayez, tous
							les deux, laissés un avis ou passé un délai de 14 jours après le premier
							avis laissé.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Vous pouvez répondre à l’avis qu’un autre membre a laissé sur votre
							profil dans un délai maximum de 14 jours suivant la publication de
							l’avis laissé à votre égard. L’avis et votre réponse, le cas échéant,
							seront publiés sur votre profil.
						</Text>
					</View>

					<Text style={[styles.textContent, {marginTop: 10, color: colors.VIOLET}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							4.3.2.{" "}
						</Text>
						Modération
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Agogolines se réserve la possibilité de ne pas publier ou supprimer tout
							avis, toute question, tout commentaire ou toute réponse dont elle
							jugerait le contenu contraire aux présentes CGU.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Agogolines se réserve la possibilité de suspendre votre compte, limiter
							votre accès aux Services ou résilier les présentes CGU dans le cas où
							vous avez reçu au moins trois avis et la moyenne des avis que vous avez
							reçus est égale ou inférieure à 2.5.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>5. Conditions financières</Text>
					</View>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							L’accès et l’inscription à la plateforme, de même que la recherche, la
							consultation et la publication d’annonces sont gratuits. En revanche, la
							réservation est payante dans les conditions décrites ci-dessous.
						</Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>5.1 </Text>
						Participation aux Frais et Prix
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Dans le cadre d’un trajet en covoiturage interurbain le montant de la
							participation aux frais est déterminé par vous, en tant que conducteur.
							Par conséquent, vous vous engagez à limiter le montant de la
							participation aux frais que vous demandez à vos passagers.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Dans le cadre d’un trajet en covoiturage urbain vous autorisé Agogolines
							à fixer le prix des trajets à votre place. Par conséquent Agogolines se
							base sur le prix du barème fiscal kilométrique applicable.
						</Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>5.2 </Text>
						Frais de Service
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Agogolines peut prélever, en contrepartie de l’utilisation de la
							plateforme, des frais de service à ses utilisateurs (ci après, les
							“Frais de Service”).
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Agogolines se réserve le droit de modifier à tout moment les modalités
							de calcul des Frais de Service. Ces modifications n’auront pas d’effet
							sur les frais de service acceptés par les utilisateurs avant la date de
							prise d’effet de ces modifications.
						</Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>5.3 </Text>
						Arrondis
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Vous reconnaissez et acceptez que Agogolines peut, à son entière
							discrétion, arrondir au chiffre inférieur ou supérieur les Frais de
							Service et la participation aux frais.
						</Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>5.4 </Text>
						Paiement et de reversement de la Participation aux Frais au Conducteur.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10, color: colors.VIOLET}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							5.4.1.{" "}
						</Text>
						Mandat d’encaissement
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Vous confiez à Agogolines un mandat d’encaissement du montant de la
							participation aux frais en votre nom et pour votre compte.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Par conséquent, dans le cadre d’un trajet en covoiturage urbain ou
							interurbain et après acceptation manuelle ou automatique de la
							réservation, Agogolines encaisse la totalité de la somme versée par le
							passager (frais de service et participation aux frais).
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Vous reconnaissez et acceptez qu’aucune des sommes perçues par
							Agogolines au nom et pour le compte du conducteur n’emporte droit à
							intérêts. Vous acceptez de répondre avec diligences à toute demande de
							Agogolines et plus généralement de toute autorité administrative ou
							judiciaire.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							En l’absence de réponse de votre part à ces demandes, nous pouvons
							procéder la suspension de votre Compte et/ou la résiliation des
							présentes CGU.
						</Text>
					</View>
					<Text style={[styles.textContent, {marginTop: 10, color: colors.VIOLET}]}>
						<Text style={[styles.fistTitle, {fontSize: 15, lineHeight: 16}]}>
							5.4.2.{" "}
						</Text>
						Versement de la Participation aux Frais au Conducteur
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							A la suite du trajet en covoiturage, les passagers disposent d’un délai
							de 24 heures pour présenter une réclamation à Agogolines. En l’absence
							de contestation de leur part dans cette période, Agogolines considère la
							confirmation du trajet comme acquise.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Lorsque le trajet en covoiturage est confirmé par le passager, vous avez
							la possibilité, en tant que conducteur, de donner instructions à
							Agogolines de vous verser l’argent sur votre compte bancaire (en
							renseignant sur votre Compte, au préalable, vos coordonnées bancaires)
							ou votre compte Paypal (en renseignant sur votre compte, au préalable,
							votre adresse e-mail Paypal).
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							L’ordre de virement à votre nom sera transmis suivant votre demande ou à
							défaut de demande de votre part dans un délai allant de 1jours à 15
							jours (sous réserve que Agogolines dispose de vos informations
							bancaires).
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							A l’issue du délai de prescription de 5 ans applicable, toute somme non
							réclamée à Agogolines sera réputée appartenir à Agogolines.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>
							6. Finalité non commerciale et non professionnelle des Services et de la
							Plateforme
						</Text>
					</View>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Vous vous engagez à n’utiliser les services et la plateforme que pour
							être mis en relation, avec des personnes souhaitant partager un trajet
							en covoiturage avec vous.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							En tant que conducteur, vous vous engagez à ne pas demander une
							participation aux frais supérieure aux frais que vous supportez
							réellement et susceptible de vous faire générer un bénéfice, étant
							précisé que s’agissant d’un partage de frais, vous devez également, en
							tant que conducteur, supporter votre part des coûts afférents au trajet
							en covoiturage. Vous êtes seul responsable d’effectuer le calcul des
							frais que vous supportez pour le trajet en covoiturage, et de vous
							assurer que le montant demandé à vos passagers n’excède pas les frais
							que vous supportez réellement (en excluant votre part de participation
							aux frais), notamment en vous référant au barème fiscal kilométrique
							forfaitaire applicable.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Vous vous engagez à fournir à Agogolines, sur simple demande de la part
							de celle-ci, une copie de votre carte grise et/ou tout autre document de
							nature à attester que vous êtes autorisé à utiliser ce véhicule sur la
							plateforme et n’en tirez aucun bénéfice.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Agogolines se réserve également la possibilité de suspendre votre
							compte, limiter votre accès aux Services ou résilier les présentes CGU
							en cas d’activité de votre part sur la plateforme.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>7. Annulation et remboursement </Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>7.1 </Text>
						Modalités de remboursement en cas d’annulation
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Les trajets en covoiturage font l’objet de la présente politique
							d’annulation, Agogolines n’offrant aucune garantie, de quelque nature
							que ce soit, en cas d’annulation, pour quelque raison que ce soit.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							L’annulation d’une place d’un trajet en covoiturage par le conducteur ou
							le passager après la confirmation de réservation est soumise aux
							stipulations ci-après:
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							- En cas d’annulation imputable au conducteur, le passager est remboursé
							de la totalité de la somme qu’il a versée. C’est notamment le cas
							lorsque le conducteur annule un trajet en covoiturage ou ne se rend pas
							au point de rendez-vous au plus tard 15 minutes après l’horaire convenu
							;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							- En cas d’annulation imputable au Passager :
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								a){" "}
							</Text>
							Si le passager annule plus de 24 heures avant l’heure prévue pour le
							départ telle que mentionnée dans l’annonce de covoiturage, le passager
							est remboursé du montant de la participation aux frais. Les frais de
							service demeurent acquis à Agogolines et le conducteur ne reçoit aucune
							somme de quelque nature que ce soit ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								b){" "}
							</Text>
							Si le passager annule moins de 24 heures ou 24 heures avant l’heure
							prévue pour le départ, telle que mentionnée dans l’annonce de
							covoiturage et plus de trente minutes après la confirmation de
							réservation, le passager ne sera pas remboursé de la participation aux
							frais versée lors de la réservation, les frais de service demeurent
							acquis à Agogolines et le conducteur reçoit la totalité de la
							participation aux frais ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								c){" "}
							</Text>
							Si le passager annule après l’heure prévue pour le départ, telle que
							mentionnée dans l’annonce, ou s’il ne se présente pas au lieu de
							rendez-vous au plus tard dans un délai de 15 minutes à compter de
							l’heure convenue, aucun remboursement n’est effectué. Le conducteur est
							dédommagé à hauteur de la totalité de la participation aux frais et les
							Frais de Services sont conservés par Agogolines.
						</Text>
					</View>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						Lorsque l’annulation intervient avant le départ et du fait du passager, la
						ou les places annulé(e)s par le passager pour ledit trajet en covoiturage
						sont de plein droit remises à la disposition d’autres passagers pouvant les
						réserver en ligne et en conséquence soumises aux conditions des présentes
						CGU.
					</Text>
					<Text style={[styles.textContent, {marginTop: 10}]}>
						Agogolines apprécie à sa seule discrétion, sur la base des éléments à sa
						disposition, la légitimité des demandes de remboursement qu’elle reçoit.
					</Text>

					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>7.2 </Text>
						Rétraction
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							En acceptant les présentes CGU, vous acceptez expressément que le
							contrat entre vous et Agogolines consistant en la mise en relation avec
							un autre Membre soit exécuté avant l’expiration du délai de rétractation
							dès la confirmation de réservation et renoncez expressément à votre
							droit de rétraction, conformément aux dispositions de l’article L.221-
							28 du Code de la consommation.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>
							8. Comportement des utilisateurs de la Plateforme et Membres
						</Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							8.1.{" "}
						</Text>
						Engagement de tous les utilisateurs de la Plateforme
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Vous reconnaissez être seul responsable du respect de l’ensemble des
							lois, règlements et obligations applicables à votre utilisation de la
							plateforme.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Par ailleurs, en utilisation la plateforme implique l’acceptation pleine
							et entière des conditions générales d’utilisation ci-après décrites. Ces
							conditions d’utilisation sont susceptibles d’être modifiées ou
							complétées à tout moment, les utilisateurs
						</Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							8.2.{" "}
						</Text>
						Engagements des Conducteurs
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							En tant que conducteur, vous vous engagez à :
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								a){" "}
							</Text>
							respecter l’ensemble des lois, règles, codes applicables à la conduite
							et au véhicule, notamment à disposer d’une assurance responsabilité
							civile valide au moment du trajet en covoiturage et être en possession
							d’un permis de conduire en vigueur ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								b){" "}
							</Text>
							vous assurer que votre assurance couvre le covoiturage et que vos
							passagers sont considérés comme tiers dans votre véhicule et donc
							couverts par votre assurance ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								c){" "}
							</Text>
							ne prendre aucun risque au volant, à n’absorber aucun produit de nature
							à altérer votre attention et vos capacités, à conduire avec vigilance et
							en toute sécurité ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								d){" "}
							</Text>
							publier des annonces de covoiturage correspondant uniquement à des
							trajets réellement envisagés ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								e){" "}
							</Text>
							effectuer le trajet en covoiturage tel que décrit dans l’annonce de
							covoiturage et respecter les horaires et lieux convenus avec les autres
							membres (notamment lieu de rendez-vous et de dépose) ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								f){" "}
							</Text>
							ne pas prendre plus de passagers que le nombre de places indiquées dans
							l’annonce de covoiturage ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								g){" "}
							</Text>
							utiliser un véhicule en parfait état de fonctionnement et conforme aux
							usages et dispositions légales applicables, notamment avec un contrôle
							technique à jour ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								h){" "}
							</Text>
							communiquer à Agogolines ou tout passager qui vous en fait la demande,
							votre permis de conduire, votre carte grise, votre attestation
							d’assurance, votre certificat de contrôle technique ainsi que tout
							document attestant de votre capacité à utiliser ce véhicule;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								i){" "}
							</Text>
							en cas d’empêchement ou de changement de l’horaire ou du trajet en
							covoiturage, en informer sans délais vos passagers ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								j){" "}
							</Text>
							en cas de trajet en covoiturage transfrontalier, disposer et tenir à
							disposition du passager et de toute autorité qui le solliciterait tout
							document de nature à justifier de votre identité et de votre faculté à
							franchir la frontière ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								k){" "}
							</Text>
							attendre les passagers sur le lieu de rencontre convenu au moins 30
							minutes au-delà de l’heure convenue ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								l){" "}
							</Text>
							vous assurer d’être joignable par téléphone par vos passagers, au numéro
							enregistré sur votre profil ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								m){" "}
							</Text>
							garantir n’avoir aucune contre-indication ou incapacité à conduire ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								n){" "}
							</Text>
							avoir un comportement convenable et responsable, au cours du trajet en
							covoiturage.
						</Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							8.3.{" "}
						</Text>
						Engagements des Passagers
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Lorsque vous utilisez la Plateforme en tant que Passager, vous vous
							engagez à :
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								a){" "}
							</Text>
							adopter un comportement convenable au cours du trajet de façon à ne
							gêner ni la concentration et la conduite du conducteur ni la
							tranquillité des autres passagers ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								b){" "}
							</Text>
							respecter le véhicule du conducteur et sa propreté ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								c){" "}
							</Text>
							en cas d’empêchement, en informer sans délai le conducteur ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								d){" "}
							</Text>
							verser au conducteur le montant de la participation aux frais convenu ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								e){" "}
							</Text>
							être au lieu de rencontre à l’heure convenue et attendre le conducteur
							au moins 15 minutes au-delà de l’heure convenue ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								f){" "}
							</Text>
							communiquer à Agogolines ou tout conducteur qui vous en fait la demande,
							votre carte d’identité ou tout document de nature à attester de votre
							identité ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								g){" "}
							</Text>
							ne transporter, lors d’un trajet, aucun objet, marchandise, substance,
							animal de nature à gêner la conduite et la concentration du conducteur
							ou dont la nature, la possession ou le transport est contraire aux
							dispositions légales en vigueur ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								h){" "}
							</Text>
							en cas de trajet transfrontalier, disposer et tenir à disposition du
							conducteur et de toute autorité qui le solliciteraient tout document de
							nature à justifier de votre identité et de votre faculté à franchir la
							frontière ;
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
								i){" "}
							</Text>
							vous assurer d’être joignable par téléphone par le conducteur, au numéro
							enregistré sur votre profil, notamment au point de rendez-vous.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Agogolines se réserve la possibilité de suspendre votre compte, limiter
							votre accès aux services ou résilier les présentes CGU, en cas de
							manquement de la part du tiers pour le compte duquel vous avez réservé
							une place aux présentes CGU.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>
							9. Suspension de comptes, limitation d’accès et résiliation{" "}
						</Text>
					</View>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Vous avez la possibilité de mettre fin à votre relation contractuelle
							avec Agogolines à tout moment, sans frais et sans motif. Pour cela, il
							vous suffit de vous rendre sur la plateforme.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Agogolines se réserve la possibilité de suspendre votre compte, limiter
							votre accès aux services ou résilier les présentes CGU, en cas de
							manquement de la part du tiers pour le compte duquel vous avez réservé
							une place aux présentes CGU.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Lorsque cela est nécessaire, vous serez notifié de la mise en place
							d’une telle mesure afin de vous permettre de donner des explications à
							Agogolines. Agogolines décidera, à sa seule discrétion, de lever les
							mesures mises en place ou non.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>10. Données personnelles </Text>
					</View>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Dans le cadre de votre utilisation de la plateforme, Agogolines est
							amenée à collecter et raiter certaines de vos données personnelles. En
							utilisant la plateforme et vous inscrivant n tant que membre, vous
							reconnaissez et acceptez le traitement de vos données personnelles ar
							Agogolines conformément à la loi applicable et aux stipulations de la
							politique de onfidentialité.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>11. Propriété intellectuelle </Text>
					</View>
					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							11.1{" "}
						</Text>
						Contenu publié par Agogolines
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							De manière générale, l’Utilisateur s’interdit de porter atteinte aux
							droits de propriété intellectuelle (droit d’auteur, droits voisins,
							droit du producteur de bases de données, droit des marques, noms de
							domaine...) de Agogolines et/ou de l’hébergeur, et/ou de tout tiers.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Agogolines est titulaire de tous les droits de propriété intellectuelle
							tant sur la structure que sur le contenu du site (textes, logos, images,
							éléments sonores, logiciels, icônes, mise en page, base de données...)
							ou a acquis régulièrement les droits permettant l'exploitation de la
							structure et du contenu du site, sans aucune limitation.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Agogolines concède à l’Utilisateur, le droit d’utiliser le Site pour ses
							besoins strictement privés, à l’exclusion de toute utilisation
							lucrative. En cas d’utilisation professionnelle, l’Utilisateur devra
							obtenir l’autorisation expresse, préalable et écrite de Agogolines.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Ainsi, il est interdit à l’utilisateur notamment de copier, reproduire,
							représenter, modifier et/ou exploiter, transférer de quelque façon que
							ce soit et à quelque fin que ce soit, tout ou partie de la structure et
							du contenu du site, sauf en cas d’autorisation expresse, préalable.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Avec l’autorisation de Agogolines l’utilisateur peut télécharger
							certains éléments, notamment textes, photographies ou musiques. Cette
							fonctionnalité est à l’entière discrétion de Agogolines, qui peut la
							modifier ou la supprimer à tout moment.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Le non-respect de ces interdictions peut notamment constituer un acte de
							contrefaçon et/ou de concurrence déloyale et parasitaire engageant la
							responsabilité civile et/ou pénale de l’utilisateur.
						</Text>
					</View>

					<Text
						style={[
							styles.textContent,
							{
								marginTop: 10,
								fontSize: 18,
								color: colors.VIOLET,
							},
						]}>
						<Text style={[styles.fistTitle, {fontSize: 18, lineHeight: 20}]}>
							11.2{" "}
						</Text>
						Contenu publié par vous sur la Plateforme
					</Text>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Vous concédez à Agogolines une licence non exclusive d’utilisation des
							contenus et données que vous fournissez dans le cadre de votre
							utilisation des services. Afin de permettre à Agogolines la diffusion
							par réseau numérique et selon tout protocole de communication,
							(notamment Internet et réseau mobile), ainsi que la mise à disposition
							au public du contenu de la plateforme, vous autorisez Agogolines, pour
							le monde entier et pour toute la durée de votre relation contractuelle
							avec Agogolines, à reproduire, représenter, adapter et traduire votre
							contenu membre.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>12. Rôle de Agogolines</Text>
					</View>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							C’est une plateforme en ligne de mise en relation sur laquelle les
							membres peuvent créer et publier des annonces de covoiturage pour des
							trajets en covoiturage. Ces Annonces de covoiturage peuvent notamment
							être consultées par les autres membres pour prendre connaissance des
							modalités du trajet.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							En utilisant la plateforme et en acceptant les présentes CGU, vous
							reconnaissez que Agogolines n’est partie à aucun accord conclu entre
							vous et les autres membres.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							De manière générale, Agogolines se dégage de toute responsabilité en cas
							d’utilisation non- conforme aux CGU et de ses services.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							L’Utilisateur est informé que toute violation des stipulations des CGU
							est susceptible d'entraîner des poursuites judiciaires et des sanctions
							à son encontre.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Dans le cadre d’un trajet en covoiturage, les membres (Conducteurs ou
							Passagers) agissent sous leur seule et entière responsabilité, notamment
							conformément aux dispositions du code civil relatives au droit des
							obligations et à la responsabilité civile contractuelle (article 1101 et
							suivants du Code Civil).
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							En sa qualité d’intermédiaire, Agogolines ne saurait voir sa
							responsabilité engagée au titre du déroulement effectif d’un trajet.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>
							13. Fonctionnement, disponibilité et fonctionnalités de la Plateforme
						</Text>
					</View>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Agogolines s’efforcera, dans la mesure du possible, de maintenir la
							plateforme accessible 7 jours sur 7 et 24 heures sur 24. L’accès à la
							plateforme pourra être temporairement suspendu, sans préavis, en raison
							d’opérations techniques de maintenance, de migration, de mises à jour ou
							en raison de pannes ou de contraintes liées au fonctionnement des
							réseaux
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							En outre, Agogolines se réserve le droit de modifier ou d’interrompre, à
							sa seule discrétion, de manière temporaire ou permanente, tout ou partie
							de l’accès à la plateforme ou de ses fonctionnalités.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>14. Modification des CGU</Text>
					</View>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Les présentes CGU expriment l’intégralité de l’accord entre vous et
							Agogolines relative à votre utilisation des services. Tout autre
							document, notamment toute mention sur la plateforme (FAQ, Blog, etc.),
							n’a qu’une valeur informative.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							Agogolines pourra être amenée à modifier les présentes Conditions
							Générales d’Utilisation afin de s’adapter à son environnement
							technologique et commercial et afin de se conformer à la réglementation
							en vigueur.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>15. Droit applicable – Litige</Text>
					</View>
					<View style={styles.textContentContainer}>
						<Text style={styles.textContent}>
							Les CGU sont soumises au droit français.
						</Text>
						<Text style={[styles.textContent, {marginTop: 10}]}>
							En cas de différends relatifs à l’interprétation, la validité ou
							l’exécution des CGU, l’Utilisateur et Agogolines conviennent de déployer
							leurs meilleurs efforts afin de régler à l’amiable le litige.
						</Text>
					</View>

					<View style={styles.fistTitleContainer}>
						<Text style={styles.fistTitle}>16. Mentions légales</Text>
					</View>
					<View style={[styles.textContentContainer, {marginBottom: 50}]}>
						<Text style={styles.textContent}>
							La plateforme est éditée par la société All Moves trades, société
							anonyme au capital de 1600 euros immatriculée au registre du commerce et
							des sociétés de Paris, sous le numéro 838 646 925 dont le siège social
							est situé au 58, avenue de Wagram – 75017 Paris (France) e-mail :
							contact@agogolines.com,
						</Text>
						<Text style={styles.textContent}>
							Pour toute question, vous pouvez contacter All Moves Trades en utilisant
							notre adresse l’adresse email.
						</Text>
					</View>
				</View>
			</ScrollView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		marginTop: Constants.statusBarHeight,
		backgroundColor: colors.TRANSPARENT,
	},
	headerDocsContainer: {
		backgroundColor: colors.VIOLET,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		height: "25%",
		paddingHorizontal: "8%",
		flexBasis: "30%",
	},

	headerText: {
		height: 22,
		color: colors.WHITE,
		fontFamily: "Montserrat-Bold",
		fontSize: 15,
		textTransform: "uppercase",
		lineHeight: 18,
	},

	scrollViewContainer: {
		backgroundColor: colors.WHITE,
		position: "absolute",
		width: "100%",
		height: "80%",
		top: "25%",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingHorizontal: "5%",
		paddingTop: 20,
		/* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
	},

	originDestination: {
		marginBottom: 12,
	},

	titleOr: {
		width: 110,
		fontFamily: "Montserrat-Medium",
		fontSize: 12,
		lineHeight: 15,
		color: "#848484",
	},
	titleDes: {
		width: 95,
		fontFamily: "Montserrat-Medium",
		fontSize: 12,
		lineHeight: 15,
		color: "#848484",
	},

	date: {
		width: 110,
		height: 30,
		fontFamily: "Montserrat-Medium",
		fontSize: 12,
		lineHeight: 15,
		color: "#848484",
		textAlign: "right",
	},

	price: {
		color: colors.VIOLET,
		width: 110,
		height: 30,
		fontFamily: "Montserrat-Bold",
		fontSize: 14,
		lineHeight: 18,
		textAlign: "right",
	},

	fistTitleContainer: {
		marginTop: 10,
	},

	fistTitle: {
		fontSize: 20,
		fontFamily: "Montserrat-Bold",
		color: colors.VIOLET,
		lineHeight: 24,
	},

	textContentContainer: {
		marginTop: 10,
	},

	textContent: {
		fontFamily: "Montserrat-Medium",
		fontSize: 15,
		textAlign: "justify",
		lineHeight: 15,
		color: "#000",
	},
});

export default CGUCGV;
