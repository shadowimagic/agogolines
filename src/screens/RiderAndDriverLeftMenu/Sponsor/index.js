import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Platform,
  Linking,
  Share,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

//colors
import { colors } from "../../../common/theme";

//images
import illustrationCadeau from "../../../../assets/images/illustration-cadeau.png";

//Constants
import Constants from "expo-constants";

//uuidv4

function SponsorScreen({ navigation }) {
  const [msg, setMsg] = useState("ZV617F");
  const [isMsgSent, setIsMsgSent] = useState(false);

  const ShareMessage = (messages) => {
    //Here is the Share API
    Share.share({ message: messages, title: "envoyé via Agogolines mobile" })
      //after successful share return result
      .then((result) => {
        // console.log(result)
        setIsMsgSent(true);
        // console.log(isMsgSent)
      })
      //If any thing goes wrong it comes here
      .catch((errorMsg) => {
        console.log(errorMsg);
        //  console.log(isMsgSent)
      });
  };

  //code aléatoire

  /* useEffect(() => {
    let uid = uuidv4();
    console.log(uid.substr(0,5));
  }) */

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.headerDocsContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons name="ios-chevron-back" size={30} color={colors.WHITE} />
        </TouchableOpacity>
        <View>
          <Text style={styles.headerText}>PARRAINER DES AMIS</Text>
        </View>
        <View>
          <Text></Text>
        </View>
      </View>
      <ScrollView style={styles.scrollViewContainer}>
        <View
          style={{
            alignItems: "center",
            marginBottom: 20,
          }}
        >
          <View style={styles.cadeauContainer}>
            <Image source={illustrationCadeau} />
          </View>
        </View>
        <View>
          <Text style={styles.bigTitle}>
            INVITE TES AMIS ET GAGNEZ 3 POINTS CHACUN !
          </Text>
        </View>
        <View style={styles.smallTextContainer}>
          <Text style={styles.smallText}>
            Quand vos amis se connecteront et rentreront le code ci-dessous,
            vous gagnerez 3 points chacun !
          </Text>
        </View>
        <View style={styles.separator}></View>

        <View
          style={{
            alignItems: "center",
            justifyContent: "flex-end",
            paddingBottom: "25%",
          }}
        >
          <Text style={styles.textLabel}>PARTAGEZ LE CODE</Text>
          <View
            style={{
              justifyContent: "center",
              width: 250,
              height: 65,
              marginTop: 10,
            }}
          >
            <TextInput
              style={styles.input}
              value={msg}
              editable={false}
              onChangeText={(val) => setMsg(val)}
            />
          </View>
          <TouchableOpacity
            style={styles.btnInvit}
            onPress={() => ShareMessage(msg)}
          >
            <Text style={styles.textInvit}>INVITER DES AMIS</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: Constants.statusBarHeight,
    backgroundColor: colors.TRANSPARENT,
  },
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: "25%",
    paddingHorizontal: "8%",
    flexBasis: "30%",
  },

  headerText: {
    height: 22,
    color: colors.WHITE,
    fontFamily: "Montserrat-Bold",
    fontSize: 15,
    textTransform: "uppercase",
    lineHeight: 18,
  },

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    position: "absolute",
    width: "100%",
    height: "80%",
    top: "25%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: "8%",
    paddingTop: 20,
    /* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
  },

  cadeauContainer: {
    width: 280,
    height: 258,
  },

  bigTitle: {
    height: 80,
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
    lineHeight: 25,
    textAlign: "center",
    color: colors.VIOLET,
  },

  smallText: {
    height: 70,
    fontSize: 14,
    lineHeight: 18,
    fontFamily: "Montserrat-Medium",
    textAlign: "center",
    color: "#919191",
  },

  separator: {
    borderBottomWidth: 0.5,
    color: "#979797",
  },

  textLabel: {
    textAlign: "center",
    fontSize: 14,
    color: "#919191",
    lineHeight: 18,
    fontFamily: "Montserrat-Medium",
    marginTop: 40,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOpacity: 0.25,
    shadowRadius: 6,
    elevation: 5,
  },

  input: {
    borderRadius: 10,
    color: colors.BLACK,
    borderWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 20,
    textAlign: "center",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
    lineHeight: 35,

    borderColor: "rgba(255,255,255,0.5)",
    backgroundColor: "white",
    shadowColor: "rgba(255,255,255,0.5)",
    elevation: 2,
  },

  textInvit: {
    fontSize: 12,
    fontFamily: "Montserrat-Medium",
    lineHeight: 15,
    textAlign: "center",
    color: colors.WHITE,
    paddingHorizontal: 55,
    paddingVertical: 15,
  },

  btnInvit: {
    justifyContent: "center",
    alignItems: "center",
    width: 250,
    height: 47,
    marginTop: 10,
    borderRadius: 12,
    backgroundColor: colors.VIOLET,
  },
});

export default SponsorScreen;
