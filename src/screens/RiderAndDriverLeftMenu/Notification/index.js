import React, { useState, useContext, useEffect} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons'

//colors
import { colors } from '../../../common/theme'

//Constants
import Constants from 'expo-constants'

import travel from '../../../../assets/svg/travel'
import { SvgXml } from 'react-native-svg';

import { ListItem } from 'react-native-elements';

import { FirebaseContext } from '../../../../redux/src';
import { useSelector, useDispatch } from 'react-redux';

function Notification({ navigation }) {
    const {api} = useContext(FirebaseContext);
    const { fetchNotifications } = api;
    const notificationData = useSelector((state) => state.notificationdata);

    const notifications = notificationData.notifications;
    const [data , setData] =useState([]);

	const auth = useSelector((state) => state.auth);
	const dispatch = useDispatch();
    const testRecords = [{"id": "-MnUMotZI_Jfp0LKFMMQ"},{"id": "-Mu0qBKwIvhXPQDdueOj"}]

    useEffect(() => {
        if(auth.info && auth.info.profile){
            dispatch(fetchNotifications(auth.info.profile.usertype));

            if (notifications!= null) {
                const notif = notifications.filter(i => !testRecords.some(j => j.id === i.id));
                setData(notif)
                // console.log("valeur de notifications:", notif);
            }
           
        }
    }, [auth.info]);

    return (
    <View style={{ flex: 1 }}>
        <View style={styles.headerDocsContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons 
                name="ios-chevron-back"
                size={30}
                color={colors.WHITE}
            />
        </TouchableOpacity>
        <View>
            <Text style={styles.headerText}>Notifications</Text>
        </View>
        <View>
            <Text></Text>
        </View>
        </View>
        <ScrollView style={styles.scrollViewContainer}>
        {
            data.length>0 ?
            
            data.map((item, i) => (
            <ListItem
                key={i}  
                style={
                (i === data.length - 1) ? 
                {paddingBottom: 50} : 
                {
                    borderBottomColor: '#CFCFCF',
                    borderBottomWidth: 1,
                }}
                containerStyle={{padding: 0}}
            >
             
              <View >
                 <Text style={styles.titleOr}>{i+1}</Text>
              </View>
               <ListItem.Content>
                    <View style={{
                       
                        width: '100%',
                        justifyContent: 'center'
                    }}>
                         <View style={styles.originDestination}>
                                <ListItem.Title style={styles.titleOr}>{item.title}</ListItem.Title>
                            </View>
                         <View style={{flexDirection: 'column'}}>
                           
                            <View style={styles.finalDestination}>
                                <ListItem.Title style={styles.titleDes}>{item.body}</ListItem.Title>
                            </View>
                        </View>
                        <View style={{
                            flexDirection: 'column',
                            justifyContent: 'space-between',
                        }}>
                        
                        </View>
                    </View>
                </ListItem.Content>
            </ListItem>
            )):
            <View style={{alignItems: 'center'}}>
                <Text>Vous n'avez pas encore de notifications</Text>
            </View>
        
        }
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Constants.statusBarHeight,
    backgroundColor: colors.TRANSPARENT,
  },
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '25%',
    paddingHorizontal: '8%',
    flexBasis: '30%'
  },

  headerText: {
    height: 22,
    color: colors.WHITE,
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    textTransform: 'uppercase',
    lineHeight: 18,
  },

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    position: 'absolute',
    width: '100%',
    height: '80%',
    top: '25%',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,    
    paddingHorizontal: '5%',
    paddingTop: 20,
    /* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
  },

    originDestination: {
        marginBottom: 12,
        marginTop:10
    },

    finalDestination: {
        marginBottom: 6,
        marginTop:6
    },


    titleOr: {
        width: '70%',
        fontFamily: 'Montserrat-Medium',
        fontSize: 14,
        lineHeight: 15,
        color: '#848484',
    },
    titleDes: {
        width: '70%',
        fontFamily: 'Montserrat-Medium',
        fontSize: 16,
        lineHeight: 15,
        color: '#848484'
    },

    date: {
        width: 110,
        height: 30,
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        lineHeight: 15,
        color: '#848484',
        textAlign: 'right'
    },

    price: {
        color: colors.VIOLET,
        width: 110,
        height: 30,
        fontFamily: 'Montserrat-Bold',
        fontSize: 14,
        lineHeight: 18,
        textAlign: 'right',
    },  
    
})
export default Notification;
