import React, { useMemo, useRef } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Platform,
} from "react-native";
import { Ionicons, AntDesign } from "@expo/vector-icons";
import Video from "react-native-video";
import VideoPlayer from "react-native-video-controls";

//colors
import { colors } from "../../../common/theme";

//Constants
import Constants from "expo-constants";

function HowItWork({ navigation, navigator }) {
  const videoRef = useRef();

  const renderVideo = useMemo(() => {
    if (Platform.OS === "ios") {
      return (
        <Video
          ref={videoRef}
          source={{
            uri: "https://firebasestorage.googleapis.com/v0/b/agogolines-8bc39.appspot.com/o/video%2Ftutorial.mp4?alt=media&token=94753d61-b276-4ba0-99c9-182059397a59",
          }}
          style={styles.backgroundVideo}
          repeat={true}
          controls
        />
      );
    }

    return (
      <VideoPlayer
        ref={videoRef}
        source={{
          uri: "https://firebasestorage.googleapis.com/v0/b/agogolines-8bc39.appspot.com/o/video%2Ftutorial.mp4?alt=media&token=94753d61-b276-4ba0-99c9-182059397a59",
        }}
        navigator={navigator}
        style={styles.backgroundVideo}
        repeat={true}
      />
    );
  }, []);

  return (
    <>
      <View style={{ flex: 1 }}>
        <View style={styles.headerDocsContainer}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons name="ios-chevron-back" size={30} color={colors.WHITE} />
          </TouchableOpacity>
          <View>
            <Text style={styles.headerText}>comment ça marche</Text>
          </View>
          <View />
        </View>
        <ScrollView style={styles.scrollViewContainer}>
          <View style={styles.agogoTextContainer}>
            <Text style={styles.agogoText}>Agogolines</Text>
          </View>
          <View style={styles.textContentContainer}>
            <Text style={styles.textContent}>
              At vero eos et accusamus et iusto odio dignissimos ducimus qui
              blanditiis praesentium voluptatum deleniti atque corrupti quos
              dolores et quas molestias excepturi sint occaecati cupiditate non
              provident, similique sunt in culpa qui officia deserunt mollitia
              animi…
            </Text>
          </View>
          <View style={styles.videoContainer}>{renderVideo}</View>
          <View
            style={[
              styles.agogoTextContainer,
              { marginTop: 40, alignItems: "flex-start" },
            ]}
          >
            <Text style={styles.agogoText}>Le mode « voyageur »</Text>
          </View>
          <View style={styles.textContentContainer}>
            <Text style={styles.textContent}>
              At vero eos et accusamus et iusto odio dignissimos ducimus qui
              blanditiis praesentium voluptatum deleniti atque corrupti quos
              dolores et quas molestias excepturi sint occaecati cupiditate non
              provident, similique sunt in culpa qui officia deserunt mollitia
              animi, idum f labora dolor Ettum f labora. rerum facilis est et
              expedita distinctio. Nam libero tempore, cum soluta nobis est
              eligendi optio cumque nihil impedit quo minus id quod maxime
              placeat facere possimus, omnis voluptas assumenda est, omnis dolor
              repellendus. ut et voluptates répudiandae sint et molestiae non
              recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut
              aut reiciendis voluptatibus maiores alias conséquatur aut
              perferendis doloribus asperiores repellat.
            </Text>
          </View>

          <View>
            <Text style={styles.subTitle}>1. Lorem impsum</Text>
          </View>
          <View style={styles.textContentContainer}>
            <Text style={styles.textContent}>
              At vero eos et accusamus et iusto odio dignissimos ducimus qui
              blanditiis praesentium voluptatum deleniti atque corrupti quos
              dolores et quas molestias excepturi sint occaecati cupiditate non
              provident, similique sunt in culpa qui officia deserunt mollitia
              animi, idum f labora dolor Ettum f labora. rerum facilis.
            </Text>
          </View>

          <View>
            <Text style={styles.subTitle}>2. Lorem impsum</Text>
          </View>
          <View style={styles.textContentContainer}>
            <Text style={styles.textContent}>
              At vero eos et accusamus et iusto odio dignissimos ducimus qui
              blanditiis praesentium voluptatum deleniti atque corrupti quos
              dolores et quas molestias excepturi sint occaecati cupiditate non
              provident, similique sunt in culpa qui officia deserunt mollitia
              animi, idum f labora dolor Ettum f labora. rerum facilis.
            </Text>
          </View>

          <View>
            <Text style={styles.subTitle}>3. Lorem impsum</Text>
          </View>
          <View style={styles.textContentContainer}>
            <Text style={styles.textContent}>
              At vero eos et accusamus et iusto odio dignissimos ducimus qui
              blanditiis praesentium voluptatum deleniti atque corrupti quos
              dolores et quas molestias excepturi sint occaecati cupiditate non
              provident, similique sunt in culpa qui officia deserunt mollitia
              animi, idum f labora dolor Ettum f labora. rerum facilis.
            </Text>
          </View>

          <View style={{ marginBottom: 60 }}>
            <View
              style={[
                styles.agogoTextContainer,
                { marginBottom: 15, marginTop: 30, alignItems: "flex-start" },
              ]}
            >
              <Text style={styles.agogoText}>Le mode « conducteur »</Text>
            </View>
            <View style={styles.textContentContainer}>
              <Text style={styles.textContent}>
                At vero eos et accusamus et iusto odio dignissimos ducimus qui
                blanditiis praesentium voluptatum deleniti atque corrupti quos
                dolores et quas molestias excepturi sint occaecati cupiditate
                non provident, similique sunt in culpa qui officia deserunt
                mollitia animi, idum f labora dolor Ettum f labora. rerum
                facilis est et expedita distinctio. Nam libero tempore, cum
                soluta nobis est eligendi optio cumque nihil impedit quo minus
                id quod maxime placeat facere possimus, omnis voluptas assumenda
                est, omnis dolor repellendus. ut et voluptates répudiandae sint
                et molestiae non recusandae. Itaque earum rerum hic tenetur a
                sapiente delectus, ut aut reiciendis voluptatibus maiores alias
                conséquatur aut perferendis doloribus asperiores repellat.
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: Constants.statusBarHeight,
    backgroundColor: colors.TRANSPARENT,
  },
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: "25%",
    paddingHorizontal: "8%",
    flexBasis: "30%",
  },

  headerText: {
    height: 22,
    color: colors.WHITE,
    fontFamily: "Montserrat-Bold",
    fontSize: 15,
    textTransform: "uppercase",
    lineHeight: 18,
  },

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    position: "absolute",
    width: "100%",
    height: "80%",
    top: "25%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: "8%",
    paddingTop: 20,
    /* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
  },

  agogoTextContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  agogoText: {
    fontSize: 20,
    fontFamily: "Montserrat-Bold",
    color: colors.VIOLET,
    textAlign: "center",
    lineHeight: 24,
  },

  textContentContainer: {
    marginTop: 10,
  },

  textContent: {
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    textAlign: "justify",
    lineHeight: 15,
    color: "#848484",
  },
  videoContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    // backgroundColor: colors.BLACK,
    height: 215,
  },
  imgPlay: {
    color: colors.WHITE,
  },
  subTitle: {
    fontSize: 15,
    lineHeight: 20,
    textAlign: "left",
    color: colors.BLACK,
    fontFamily: "Montserrat-Bold",
    marginTop: 30,
  },
  backgroundVideo: {
    width: "100%",
    height: 215,
    backgroundColor: colors.BLACK,
  },
});

export default HowItWork;
