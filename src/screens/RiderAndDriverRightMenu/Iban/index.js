import React, { useState, useEffect, useRef, useContext } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  ActivityIndicator,
  Alert,
  SafeAreaView,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

//colors
import { colors } from "../../../common/theme";

//Constants
import Constants from "expo-constants";

import { FirebaseContext } from "../../../../redux/src";
import { useSelector, useDispatch } from "react-redux";

import edit_profile from "../../../../assets/svg/edit_profile";
import { SvgXml } from "react-native-svg";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { getStripeIdAccount } from "../../../../redux/src/other/StripeFunctions";
import { MaskedTextInput } from "react-native-mask-text";
import { stripekey } from "../../../common/stripeKey";
var stripe_url = "https://api.stripe.com/v1/";
export const html = `<script src="https://js.stripe.com/v3/"></script>`;
var secret_key = stripekey;

function Iban({ navigation }) {
  const { api } = useContext(FirebaseContext);
  const { fetchIban, addIban, fetchDriverInfos } = api;
  const driverdata = useSelector((state) => state.driverdata);
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const [idCniVerso, setIdCniVerso] = useState(null);
  const [idCniRecto, setIdCniRecto] = useState(null);

  useEffect(() => {
    if (driverdata.driverdata) {
      setIdCniVerso(driverdata.driverdata.idcnirecto);
      setIdCniRecto(driverdata.driverdata.idcniverso);
    }
  }, [driverdata]);

  //state userData
  const [data, setData] = useState({
    ville: "",
    adresse: "",
    codeP: "",
    dateNaiss: "",
    iban: "",
    valider: "non",
    idCarte: "non",
    idStripeAccount: "non",
    tokenStripeAccount: "non",
  });

  const handleChange = (name, value) => {
    setData({ ...data, [name]: value });
  };
  const [loading, setLoading] = useState(false);
  const [disabledVille, setDisabledVille] = useState(false);
  const [disabledAdresse, setDisabledAdresse] = useState(false);
  const [disabledCodeP, setDisabledCodeP] = useState(false);
  const [disabledDateNaiss, setDisabledDateNaiss] = useState(false);
  const [disabledIban, setDisabledIban] = useState(false);
  const ibanData = useSelector((state) => state.iban);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      dispatch(fetchDriverInfos());
      dispatch(fetchIban());
    }
  }, [auth.info, fetchIban]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      dispatch(fetchIban());
      if (ibanData.iban !== null) {
        setData({
          ville: ibanData.iban.ville,
          adresse: ibanData.iban.adresse,
          codeP: ibanData.iban.codeP,
          dateNaiss: ibanData.iban.dateNaiss,
          iban: ibanData.iban.iban,
          valider: "non",
          idCarte: "non",
          idStripeAccount: ibanData.iban.idStripeAccount,
          tokenStripeAccount: "non",
        });
      }
    }
  }, [auth.info, fetchIban]);

  const handleSubmit = async () => {
    let ibanPattern = new RegExp(
      /^DE\d{2}[ ]\d{4}[ ]\d{4}[ ]\d{4}[ ]\d{4}[ ]\d{2}|DE\d{20}$/i
    );
    if (
      data.ville &&
      data.adresse &&
      data.codeP &&
      data.iban &&
      !ibanPattern.test(data.iban)
    ) {
      let date_naiss = data.dateNaiss;
      let res = date_naiss.split("-");
      let annee = res[2];
      let mois = res[1];
      let jour = res[0];
      setLoading(true);

      var accountDetails = {
        "account[individual][first_name]":
          auth.info && auth.info.profile ? auth.info.profile.firstName : "",
        "account[individual][last_name]":
          auth.info && auth.info.profile ? auth.info.profile.lastName : "",
        "account[tos_shown_and_accepted]": true,
        "account[individual][verification][document][front]": idCniRecto,
        "account[individual][verification][document][back]": idCniVerso,
        "account[individual][verification][additional_document][front]":
          idCniRecto,
        "account[individual][dob][day]": jour,
        "account[individual][dob][month]": mois,
        "account[individual][dob][year]": annee,
        "account[individual][address][line1]": data.adresse,
        "account[individual][address][city]": data.ville,
        "account[individual][address][postal_code]": data.codeP,
        "account[individual][phone]": auth.info.profile.mobile,
        "account[individual][email]": auth.info.profile.email,
        "account[business_type]": "individual", // need to be input text
      };

      var bankDetails = {
        "bank_account[country]": "FR",
        "bank_account[currency]": "eur",
        "bank_account[account_holder_name]": `${auth.info.profile.firstName} ${auth.info.profile.lastName}`,
        "bank_account[account_holder_type]": "individual",
        "bank_account[account_number]": data.iban,
      };

      var formBankBody = [];
      for (var property in bankDetails) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(bankDetails[property]);
        formBankBody.push(encodedKey + "=" + encodedValue);
      }
      formBankBody = formBankBody.join("&");

      var formAccountBody = [];
      for (var property in accountDetails) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(accountDetails[property]);
        formAccountBody.push(encodedKey + "=" + encodedValue);
      }
      formAccountBody = formAccountBody.join("&");

      const rawAccountToken = await fetch(stripe_url + "tokens", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + secret_key,
        },
        body: formAccountBody,
      });

      const accountToken = await rawAccountToken.json();

      const rawBankToken = await fetch(stripe_url + "tokens", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + secret_key,
        },
        body: formBankBody,
      });

      const bankToken = await rawBankToken.json();

      if (!accountToken || !bankToken) {
        Alert.alert("REGISTER IBAN FAILED!");
        return;
      }

      const stripeData = await getStripeIdAccount(
        bankToken.id,
        accountToken.id,
        auth.info.profile.email,
        auth.info.uid
      );

      if (stripeData) {
        let newIbanData = {
          ville: data.ville,
          adresse: data.adresse,
          codeP: data.codeP,
          dateNaiss: data.dateNaiss,
          iban: data.iban,
          valider: "non",
          idCarte: "non",
          idStripeAccount: stripeData?.bank_account?.account,
        };
        setLoading(false);

        dispatch(addIban(newIbanData));
        Alert.alert("Création du compte de paiement avec succès");
        navigation.pop();
      } else {
        Alert.alert("Impossible de récupérer les fournisseurs de paiement");
        setLoading(false);
      }
    } else {
      setLoading(false);

      Alert.alert("Veuillez remplir le formulaire");
    }
  };

  return (
    <KeyboardAwareScrollView
      extraHeight={200}
      enableOnAndroid
      scrollEnabled={false}
      contentContainerStyle={{ height: "100%" }}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: colors.VIOLET,
        }}
      >
        <View style={styles.headerDocsContainer}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons name="ios-chevron-back" size={30} color={colors.WHITE} />
          </TouchableOpacity>
          <View>
            <Text style={styles.headerText}>IBAN</Text>
          </View>
          <View>
            <Text></Text>
          </View>
        </View>
        <SafeAreaView
          style={{
            flex: 1,
            backgroundColor: colors.WHITE,
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
          }}
        >
          <ScrollView style={styles.scrollViewContainer}>
            {loading ? (
              <ActivityIndicator color={colors.VIOLET} size="large" />
            ) : (
              <>
                <View
                  style={{
                    marginBottom: 20,
                    alignItems: "center",
                  }}
                >
                  <View>
                    <Text style={styles.textMesCB}>
                      Merci de renseigner les informations sur votre IBAN
                      (obligatoire)
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    marginTop: 20,
                    alignItems: "center",
                  }}
                >
                  <View style={styles.formContainer}>
                    <View>
                      <Text style={styles.labelForm}>Ville</Text>
                    </View>
                    <TouchableOpacity
                      onPress={() => setDisabledVille(!disabledVille)}
                    >
                      <SvgXml xml={edit_profile} />
                    </TouchableOpacity>
                  </View>
                  <TextInput
                    style={styles.inputStyle}
                    value={data.ville}
                    onChangeText={(val) => handleChange("ville", val)}
                    editable={disabledVille}
                    selectTextOnFocus={disabledVille}
                  />

                  <View style={styles.formContainer}>
                    <View>
                      <Text style={styles.labelForm}>Adresse</Text>
                    </View>
                    <TouchableOpacity
                      onPress={() => setDisabledAdresse(!disabledAdresse)}
                    >
                      <SvgXml xml={edit_profile} />
                    </TouchableOpacity>
                  </View>

                  <TextInput
                    style={styles.inputStyle}
                    value={data.adresse}
                    onChangeText={(val) => handleChange("adresse", val)}
                    editable={disabledAdresse}
                    selectTextOnFocus={disabledAdresse}
                  />

                  <View style={styles.formContainer}>
                    <View>
                      <Text style={styles.labelForm}>Code Postal</Text>
                    </View>
                    <TouchableOpacity
                      onPress={() => setDisabledCodeP(!disabledCodeP)}
                    >
                      <SvgXml xml={edit_profile} />
                    </TouchableOpacity>
                  </View>
                  <TextInput
                    style={styles.inputStyle}
                    value={data.codeP}
                    onChangeText={(val) => handleChange("codeP", val)}
                    editable={disabledCodeP}
                    selectTextOnFocus={disabledCodeP}
                  />

                  <View style={styles.formContainer}>
                    <View>
                      <Text style={styles.labelForm}>Date de Naissance</Text>
                    </View>
                    <TouchableOpacity
                      onPress={() => setDisabledDateNaiss(!disabledDateNaiss)}
                    >
                      <SvgXml xml={edit_profile} />
                    </TouchableOpacity>
                  </View>

                  <MaskedTextInput
                    mask="99-99-9999"
                    value={data.dateNaiss}
                    onChangeText={(val) => handleChange("dateNaiss", val)}
                    style={styles.inputStyle}
                    editable={disabledDateNaiss}
                    selectTextOnFocus={disabledDateNaiss}
                  />

                  <View style={styles.formContainer}>
                    <View>
                      <Text style={styles.labelForm}>Iban</Text>
                    </View>
                    <TouchableOpacity
                      onPress={() => setDisabledIban(!disabledIban)}
                    >
                      <SvgXml xml={edit_profile} />
                    </TouchableOpacity>
                  </View>

                  <TextInput
                    style={styles.inputStyle}
                    value={data.iban}
                    onChangeText={(val) => handleChange("iban", val)}
                    editable={disabledIban}
                    selectTextOnFocus={disabledIban}
                  />
                </View>

                <View style={styles.buttonAjMoyPayContainer}>
                  <TouchableOpacity
                    style={styles.btnAjPaiement}
                    onPress={handleSubmit}
                  >
                    <Text style={styles.textBtn}>ENREGISTRER</Text>
                  </TouchableOpacity>
                </View>
              </>
            )}
          </ScrollView>
        </SafeAreaView>
      </View>
    </KeyboardAwareScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: Constants.statusBarHeight,
    backgroundColor: colors.VIOLET,
  },
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: "8%",
    paddingTop: "10%",
    height: "15%",
  },

  buttonModif: {
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#979797",
    height: 35,
    width: 117,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },

  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
  },

  inputStyle: {
    width: "98%",
    height: 50,
    backgroundColor: "white",
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: {
      height: 0,
      width: 2,
    },
    shadowRadius: 6,
    shadowOpacity: 0.26,
    elevation: 3,
    padding: 10,
    marginTop: 10,
  },

  formContainer: {
    width: "98%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 30,
  },

  labelForm: {
    fontFamily: "Montserrat-Bold",
    fontSize: 15,
    lineHeight: 16,
    color: "#949494",
  },

  textMesCB: {
    fontSize: 15,
    lineHeight: 20,
    textAlign: "center",
    fontFamily: "Montserrat-Bold",
  },

  textLabel: {
    fontSize: 14,
    lineHeight: 18,
    color: "#979797",
    fontFamily: "Montserrat-Medium",
    marginTop: 16,
    marginBottom: 8,
  },

  bankDefaultCard: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 270,
    borderRadius: 10,
    shadowRadius: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: "rgba(137,137,137,0.5)",
    paddingTop: 25,
    paddingBottom: 15,
    backgroundColor: "white",
    shadowColor: "rgba(137,137,137,0.5)",
    shadowOpacity: 0.8,
    elevation: 5,
  },

  bankOptionalCard: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 270,
    borderRadius: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: "rgba(137,137,137,0.5)",
    paddingTop: 25,
    paddingBottom: 15,
    backgroundColor: "white",
  },

  separator: {
    borderBottomWidth: 1,
    marginVertical: 25,
    borderBottomColor: "#CFCFCF",
  },

  textDefault: {
    fontSize: 14,
    lineHeight: 18,
    color: "#979797",
    fontFamily: "Montserrat-Medium",
    marginTop: 16,
    marginBottom: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#979797",
    width: 130,
    marginBottom: 12,
  },

  buttonAjMoyPayContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 45,
    marginBottom: 70,
  },

  btnAjPaiement: {
    borderRadius: 11,
    backgroundColor: colors.VIOLET,
    paddingHorizontal: 10,
    width: "98%",
  },

  textBtn: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    color: colors.WHITE,
    fontSize: 16,
    lineHeight: 19,
    fontFamily: "Montserrat-Bold",
    textAlign: "center",
  },
  headerText: {
    height: 22,
    color: colors.WHITE,
    fontFamily: "Montserrat-Bold",
    fontSize: 15,
    textTransform: "uppercase",
    lineHeight: 18,
  },

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    paddingBottom: 100,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: "8%",
    paddingTop: 20,
  },
  loadingScreen: {
    height: "100%",
    backgroundColor: colors.WHITE,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default Iban;
