import React, {useState, useContext, useEffect} from "react";
import {
	View,
	Text,
	StyleSheet,
	Image,
	ScrollView,
	TouchableOpacity,
	TextInput,
	Alert,
	Modal,
	Platform,
	ActivityIndicator,
	SafeAreaView,
} from "react-native";
import {Ionicons} from "@expo/vector-icons";

import ActionSheet from "react-native-actions-sheet";

//colors
import {colors} from "../../../common/theme";

//images
import edit from "../../../../assets/images/edit.png";
import PictureModal from "../../../components/PictureModal";

//Constants
import Constants from "expo-constants";
import app from "firebase/app";
import {v4 as uuidv4} from "uuid";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

//yup
import * as yup from "yup";

import {FontAwesome} from "@expo/vector-icons";
import DeleteAccScreen from "./DeleteAccScreen";

//redux && react-redux
import {useDispatch, useSelector} from "react-redux";
import {FirebaseContext} from "../../../../redux/src";

//language
import {language} from "../../../../config";
import {SvgXml} from "react-native-svg";
import edit_profile from "../../../../assets/svg/edit_profile";
import user_profile_logo from "./../../../../assets/svg/user_profile_logo";

const verifSchema = yup.object({
	nom: yup.string().required().min(4),
	prenom: yup.string().required().min(4),
	email: yup.string().email().required(),
	telephone: yup.string().required().min(9),
});

function ProfileScreen({navigation}) {
	const [displayModal, setDisplayModal] = useState(false);
	const [disabledNom, setDisabledNom] = useState(false);
	const [disabledEmail, setDisabledEmail] = useState(false);
	const [disabledPrenom, setDisabledPrenom] = useState(false);
	const [disabledMobile, setDisabledMobile] = useState(false);
	const [modalVisible, setModalVisible] = useState(false);
	const [loading, setLoading] = useState(false);

	const {api} = useContext(FirebaseContext);

	const auth = useSelector((state) => state.auth);

	const {updateProfile, updateProfileImage, deleteUser, signOut} = api;
	const dispatch = useDispatch();

	//state userData
	const [profileData, setProfileData] = useState({
		email: auth.info.profile.email,
		firstName: auth.info.profile.firstName,
		lastName: auth.info.profile.lastName,
		mobile: auth.info.profile.mobile,
		profile_image: auth.info.profile.profile_image,
	});

	const [profileImg, setProfileImg] = useState(auth.info.profile.profile_image);

	useEffect(() => {
		// setProfileData({
		// 	...profileData,
		// 	email: auth.info.profile.email,
		// 	firstName: auth.info.profile.firstName,
		// 	lastName: auth.info.profile.lastName,
		// 	mobile: auth.info.profile.mobile,
		// });
		// setProfileImg(auth.info.profile.profile_image,);
		//console.log(profileData);
		//console.log(profileImg);
	}, []);

	//handleTextChange
	const handleTextChange = (name, value) => {
		setProfileData({...profileData, [name]: value});
	};

	//validate email
	const validateEmail = (email) => {
		const re =
			/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const emailValid = re.test(email);
		return emailValid;
	};

	//register button click after all validation
	const saveProfile = async () => {
		//console.log("valeur de profileData:",profileData);
		if (
			profileData.firstName &&
			profileData.firstName.length > 0 &&
			profileData.lastName &&
			profileData.lastName.length > 0 &&
			profileData.mobile &&
			profileData.mobile.length > 0
			//validateEmail(profileData.email)
		) {
			let userData = {
				firstName: profileData.firstName,
				lastName: profileData.lastName,
				mobile: profileData.mobile,
				numero:profileData.mobile,
				//email: profileData.email,
				profile_image: profileData.profile_image,
			};
			dispatch(updateProfile(auth.info, userData));
			Alert.alert(language.alert, language.profile_updated);
			props.navigation.pop();
		} else {
			Alert.alert(language.alert, language.no_details_error);
		}
	};

	return (
		<KeyboardAwareScrollView
			extraHeight={200}
			enableOnAndroid
			scrollEnabled={false}
			contentContainerStyle={{ height: "100%" }}
	  	>
			<SafeAreaView style={{flex: 1}}>
				<Modal visible={modalVisible} animationType="slide" transparent={true}>
					<PictureModal
						onClose={() => {
							setModalVisible(false);
						}}
						image={async (data) => {
							setLoading(true);
							const folderName = uuidv4().toString();
							const ppName = "pp" + uuidv4().toString().substring(0, 5) + ".png";
							await app
								.storage()
								.ref("photosprofiles/" + folderName)
								.child(ppName)
								.put(data);
							const imageUrl = await app
								.storage()
								.ref("photosprofiles/" + folderName)
								.child(ppName)
								.getDownloadURL();
							await app.database().ref("users/" +   auth.info.uid).update({profile_image: imageUrl});
							//console.log(imageUrl);
							setProfileData({...profileData, profile_image: imageUrl});
							setProfileImg(imageUrl);
							//console.log(profileData);
							//console.log(profileImg);
							setLoading(false);
						}}
					/>
				</Modal>
				<Modal visible={displayModal} animationType="slide" transparent={true}>
					<DeleteAccScreen setDisplayModal={setDisplayModal} />
				</Modal>
				<View style={styles.headerDocsContainer}>
					<TouchableOpacity onPress={() => navigation.goBack()}>
						<Ionicons name="ios-chevron-back" size={30} color={colors.WHITE} />
					</TouchableOpacity>
					<View>
						<Text style={styles.headerText}>Mon Profil</Text>
					</View>
					<View>
						<Text></Text>
					</View>
				</View>
				<ScrollView style={styles.scrollViewContainer}>
					<View
						style={{
							alignItems: "center",
							marginBottom: 20,
						}}>
						{profileImg && (
							<Image
								source={
									profileData.profile_image == null
										? require("../../../../assets/images/userprofil.png")
										: {uri: profileData.profile_image}
								}
								style={{width: 100, height: 100, borderRadius: 50}}
								resizeMode="cover"
							/>
						)}
						{!profileImg && <SvgXml xml={user_profile_logo} />}
						<TouchableOpacity
							style={styles.buttonModif}
							onPress={() => {
								setModalVisible(true);
							}}>
							<Text style={styles.textModif}>Modifier</Text>
						</TouchableOpacity>
					</View>
					{loading ? (
						<View style={styles.loading}>
							<ActivityIndicator color={colors.VIOLET} size="large" />
						</View>
					) : null}
					<View>
						<View>
							<View style={styles.labelContainer}>
								<Text style={styles.labelForm}>Nom</Text>
								<TouchableOpacity onPress={() => setDisabledNom(!disabledNom)}>
									<SvgXml xml={edit_profile} />
								</TouchableOpacity>
							</View>
							<View>
								<TextInput
									value={profileData.lastName}
									onChangeText={(val) => handleTextChange("lastName", val)}
									style={styles.textInput}
									editable={disabledNom}
									selectTextOnFocus={disabledNom}
								/>
							</View>
						</View>
						<View>
							<View style={styles.labelContainer}>
								<Text style={styles.labelForm}>Prenom</Text>
								<TouchableOpacity onPress={(val) => setDisabledPrenom(!disabledPrenom)}>
									<SvgXml xml={edit_profile} />
								</TouchableOpacity>
							</View>
							<View>
								<TextInput
									value={profileData.firstName}
									onChangeText={(val) => handleTextChange("firstName", val)}
									style={styles.textInput}
									editable={disabledPrenom}
									selectTextOnFocus={disabledPrenom}
								/>
							</View>
						</View>
						<View>
							<View style={styles.labelContainer}>
								<Text style={styles.labelForm}>Email</Text>
								{/**<TouchableOpacity onPress={() => setDisabledEmail(!disabledEmail)}>
									<SvgXml xml={edit_profile} />
								</TouchableOpacity>
								**/}
							</View>
							<View>
								<TextInput
									value={profileData.email}
									onChangeText={(val) => handleTextChange("email", val)}
									style={styles.textInput}
									editable={disabledEmail}
									selectTextOnFocus={disabledEmail}
								/>
							</View>
						</View>
						<View style={styles.labelContainer}>
							<Text style={styles.labelForm}>Telephone</Text>
							<TouchableOpacity onPress={() => setDisabledMobile(!disabledMobile)}>
								<SvgXml xml={edit_profile} />
							</TouchableOpacity>
						</View>
						<TextInput
							keyboardType="numeric"
							value={profileData.mobile}
							onChangeText={(val) => handleTextChange("mobile",val)}
							style={styles.textInput}
							editable={disabledMobile}
							selectTextOnFocus={disabledMobile}
						/>
						<View style={{alignItems: "center"}}>
							<TouchableOpacity style={styles.buttonModif} onPress={saveProfile}>
								<Text style={{color: "black"}}>Valider</Text>
							</TouchableOpacity>
						</View>
						<View style={{alignItems: "center"}}>
							<TouchableOpacity
								style={styles.buttonDelAcc}
								onPress={() => setDisplayModal(true)}>
								<FontAwesome name="trash-o" size={24} color="black" />
								<Text style={styles.textDelAcc}>Supprimer mon compte</Text>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
			</SafeAreaView>
		</KeyboardAwareScrollView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		marginTop: Constants.statusBarHeight,
		backgroundColor: colors.TRANSPARENT,
	},
	headerDocsContainer: {
		backgroundColor: colors.VIOLET,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		height: "25%",
		paddingHorizontal: "8%",
		flexBasis: "30%",
	},

	buttonModif: {
		borderRadius: 25,
		borderWidth: 1,
		borderColor: "#979797",
		height: 35,
		width: 117,
		justifyContent: "center",
		alignItems: "center",
		marginTop: 20,
	},

	textModif: {
		paddingVertical: 8,
		fontSize: 15,
		lineHeight: 20,
		color: "#979797",
		fontFamily: "Montserrat-Medium",
	},

	labelForm: {
		textTransform: "uppercase",
		width: 265,
		height: 27,
		fontSize: 13,
		lineHeight: 16,
		color: "#A4A4A4",
		fontFamily: "Montserrat-Medium",
	},

	textInput: {
		height: 40,
		fontFamily: "Montserrat-Medium",
		fontSize: 15,
		lineHeight: 19,
		paddingBottom: 20,
	},

	labelContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
	},

	buttonDelAcc: {
		marginTop: 20,
		width: "75%",
		height: 30,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		paddingHorizontal: 10,
		marginBottom: 50,
	},

	textDelAcc: {
		fontSize: 15,
		lineHeight: 20,
		textAlign: "center",
		color: colors.BLACK,
		marginLeft: 10,
	},

	headerText: {
		height: 22,
		color: colors.WHITE,
		fontFamily: "Montserrat-Bold",
		fontSize: 15,
		textTransform: "uppercase",
		lineHeight: 18,
	},

	scrollViewContainer: {
		backgroundColor: colors.WHITE,
		position: "absolute",
		width: "100%",
		height: "75%",
		top: "25%",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingHorizontal: "8%",
		paddingTop: 20,
		/* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
	},
});

export default ProfileScreen;
