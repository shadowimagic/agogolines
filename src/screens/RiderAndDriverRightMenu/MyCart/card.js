import React, {useState, useEffect} from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native'

//FontAwesome
import { FontAwesome } from '@expo/vector-icons'
import garbage from '../../../../assets/svg/garbage'
import { SvgXml } from 'react-native-svg';

const Card = ({ data, cardImage, tickStyle, delCard, defaut}) => {
    const [key, setKey] = useState('');

    useEffect(() => {
        delCard(key);
    }, [key]);

   /*  const cardHide = (numHide) => {
        let hideNum = [];
        for(let i = 0; i < numHide.length; i++){
            if(i < numHide.length-4){
                hideNum.push("*");
            }else{
                hideNum.push(numHide[i]);
            }
        }
        return hideNum.join("");
    } */
    return (
        <>
        {
            defaut === 'oui' ?
            <>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <TouchableOpacity style={styles.bankDefaultCard}>
                        <Image style={{width: 40}} source={cardImage} />
                        <Text style={{width: 150}}>**** **** ****{data.numerocarte}</Text>
                        <Image source={tickStyle} style={{width: 20}} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setKey(data.key)}>
                        <SvgXml xml={garbage} style={{width: 50, height: 50}} />
                    </TouchableOpacity>
                </View>
            </> 
            
            :

            <>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <TouchableOpacity style={styles.bankOptionalCard}>
                        <Image style={{width: 40}} source={cardImage} />
                        <Text style={{width: 150}}>**** **** ****{data.numerocarte}</Text>
                        <Image source={tickStyle} style={{width: 20}} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setKey(data.key)}>
                        <SvgXml xml={garbage} style={{width: 50, height: 50}} />
                    </TouchableOpacity>
                </View>
            </> 
        }
        </>
    )
}

const styles = StyleSheet.create({
textLabel: {
    fontSize: 14,
    lineHeight: 18,
    color: '#979797',
    fontFamily: 'Montserrat-Medium',
    marginTop: 16,
    marginBottom: 8
},

  bankDefaultCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 270,
    borderRadius: 10,
    shadowRadius: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'rgba(137,137,137,0.5)',
    paddingTop: 25,
    paddingBottom: 15,
    backgroundColor: 'white',
    shadowColor: 'rgba(137,137,137,0.5)',
    shadowOpacity: 0.8,
    elevation: 5,
  },
  bankOptionalCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 270,
    borderRadius: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'rgba(137,137,137,0.5)',
    paddingTop: 25,
    paddingBottom: 15,
    backgroundColor: 'white',
  },
separator: {
  borderBottomWidth: 1, 
  marginVertical: 25, 
  borderBottomColor: '#CFCFCF',
},
})

export default Card;