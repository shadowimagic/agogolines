import React, {useState, useEffect} from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native'

//images
import visa from '../../../../assets/images/visa.png'
import tick from '../../../../assets/images/tick.png'
import mastercard from '../../../../assets/images/mastercard.png'
import americanExpress from '../../../../assets/images/american-express.png'

import Card from './card';

const CardList = ({item, displayDefault, deleteCard}) => {

  //5105 1051 0510 5100
  //4012 8888 8888 1881

  const  supprimerCarte = (key)=>{
    deleteCard(key);
  }
  const [defaut, setDefaut] = useState('');
  
  useEffect(() => {
    displayDefault(defaut);
  }, [defaut]);

    return(
    <>
      {
        item.item.defaut === 'oui' ? 
        <>
          <Text style={[styles.textDefault, {borderBottomWidth: 0}]}>Par défaut: </Text>
          {
            item.item.type === 'visa' ? 
            <>
              <Card 
                data={item.item} 
                default='oui' 
                cardImage={visa}
                tickStyle={tick}
                delCard={(key) => supprimerCarte(key)}
              />
              <View style={styles.separator}></View>
            </>: null
          }                
          {
            item.item.type === 'mastercard' ? 
            <>
              <Card 
                data={item.item} 
                default='oui' 
                cardImage={mastercard}
                tickStyle={tick}
                delCard={(key) => supprimerCarte(key)}
              />
              <View style={styles.separator}></View>
            </> : null
          }
          {
              item.item.type === 'americancard' ? 
            <>
                <Card 
                  data={item.item} 
                  default='oui' 
                  cardImage={americanExpress}
                  tickStyle={tick}
                  delCard={(key) => supprimerCarte(key)}
                />
            <View style={styles.separator}></View>
            </>: null
          }
        </>: null
      }
    {
      item.item.defaut !='oui' && item.item.type === 'visa' ?
      <>
        <Card 
          data={item.item} 
          cardImage={visa}
          default='non' 
          delCard={(key) => supprimerCarte(key)}
        />
        <TouchableOpacity onPress={() => setDefaut(item.item.key)}>
          <Text style={styles.textDefault}>Définir par défaut</Text>
        </TouchableOpacity>
      </>: null
    }
    {
      item.item.defaut != 'oui' && item.item.type === 'mastercard' ?
      <>
          <Card 
            data={item.item} 
            cardImage={mastercard}
            default='non' 
            delCard={(key) => supprimerCarte(key)}
          />
          <TouchableOpacity onPress={() => setDefaut(item.item.key)}>
            <Text style={styles.textDefault}>Définir par défaut</Text>
          </TouchableOpacity>
      </>: null
    }
    {
      item.item.defaut != 'oui' && item.item.type === 'americancard' ?
      <>
        <Card 
          data={item.item} 
          cardImage={americanExpress}
          default='non' 
          delCard={(key) => supprimerCarte(key)}
        />
        <TouchableOpacity onPress={() => setDefaut(item.item.key)}>
          <Text style={styles.textDefault}>Définir par défaut</Text>
        </TouchableOpacity>
      </>:  null
    }
  </>
  )
}  

const styles = StyleSheet.create({
  textDefault: {
    fontSize: 14,
    lineHeight: 18,
    color: '#979797',
    fontFamily: 'Montserrat-Medium',
    marginTop: 16,
    marginBottom: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#979797',  
    width: 130,
    marginBottom: 12
  },    
  separator: {
    borderBottomWidth: 1, 
    marginVertical: 25, 
    borderBottomColor: '#CFCFCF',
  },
})

export default CardList;