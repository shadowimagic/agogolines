import React, { useState, useEffect, useRef, useContext } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Modal
} from 'react-native';
import { Ionicons } from '@expo/vector-icons'

//colors
import { colors } from '../../../common/theme'

import { AntDesign } from '@expo/vector-icons'; 

import MaxCartModal from './MaxCartModal/index'

import { FirebaseContext } from '../../../../redux/src';

//Constants
import Constants from 'expo-constants'

import { useSelector, useDispatch } from 'react-redux';

import CardList from './cardList'

function MyCart({ navigation }) {
  const [displayModal, setDisplayModal] = useState(false);
  const [change, setChange] = useState('');
  const [data, setData] = useState([]);


  const { api } = useContext(FirebaseContext);
  const {
    fetchCreditCard,
    addCreditCard
  } = api;

  const auth = useSelector(state => state.auth);
  const creditcarddata = useSelector(state => state.creditcarddata);
  const dispatch = useDispatch();

  const deleteCard = (key) => {
    if (key != '') {

      setData((prevTodo) => {
        return prevTodo.filter((todo) =>
          todo.key !== key);
      });
     // console.log("valeur de key1:" + key)
      setChange(key)
    }
  };

  const displayDefault = (key) => {
    if (key != '') {
      data.map((element) => {
        element.defaut = 'non'
      })
      data.map((element) => {
        if (element.key === key) {
          element.defaut = 'oui';
        }
      })
    //  console.log("valeur de key2:" + key)
      setChange(key)
    }
  };

  useEffect(() => {
    if (change != '') {
    //  console.log("valeur de data en cour pure:" + JSON.stringify(data))
      dispatch(addCreditCard(data));
    }
  }, [change]);

  useEffect(() => {

    if (creditcarddata.creditcards != null) {
      setData([]);
      creditcarddata.creditcards.map((element) => {
        setData((prevTodo) => {
          return [
            element,
            ...prevTodo,
          ];
        });
      })
    }
  }, [creditcarddata.creditcards]);

  useEffect(() => {
  if (auth.info && auth.info.profile) {
      dispatch(fetchCreditCard());
  }
  }, [auth.info]);

  return (
    <View style={{ flex: 1 }}>
      <Modal
        transparent={displayModal}
        visible={displayModal}
      >
        <MaxCartModal setDisplayModal={setDisplayModal} />
      </Modal>
      <View style={styles.headerDocsContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons 
            name="ios-chevron-back"
            size={30}
            color={colors.WHITE}
          />
        </TouchableOpacity>
        <View>
          <Text style={styles.headerText}>Moyen de paiment</Text>
        </View>
        <View>
            <Text></Text>
        </View>
      </View>
      <View style={styles.scrollViewContainer}>
        <View style={{
          alignItems: 'center',
          marginBottom: 20
        }}>
          <View>
            <Text style={styles.textMesCB}>
              MES CARTES BANCAIRES
              </Text>
          </View>
        </View>

          <FlatList 
            data={data}
            keyExtractor={item => item.key}
            renderItem={(item) => <CardList
              item={item} 
              displayDefault={(key) => displayDefault(key)} 
              deleteCard={(key) => deleteCard(key)}
            />}
          />
        
        <View style={styles.buttonAjMoyPayContainer}>
          <TouchableOpacity style={styles.btnAjPaiement} 
            onPress={() => data.length>=3 ? setDisplayModal(true) : navigation.navigate('PaymentScreen')}
          >
            <AntDesign name="plus" size={24} color={colors.WHITE} />
            <Text style={styles.textBtn}>
              AJOUTER MOYEN DE PAIEMENT
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Constants.statusBarHeight,
    backgroundColor: colors.TRANSPARENT,
  },
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '25%',
    paddingHorizontal: '8%',
    flexBasis: '30%'
  },

  buttonModif: {
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#979797',
    height: 35,
    width: 117,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },

  textModif: {
    paddingVertical: 8,
    fontSize: 15,
    lineHeight: 20,
    color: '#979797',
    fontFamily: 'Montserrat-Medium'
  },

  labelForm: {
    textTransform: 'uppercase',
    width: 265,
    height: 27,
    fontSize: 13,
    lineHeight: 16,
    color: '#A4A4A4',
    fontFamily: 'Montserrat-Medium'
  },

  textInput: {
    height: 27, 
    fontFamily: 'Montserrat-Medium',
    fontSize: 15,
    lineHeight: 19,
    paddingBottom: 10
  },

  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  buttonDelAcc: {
    marginTop: 135,
    width: 200,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginBottom: 50
  },

  textDelAcc: {
    fontSize: 15,
    lineHeight: 20,
    textAlign: 'center',
    color: colors.BLACK,
    marginLeft: 10
  },

  textMesCB: {
    fontSize: 15,
    lineHeight: 20,
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold'
  },

  textLabel: {
    fontSize: 14,
    lineHeight: 18,
    color: '#979797',
    fontFamily: 'Montserrat-Medium',
    marginTop: 16,
    marginBottom: 8
  },

  bankDefaultCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 270,
    borderRadius: 10,
    shadowRadius: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'rgba(137,137,137,0.5)',
    paddingTop: 25,
    paddingBottom: 15,
    backgroundColor: 'white',
    shadowColor: 'rgba(137,137,137,0.5)',
    shadowOpacity: 0.8,
    elevation: 5,
  },

  bankOptionalCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 270,
    borderRadius: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'rgba(137,137,137,0.5)',
    paddingTop: 25,
    paddingBottom: 15,
    backgroundColor: 'white',
  },

  separator: {
    borderBottomWidth: 1, 
    marginVertical: 25, 
    borderBottomColor: '#CFCFCF'
  },

  textDefault: {
    fontSize: 14,
    lineHeight: 18,
    color: '#979797',
    fontFamily: 'Montserrat-Medium',
    marginTop: 16,
    marginBottom: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#979797',  
    width: 130,
    marginBottom: 12
  },

  buttonAjMoyPayContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 45,
    marginBottom: 60,
  },

  btnAjPaiement: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 11,
    backgroundColor: colors.VIOLET,
    paddingHorizontal: 10
  },

  textBtn: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    color: colors.WHITE,
    fontSize: 12,
    lineHeight: 15,
    fontFamily: 'Montserrat-Bold',
  },
  headerText: {
    height: 22,
    color: colors.WHITE,
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    textTransform: 'uppercase',
    lineHeight: 18,
  },

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    position: 'absolute',
    width: '100%',
    height: '80%',
    top: '25%',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,    
    paddingHorizontal: '8%',
    paddingTop: 20
    /* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
  },
})

export default MyCart;