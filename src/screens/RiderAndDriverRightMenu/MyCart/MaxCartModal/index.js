import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

//colors
import { colors } from '../../../../common/theme'

const MaxCartModal = ({ setDisplayModal }) => {
  return (
    <View style={styles.container}>
      <View style={{justifyContent: 'center', alignItems: 'center', marginBottom: 20}}>
        <Text style={styles.modalText}>Vous ne pouvez enregistrer</Text>
        <Text style={styles.modalText}>que 3 cartes maximum.</Text>
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center', marginBottom: 20}}>
        <Text style={styles.modalTextWrap}>
          Veuillez en supprimer pour pouvoir en
        </Text>
        <Text style={styles.modalTextWrap}>ajouter d'autre.</Text>
      </View>
      <TouchableOpacity style={styles.btnPrPhotoContainer} 
        onPress={() => setDisplayModal(false)}>
        <Text style={styles.textBtn}>
          J'ai compris
        </Text>
      </TouchableOpacity>
    </View> 
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    opacity: 0.8
  },
  
  btnPrPhotoContainer: {
    width: 200,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.VIOLET,
    borderRadius: 12,
    paddingHorizontal: 8,
    paddingVertical: 12,
    marginBottom: 15
  },

  btnAnnContainer: {
    borderWidth: 1,
    borderColor: colors.WHITE,
    borderRadius: 12,
    paddingHorizontal: 8,
    paddingVertical: 12,
  },

  textBtn: {
    width: 185,
    textAlign: 'center',
    color: colors.WHITE,
    fontFamily: 'Montserrat-Medium',
    textTransform: 'uppercase',
    lineHeight: 15,
    fontSize: 12,
  },

  modalText: {
    color: colors.WHITE,
    fontSize: 18,
    fontFamily: 'Montserrat-Medium',
    lineHeight: 22,
    textAlign: 'center',
  },

  modalTextWrap: {
    color: '#949494',
    fontSize: 14,
    lineHeight: 18,
    textAlign: 'center',
  }
})

export default MaxCartModal;