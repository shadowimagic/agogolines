import React, { useState, useContext, useEffect} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons'

//colors
import { colors } from '../../../common/theme'

//Constants
import Constants from 'expo-constants'

import travel from '../../../../assets/svg/travel'
import { SvgXml } from 'react-native-svg';

import { ListItem } from 'react-native-elements';

import { FirebaseContext } from '../../../../redux/src';
import { useSelector, useDispatch } from 'react-redux';


const Setting = (props) => {
    const {api} = useContext(FirebaseContext);
    const { fetchBooking } = api;
    const bookingData = useSelector((state) => state.bookingdata);
	const auth = useSelector((state) => state.auth);
	const dispatch = useDispatch();

    let histoFullList = [{
        titleTop:bookingData.booking !== null ?  bookingData.booking.addr_dep:null,
        titleWrap: bookingData.booking !== null ? bookingData.booking.addr_dest:null,
        date: bookingData.booking !== null ? JSON.stringify(new Date(bookingData.booking.rider_trip_propose_date_time)).substring(1,11):null,
        price: bookingData.booking !== null ? bookingData.booking.propose_negoPrice:null,
    }];

    useEffect(() => {
        if(auth.info && auth.info.profile){
            dispatch(fetchBooking());
        }
    }, [auth.info]);

    return (
    <View style={{ flex: 1 }}>
        <View style={styles.headerDocsContainer}>
        <TouchableOpacity onPress={() => props.navigation.goBack() }>
            <Ionicons 
                name="ios-chevron-back"
                size={30}
                color={colors.WHITE}
            />
        </TouchableOpacity>
        <View>
            <Text style={styles.headerText}>paramètres</Text>
        </View>
        <View>
            <Text></Text>
        </View>
        </View>
        <ScrollView style={styles.scrollViewContainer}>
		<View style={{alignItems: 'center'}}>
                <Text>Vous n'avez pas encore de paramètres</Text>
            </View>
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Constants.statusBarHeight,
    backgroundColor: colors.TRANSPARENT,
  },
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '25%',
    paddingHorizontal: '8%',
    flexBasis: '30%'
  },

  headerText: {
    height: 22,
    color: colors.WHITE,
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    textTransform: 'uppercase',
    lineHeight: 18,
  },

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    position: 'absolute',
    width: '100%',
    height: '80%',
    top: '25%',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,    
    paddingHorizontal: '5%',
    paddingTop: 20,
    /* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
  },

    originDestination: {
        marginBottom: 12,
    },

    titleOr: {
        width: '70%',
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        lineHeight: 15,
        color: '#848484',
    },
    titleDes: {
        width: '70%',
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        lineHeight: 15,
        color: '#848484'
    },

    date: {
        width: 110,
        height: 30,
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        lineHeight: 15,
        color: '#848484',
        textAlign: 'right'
    },

    price: {
        color: colors.VIOLET,
        width: 110,
        height: 30,
        fontFamily: 'Montserrat-Bold',
        fontSize: 14,
        lineHeight: 18,
        textAlign: 'right',
    },  
    
})
export default Setting;



