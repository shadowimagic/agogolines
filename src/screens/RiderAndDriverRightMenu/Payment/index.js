import React, {useState, useEffect, useContext, useRef} from "react";
import {
	View,
	Text,
	StyleSheet,
	Image,
	ScrollView,
	TouchableOpacity,
	TextInput,
	Alert,
	SafeAreaView,
} from "react-native";
import {Ionicons} from "@expo/vector-icons";

import {useSelector, useDispatch} from "react-redux";
import {FirebaseContext} from "../../../../redux/src";

//colors
import {colors} from "../../../common/theme";
//images
import visa from "../../../../assets/images/visa.png";
import mastercard from "../../../../assets/images/mastercard.png";
import americanExpress from "../../../../assets/images/american-express.png";


//Constants
import Constants from "expo-constants";
// stripe key
import { stripekey } from "../../../common/stripeKey";
var stripe_url = "https://api.stripe.com/v1/";


import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

function PaymentScreen({navigation}) {
	let [mois, setMois] = useState("");
	let [annee, setAnne] = useState("");
	const [numeroCard, setNumeroCard] = useState(null);
	const [cryptogramme, setCryptogramme] = useState(null);
	let [cardData, setCardData] = useState([]);

	const {api} = useContext(FirebaseContext);
	const {fetchCreditCard, addCreditCard} = api;

	const auth = useSelector((state) => state.auth);
	const creditcarddata = useSelector((state) => state.creditcarddata);
	const dispatch = useDispatch();

	useEffect(() => {
		if (auth.info && auth.info.profile) {
			dispatch(fetchCreditCard());
		}
	}, [auth.info]);

	const verifyNumber = () => {
		let sum = 0;
		let temp = 0;
		let cardNumberCopy = numeroCard;
		let checkDigit = parseInt(numeroCard.slice(-1));
		let parity = cardNumberCopy.length % 2;

		for (let i = 0; i <= cardNumberCopy.length - 2; i++) {
			if (i % 2 === parity) {
				temp = +cardNumberCopy[i] * 2;
			} else {
				temp = +cardNumberCopy[i];
			}
			if (temp > 9) {
				temp -= 9;
			}
			sum += temp;
		}
		return (sum + checkDigit) % 10 === 0;
	};

	const getCardType = (number) => {
		// visa
		var re = new RegExp("^4");
		if (number.match(re) != null) return "visa";

		// Mastercard
		// Updated for Mastercard 2017 BINs expansion
		if (
			/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(
				number
			)
		)
			return "mastercard";

		// AMEX
		re = new RegExp("^3[47]");
		if (number.match(re) != null) return "americancard";

		// Discover
		re = new RegExp(
			"^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)"
		);
		if (number.match(re) != null) return "americancard";

		// Diners
		re = new RegExp("^36");
		if (number.match(re) != null) return "diners";

		// Diners - Carte Blanche
		re = new RegExp("^30[0-5]");
		if (number.match(re) != null) return "Diners - Carte Blanche";

		// JCB
		re = new RegExp("^35(2[89]|[3-8][0-9])");
		if (number.match(re) != null) return "jcb";

		// Visa Electron
		re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
		if (number.match(re) != null) return "visa";

		return "";
	};

	const validCryptogramme = (number) => {
		var re = new RegExp("^[0-9]{3}?$");

		return number.match(re) != null;
	};



	  const onPayment = (information) => {
        const card = {
            'card[number]': information.card.number,
            'card[exp_month]': information.card.exp_month,
            'card[exp_year]': information.card.exp_year,
            'card[cvc]': information.card.cvc,
        };

        return fetch(stripe_url + "tokens", {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: "Bearer " + stripekey,
          },
          method: 'post',
          body: Object.keys(card)
            .map(key => key + '=' + card[key])
            .join('&')
        }).then(response => response.json());
      };


	const validate = async() => {

		if (
			annee != "" &&
			mois != "" &&
			verifyNumber() === true &&
			validCryptogramme(cryptogramme) === true
		) {
			let information = {
				card: {
				  number: numeroCard,
				  exp_month: mois,
				  exp_year: annee,
				  cvc: cryptogramme,
				  name: auth.info && auth.info.profile ? auth.info.profile.lastName :""
				}
			  }
			let cardidpay = await onPayment(information);
			setCardData({
				defaut: "oui",
				mois: cardidpay.card.exp_month,
				annee: cardidpay.card.exp_year,
				numerocarte: cardidpay.card.last4,
				init: "non",
				type: getCardType(numeroCard),
				cardid:cardidpay.card.id,
				cardToken: cardidpay.id,
				firstCharge: true
			});

			if (creditcarddata.creditcards != null) {
				creditcarddata.creditcards.map((element) => {
					element.defaut = "non";
				});

			
              
                  
             
				creditcarddata.creditcards.push({
					key: new Date().getTime().toString(),
					defaut: "oui",
					mois: cardidpay.card.exp_month,
					annee: cardidpay.card.exp_year,
					numerocarte: cardidpay.card.last4,
					init: "non",
					type: getCardType(numeroCard),
					cardid:cardidpay.card.id,
					cardToken: cardidpay.id,
					firstCharge: true
				});

				dispatch(addCreditCard(creditcarddata.creditcards));
				navigation.goBack();
				initialize();
			}
			if (creditcarddata.creditcards === null) {
				
				  
				dispatch(
					addCreditCard([
						{
							key: new Date().getTime().toString(),
							defaut: "oui",
							mois: cardidpay.card.exp_month,
							annee: cardidpay.card.exp_year,
							numerocarte: cardidpay.card.last4,
							init: "non",
							type: getCardType(numeroCard),
							cardid:cardidpay.card.id,
							cardToken: cardidpay.id,
							firstCharge: true
						},
					])
				);
				navigation.goBack();
				initialize();
			}
		}

		if (
			annee == "" ||
			mois == "" ||
			verifyNumber() === false ||
			validCryptogramme(cryptogramme) === false
		) {
			Alert.alert("Données bancaires invalides");
		}
	};

	const initialize = () => {
		setCardData({
			defaut: "oui",
			mois: "",
			annee: "",
			cryptogramme: "",
			numerocarte: "",
			init: "non",
			type: "",
			action: "fermer",
			cardid:""
		});
	};

	return (
		<KeyboardAwareScrollView
				extraHeight={200}
				enableOnAndroid
				scrollEnabled={false}
				contentContainerStyle={{ height: "100%" }}
    		>
		<SafeAreaView style={{flex: 1}}>
			<View style={styles.headerDocsContainer}>
				<TouchableOpacity onPress={() => navigation.goBack()}>
					<Ionicons name="ios-chevron-back" size={30} color={colors.WHITE} />
				</TouchableOpacity>
				<View>
					<Text style={styles.headerText}>Moyen de paiment</Text>
				</View>
				<View>
					<Text></Text>
				</View>
			</View> 
				<ScrollView style={styles.scrollViewContainer}>
					<View
						style={{
							alignItems: "center",
							marginBottom: 20,
						}}>
						<View>
							<Text style={styles.textMesCB}>AJOUTER UN MOYEN DE PAIEMENT</Text>
						</View>
					</View>
					<View>
						<View style={styles.cardContainer}>
							<View style={styles.card}>
								<Image source={visa} />
							</View>
							<View style={styles.card}>
								<Image source={mastercard} />
							</View>
							<View style={styles.card}>
								<Image source={americanExpress} />
							</View>
						</View>
						<Text style={styles.textDefault}>Numéro de carte</Text>
						<TextInput
							style={styles.textInput}
							value={numeroCard}
							onChangeText={(val) => setNumeroCard(val)}
							keyboardType="numeric"
							maxLength={16}
						/>
						<View style={styles.validCryptoContainer}>
							<View>
								<Text style={styles.textLabel}>Validité</Text>
								<View
									style={{
										flexDirection: "row",
										justifyContent: "space-between",
										width: 135,
									}}>
									<TextInput
										style={styles.textInputSeparated}
										keyboardType="numeric"
										placeholder="MM"
										value={mois}
										onChangeText={(val) => setMois(val)}
										maxLength={2}
									/>
									<TextInput
										style={styles.textInputSeparated}
										keyboardType="numeric"
										placeholder="AAAA"
										value={annee}
										onChangeText={(val) => setAnne(val)}
										maxLength={4}
									/>
								</View>
							</View>
							<View>
								<Text style={styles.textLabel}>Cryptograme</Text>
								<TextInput
									style={styles.textInputSeparated}
									keyboardType="numeric"
									value={cryptogramme}
									onChangeText={(val) => setCryptogramme(val)}
									maxLength={3}
								/>
							</View>
						</View>
					</View>
					<View style={styles.buttonAjMoyPayContainer}>
						<TouchableOpacity style={styles.btnAjPaiement} onPress={validate}>
							<Text style={styles.textBtn}>Enregistrer</Text>
						</TouchableOpacity>
					</View>
				</ScrollView>
			</SafeAreaView>
		</KeyboardAwareScrollView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		marginTop: Constants.statusBarHeight,
		backgroundColor: colors.TRANSPARENT,
	},
	headerDocsContainer: {
		backgroundColor: colors.VIOLET,
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		height: "25%",
		paddingHorizontal: "8%",
		flexBasis: "30%",
	},

	textInput: {
		height: 45,
		width: 300,
		paddingHorizontal: 10,
		fontFamily: "Montserrat-Medium",
		fontSize: 15,
		lineHeight: 19,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: "#979797",
	},

	textMesCB: {
		fontSize: 15,
		lineHeight: 20,
		textAlign: "center",
		fontFamily: "Montserrat-Bold",
	},

	textDefault: {
		fontSize: 14,
		lineHeight: 18,
		height: 20,
		color: colors.BLACK,
		fontFamily: "Montserrat-Bold",
		marginTop: 35,
		marginBottom: 10,
	},

	textLabel: {
		fontSize: 14,
		lineHeight: 18,
		height: 20,
		color: colors.BLACK,
		fontFamily: "Montserrat-Bold",
	},

	buttonAjMoyPayContainer: {
		justifyContent: "center",
		alignItems: "center",
		marginTop: 50,
	},

	btnAjPaiement: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		borderRadius: 11,
		backgroundColor: colors.VIOLET,
		paddingHorizontal: 10,
	},

	textBtn: {
		paddingVertical: 15,
		paddingHorizontal: 30,
		color: colors.WHITE,
		fontSize: 12,
		lineHeight: 15,
		fontFamily: "Montserrat-Bold",
		textTransform: "uppercase",
	},

	cardContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		paddingHorizontal: 45,
	},

	card: {
		width: 65,
		height: 65,
		justifyContent: "center",
		alignItems: "center",
		borderWidth: 1,
		borderRadius: 10,
		borderColor: "#D4D4D4",
	},

	validCryptoContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginTop: 20,
	},

	textInputSeparated: {
		height: 45,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: "#979797",
		paddingHorizontal: 10,
		width: 65,
		marginTop: 10,
		fontFamily: "Montserrat-Medium",
		fontSize: 15,
		lineHeight: 19,
	},

	headerText: {
		height: 22,
		color: colors.WHITE,
		fontFamily: "Montserrat-Bold",
		fontSize: 15,
		textTransform: "uppercase",
		lineHeight: 18,
	},

	scrollViewContainer: {
		backgroundColor: colors.WHITE,
		position: "absolute",
		width: "100%",
		height: "80%",
		top: "25%",
		borderTopLeftRadius: 30,
		borderTopRightRadius: 30,
		paddingHorizontal: "8%",
		paddingTop: 20,
	},
});

export default PaymentScreen;
