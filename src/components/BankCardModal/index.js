import React, {useState, useContext, useEffect} from 'react';
import { View, Text,KeyboardAvoidingView,Keyboard,
    StyleSheet, TouchableOpacity, TouchableWithoutFeedBack, Image,TextInput, SafeAreaView, ScrollView  } from 'react-native';

//images
import creditCard from '../../../assets/images/creditocard.png'

import { Feather } from '@expo/vector-icons'; 
import { colors } from "../../common/theme";

import {useSelector, useDispatch} from "react-redux";
import {FirebaseContext} from "../../../redux/src"
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { stripekey } from "../../common/stripeKey";
var stripe_url = "https://api.stripe.com/v1/";

const BankCardModal = (props) => {
    const auth = useSelector(state => state.auth);
    const creditcarddata = useSelector(state => state.creditcarddata);
    const dispatch = useDispatch();
    const { api } = useContext(FirebaseContext);

    const {
		fetchCreditCard,
		addCreditCard
	} = api;
    const [loading, setLoading] = useState(false);
    let [mois, setMois] = useState("");
	let [annee, setAnne] = useState("");
	const [numeroCard, setNumeroCard] = useState(null);
	const [cryptogramme, setCryptogramme] = useState(null);

	let [cardData, setCardData] = useState([]);

    
	const verifyNumber = () => {
		let sum = 0;
		let temp = 0;
		let cardNumberCopy = numeroCard;
		let checkDigit = parseInt(numeroCard.slice(-1));
		let parity = cardNumberCopy.length % 2;

		for (let i = 0; i <= cardNumberCopy.length - 2; i++) {
			if (i % 2 === parity) {
				temp = +cardNumberCopy[i] * 2;
			} else {
				temp = +cardNumberCopy[i];
			}
			if (temp > 9) {
				temp -= 9;
			}
			sum += temp;
		}
		return (sum + checkDigit) % 10 === 0;
	};

	const getCardType = (number) => {
		// visa
		var re = new RegExp("^4");
		if (number.match(re) != null) return "visa";

		// Mastercard
		// Updated for Mastercard 2017 BINs expansion
		if (
			/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(
				number
			)
		)
			return "mastercard";

		// AMEX
		re = new RegExp("^3[47]");
		if (number.match(re) != null) return "americancard";

		// Discover
		re = new RegExp(
			"^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)"
		);
		if (number.match(re) != null) return "americancard";

		// Diners
		re = new RegExp("^36");
		if (number.match(re) != null) return "diners";

		// Diners - Carte Blanche
		re = new RegExp("^30[0-5]");
		if (number.match(re) != null) return "Diners - Carte Blanche";

		// JCB
		re = new RegExp("^35(2[89]|[3-8][0-9])");
		if (number.match(re) != null) return "jcb";

		// Visa Electron
		re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
		if (number.match(re) != null) return "visa";

		return "";
	};

	const validCryptogramme = (number) => {
		var re = new RegExp("^[0-9]{3}?$");

		return number.match(re) != null;
	};

    
      const onPayment = (information) => {
        const card = {
            'card[number]': information.card.number,
            'card[exp_month]': information.card.exp_month,
            'card[exp_year]': information.card.exp_year,
            'card[cvc]': information.card.cvc,
        };

        return fetch(stripe_url + "tokens", {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: "Bearer " + stripekey,
          },
          method: 'post',
          body: Object.keys(card)
            .map(key => key + '=' + card[key])
            .join('&')
        }).then(response => response.json());
      };

	const validate = async () => {

        setLoading(true);
		
		if (
			annee != "" &&
			mois != "" &&
			verifyNumber() === true &&
			validCryptogramme(cryptogramme) === true
		) {
			let information = {
				card: {
				  number: numeroCard,
				  exp_month: mois,
				  exp_year: annee,
				  cvc: cryptogramme,
				  name: auth.info && auth.info.profile ? auth.info.profile.lastName :""
				}
			  }
			let cardidpay = await onPayment(information);

			setCardData({
				defaut: "oui",
				mois: cardidpay.card.exp_month,
				annee: cardidpay.card.exp_year,
				numerocarte: cardidpay.card.last4,
				init: "non",
				type: getCardType(numeroCard),
				cardid:cardidpay.card.id,
				cardToken: cardidpay.id,
                firstCharge: true
			});

          

			if (creditcarddata.creditcards != null) {
				creditcarddata.creditcards.map((element) => {
					element.defaut = "non";
				});

                


				creditcarddata.creditcards.push({
					key: new Date().getTime().toString(),
					defaut: "oui",
					mois: cardidpay.card.exp_month,
					annee: cardidpay.card.exp_year,
					numerocarte: cardidpay.card.last4,
					init: "non",
					type: getCardType(numeroCard),
					cardid:cardidpay.card.id,
					cardToken: cardidpay.id,
                    firstCharge: true
				});
            
                  
				dispatch(addCreditCard(creditcarddata.creditcards));
                setLoading(false);
                props.setDisplayModal(false)
				initialize();
			}
			if (creditcarddata.creditcards === null) {
             
				dispatch(
					addCreditCard([
						{
							key: new Date().getTime().toString(),
							defaut: "oui",
							mois: cardidpay.card.exp_month,
							annee: cardidpay.card.exp_year,
							numerocarte: cardidpay.card.last4,
							init: "non",
							type: getCardType(numeroCard),
							cardid:cardidpay.card.id,
							cardToken: cardidpay.id,
                            firstCharge: true
						},
					])
				);
                setLoading(false);
               // props.onCloseModal()
                props.setDisplayModal(false)
				initialize();
			}
		}else {
            setLoading(false);
            Alert.alert("Données bancaires invalides");
        }

		/**if (
			annee == "" ||
			mois == "" ||
			verifyNumber() === false ||
			validCryptogramme(cryptogramme) === false
		) {
			Alert.alert("Données bancaires invalides");
		}**/
	};

	const initialize = () => {
		setCardData({
			defaut: "oui",
			mois: "",
			annee: "",
			cryptogramme: "",
			numerocarte: "",
			init: "non",
			type: "",
			action: "fermer",
            cardid:""
		});
	};

    
	useEffect(() => {
		if (auth.info && auth.info.profile) {
			dispatch(fetchCreditCard());
		}
	}, [auth.info]);

    return (


    <KeyboardAwareScrollView
      extraHeight={200}
      enableOnAndroid
      scrollEnabled={false}
      contentContainerStyle={{ height: "100%" }}
    >
      <SafeAreaView style={{ flex: 1 }}>
        
        <View style={styles.container}>

               
            <View style={{
                width: 335, 
                height: 400, 
                borderRadius: 20,
                elevation: 2
                }}
            >
                 <Spinner
                //visibility of Overlay Loading Spinner
                visible={loading}
                //Text with the Spinner
                textContent={'Enregistrement en cours...'}
                //Text style of the Spinner Text
                textStyle={styles.spinnerTextStyle}
                />
                <View style={{
                    width: '100%',
                    height: 60,
                    borderTopRightRadius: 20,
                    borderTopLeftRadius: 20,
                    backgroundColor: '#5A48CB',
                    justifyContent: 'center',
                    alignItems: 'center'
                    }}
                >
                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                        <Image source={creditCard} style={{
                            backgroundColor: 'white',
                            width: 25,
                            height: 15
                        }} />
                        <Text style={{
                            marginLeft: 10,
                            color: '#fff',
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 15,
                            lineHeight: 20,
                            textAlign: 'center'
                        }}>CARTE BANCAIRE</Text>
                    </View>
                </View>
                <View style={styles.priceContainer}>
                    <Text style={styles.price}>15,30 €</Text>
                </View>
                <View style={{marginLeft: 20, marginTop:25}}>
                    <Text style={{
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 15,
                        lineHeight: 18
                    }}>Numéro de carte</Text>
                    <TextInput 
                        style={{
                        width: 300,
                        borderWidth: 1,
                        borderColor: '#D4D4D4',
                        height: 45,
                        borderRadius: 10,
                        marginTop: 10,
                        textAlign: 'center'
                        }}
                        value={numeroCard}
                        onChangeText={(val) => setNumeroCard(val)}
                        keyboardType="numeric"
                        maxLength={16}
                    />
                </View>
                <View style={{marginLeft: 20, marginTop:25, flexDirection: 'row', justifyContent: 'space-around'}}>
                    <View>
                        <Text style={{
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 15,
                            lineHeight: 18
                        }}>Validité</Text>
                        <View style={{flexDirection: 'row'}}>
                            <TextInput 
                                style={{
                                width: 65,
                                borderWidth: 1,
                                borderColor: '#D4D4D4',
                                height: 45,
                                borderRadius: 10,
                                marginTop: 10,
                                textAlign: 'center',
                                marginRight: 8
                                }}
                                keyboardType="numeric"
                                placeholder="MM"
                                value={mois}
                                onChangeText={(val) => setMois(val)}
                                maxLength={2}
                            />
                            <TextInput 
                                style={{
                                width: 75,
                                borderWidth: 1,
                                borderColor: '#D4D4D4',
                                height: 45,
                                borderRadius: 10,
                                marginTop: 10,
                                textAlign: 'center'
                                }}
                                keyboardType="numeric"
                                placeholder="AAAA"
                                value={annee}
                                onChangeText={(val) => setAnne(val)}
                                maxLength={4}
                            />
                        </View>
                    </View>    
                    <View>
                        <Text style={{
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 15,
                            lineHeight: 18
                        }}>Cryptogramme</Text>
                        <View style={{flexDirection: 'row'}}>
                            <TextInput 
                                style={{
                                width: 65,
                                borderWidth: 1,
                                borderColor: '#D4D4D4',
                                height: 45,
                                borderRadius: 10,
                                marginTop: 10,
                                textAlign: 'center',
                                marginRight: 8
                                }}
                                keyboardType="numeric"
                                value={cryptogramme}
                                onChangeText={(val) => setCryptogramme(val)}
                                maxLength={3}
                            />
                        </View>
                    </View>    
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginTop: 30}}>
                    <TouchableOpacity style={{
                        width: 125,
                        borderRadius: 10,
                        borderWidth: 1
                    }}
                        onPress={() => props.setDisplayModal(false)}
                    >
                        <Text style={{
                                paddingVertical: 10,
                                textAlign: 'center',
                                fontFamily: 'Montserrat-Bold',
                                fontSize: 15,
                                lineHeight: 18,
                            }}>ANNULER</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        width: 150,
                        borderRadius: 10,
                        backgroundColor: '#5A48CB'
                    }}
                    onPress={() => validate()}
                    >
                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{
                                paddingVertical: 10,
                                textAlign: 'center',
                                fontFamily: 'Montserrat-Bold',
                                fontSize: 15,
                                lineHeight: 18,
                                color: 'white'
                            }}>ENREGISTRER</Text>
                            <Feather name="chevrons-right" size={24} color="white" />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
     
      </SafeAreaView>
    </KeyboardAwareScrollView>



        

    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    opacity: 1
  },
  spinnerTextStyle: {
    color: colors.VIOLET
  },
  priceContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
},
price: {
    fontSize: 20,
    fontFamily: 'Montserrat-Bold',
    lineHeight: 24,
    color: '#5A48CB',
    textAlign: 'center'
},
})

export default BankCardModal;