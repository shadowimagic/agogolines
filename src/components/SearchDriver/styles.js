import { StyleSheet } from 'react-native';
import { colors } from '../../common/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
	},
	bottomBanner: {
		position: "absolute",
		alignSelf: "center",
		bottom: 0
	  },
	modal: {
		backgroundColor: 'white',
		width: 350,
		height: 190,
		borderRadius: 13,
		shadowColor: 'black',
		shadowOffset: { height: 2, width: 0 },
		shadowOpacity: 0.26,
		shadowRadius: 6,
		elevation: 6,
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'center'
		// justifyContent: 'center',
	},
	image: {
		marginTop: 15,
	},
	text: {
		marginTop: 15,
		fontSize: 14,
		fontFamily: 'Montserrat-Bold',
		textTransform: 'uppercase',
		color: colors.GREYD,
	},
	textViolet: {
		color: colors.VIOLET,
	},
	btnTitleStyle: {
		textAlign: 'center',
		textTransform: 'uppercase',
		padding: 16,
		color: 'black',
	},
	buttonStyle: {
		marginTop: 30,
		backgroundColor: 'transparent',
		borderColor: 'black',
		borderWidth: 2,
		borderRadius: 13,
	},
});

export default styles;
