import React, { useEffect } from "react";
import { View, Text, Image, Alert } from "react-native";
import { Button } from "react-native-elements";
import cardot from "../../../assets/images/car-dot.png";
import styles from "./styles";
import { SvgXml } from "react-native-svg";
import bigCar from "../../../assets/svg/bigCar";

import { EventRegister } from "react-native-event-listeners";
// import { AdMobBanner, setTestDeviceIDAsync } from "expo-ads-admob";

const SearchDriver = (props) => {
  const fermeture = () => {
    EventRegister.emit("initrider", true);
  };

  // React.useEffect(() => {
  //   setTestDeviceIDAsync("EMULATOR");
  // }, []);

  useEffect(() => {
    const timer = setTimeout(() => {
      props.setDisplayResearchDriver(false);
      props.setTravelCompleted(true);      
      props.setAnnuler(true)
    }, 30000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <SvgXml xml={bigCar} style={{ height: 17, width: 39 }} />
        <Text style={styles.text}>
          recherche d'un <Text style={styles.textViolet}>conducteur...</Text>
        </Text>
        <Button
          title="annuler"
          buttonStyle={styles.buttonStyle}
          titleStyle={styles.btnTitleStyle}
          onPress={() => {
            props.setDisplayResearchDriver(false);
            props.setTravelInit(true);
            props.setAnnuler(true)
            fermeture();
          }}
        />
      </View>
      {/* <AdMobBanner
        style={styles.bottomBanner}
        bannerSize="smartBanner"
        adUnitID="ca-app-pub-5608997176551587/4926705154"
        servePersonalizedAds // true or false
        onDidFailToReceiveAdWithError={(e) => console.log(e)}
      /> */}
    </View>
  );
};

export default SearchDriver;
