import React, { useState, useEffect, useContext } from "react";
import { View, Text, Image, TouchableOpacity, Linking } from "react-native";
import { Button } from "react-native-elements";
import styles from "./styles";
import { AntDesign, MaterialCommunityIcons } from "@expo/vector-icons";
import { Alert } from "react-native";
import { colors } from "../../common/theme";
import { FirebaseContext } from "../../../redux/src";
import { useSelector, useDispatch } from "react-redux";


const ValidateRouteRider = (props) => {


    return (
        <View style={styles.modal}>
        <View style={styles.container}>
            <View style={styles.btnTitleStyleViolet}>
                <Text style={styles.userName}>Nouvelle Course</Text>
            </View>
            <Text>prix: {props.dataRider.prix}€</Text>
            <Text>addresse de départ: {props.dataRider.addr_dep}</Text>
            <Text>addresse d'arriver: {props.dataRider.addr_dest}</Text>
            <Text>durer: {props.dataRider.durer} min</Text>
            <Text>mode de paiement: {props.dataRider.mode_paiement}</Text>
            <Text>Nb. de passager: {props.dataRider.passager_nb}</Text>
            <View style={styles.bodyBottomTwo}>
                <View style={styles.btnElement}>
                    <Button
                        title="Accepter"        
                        buttonStyle={styles.buttonStyleViolet}
                        titleStyle={styles.btnTitleStyleViolet}
                        icon={
                            <MaterialCommunityIcons
                                name="chevron-double-right"
                                size={24}
                                color="white"
                            />
                        }
                        iconPosition="right"
                        onPress={() => props.validation()}
                    />
                    <Button
                        title="Refuser"
                        buttonStyle={styles.buttonStyle}
                        titleStyle={styles.btnTitleStyle}
                        onPress={() => {
                            console.log("course refuser")
                            props.setProposeRider(false)
                        }}
                    />
                </View>
            </View>
        </View>
        </View>
    );
}

export default ValidateRouteRider