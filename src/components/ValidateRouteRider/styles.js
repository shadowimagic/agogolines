import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../common/theme";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    width: "100%",
    alignSelf: "center",
  },
  modal: {
    backgroundColor: "white",
    height: "50%",
    width: "90%",
    marginHorizontal: 10,

    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
    //alignItems: 'center',
  },
  bodyTop: {
    height: "30%",
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
  },
  userImage: {
    width: 47,
    height: 47,
    marginRight: 20,
    borderRadius: 12,
  },
  userName: {
    fontWeight: "bold",
  },
  starIcon: {
    flexDirection: "row",
    marginVertical: 5,
  },
  stopPhoneIcon: {
    flexDirection: "row",
    marginLeft: "auto",
    marginRight: -6,
  },
  iconMargin: {
    marginTop: Platform.OS === "ios" ? 15 : 16,
    marginRight: Platform.OS === "ios" ? 15 : 3,
  },
  element: {
    flexDirection: "row",
  },
  bodyBottom: {
    backgroundColor: "white",
    height: "70%",
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  textContainer: {
    marginLeft: 5,
  },
  elementHeaderText: {
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  elementBodyText: {
    color: colors.GREYD,
  },
  elementBodyTextViolet: {
    color: colors.VIOLET,
    fontWeight: "bold",
    fontSize: 16,
  },
  bodyBottomOne: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bodyBottomTwo: {
    flexDirection: "row",
    marginTop: "10%",
    marginBottom: 10,
  },
  btnTitleStyle: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 16,
    color: "black",
  },
  btnTitleStyleViolet: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 16,
    color: "white",
  },
  buttonStyle: {
    marginTop: 5,
    backgroundColor: "transparent",
    borderColor: "black",
    borderWidth: 2,
    borderRadius: 13,
  },
  buttonStyleViolet: {
    marginTop: 5,
    backgroundColor: colors.VIOLET,
    borderRadius: 13,
  },
});

export default styles;
