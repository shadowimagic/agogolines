import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image,TextInput } from 'react-native';
import {SvgXml} from "react-native-svg";
import travel from "../../../assets/svg/travel";

const TravelEnCoursCancel = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.btnContainer}>
                <View style={styles.twoBtnWrap}>
                    <TouchableOpacity 
                        onPress={props.onFollowTravelsModal} 
                        style={[styles.btnVioletContainer, {
                            borderWidth: 0
                        }]}
                    >
                        <Text style={[styles.btn, {
                            fontSize: 22,
                            lineHeight: 27,
                            fontFamily: 'Montserrat-Bold'
                        }]}>Que s’est-il passé ?</Text>  
                    </TouchableOpacity>
                </View>
                <View style={styles.twoBtnWrap}>
                    <TouchableOpacity 
                        onPress={props.onFollowTravelsModal} 
                        style={styles.btnVioletContainer}
                    >
                        <Text style={styles.btn}>Passager injoignable</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.twoBtnWrap}>
                    <TouchableOpacity 
                        onPress={props.onFollowTravelsModal} 
                        style={styles.btnVioletContainer}
                    >
                        <Text style={styles.btn}>Numéro de passager invalide</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.twoBtnWrap}>
                    <TouchableOpacity 
                        onPress={props.onFollowTravelsModal} 
                        style={styles.btnVioletContainer}
                    >
                        <Text style={styles.btn}>Problème sur la route</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.twoBtnWrap}>
                    <TouchableOpacity 
                        onPress={props.onFollowTravelsModal} 
                        style={styles.btnVioletContainer}
                    >
                        <Text style={styles.btn}>Problème avec l'application</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.twoBtnWrap}>
                    <TouchableOpacity 
                        onPress={props.onFollowTravelsModal} 
                        style={styles.btnVioletContainer}
                    >
                        <Text style={styles.btn}>Problème avec le véhicule</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.twoBtnWrap}>
                    <TouchableOpacity 
                        onPress={props.onFollowTravelsModal} 
                        style={styles.btnVioletContainer}
                    >
                        <Text style={styles.btn}>Mauvaise attitude du passager</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.twoBtnWrap, {marginTop: 20}]}>
                    <TouchableOpacity 
                        onPress={props.onFollowTravelsModal} 
                        style={[styles.btnVioletContainer, {
                            backgroundColor: '#5a48cb',
                            borderWidth: 0,
                        }]}
                    >
                        <Text style={[styles.btn,
                        {
                            fontFamily: 'Montserrat-Bold',
                            fontSize: 18,
                            lineHeight: 22
                        }]}>Je continue ma course</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View> 
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    opacity: 0.8
  },
  upperText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    fontSize: 18, 
    lineHeight: 22,
    textAlign: 'center',
  },
  departArrContainer: {
    backgroundColor: '#FFFEFE',
    width: 335,
    height: 120,
    marginTop: 20,
    borderRadius: 13,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textBoxContainer: {
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'space-around',
    marginLeft: 13
  },
  separator: {
      width: '100%',
      borderWidth: 1,
      borderColor: '#ECECEC'
  },
  mainText: {
      fontFamily: 'Montserrat-Bold',
      fontSize: 12,
      lineHeight: 15,
      textTransform: 'uppercase',
      textAlign: 'left'
  },
  wrapText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    lineHeight: 18,
    color: '#848484',
    textAlign: 'left'
  }, 
  btnContainer: {
      marginTop: 40
  },
  twoBtnWrap: {
      flexDirection: 'column',
  },
  btnVioletContainer: {
      width: 302,
      height: 60,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 13,
      marginBottom: 12,
      borderWidth: 1,
      borderColor: '#979797'
  },
  btnTransparentContainer: {
    width: 200,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#FFF'
  },
  btn: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 18,
    color: '#FFF',
    textAlign: 'center'
  }
})

export default TravelEnCoursCancel;