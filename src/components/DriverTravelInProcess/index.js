import React, { useContext, useMemo } from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet } from "react-native";
import { colors } from "../../common/theme";
import { SvgXml } from "react-native-svg";
import position from "../../../assets/svg/position";
import riderImg from "../../../assets/images/avatar_login.png";
import { FirebaseContext } from "../../../redux/src";
import { useDispatch, useSelector } from "react-redux";

const DriverTravelInProcess = (props) => {
  const { closeDriverTravelInProcess, openDriverValidatePaymentMethod } = props;
  const { api } = useContext(FirebaseContext);
  const { addTravel } = api || {};
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;

  const isRiderReady = useMemo(
    () => travelInformations?.is_rider_picked_up,
    [travelInformations?.is_rider_picked_up]
  );

  const dispatch = useDispatch();

  const driverValidateModePaiement = () => {
    if (travelInformations) {
      travelInformations.arriver_dest_passager = true;
      travelInformations.arriver_dest_passager_at = new Date().getTime();
      dispatch(addTravel(travelInformations));
    }
  };

  const onArrived = () => {
    driverValidateModePaiement();
    closeDriverTravelInProcess();
    openDriverValidatePaymentMethod();
  };

  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginLeft: 13,
            marginTop: 10,
          }}
        >
          <Image
            source={
              travelInformations &&
              travelInformations?.propose_profile_img_rider &&
              travelInformations?.propose_profile_img_rider != "non"
                ? { uri: travelInformations?.propose_profile_img_rider }
                : riderImg
            }
            style={{ width: 40, height: 40, borderRadius: 25 }}
          />
          <Text
            style={{
              color: "#969696",
              fontSize: 14,
              lineHeight: 17,
              textAlign: "left",
              left: 15,
              fontFamily: "Montserrat-Medium",
            }}
          >
            En route vers...
          </Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <SvgXml xml={position} style={{ marginLeft: 22, marginTop: 10 }} />
          <View
            style={{
              width: "38%",
              marginRight: 20,
              marginTop: 10,
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                color: "#000",
                fontSize: 14,
                lineHeight: 18,
                textAlign: "left",
                marginLeft: 12,
                fontFamily: "Montserrat-Medium",
              }}
              numberOfLines={2}
            >
              {travelInformations?.propose_dest_rider &&
                travelInformations?.propose_dest_rider}
            </Text>
          </View>
          <TouchableOpacity
            onPress={onArrived}
            style={{
              width: 117,
              height: 36,
              borderRadius: 11,
              backgroundColor: isRiderReady ? "#5A48CB" : "#D5D4C8",
              justifyContent: "center",
              marginTop: -10,
              marginRight: 14,
            }}
            disabled={!isRiderReady}
          >
            <Text
              style={{
                fontFamily: "Montserrat-Bold",
                color: "white",
                lineHeight: 18,
                textAlign: "center",
              }}
            >
              ARRIVÉ
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    width: "100%",
    marginBottom: 20,
  },
  modal: {
    backgroundColor: "white",
    width: "90%",
    marginHorizontal: 15,
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
    alignSelf: "center",
    paddingBottom: 10,
  },
  text: {
    marginTop: 10,
    fontSize: 15,
    lineHeight: 20,
    fontFamily: "Montserrat-Bold",
    color: "#5a48cb",
  },
  secondText: {
    fontSize: 14,
    fontFamily: "Montserrat-Bold",
    color: "#9E9E9E",
    lineHeight: 20,
  },

  btnTitleStyle: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 18,
    color: "white",
    fontFamily: "Montserrat-Bold",
  },
  buttonStyle: {
    marginTop: 10,
    backgroundColor: "#5a48cb",
    borderRadius: 12,
    alignSelf: "center",
  },
  bodyTop: {
    height: "30%",
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  userImage: {
    width: 47,
    height: 47,
    marginRight: 10,
    borderRadius: 12,
  },
  userName: {
    fontWeight: "bold",
  },
  starIcon: {
    flexDirection: "row",
    marginVertical: 5,
  },
  stopPhoneIcon: {
    flexDirection: "row",
    marginLeft: "auto",
    marginRight: 10,
  },
  iconMargin: {
    marginTop: 10,
    marginRight: 10,
  },
  element: {
    flexDirection: "row",
  },
  bodyBottom: {
    backgroundColor: "white",
    height: "70%",
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  textContainer: {
    marginLeft: 5,
  },
  elementHeaderText: {
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  elementBodyText: {
    color: colors.GREYD,
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    lineHeight: 18,
  },
  elementBodyTextViolet: {
    fontWeight: "bold",
    fontSize: 20,
    lineHeight: 24,
    fontFamily: "Montserrat-Bold",
    color: "#FFB406",
  },
  bodyBottomOne: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bodyBottomTwo: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: "10%",
    marginBottom: 10,
  },
});

export default DriverTravelInProcess;
