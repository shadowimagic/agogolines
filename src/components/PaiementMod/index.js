import React, { useState, useEffect, useContext, useRef } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput,
  Modal,
  Alert,
} from "react-native";

import { useSelector, useDispatch } from "react-redux";
import { FirebaseContext } from "../../../redux/src";

//images
import payment from "../../../assets/svg/payment";
import coins from "../../../assets/svg/coins";
import creditCard from "../../../assets/svg/creditcards";
import giftBox from "../../../assets/svg/bonus";
import tickets from "../../../assets/svg/tickets";

import { SvgXml } from "react-native-svg";
import { colors } from "../../common/theme";

//components
import CardList from "../CardList";

import { AntDesign } from "@expo/vector-icons";

import { Feather } from "@expo/vector-icons";

import BankCardModal from "../BankCardModal";

import { EventRegister } from "react-native-event-listeners";

const PaiementMod = (props) => {
  const fermeture = () => {
    EventRegister.emit("initrider", true);
  };

  const mounted = useRef(false);

  const [hideBonus, setHideBonus] = useState(false);
  const [openCB, setOpenCB] = useState(false);
  const [displayModal, setDisplayModal] = useState(false);
  const [modePaiement, setModePaiement] = useState("especes");
  const [espece, setEspece] = useState(true);
  const [promo, setPromo] = useState("");
  const [activePromo, setActivePromo] = useState(false);
  const [activePoint, setActivePoint] = useState(false);

  const dispatch = useDispatch();
  const { api } = useContext(FirebaseContext);

  const { fetchCreditCard, fetchPoint, chooseCreditCard, fetchCommunication } =
    api || {};

  const auth = useSelector((state) => state.auth);
  const creditcarddata = useSelector((state) => state.creditcarddata);

  const communicationdata = useSelector((state) => state.communicationdata);
  const communicationInformations = communicationdata.communication;

  const pointdata = useSelector((state) => state.pointdata);
  const pointInformations = pointdata.point;

  //const communicationdata = useSelector(state => state.communicationdata);
  //const communicationInformations = communicationdata.communication;

  const [selectedCard, setSelectedCard] = useState(null);

  const onChangeCard = (index) => {
    if (creditcarddata?.creditcards?.[index]) {
      if (creditcarddata.creditcards[index].cardid !== selectedCard?.cardid) {
        setSelectedCard({
          ...creditcarddata.creditcards[index],
          index,
        });
      }
    }
  };

  const onPressEnregister = async () => {
    selectedCard && dispatch(chooseCreditCard(selectedCard));
    if (communicationInformations && communicationInformations.montant) {
      if (communicationInformations.code === promo) {
        props.onPressRecherche(
          modePaiement,
          communicationInformations.montant,
          communicationInformations.code
        );
        props.onSave();
        props.onPressNext(modePaiement);
      } else {
        props.onPressRecherche(modePaiement, 0, "0");
        props.onSave();
        props.onPressNext(modePaiement);
        //Alert.alert("Code promo incorrect");
      }
    } else {
      props.onPressRecherche(modePaiement, 0, "0");
      props.onSave();
      props.onPressNext(modePaiement);
    }
  };

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      dispatch(fetchCreditCard());
      dispatch(fetchPoint());
      fetchCommunication && dispatch(fetchCommunication());
    }
  }, [auth.info]);

  useEffect(() => {
    if (creditcarddata?.creditcards?.length) {
      if (!mounted?.current) {
        setSelectedCard({
          ...creditcarddata.creditcards[0],
          index: 0,
        });
        mounted.current = true;
      }
    }
  }, [creditcarddata?.creditcards]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      if (pointInformations) {
        setActivePoint(true);
      }
    }
  }, [auth.info, pointInformations, activePoint]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      if (communicationInformations && communicationInformations.montant) {
        if (communicationInformations.code === promo) {
          setActivePromo(true);
        }
      }
    }
  }, [auth.info, communicationInformations, activePromo]);

  return (
    <View style={styles.container}>
      <Modal transparent={true} visible={displayModal}>
        <BankCardModal setDisplayModal={setDisplayModal} />
      </Modal>
      <View style={styles.cardContainer}>
        <View style={styles.cardHeader}>
          <View>
            <SvgXml xml={payment} style={{ width: 22, height: 26 }} />
          </View>
          <Text style={styles.cardHeaderText}>MODE DE PAIEMENT</Text>
        </View>
        <View
          style={[
            hideBonus && openCB
              ? styles.priceContainerWithOpenCB
              : styles.priceContainer,
          ]}
        >
          <Text style={styles.price}>
            {props.data && props.data.prix_affiner
              ? props.data.prix_affiner + "€"
              : 0 + "€"}
          </Text>
        </View>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <View style={styles.textContainer}>
            <Text style={styles.text}>
              Vous ne serez débité que si la course a lieu et du montant
              définitif négocié avec le conducteur.
            </Text>
          </View>
        </View>

        <View
          style={[
            styles.bonusThere,
            hideBonus && openCB ? styles.bonusHide : styles.bonusThere,
          ]}
        >
          {!hideBonus && activePoint && (
            <TouchableOpacity
              style={{
                width: 300,
                borderRadius: 10,
                shadowColor: "black",
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.23,
                shadowRadius: 2.62,
                elevation: 4,
                backgroundColor: "white",
                paddingVertical: 5,
              }}
              onPress={() => {
                setEspece(false);
                setHideBonus(false);
                setModePaiement("bonus");
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-around",
                  alignItems: "center",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <SvgXml xml={giftBox} style={{ width: 22, height: 26 }} />
                  <Text style={[!espece ? styles.bonus : styles.bonusUnFocus]}>
                    Bonus
                  </Text>
                </View>
                <View
                  style={{
                    width: 1,
                    height: 40,
                    backgroundColor: "#D4D4D4",
                  }}
                ></View>
                <View style={{ flexDirection: "column" }}>
                  <Text
                    style={{
                      fontFamily: "Montserrat-Bold",
                      color: "#979797",
                      fontSize: 12,
                      lineHeight: 15,
                    }}
                  >
                    Payez cette course
                  </Text>
                  <Text
                    style={{
                      fontFamily: "Montserrat-Bold",
                      color: "#979797",
                      fontSize: 12,
                      lineHeight: 15,
                    }}
                  >
                    avec vos points
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          <TouchableOpacity
            style={[
              styles.roundedCart,
              openCB && hideBonus ? styles.margB : styles.roundedCart,
            ]}
            onPress={() => {
              setOpenCB(!openCB);
              setHideBonus(!hideBonus);
              //  setMode("cartecredit");
              setModePaiement("cartecredit");
              setEspece(false);
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <SvgXml xml={creditCard} style={{ width: 22, height: 15 }} />
                <Text style={[hideBonus ? styles.textCBFocus : styles.textCB]}>
                  Carte bancaire
                </Text>
              </View>
              <View
                style={{
                  width: 1,
                  height: 40,
                  backgroundColor: "#D4D4D4",
                }}
              ></View>
              <Text
                style={{
                  fontFamily: "Montserrat-Medium",
                  color: "#979797",
                  fontSize: 12,
                  lineHeight: 15,
                }}
              >
                Gagnez 1 point !
              </Text>
              {/*  */}
            </View>
            {openCB && (
              <View style={{ alignItems: "center" }}>
                {creditcarddata.creditcards && (
                  <CardList
                    creditcards={creditcarddata.creditcards}
                    selectedCard={selectedCard}
                    onChangeCard={onChangeCard}
                  />
                )}
                {creditcarddata.creditcards === null ||
                creditcarddata?.creditcards?.length < 3 ? (
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      marginTop: 20,
                      backgroundColor: "#5A48CB",
                      borderRadius: 10,
                      width: 270,
                      paddingVertical: 6,
                    }}
                    onPress={() => setDisplayModal(true)}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-around",
                        width: "95%",
                      }}
                    >
                      <AntDesign name="plus" size={20} color="white" />
                      <Text
                        style={{
                          fontSize: 12,
                          color: "#fff",
                          lineHeight: 15,
                          fontFamily: "Montserrat-Bold",
                        }}
                      >
                        AJOUTER MOYEN DE PAIEMENT
                      </Text>
                    </View>
                    <View></View>
                  </TouchableOpacity>
                ) : null}
              </View>
            )}
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              width: 300,
              borderRadius: 10,
              shadowColor: "black",
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.23,
              shadowRadius: 2.62,
              elevation: 4,
              backgroundColor: "white",
              paddingVertical: 5,
            }}
            onPress={() => {
              setEspece(!espece);
              setOpenCB(false);
              setHideBonus(false);
              // setMode("especes");
              setModePaiement("especes");
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <SvgXml xml={coins} style={{ width: 25, height: 20 }} />
                <Text style={[espece ? styles.textCBFocus : styles.textCB]}>
                  Espèces
                </Text>
              </View>
              <View
                style={{
                  width: 1,
                  height: 40,
                  backgroundColor: "#D4D4D4",
                }}
              ></View>
              <Text
                style={{
                  fontFamily: "Montserrat-Medium",
                  color: "#979797",
                  fontSize: 12,
                  lineHeight: 15,
                }}
              >
                Gagnez 1 point !
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        {activePromo && (
          <View
            style={[
              hideBonus && openCB
                ? styles.codePromoContainerWithOpenCB
                : styles.codePromoContainer,
            ]}
          >
            <SvgXml xml={tickets} style={{ width: 17, height: 17 }} />
            <Text
              style={{
                fontFamily: "Montserrat-Bold",
                color: "#000",
                fontSize: 12,
                lineHeight: 15,
                textTransform: "uppercase",
              }}
            >
              Code promo
            </Text>
          </View>
        )}
        {activePromo && (
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <TextInput
              style={{
                height: 36,
                width: 136,
                paddingHorizontal: 10,
                fontFamily: "Montserrat-Medium",
                fontSize: 15,
                lineHeight: 20,
                borderRadius: 10,
                borderWidth: 1,
                textAlign: "center",
              }}
              onChangeText={(text) => setPromo(text)}
            />
          </View>
        )}
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
            alignItems: "center",
            marginTop: 95,
          }}
        >
          <TouchableOpacity
            style={{
              width: 125,
              borderRadius: 10,
              borderWidth: 1,
            }}
            onPress={() => {
              props.setDisplayPaieMod(false);
              fermeture();
            }}
          >
            <Text
              style={{
                paddingVertical: 10,
                textAlign: "center",
                fontFamily: "Montserrat-Bold",
                fontSize: 15,
                lineHeight: 18,
              }}
            >
              ANNULER
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: 150,
              borderRadius: 10,
              backgroundColor: "#5A48CB",
            }}
            onPress={onPressEnregister}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  paddingVertical: 10,
                  textAlign: "center",
                  fontFamily: "Montserrat-Bold",
                  fontSize: 15,
                  lineHeight: 18,
                  color: "white",
                }}
              >
                ENREGISTRER
              </Text>
              <Feather name="chevrons-right" size={24} color="white" />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    opacity: 1,
  },
  cardContainer: {
    width: 335,
    paddingBottom: 20,
    zIndex: 2,
    borderWidth: 1,
    borderRadius: 18,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    backgroundColor: "white",
  },
  cardHeader: {
    flexDirection: "row",
    width: "100%",
    height: 60,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#5A48CB",
    borderTopLeftRadius: 18,
    borderTopRightRadius: 18,
  },
  cardHeaderText: {
    color: "white",
    fontFamily: "Montserrat-Bold",
    marginLeft: 10,
    fontSize: 15,
    lineHeight: 18,
    textAlign: "center",
  },
  priceContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  priceContainerWithOpenCB: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
  },
  price: {
    fontSize: 20,
    fontFamily: "Montserrat-Bold",
    lineHeight: 24,
    color: "#5A48CB",
    textAlign: "center",
  },
  textContainer: {
    width: 265,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
  },
  text: {
    color: "#979797",
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    lineHeight: 15,
  },
  bonusThere: {
    alignItems: "center",
    marginTop: 10,
    flexDirection: "column",
    justifyContent: "space-around",
    height: 170,
  },

  bonusHide: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    height: 120,
    marginTop: 70,
  },

  roundedCart: {
    width: 300,
    borderRadius: 10,
    paddingVertical: 8,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    backgroundColor: "white",
  },

  margB: {
    marginTop: 10,
    marginBottom: 10,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    backgroundColor: "white",
  },

  codePromoContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  codePromoContainerWithOpenCB: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 75,
  },
  bonus: {
    marginLeft: 5,
    fontFamily: "Montserrat-Bold",
    color: "#5A48CB",
    fontSize: 15,
    lineHeight: 18,
    textAlign: "center",
  },
  bonusUnFocus: {
    marginLeft: 5,
    fontFamily: "Montserrat-Medium",
    color: "#999999",
    fontSize: 15,
    lineHeight: 18,
    textAlign: "center",
  },

  textCB: {
    marginLeft: 5,
    fontFamily: "Montserrat-Medium",
    color: "#999999",
    fontSize: 15,
    lineHeight: 18,
    textAlign: "center",
  },
  textCBFocus: {
    marginLeft: 5,
    fontFamily: "Montserrat-Bold",
    color: colors.VIOLET,
    fontSize: 15,
    lineHeight: 18,
    textAlign: "center",
  },
});

export default PaiementMod;
