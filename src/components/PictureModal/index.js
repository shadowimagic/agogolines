import React from "react";
import { Alert, View, Linking } from "react-native";
import styles from "./styles";
import { Button } from "react-native-elements";
import * as ImagePicker from "expo-image-picker";

import {
  getCameraPermission,
  getLibraryPermission,
} from "../../functions/PermissionFunctions";

const PictureModal = (props) => {
  const pickImage = async (type) => {
    let permission = false;
    if (type === "camera") {
      permission = await getCameraPermission();
    } else {
      permission = await getLibraryPermission();
    }

    if (permission) {
      let imageOptions = {
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        aspect: [4, 4],
        allowsEditing: true,
        quality: 1,
        base64: true,
        exif: true,
      };
      let result;
      if (type === "local") {
        result = await ImagePicker.launchImageLibraryAsync(imageOptions);
      } else if (type === "camera") {
        result = await ImagePicker.launchCameraAsync(imageOptions);
      }

      if (!result.cancelled) {
        let data = "data:image/jpeg;base64," + result.base64;

        const blob = await new Promise((resolve, reject) => {
          const xhr = new XMLHttpRequest();
          xhr.onload = function () {
            resolve(xhr.response);
          };
          xhr.onerror = function () {
            Alert.alert(language.alert, language.image_upload_error);
            setLoader(false);
          };
          xhr.responseType = "blob";
          xhr.open("GET", Platform.OS == "ios" ? data : result.uri, true);
          xhr.send(null);
        });
        // result.blob = blob;
        props.image(blob);
        props.onClose();
      }
    } else {
      Alert.alert(
        "",
        "Cette application utilise l'appareil photo pour prendre votre photo de profil.",
        [
          {
            text: "Oui",
            onPress: () => {
              Linking.openSettings();
            },
          },
          { text: "Non" },
        ]
      );
    }
  };

  return (
    <View style={styles.screen}>
      <View style={styles.uploadBtnWrapper}>
        <View style={styles.btnContainer}>
          <Button
            title={"prendre une photo"}
            buttonStyle={styles.uploadBtnStyle}
            titleStyle={styles.btnTitleStyle}
            onPress={() => {
              pickImage("camera");
            }}
          />
        </View>
      </View>
      <View style={styles.uploadBtnWrapper}>
        <View style={styles.btnContainer}>
          <Button
            title={"importer une photo"}
            buttonStyle={styles.uploadBtnStyle}
            titleStyle={styles.btnTitleStyle}
            onPress={() => {
              pickImage("local");
            }}
          />
        </View>
      </View>
      <View style={styles.cancelBtnWrapper}>
        <View style={styles.btnContainer}>
          <Button
            title={"annuler"}
            buttonStyle={styles.cancelBtnStyle}
            titleStyle={styles.btnTitleStyle}
            onPress={props.onClose}
          />
        </View>
      </View>
    </View>
  );
};

export default PictureModal;
