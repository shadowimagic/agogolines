import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../common/theme';

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		backgroundColor: 'black',
		opacity: 0.83,
		alignItems: 'center',
		justifyContent: 'center',
	},
	uploadBtnWrapper: {
		marginVertical: 10,
		width: '100%',
		alignItems: 'center',
	},
	cancelBtnWrapper: {
		marginTop: 30,
		width: '100%',
		alignItems: 'center',
	},
	btnContainer: {
		width: '60%',
	},
	uploadBtnStyle: {
		textAlign: 'center',
		borderRadius: 13,
		backgroundColor: colors.VIOLET,
		paddingVertical: 10,
		paddingHorizontal: 20,
	},
	cancelBtnStyle: {
		textAlign: 'center',
		borderRadius: 13,
		backgroundColor: 'transparent',
		paddingVertical: 10,
		paddingHorizontal: 20,
		borderColor: 'white',
		borderWidth: 1,
	},
	btnTitleStyle: {
		textTransform: 'uppercase',
	},
});

export default styles;
