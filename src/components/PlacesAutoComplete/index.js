import React, { useEffect, useRef, useState } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from "react-native";
import { colors } from "./../../common/theme";
import { SvgXml } from "react-native-svg";
import travel from "./../../../assets/svg/travel";
import { Ionicons } from "@expo/vector-icons";
import { Input } from "react-native-elements";
import { useSelector } from "react-redux";
import { Google_Map_Key } from "../../../config";
import useDebounce from "../../hooks/useDebounce";

function PlacesAutoComplete(props) {
  const auth = useSelector((state) => state.auth);
  const [schowInputDep, setSchowInputDep] = useState(false);
  const [schowInputDest, setSchowInputDest] = useState(false);
  const [isShowingResultsDepart, setIsShowingResultsDepart] = useState(false);
  const [isShowingResultsDestination, setIsShowingResultsDestination] =
    useState(false);
  const [searchResultsDepart, setSearchResultsDepart] = useState([]);
  const [searchResultsDestination, setSearchResultsDestination] = useState([]);
  const [searchKeyWordDepart, setSearchKeyWordDepart] = useState(null);
  const [searchKeyWordDestination, setSearchKeyWordDestination] =
    useState(null);
  const inputRef = useRef(null);

  const searchLocation = async (text, type) => {
    if (type == "depart") {
      setSearchKeyWordDepart(text);
    } else {
      setSearchKeyWordDestination(text);
    }
  };

  useDebounce(
    async () => {
      if (schowInputDep) {
        const response = await fetch(
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=" +
            Google_Map_Key +
            "&input=" +
            searchKeyWordDepart
        );
        const json = await response.json();

        if (json.predictions) {
          setSearchResultsDepart(json.predictions);
          setIsShowingResultsDepart(true);
        }
      }
    },
    500,
    [searchKeyWordDepart]
  );

  useDebounce(
    async () => {
      if (schowInputDest) {
        const response = await fetch(
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=" +
            Google_Map_Key +
            "&input=" +
            searchKeyWordDestination
        );
        const json = await response.json();

        if (json.predictions) {
          setSearchResultsDestination(json.predictions);
          setIsShowingResultsDestination(true);
        }
      }
    },
    500,
    [searchKeyWordDestination]
  );

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      if (props.locationType == "depart") {
        setSchowInputDep(true);
      } else {
        setSchowInputDest(true);
      }
    }
  }, [props.locationType]);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.modalView}>
        <Ionicons
          name="ios-chevron-back"
          size={35}
          color={colors.BLACK}
          style={{ alignSelf: "flex-start", left: 10 }}
          onPress={() => {
            props.onIconPress();
            console.log("close modal");
          }}
        />

        <View style={styles.modalContentContainer}>
          <SvgXml xml={travel} />
          <View
            style={{
              flexDirection: "column",
              alignSelf: "center",
              width: "90%",
            }}
          >
            {schowInputDep ? (
              <Input
                ref={inputRef}
                editable={true}
                clearButtonMode="while-editing"
                placeholder="Où etes-vous ?"
                returnKeyType="search"
                placeholderTextColor="#848484"
                value={searchKeyWordDepart}
                //onChangeText={(text) => setSearchKeyWordDepart(text)}
                onChangeText={(text) => searchLocation(text, "depart")}
                containerStyle={{
                  backgroundColor: "#f5f3f3",
                  width: "85%",
                  height: 35,
                  borderRadius: 8,
                  bottom: 15,
                }}
                inputContainerStyle={{
                  borderBottomWidth: 0,
                  height: "100%",
                  width: "100%",
                }}
                inputStyle={{
                  fontSize: 14,
                  fontFamily: "Montserrat-Medium",
                  color: "#848484",
                  lineHeight: 17,
                }}
              />
            ) : (
              <TouchableOpacity
                style={[styles.modalContentButton, { bottom: 7 }]}
                onPress={() => {
                  setSchowInputDep(true);
                  console.log("Départ");
                }}
              >
                <View style={styles.modalContentButtonView}>
                  <Text style={styles.modalContentTitleButton}> Départ </Text>
                  <Text style={styles.modalContentLocationButton}>
                    {props.depLocation.address_name
                      ? props.depLocation.address_name
                      : "Où etes-vous ?"}
                  </Text>
                </View>
              </TouchableOpacity>
            )}

            {schowInputDest ? (
              <Input
                ref={inputRef}
                editable={true}
                clearButtonMode="while-editing"
                placeholder="Où allez-vous ?"
                returnKeyType="search"
                placeholderTextColor="#848484"
                value={searchKeyWordDestination}
                //onChangeText={(text) => setSearchResultsDestination(text)}
                onChangeText={(text) => searchLocation(text, "destination")}
                containerStyle={{
                  backgroundColor: "#f5f3f3",
                  width: "85%",
                  height: 35,
                  borderRadius: 8,
                  top: 15,
                }}
                inputContainerStyle={{
                  borderBottomWidth: 0,
                  height: "100%",
                  width: "100%",
                }}
                inputStyle={{
                  fontSize: 14,
                  fontFamily: "Montserrat-Medium",
                  color: "#848484",
                  lineHeight: 17,
                }}
              />
            ) : (
              <TouchableOpacity
                style={[styles.modalContentButton, { top: 7 }]}
                onPress={() => {
                  setSchowInputDest(true);
                }}
              >
                <View
                  style={[styles.modalContentButtonView, { marginVertical: 8 }]}
                >
                  <Text style={styles.modalContentTitleButton}> Arrivée </Text>
                  <Text style={styles.modalContentLocationButton}>
                    {props.destLocation.address_name
                      ? props.destLocation.address_name
                      : "Où allez-vous ?"}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
          </View>
        </View>
        <View style={styles.modalDivider} />
      </View>

      {
        <View
          style={{
            alignSelf: "center",
            position: "absolute",
            top: 185,
            width: "100%",
          }}
        >
          {isShowingResultsDepart && (
            <FlatList
              data={searchResultsDepart}
              renderItem={({ item, index }) => {
                return (
                  <TouchableOpacity
                    key={item.place_id}
                    style={[
                      {
                        width: "100%",
                        justifyContent: "center",
                        borderBottomColor: "#ECECEC",
                        paddingLeft: 15,
                        height: 50,
                      },
                      index == searchResultsDepart.length - 1
                        ? { borderBottomWidth: 0 }
                        : { borderBottomWidth: 1 },
                    ]}
                    onPress={() => {
                      setSearchKeyWordDepart(item.description);
                      setIsShowingResultsDepart(false);
                      props.onCalculPosition(
                        item.place_id,
                        item.description,
                        "depart"
                      );
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Montserrat-Medium",
                        color: "#848484",
                        fontWeight: "bold",
                        fontSize: 14,
                      }}
                    >
                      {item.description}
                    </Text>
                  </TouchableOpacity>
                );
              }}
              keyExtractor={(item, index) => {
                return item.place_id;
              }}
              style={styles.searchResultsContainer}
            />
          )}
          {isShowingResultsDestination && (
            <FlatList
              data={searchResultsDestination}
              renderItem={({ item, index }) => {
                return (
                  <TouchableOpacity
                    key={item.place_id}
                    style={[
                      {
                        width: "100%",
                        justifyContent: "center",
                        borderBottomColor: "#ECECEC",
                        paddingLeft: 15,
                        height: 50,
                      },
                      index == searchResultsDestination.length - 1
                        ? { borderBottomWidth: 0 }
                        : { borderBottomWidth: 1 },
                    ]}
                    onPress={() => {
                      //console.log("destination :"+ JSON.stringify(item));
                      setSearchKeyWordDestination(item.description);
                      setIsShowingResultsDestination(false);
                      props.onCalculPosition(
                        item.place_id,
                        item.description,
                        "destination"
                      );
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Montserrat-Medium",
                        color: "#848484",
                        fontWeight: "bold",
                        fontSize: 14,
                      }}
                    >
                      {item.description}
                    </Text>
                  </TouchableOpacity>
                );
              }}
              keyExtractor={(item, index) => {
                return item.place_id;
              }}
              style={styles.searchResultsContainer}
            />
          )}
        </View>
      }
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    width: "100%",
    height: "100%",
  },
  modalView: {
    position: "absolute",
    backgroundColor: "#FFFEFE",
    paddingTop: 10,
  },
  modalDivider: {
    borderBottomWidth: 1,
    borderColor: "#e1dede",
    height: 10,
    marginBottom: 10,
    width: "100%",
  },
  modalTitleText: {
    alignSelf: "center",
    color: colors.BLACK,
    fontFamily: "Montserrat-Bold",
    textAlign: "center",
    fontSize: 18,
    lineHeight: 22,
  },
  modalContentContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    marginBottom: 10,
  },
  modalFooterButtonTitle: {
    color: colors.WHITE,
    fontSize: 16,
    fontFamily: "Montserrat-Bold",
    textTransform: "uppercase",
    textAlign: "center",
  },
  modalFooterButtonContainer: {
    borderRadius: 13,
    width: "40%",
    marginTop: "5%",
  },
  modalFooterButton: {
    borderRadius: 13,
    backgroundColor: colors.VIOLET,
  },
  modalContentButtonView: {
    height: 40,
    width: "100%",
    flexDirection: "column",
  },
  modalContentButton: {
    height: 40,
    width: "100%",
  },
  modalContentTitleButton: {
    fontSize: 12,
    lineHeight: 15,
    color: colors.BLACK,
    fontFamily: "Montserrat-Bold",
    textTransform: "uppercase",
  },
  modalContentLocationButton: {
    fontSize: 14,
    lineHeight: 18,
    fontFamily: "Montserrat-Medium",
    color: "#848484",
    width: 300,
  },
  searchResultsContainer: {
    width: "100%",
  },
});

export default PlacesAutoComplete;
