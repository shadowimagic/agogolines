import React, { useContext } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
} from "react-native";

//colors
import { colors } from "../../common/theme";

import riderImg from "../../../assets/images/avatar_login.png";
import tel from "../../../assets/svg/tel";
import email from "../../../assets/svg/email";

import { SvgXml } from "react-native-svg";
import { useDispatch, useSelector } from "react-redux";
import { FirebaseContext } from "../../../redux/src";

import { EventRegister } from "react-native-event-listeners";
import { RequestPushMsg } from "../../../redux/src/other/NotificationFunctions";
import { language } from "../../../config";
import { wazeRouting } from "../../functions/WazeFunctions";

function ConfirmPickUpRider(props) {
  const {
    closeConfirmPickUpRider,
    openRouteConfirm,
    openDriverTravelInProcess,
  } = props;
  const { api } = useContext(FirebaseContext);
  const { addTravel, addCancellationReason } = api || {};
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;

  const dispatch = useDispatch();

  let reasonsData = {
    reasons: null,
    create_date: null,
    create_time: null,
    riderId: null,
    riderFirstName: null,
    riderLastName: null,
    riderProfileImg: null,
    riderEmail: null,
    riderMobile: null,
  };

  const onRoutingToDestination = () => {
    if (travelInformations) {
      const longitude = travelInformations?.rider_long_dest;
      const latitude = travelInformations?.rider_lat_dest;

      wazeRouting({
        longitude,
        latitude,
      });
    }

    const timeout = setTimeout(() => {
      handleConfirmPickUp();
      clearTimeout(timeout);
    }, 50);
  };

  const handleConfirmPickUp = () => {
    travelInformations.recuperer_rider = true;
    dispatch(addTravel(travelInformations));
    closeConfirmPickUpRider();
    openDriverTravelInProcess();
  };

  const onConfirmPickUpRider = () => {
    Alert.alert("voulez-vous acheminer vers la destination du client ?", "", [
      {
        text: "Oui",
        onPress: onRoutingToDestination,
      },
      {
        text: "Non",
        onPress: handleConfirmPickUp,
      },
    ]);
  };

  const onRiderAbsent = () => {
    if (travelInformations) {
      reasonsData.reasons = "Passager absent";
      reasonsData.create_date =
        new Date().getDate() +
        "/" +
        new Date().getMonth() +
        "/" +
        new Date().getFullYear();
      reasonsData.create_time = new Date().getTime();
      reasonsData.riderId = travelInformations.propose_rider_id;
      reasonsData.riderEmail = travelInformations.propose_email_rider;
      reasonsData.riderLastName = travelInformations.propose_lastname_rider;
      reasonsData.riderMobile = travelInformations.propose_phone_rider;
      reasonsData.riderFirstName = travelInformations.propose_firstname_rider;
      reasonsData.riderProfileImg =
        travelInformations.propose_profile_img_rider || null;

      dispatch(addCancellationReason(reasonsData));

      travelInformations.recuperer_rider = false;
      travelInformations.isFound = false;
      travelInformations.isAffiner_rider = false;
      travelInformations.isAffiner_driver = false;
      travelInformations.arriver_vers_passager = false;
      travelInformations.status = "init";
      dispatch(addTravel(travelInformations));

      RequestPushMsg(
        travelInformations.rider_pushToken,
        language.notification_title,
        "Votre conducteur a annulé la course"
      );
    }
    closeConfirmPickUpRider();
    openRouteConfirm();
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.headerDocsContainer}>
        <Text style={styles.headerText}>Avez-vous récupéré</Text>
        <Text style={styles.headerText}>votre voyageur ?</Text>
      </View>

      <View style={styles.boxUnderHeaderText}>
        <>
          <Image
            source={
              travelInformations &&
              travelInformations != null &&
              travelInformations.propose_profile_img_rider &&
              travelInformations.propose_profile_img_rider != "non"
                ? { uri: travelInformations.propose_profile_img_rider }
                : riderImg
            }
            style={{ width: 40, height: 40, borderRadius: 25 }}
          />
          <View style={{ flexDirection: "column", marginRight: 50 }}>
            <Text style={styles.profileTextNom}>
              {travelInformations && travelInformations != null
                ? travelInformations.propose_firstname_rider
                : "Juliette"}
            </Text>
            <Text style={styles.profileTextNom}>
              {travelInformations && travelInformations != null
                ? travelInformations.propose_lastname_rider
                : "FERNANDEZ"}
            </Text>
          </View>
        </>
        <View style={{ flexDirection: "row" }}>
          <SvgXml xml={email} style={{ marginRight: 10 }} />
          <SvgXml xml={tel} />
        </View>
      </View>
      <View style={styles.scrollViewContainer}>
        <TouchableOpacity
          onPress={onConfirmPickUpRider}
          style={{
            width: "85%",
            height: 50,
            backgroundColor: "#5A48CB",
            alignSelf: "center",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 13,
            top: "15%",
          }}
        >
          <Text
            style={[
              styles.profileText,
              {
                fontSize: 20,
                lineHeight: 24,
                color: "white",
              },
            ]}
          >
            Oui, c’est parti !
          </Text>
        </TouchableOpacity>
        <View
          style={{
            width: "85%",
            height: 50,
            alignSelf: "center",
            top: "20%",
          }}
        >
          <Text
            style={{
              fontFamily: "Montserrat-Medium",
              fontSize: 13,
              lineHeight: 16,
              color: "#818181",
              textAlign: "justify",
            }}
          >
            Si vous indiquez qu’il est absent, la course sera automatiquement
            annulée.
          </Text>
          <Text
            style={{
              fontFamily: "Montserrat-Medium",
              fontSize: 13,
              lineHeight: 16,
              color: "#818181",
              textAlign: "justify",
            }}
          >
            Tenter de joindre le passager par téléphone, s’il ne répond pas au
            bout de 5min, vous pouvez confirmer ci-dessous son absence.
          </Text>
        </View>
        <TouchableOpacity
          onPress={onRiderAbsent}
          style={{
            width: "85%",
            height: 50,
            alignSelf: "center",
            alignItems: "center",
            justifyContent: "center",
            borderColor: "black",
            borderWidth: 1,
            borderRadius: 13,
            top: "37%",
          }}
        >
          <Text
            style={[
              styles.profileText,
              {
                fontSize: 20,
                lineHeight: 24,
                color: "black",
              },
            ]}
          >
            Non, il est absent.
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    height: "40%",
    paddingHorizontal: "8%",
    flexBasis: "40%",
    justifyContent: "center",
    alignItems: "center",
  },

  headerText: {
    color: colors.WHITE,
    fontFamily: "Montserrat-Medium",
    fontSize: 22,
    lineHeight: 27,
  },

  profileText: {
    fontFamily: "Montserrat-Medium",
    fontSize: 16,
    lineHeight: 19,
    color: colors.BLACK,
  },

  profileTextNom: {
    fontFamily: "Montserrat-Medium",
    fontSize: 16,
    lineHeight: 19,
    left: 12,
    color: colors.BLACK,
  },

  boxUnderHeaderText: {
    width: "90%",
    height: 75,
    backgroundColor: colors.WHITE,
    position: "absolute",
    alignSelf: "center",
    top: "30%",
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 6,
    elevation: 8,
    zIndex: 1,
    borderRadius: 12,
    justifyContent: "space-around",
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 13,
  },

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    position: "absolute",
    width: "100%",
    height: "80%",
    top: "36%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: "5%",
    justifyContent: "flex-start",
  },
});

export default ConfirmPickUpRider;
