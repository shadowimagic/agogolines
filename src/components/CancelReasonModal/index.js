import React, { useContext, useEffect, useState } from "react";
import { View, StyleSheet, Text } from "react-native";
import { Button } from "react-native-elements";
import { colors } from "../../common/theme";
import moment from "moment";
import { FirebaseContext } from "../../../redux/src";
import { useDispatch, useSelector } from "react-redux";
import { EventRegister } from "react-native-event-listeners";
import { RequestPushMsg } from "../../../redux/src/other/NotificationFunctions";
import { language } from "../../../config";

const CancelReasonModal = (props) => {
  const { closeCancelReason, onSelectedCancelReason } = props;
  const { api } = useContext(FirebaseContext);
  const { addCancellationReason, addTravel } = api || {};
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;
  let reasonsData = {
    reasons: null,
    create_date: null,
    create_time: null,
    riderId: null,
    riderFirstName: null,
    riderLastName: null,
    riderEmail: null,
    riderMobile: null,
  };
  const [reason, setReasons] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    if (reason) {
      if (travelInformations) {
        reasonsData.reasons = reason;
        reasonsData.create_date =
          new Date().getDate() +
          "/" +
          new Date().getMonth() +
          "/" +
          new Date().getFullYear();
        reasonsData.create_time = new Date().getTime();
        reasonsData.riderId = travelInformations.propose_rider_id;
        reasonsData.riderEmail = travelInformations.propose_email_rider;
        reasonsData.riderLastName = travelInformations.propose_lastname_rider;
        reasonsData.riderMobile = travelInformations.propose_phone_rider;
        reasonsData.riderFirstName = travelInformations.propose_firstname_rider;

        dispatch(addCancellationReason(reasonsData));
        travelInformations.isFound = false;
        travelInformations.isAffiner_rider = false;
        travelInformations.isAffiner_driver = false;
        travelInformations.status = "init";
        dispatch(addTravel(travelInformations));
        EventRegister.emit("cancelreason", true);
        RequestPushMsg(
          travelInformations.rider_pushToken,
          language.notification_title,
          "Votre conducteur a annulé la course"
        );
      }

      onSelectedCancelReason();
    }
  }, [reason]);

  return (
    <View style={styles.screen}>
      <View style={styles.textContainer}>
        <Text style={styles.textTitle}> Que s’est-il passé ? </Text>
      </View>

      <View style={styles.cancelBtnWrapper}>
        <View style={styles.btnContainer}>
          <Button
            title={"Passager injoignable"}
            buttonStyle={styles.cancelBtnStyle}
            titleStyle={styles.btnTitleStyle}
            onPress={() => {
              setReasons("Passager injoignable");
            }}
          />
        </View>
      </View>

      <View style={styles.cancelBtnWrapper}>
        <View style={styles.btnContainer}>
          <Button
            title={"Numéro de passager invalide"}
            buttonStyle={styles.cancelBtnStyle}
            titleStyle={styles.btnTitleStyle}
            onPress={() => {
              setReasons("Numéro de passager invalide");
            }}
          />
        </View>
      </View>

      <View style={styles.cancelBtnWrapper}>
        <View style={styles.btnContainer}>
          <Button
            title={"Problème sur la route"}
            buttonStyle={styles.cancelBtnStyle}
            titleStyle={styles.btnTitleStyle}
            onPress={() => {
              setReasons("Problème sur la route");
            }}
          />
        </View>
      </View>

      <View style={styles.cancelBtnWrapper}>
        <View style={styles.btnContainer}>
          <Button
            title={"Problème avec l’application"}
            buttonStyle={styles.cancelBtnStyle}
            titleStyle={styles.btnTitleStyle}
            onPress={() => {
              setReasons("Problème avec l’application");
            }}
          />
        </View>
      </View>

      <View style={styles.cancelBtnWrapper}>
        <View style={styles.btnContainer}>
          <Button
            title={"Problème avec le véhicule"}
            buttonStyle={styles.cancelBtnStyle}
            titleStyle={styles.btnTitleStyle}
            onPress={() => {
              setReasons("Problème avec le véhicule");
            }}
          />
        </View>
      </View>

      <View style={styles.cancelBtnWrapper}>
        <View style={styles.btnContainer}>
          <Button
            title={"Mauvaise attitude du passager"}
            buttonStyle={styles.cancelBtnStyle}
            titleStyle={styles.btnTitleStyle}
            onPress={() => {
              setReasons("Mauvaise attitude du passager");
            }}
          />
        </View>
      </View>

      <View style={styles.conitnueBUttonWrapper}>
        <View style={styles.btnContainer}>
          <Button
            title={"Je continue ma course"}
            buttonStyle={styles.uploadBtnStyle}
            titleStyle={styles.btnTitleStyle}
            onPress={closeCancelReason}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "black",
    opacity: 0.83,
    alignItems: "center",
    justifyContent: "center",
  },
  textContainer: {
    marginBottom: 50,
    marginTop: 25,
  },
  validateButtonWrapper: {
    width: "100%",
    alignItems: "center",
  },
  cancelBtnWrapper: {
    width: "100%",
    alignItems: "center",
    marginBottom: 20,
  },
  conitnueBUttonWrapper: {
    width: "100%",
    alignItems: "center",
    marginBottom: 20,
    top: 20,
  },
  btnContainer: {
    width: "80%",
    height: 50,
  },
  uploadBtnStyle: {
    textAlign: "center",
    borderRadius: 13,
    backgroundColor: colors.VIOLET,
    height: "100%",
  },
  cancelBtnStyle: {
    textAlign: "center",
    borderRadius: 13,
    backgroundColor: "transparent",
    borderColor: "white",
    height: "100%",
    borderWidth: 1,
  },
  btnTitleStyle: {
    textTransform: "uppercase",
    fontFamily: "Montserrat-Bold",
    fontSize: 12,
    lineHeight: 15,
    color: "white",
  },
  textTitle: {
    textTransform: "uppercase",
    fontFamily: "Montserrat-Bold",
    fontSize: 18,
    lineHeight: 22,
    textAlign: "center",
    color: "white",
  },
});

export default CancelReasonModal;
