import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-elements";
import { SvgXml } from "react-native-svg";
import notFound from "../../../assets/svg/not-found";
import { colors } from "../../common/theme";

function NoDriverFoundByRiderModal(props) {
  
  return (
    <View style={styles.container}>
      <View style={styles.modalView}>
        <View style={styles.modalHeader}>
          <SvgXml xml={notFound} style={{ width: 35, height: 35 }} />
        </View>
        <Text style={[styles.modalTitleText, { marginTop: 15 }]}>
          Aucun conducteur n’est disponible
        </Text>
        <Text style={[styles.modalTitleText, { marginBottom: 5 }]}>
          actuellement.
        </Text>
        <Text
          style={[
            styles.modalTitleText,
            { marginBottom: 15, fontFamily: "Montserrat-Medium" },
          ]}
        >
          Veuillez réessayer ultérieurement.
        </Text>

        <Button
          title="réessayer"
          titleStyle={styles.modalFooterButtonTitle}
          buttonStyle={styles.modalFooterButton}
          containerStyle={styles.modalFooterButtonContainer}
          onPress={props.onRetry}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  modalView: {
    backgroundColor: "#FFFEFE",
    borderTopLeftRadius: 13,
    borderTopRightRadius: 13,
    paddingTop: 15,
    height: "33%",
    width: "90%",
    alignItems: "center",
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    shadowOpacity: 0.8,
    elevation: 3,
  },
  modalHeader: {
    flexDirection: "row-reverse",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  modalDivider: {
    borderBottomColor: colors.BLACK,
    borderBottomWidth: 1,
    borderColor: "#E1DEDE",
    borderStyle: "solid",
    height: 10,
    marginBottom: 10,
    width: "90%",
  },
  modalTitleText: {
    alignSelf: "center",
    color: "#9A9A9A",
    fontFamily: "Montserrat-Bold",
    textAlign: "center",
    fontSize: 14,
    lineHeight: 18,
  },
  modalContentContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  modalFooterButtonTitle: {
    color: colors.WHITE,
    fontSize: 16,
    fontFamily: "Montserrat-Bold",
    textTransform: "uppercase",
    textAlign: "center",
  },
  modalFooterButtonContainer: {
    borderRadius: 13,
    width: "50%",
  },
  modalFooterButton: {
    borderRadius: 13,
    backgroundColor: colors.VIOLET,
  },
});

export default NoDriverFoundByRiderModal;
