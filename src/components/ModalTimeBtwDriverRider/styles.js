import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../common/theme";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    width: 380,
    alignSelf: "center",
    alignItems: "center",
  },
  bottomBanner: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
  },
  modal: {
    backgroundColor: "white",
    height: "30%",
    marginHorizontal: 15,
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
  },
  bodyTop: {
    height: "30%",
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
  },
  userImage: {
    width: 47,
    height: 47,
    marginRight: 20,
    borderRadius: 12,
  },
  userName: {
    fontWeight: "bold",
  },
  starIcon: {
    flexDirection: "row",
    marginVertical: 5,
  },
  stopPhoneIcon: {
    flexDirection: "row",
    marginLeft: "auto",
    marginRight: 10,
  },
  iconMargin: {
    marginTop: 10,
    marginRight: 10,
  },
  element: {
    flexDirection: "row",
  },
  bodyBottom: {
    backgroundColor: "white",
    height: "70%",
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  textContainer: {
    marginLeft: 5,
  },
  elementHeaderText: {
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  elementBodyText: {
    color: colors.GREYD,
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    lineHeight: 18,
  },
  elementBodyTextViolet: {
    fontWeight: "bold",
    fontSize: 20,
    lineHeight: 24,
    fontFamily: "Montserrat-Bold",
    color: "#FFB406",
  },
  bodyBottomOne: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bodyBottomTwo: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: "10%",
    marginBottom: 10,
  },
  btnTitleStyle: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 16,
    color: "black",
  },
  btnTitleStyleViolet: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 16,
    color: "white",
  },
  buttonStyle: {
    marginTop: 5,
    backgroundColor: "transparent",
    borderColor: "black",
    borderWidth: 2,
    borderRadius: 13,
  },
  buttonStyleViolet: {
    marginTop: 5,
    backgroundColor: colors.VIOLET,
    borderRadius: 13,
  },
});

export default styles;
