import { AntDesign } from "@expo/vector-icons";
// import { AdMobBanner, setTestDeviceIDAsync } from "expo-ads-admob";
import React, { useContext, useEffect, useState } from "react";
import { Image, Linking, Text, TouchableOpacity, View } from "react-native";
import { EventRegister } from "react-native-event-listeners";
import { SvgXml } from "react-native-svg";
import { useDispatch, useSelector } from "react-redux";
import user from "../../../assets/images/avatar_login.png";
import car from "../../../assets/svg/car";
import email from "../../../assets/svg/email";
import tel from "../../../assets/svg/tel";
import { FirebaseContext } from "../../../redux/src";
import { colors } from "../../common/theme";
import { callNumber } from "../phoneTrigger/index";
import styles from "./styles";

function ModalTimeBtwDriverRider(props) {
  const { api } = useContext(FirebaseContext);
  const { fetchOneTravel, updateTravel, fetchComment, fetchPosition } = api;
  const auth = useSelector((state) => state.auth);
  const travelData = useSelector((state) => state.travel);
  const donner = travelData.travel;

  const positionData = useSelector((state) => state.positiondata);
  const position = positionData.position;

  const comments = useSelector((state) => state.comment);
  const commentInformations = comments.comment;

  const [note, setNote] = useState(0);

  const dispatch = useDispatch();
  const [geoData, setGeoData] = useState(1);

  const annulation = () => {
    EventRegister.emit("initrider", true);
  };

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      if (donner != null) {
        const interval = setInterval(() => {
          dispatch(fetchPosition(props?.datadriver?.driver_id));
          dispatch(fetchOneTravel(props?.datadriver?.driver_id));
        }, 7000);

        return () => {
          clearInterval(interval);
        };
      }
    }
  }, [auth.info]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      if (donner && position && position.location) {
        let distance_in_mn = position.location.distance * 1000;
        setGeoData(Math.round(position.location.duration) || 10);

        if (distance_in_mn <= 500 || donner.arriver_vers_passager === true) {
          props.setDisplayTimeBtwRD(false);
          props.setDisplayDriverPURider(true);
        }
      } else {
        if (donner.arriver_vers_passager === true) {
          props.setDisplayTimeBtwRD(false);
          props.setDisplayDriverPURider(true);
        }
      }

      if (
        donner != null &&
        donner.isFound == false &&
        donner.status == "init"
      ) {
        props.setDisplayTimeBtwRD(false);
        props.setDisplayDriverCancelTravel(true);
      }
    }
  }, [auth.info, donner, position?.location]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      dispatch(fetchComment(props?.datadriver?.driver_id));
    }
  }, [auth.info, props.datadriver]);

  // React.useEffect(() => {
  //   setTestDeviceIDAsync("EMULATOR");
  // }, []);

  useEffect(() => {
    if (auth.info && props.datadriver && auth.info.profile) {
      if (commentInformations) {
        const taille = commentInformations.length;
        let somme = 0;
        let moyenne = 0;

        commentInformations.forEach((element) => {
          somme = somme + element.etoile;
        });
        if (taille != 0 && somme != 0) {
          moyenne = Math.round(somme / taille);

          setNote(moyenne);
        } else {
          setNote(0);
        }
      }
    }
  }, [auth.info, props.datadriver]);

  const afficheEtoile = () => {
    if (note === 1) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 2) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 3) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 4) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 5) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
        </>
      );
    } else {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <View style={styles.bodyTop}>
          <View style={styles.userImage}>
            <Image
              source={
                props.datadriver && props?.datadriver?.driver_profile_img
                  ? { uri: props?.datadriver?.driver_profile_img }
                  : user
              }
              style={{ width: "100%", height: "100%", borderRadius: 50 }}
            />
          </View>

          <View style={styles.userInfo}>
            <Text style={styles.userName}>
              {props?.datadriver?.driver_firstName}{" "}
              {props?.datadriver?.driver_lastName}
            </Text>
            <View style={styles.starIcon}>{afficheEtoile()}</View>
            <SvgXml xml={car} style={{ width: 27, height: 11 }} />
          </View>
          <View style={styles.stopPhoneIcon}>
            <TouchableOpacity
              style={styles.iconMargin}
              onPress={() =>
                Linking.openURL(`sms: ${props?.datadriver?.driver_mobile}`)
              }
            >
              <SvgXml xml={email} style={{ width: 50, height: 50 }} />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.iconMargin}
              onPress={() => {
                callNumber(
                  donner && donner.driver_mobile
                    ? donner.driver_mobile
                    : props?.datadriver?.driver_mobile
                );
              }}
            >
              <SvgXml xml={tel} style={{ width: 50, height: 50 }} />
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            marginTop: 20,
            height: "50%",
          }}
        >
          <Text
            style={{
              fontFamily: "Montserrat-Bold",
              fontSize: 14,
              lineHeight: 18,
              color: "#9E9E9E",
            }}
          >
            Il arrivera dans {geoData && geoData} min
          </Text>
          <Text
            style={{
              fontFamily: "Montserrat-Bold",
              fontSize: 14,
              lineHeight: 18,
              color: "#9E9E9E",
            }}
          >
            Votre conducteur est en route.
          </Text>

          <TouchableOpacity
            style={{
              width: 150,
              height: 37,
              marginTop: 20,
              backgroundColor: colors.VIOLET,
              borderWidth: 1,
              borderColor: colors.VIOLET,
              borderRadius: 12,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={annulation}
          >
            <Text
              style={{
                fontSize: 10,
                color: colors.WHITE,
                lineHeight: 18,
                textAlign: "center",
                fontFamily: "Montserrat-Bold",
              }}
            >
              ANNULER LA COURSE
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* 
      <AdMobBanner
        style={styles.bottomBanner}
        bannerSize="smartBanner"
        adUnitID="ca-app-pub-5608997176551587/4926705154"
        servePersonalizedAds // true or false
        onDidFailToReceiveAdWithError={(e) => console.log(e)}
      /> */}
    </View>
  );
}

export default ModalTimeBtwDriverRider;
