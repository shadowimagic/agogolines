import React, { useState, useEffect, useContext } from "react";
import { View, Text, Image, TouchableOpacity, Linking } from "react-native";
import { Button } from "react-native-elements";
import styles from "./styles";
import { AntDesign, MaterialCommunityIcons } from "@expo/vector-icons";
import { Alert } from "react-native";
import { colors } from "../../common/theme";
import car from "../../../assets/svg/car";
import user from "../../../assets/images/avatar_login.png";
import { callNumber } from "../phoneTrigger/index";
import { SvgXml } from "react-native-svg";
import email from "../../../assets/svg/email";
import tel from "../../../assets/svg/tel";
import euro from "../../../assets/svg/eurodriver";
import clock from "../../../assets/svg/clock";
import position from "../../../assets/svg/position";
import { FirebaseContext } from "../../../redux/src";
import { useSelector, useDispatch } from "react-redux";

// Functions
import { setData } from "../../functions/AsyncStorageFunctions";

import { EventRegister } from "react-native-event-listeners";

const ValidateRoute = (props) => {
  const fermeture = () => {
    EventRegister.emit("initrider", true);

    if (props.datadriver && props?.datadriver?.driver_id) {
      dispatch(
        updateTravel({
          isAffiner_rider: false,
          driver_id: props?.datadriver?.driver_id,
          isFound: false,
        })
      );
    }
  };

  const { api } = useContext(FirebaseContext);
  const {
    addBooking,
    updateTravel,
    fetchComment,
    fetchOneTravel,
    deleteTravel,
  } = api;
  const auth = useSelector((state) => state.auth);

  const comments = useSelector((state) => state.comment);
  const commentInformations = comments.comment;

  const travelData = useSelector((state) => state.traveldata);
  const creditcarddata = useSelector((state) => state.creditcarddata);

  const { cardToUse } = creditcarddata;

  const travelInf = useSelector((state) => state.travel);
  const travelInformations = travelInf.travel;

  const [travel, setTravel] = useState(null);
  const [note, setNote] = useState(0);
  const dispatch = useDispatch();

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      const interval = setInterval(() => {
        dispatch(fetchComment(props?.datadriver?.driver_id));

        //console.log ("valeur de commentaire:", commentInformations);
        if (commentInformations) {
          //console.log ("valeur de commentaire:", commentInformations);
          const taille = commentInformations.length;
          let somme = 0;
          let moyenne = 0;

          commentInformations.forEach((element) => {
            somme = somme + element.etoile;
          });
          if (taille != 0 && somme != 0) {
            moyenne = Math.round(somme / taille);
            //console.log ("valeur de moyenne commentaire:", moyenne);
            setNote(moyenne);
          } else {
            setNote(0);
          }
        }
      }, 8000);

      return () => {
        clearInterval(interval);
      };
    }
  }, [auth.info, commentInformations]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      const interval = setInterval(() => {
        dispatch(fetchOneTravel(props?.datadriver?.driver_id));
      }, 5000);

      return () => {
        clearInterval(interval);
      };
    }
  }, [auth.info, travelInformations]);

  useEffect(() => {
    if (auth.info && props.datadriver && auth.info.profile) {
      if (travelData && travelData.travel) {
        let data = travelData.travel;
        setTravel(data);
      }
    }
  }, [auth.info, props.datadriver]);

  const afficheEtoile = () => {
    // console.log("valeur de note:",note);
    if (note === 1) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 2) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 3) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 4) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 5) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
        </>
      );
    } else {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    }
  };

  const validation = async () => {
    if (props.riderPriceDone == true) {
      if (travelInformations.mode_paiement == "bonus") {
        let montantpoint = travelInformations.prix_point;
        if (travelInformations.propose_negoPrice <= montantpoint) {
          dispatch(addBooking(travelData.travel));
          dispatch(
            updateTravel({
              isFound: true,
              status: "accepted",
              isAffiner_rider: true,
              isAffiner_driver: true,
              accepted_at: new Date().getTime(),
              driver_id: props?.datadriver?.driver_id,
            })
          );

          props.setTravelCompleted(false);
          props.riderPriceDone
            ? props.setTravelAccepted(true)
            : props.setDisplayTravelConfirm(true);
        } else {
          Alert.alert(
            "Vous n'avez pas assez de points. Merci d'annuler et de  choisir un autre mode de paiement."
          );
        }
      }
      if (travelInformations.mode_paiement === "especes") {
        dispatch(addBooking(travelData.travel));
        dispatch(
          updateTravel({
            isFound: true,
            status: "accepted",
            isAffiner_rider: true,
            isAffiner_driver: true,
            accepted_at: new Date().getTime(),
            driver_id: props?.datadriver?.driver_id,
          })
        );

        props.setTravelCompleted(false);
        props.riderPriceDone
          ? props.setTravelAccepted(true)
          : props.setDisplayTravelConfirm(true);
      }

      if (travelInformations.mode_paiement === "cartecredit") {
        let montantpoint = travelInformations.prix_point;
        let montantpromo = travelInformations.prix_promo;
        let sommepp = montantpoint + montantpromo;
        if (travelInformations.propose_negoPrice <= montantpoint) {
          travelInformations.propose_negoPrice = 0;
          travelInformations.prix_point = 0;
          dispatch(deleteTravel(travelInformations, "non"));

          dispatch(addBooking(travelData.travel));
          dispatch(
            updateTravel({
              isFound: true,
              status: "accepted",
              isAffiner_rider: true,
              isAffiner_driver: true,
              accepted_at: new Date().getTime(),
              driver_id: props?.datadriver?.driver_id,
            })
          );

          props.setTravelCompleted(false);
          props.riderPriceDone
            ? props.setTravelAccepted(true)
            : props.setDisplayTravelConfirm(true);
        } else if (travelInformations.propose_negoPrice <= montantpromo) {
          travelInformations.propose_negoPrice = 0;
          travelInformations.prix_promo = 0;
          dispatch(deleteTravel(travelInformations, "non"));

          dispatch(addBooking(travelData.travel));
          dispatch(
            updateTravel({
              isFound: true,
              status: "accepted",
              isAffiner_rider: true,
              isAffiner_driver: true,
              accepted_at: new Date().getTime(),
              driver_id: props?.datadriver?.driver_id,
            })
          );

          props.setTravelCompleted(false);
          props.riderPriceDone
            ? props.setTravelAccepted(true)
            : props.setDisplayTravelConfirm(true);
        } else if (travelInformations.propose_negoPrice <= sommepp) {
          travelInformations.propose_negoPrice = 0;
          travelInformations.prix_promo = 0;
          travelInformations.prix_point = 0;
          dispatch(deleteTravel(travelInformations, "non"));

          dispatch(addBooking(travelData.travel));
          dispatch(
            updateTravel({
              isFound: true,
              status: "accepted",
              isAffiner_rider: true,
              isAffiner_driver: true,
              accepted_at: new Date().getTime(),
              driver_id: props?.datadriver?.driver_id,
            })
          );

          props.setTravelCompleted(false);
          props.riderPriceDone
            ? props.setTravelAccepted(true)
            : props.setDisplayTravelConfirm(true);
        } else {
          let diff = Math.abs(travelInformations.propose_negoPrice - sommepp);

          travelInformations.propose_negoPrice = diff;
          travelInformations.prix_promo = 0;
          travelInformations.prix_point = 0;
          dispatch(deleteTravel(travelInformations, "non"));

          dispatch(addBooking(travelData.travel));
          dispatch(
            updateTravel({
              isFound: true,
              status: "accepted",
              isAffiner_rider: true,
              isAffiner_driver: true,
              accepted_at: new Date().getTime(),
              driver_id: props?.datadriver?.driver_id,
            })
          );

          props.setTravelCompleted(false);
          props.riderPriceDone
            ? props.setTravelAccepted(true)
            : props.setDisplayTravelConfirm(true);
        }
      }
      await setData("travelId", travelInformations.driver_id);
      if (cardToUse) {
        await setData("cardToUse", cardToUse.cardid);
      }
    } else {
      props.setTravelCompleted(false);
      props.riderPriceDone
        ? props.setTravelAccepted(true)
        : props.setDisplayTravelConfirm(true);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <View style={styles.bodyTop}>
          <View style={styles.userImage}>
            <Image
              source={
                travelInformations && travelInformations.driver_profile_img
                  ? { uri: travelInformations.driver_profile_img }
                  : user
              }
              style={{ width: "100%", height: "100%", borderRadius: 50 }}
            />
          </View>

          <View style={styles.userInfo}>
            <Text style={styles.userName}>
              {travelInformations && travelInformations.driver_firstName
                ? travelInformations.driver_firstName
                : props?.datadriver?.driver_firstName}{" "}
              {travelInformations && travelInformations.driver_lastName
                ? travelInformations.driver_lastName
                : props?.datadriver?.driver_lastName}
            </Text>
            <View style={styles.starIcon}>{afficheEtoile()}</View>
            <SvgXml xml={car} style={{ width: 27, height: 11 }} />
          </View>
          <View style={styles.stopPhoneIcon}>
            <TouchableOpacity
              style={styles.iconMargin}
              onPress={() =>
                Linking.openURL(
                  `sms: ${
                    travelInformations && travelInformations.driver_mobile
                      ? travelInformations.driver_mobile
                      : props?.datadriver?.driver_mobile
                  }`
                )
              }
            >
              <SvgXml xml={email} style={{ width: 50, height: 50 }} />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.iconMargin}
              onPress={() => {
                callNumber(
                  travelInformations && travelInformations.driver_mobile
                    ? travelInformations.driver_mobile
                    : props?.datadriver?.driver_mobile
                );
              }}
            >
              <SvgXml xml={tel} style={{ width: 50, height: 50 }} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bodyBottom}>
          <View style={styles.bodyBottomOne}>
            <View style={styles.element}>
              <SvgXml xml={clock} style={{ width: 50, height: 50 }} />
              <View style={styles.textContainer}>
                <Text style={styles.elementHeaderText}>temps trajet</Text>
                <Text style={styles.elementBodyText}>
                  {travelInformations && travelInformations.rider_trip_time
                    ? travelInformations.rider_trip_time
                    : props.datarider.durer}{" "}
                  min
                </Text>
              </View>
            </View>
            <View style={styles.element}>
              <SvgXml xml={position} style={{ width: 50, height: 50 }} />
              <View style={styles.textContainer}>
                <Text style={styles.elementHeaderText}>distance</Text>
                <Text style={styles.elementBodyText}>
                  {travelInformations && travelInformations.rider_trip_distance
                    ? travelInformations.rider_trip_distance
                    : props.datarider.distance}{" "}
                  Km
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.bodyBottomTwo}>
            <View style={styles.element}>
              <View
                style={{
                  width: 25,
                  height: 25,
                  borderRadius: 20,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <SvgXml xml={euro} style={{ width: 50, height: 50 }} />
              </View>
              <View style={styles.textContainer}>
                <Text style={styles.elementHeaderText}>tarif validé</Text>
                <Text style={styles.elementBodyTextViolet}>
                  {travelInformations &&
                  travelInformations.propose_negoPrice_rider
                    ? travelInformations.propose_negoPrice_rider + " €"
                    : props.datarider.prix_affiner + " €"}
                </Text>
              </View>
            </View>
            <View style={styles.btnElement}>
              <Button
                title="valider"
                buttonStyle={styles.buttonStyleViolet}
                titleStyle={styles.btnTitleStyleViolet}
                icon={
                  <MaterialCommunityIcons
                    name="chevron-double-right"
                    size={24}
                    color="white"
                  />
                }
                iconPosition="right"
                onPress={validation}
              />
              <Button
                title="annuler"
                buttonStyle={styles.buttonStyle}
                titleStyle={styles.btnTitleStyle}
                onPress={() => {
                  props.setTravelCompleted(false);
                  props.setTravelInit(true);
                  fermeture();
                }}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default ValidateRoute;
