import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native";

//images
import visa from "../../../assets/images/visa.png";
import tick from "../../../assets/images/tick.png";
import mastercard from "../../../assets/images/mastercard.png";
import americanExpress from "../../../assets/images/american-express.png";

import Card from "../Card";

const images = {
  visa: visa,
  mastercard: mastercard,
  americanExpress: americanExpress,
};

const CardList = ({ creditcards, selectedCard, onChangeCard }) => {
  //5105 1051 0510 5100
  //4012 8888 8888 1881

  const onPressCard = (index) => () => {
    onChangeCard(index);
  };

  return (
    <>
      {creditcards.map((card, index) => {
        const isSelected = card.cardid === selectedCard?.cardid;
        return (
          <Card
            data={card}
            defaut="oui"
            cardImage={images?.[card.type] || images.visa}
            tickStyle={isSelected && tick}
            vert={isSelected && "#79FF8C"}
            key={card.key}
            onPressCard={onPressCard(index)}
          />
        );
      })}
    </>
  );
};

const styles = StyleSheet.create({
  textDefault: {
    fontSize: 14,
    lineHeight: 18,
    color: "#979797",
    fontFamily: "Montserrat-Medium",
    marginTop: 16,
    marginBottom: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#979797",
    width: 130,
    marginBottom: 12,
  },
  separator: {
    borderBottomWidth: 1,
    marginVertical: 25,
    borderBottomColor: "#CFCFCF",
  },
});

export default CardList;
