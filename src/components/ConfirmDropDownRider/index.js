import React, { useContext, useMemo } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";

//colors
import { colors } from "../../common/theme";

import riderImg from "../../../assets/images/avatar_login.png";

//Constants
import Constants from "expo-constants";
import { useDispatch, useSelector } from "react-redux";
import { FirebaseContext } from "../../../redux/src";

const DropDownRiderByDriver = (props) => {
  const { closeDropDownRiderByDriver, reInitTravel } = props;
  const { api } = useContext(FirebaseContext);
  const { updateProfile, addTravelHistorique } = api || {};
  const auth = useSelector((state) => state.auth);
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;
  const dispatch = useDispatch();

  const returnDate = () => {
    let now = new Date();
    let day = ("0" + now.getDate()).slice(-2);
    let month = ("0" + (now.getMonth() + 1)).slice(-2);
    let today = now.getFullYear() + "-" + month + "-" + day;

    return today;
  };

  const isPaid = useMemo(() => !!travelInformations?.isPaid);

  const returnNextDate = () => {
    let now = new Date();
    now.setDate(now.getDate() + 1);

    let day = ("0" + now.getDate()).slice(-2);
    let month = ("0" + (now.getMonth() + 1)).slice(-2);
    let nextdate = now.getFullYear() + "-" + month + "-" + day;

    return nextdate;
  };

  const confirmDoneTravel = () => {
    if (auth?.info?.profile?.trajet) {
      let resultat = auth.info.profile.trajet - 1;
      let trajetData = {
        trajet: 0,
        dateTrajet: "",
      };
      if (auth.info.profile.trajet >= 0) {
        if (resultat == 0) {
          trajetData.trajet = 2;
          trajetData.dateTrajet = returnNextDate();
        } else {
          trajetData.trajet = resultat;
          trajetData.dateTrajet = returnDate();
        }
        dispatch(updateProfile(auth.info, trajetData));
        dispatch(addTravelHistorique(travelInformations));
      }
    }
    closeDropDownRiderByDriver();
    reInitTravel(true);
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.headerDocsContainer}>
        <Text style={styles.headerText}>Confirmez-vous avoir</Text>
        <Text style={styles.headerText}>déposé votre voyageur ?</Text>
      </View>

      <View style={styles.boxUnderHeaderText}>
        <Image
          source={
            travelInformations &&
            travelInformations != null &&
            travelInformations.propose_profile_img_rider &&
            travelInformations.propose_profile_img_rider != "non"
              ? { uri: travelInformations.propose_profile_img_rider }
              : riderImg
          }
          style={{ width: 40, height: 40, borderRadius: 25 }}
        />
        <View style={{ flexDirection: "column", marginLeft: 11 }}>
          <Text style={styles.profileText}>
            {travelInformations && travelInformations != null
              ? travelInformations.propose_firstname_rider
              : "Juliette"}
          </Text>
          <Text style={styles.profileText}>
            {travelInformations && travelInformations != null
              ? travelInformations.propose_lastname_rider
              : "FERNANDEZ"}
          </Text>
        </View>
      </View>
      <View style={styles.scrollViewContainer}>
        <TouchableOpacity
          onPress={confirmDoneTravel}
          style={{
            width: "90%",
            height: 55,
            backgroundColor: isPaid ? "#5A48CB" : "#D5D4C8",
            marginTop: 100,
            alignSelf: "center",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 13,
          }}
          disabled={!isPaid}
        >
          <Text
            style={[
              styles.profileText,
              {
                fontSize: 16,
                lineHeight: 24,
                color: "white",
              },
            ]}
            numberOfLines={1}
          >
            {isPaid ? "OUI !" : "En attente de paiement ..."}
          </Text>
        </TouchableOpacity>
        <View
          style={{
            width: 285,
            height: 55,
            marginTop: 22,
            alignSelf: "center",
          }}
        >
          <Text
            style={{
              fontFamily: "Montserrat-Bold",
              fontSize: 13,
              lineHeight: 16,
              color: "#818181",
              textAlign: "justify",
            }}
          >
            En cliquant sur oui, vous confirmez avoir déposé votre passager et
            vous pourrez être payé du montant de la course.
          </Text>
        </View>
        {/* <TouchableOpacity
          onPress={() => {
            setInitAllModal();
            btnSend();
          }}
          style={{
            width: 282,
            height: 55,
            marginTop: 20,
            alignSelf: "center",
            alignItems: "center",
            justifyContent: "center",
            borderColor: "black",
            borderWidth: 1,
            borderRadius: 13,
          }}
        >
          <Text
            style={[
              styles.profileText,
              {
                fontSize: 20,
                lineHeight: 24,
                color: "black",
              },
            ]}
          >
            NON
          </Text>
        </TouchableOpacity> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: Constants.statusBarHeight,
    backgroundColor: colors.TRANSPARENT,
  },
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    height: "40%",
    paddingHorizontal: "8%",
    flexBasis: "40%",
    justifyContent: "center",
    alignItems: "center",
  },

  headerText: {
    color: colors.WHITE,
    fontFamily: "Montserrat-Bold",
    fontSize: 22,
    lineHeight: 27,
  },

  profileText: {
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
    lineHeight: 19,
    color: colors.BLACK,
  },

  boxUnderHeaderText: {
    width: 350,
    height: 75,
    backgroundColor: colors.WHITE,
    position: "absolute",
    alignSelf: "center",
    top: "30%",
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 6,
    elevation: 8,
    zIndex: 99999,
    borderRadius: 12,
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 13,
  },

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    position: "absolute",
    width: "100%",
    height: "80%",
    top: "36%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: "5%",
    paddingTop: 20,
    /* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
  },

  originDestination: {
    marginBottom: 12,
  },

  titleOr: {
    width: 110,
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    lineHeight: 15,
    color: "#848484",
  },
  titleDes: {
    width: 95,
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    lineHeight: 15,
    color: "#848484",
  },

  date: {
    width: 110,
    height: 30,
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    lineHeight: 15,
    color: "#848484",
    textAlign: "right",
  },

  price: {
    color: colors.VIOLET,
    width: 110,
    height: 30,
    fontFamily: "Montserrat-Bold",
    fontSize: 14,
    lineHeight: 18,
    textAlign: "right",
  },
});

export default DropDownRiderByDriver;
