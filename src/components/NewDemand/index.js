import React, { useEffect, useState, useContext, useMemo } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Linking,
  SafeAreaView,
} from "react-native";

import { colors } from "../../common/theme";
import userProfil from "../../../assets/images/userprofil.png";
import { useSelector, useDispatch } from "react-redux";
import { FirebaseContext } from "../../../redux/src";
import { Feather } from "@expo/vector-icons";
import moment from "moment";
import { SvgXml } from "react-native-svg";
import travel from "../../../assets/svg/travel";
import email from "../../../assets/svg/email";
import tel from "../../../assets/svg/tel";
import euro from "../../../assets/svg/eurodriver";
import userDriver from "../../../assets/svg/userdriver";
import { callNumber } from "../phoneTrigger/index";
import InputSpinner from "react-native-input-spinner";
import searchLoading from "../../../assets/svg/searchLoading";
import Modal from "react-native-modal";
import ValidationTravelModal from "./../ValidationTravelModal/index";
import TravelValidatedByDriver from "./../TravelValidatedByDriver/index";
import DeniedTravelByDriverModal from "../DeniedTravelByDriverModal";
import confirm from "../../../assets/svg/confirm";
import juste from "../../../assets/svg/juste";

import { EventRegister } from "react-native-event-listeners";
import { RequestPushMsg } from "../../../redux/src/other/NotificationFunctions";
import { language } from "../../../config";
import { deleteRiderRequest } from "../../../redux/src/actions/riderRequest";

const NewDemand = (props) => {
  const { onValidatedTravel, closeNewDemand, onDestPress } = props;
  moment.locale("fr", {
    months:
      "janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split(
        "_"
      ),
    monthsShort:
      "janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split(
        "_"
      ),
    monthsParseExact: true,
    weekdays: "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
    weekdaysShort: "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
    weekdaysMin: "Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),
    weekdaysParseExact: true,
    longDateFormat: {
      LT: "HH:mm",
      LTS: "HH:mm:ss",
      L: "DD/MM/YYYY",
      LL: "D MMMM YYYY",
      LLL: "D MMMM YYYY HH:mm",
      LLLL: "dddd D MMMM YYYY HH:mm",
    },
    calendar: {
      sameDay: "[Aujourd’hui à] LT",
      nextDay: "[Demain à] LT",
      nextWeek: "dddd [à] LT",
      lastDay: "[Hier à] LT",
      lastWeek: "dddd [dernier à] LT",
      sameElse: "L",
    },
    relativeTime: {
      future: "dans %s",
      past: "il y a %s",
      s: "quelques secondes",
      m: "une minute",
      mm: "%d minutes",
      h: "une heure",
      hh: "%d heures",
      d: "un jour",
      dd: "%d jours",
      M: "un mois",
      MM: "%d mois",
      y: "un an",
      yy: "%d ans",
    },
    dayOfMonthOrdinalParse: /\d{1,2}(er|e)/,
    ordinal: function (number) {
      return number + (number === 1 ? "er" : "e");
    },
    meridiemParse: /PD|MD/,
    isPM: function (input) {
      return input.charAt(0) === "M";
    },
    // In case the meridiem units are not separated around 12, then implement
    // this function (look at locale/id.js for an example).
    // meridiemHour : function (hour, meridiem) {
    //     return /* 0-23 hour, given meridiem token and hour 1-12 */ ;
    // },
    meridiem: function (hours, minutes, isLower) {
      return hours < 12 ? "PD" : "MD";
    },
    week: {
      dow: 1, // Monday is the first day of the week.
      doy: 4, // Used to determine first week of the year.
    },
  });

  const dispatch = useDispatch();
  const { api } = useContext(FirebaseContext);
  const { addTravel, deleteOneTravel, updateRiderRequest } = api || {};
  const auth = useSelector((state) => state.auth);
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;

  const [hasValidateAfterAffiner, setHasValidateAfterAffiner] = useState(false);

  const [validationModalVisible, setValidationModalVisible] = useState(false);
  const [deniedModalVisible, setDeniedModalVisible] = useState(false);
  const [travelValidated, setTravelValidated] = useState(false);

  const [newDemandRider, setNewDemandRider] = useState({
    rider_lastName: auth?.info?.profile.lastName,
    rider_firstName: auth?.info?.profile.firstName,
    rider_email: auth?.info?.profile.email,
    rider_nbPlace: 5,
    rider_profileImg: auth?.info?.profile.profile_image,
    rider_mobile: auth?.info?.profile.mobile,
    rider_depAddress: "",
    rider_destAddress: "",
    rider_proposedPrice: 0,
    rider_duration_btw_driver: "",
    rider_travelDuration: 60,
    rider_travelDistance: 1,
  });

  const [proposedPrice, setProposedPrice] = useState(
    travelInformations?.propose_negoPrice_rider || 0
  );

  const onCloseModal = () => {
    console.log("Votre conducteur a refuser la course");
    travelInformations.isFound = false;
    travelInformations.isAffiner_driver = false;
    travelInformations.isAffiner_rider = false;
    travelInformations.status = "init";
    dispatch(addTravel(travelInformations));
    dispatch(deleteRiderRequest(travelInformations.rider_id));
    closeNewDemand();
    RequestPushMsg(
      travelInformations.rider_pushToken,
      language.notification_title,
      "Votre conducteur a refuser la course"
    );
  };

  const handleValidatedTravel = () => {
    setValidationModalVisible(false);
    setDeniedModalVisible(false);
    setTravelValidated(false);
    onValidatedTravel();
  };

  const handleAccepted = () => {
    travelInformations.propose_negoPrice_rider = proposedPrice;
    travelInformations.propose_negoPrice = proposedPrice;
    travelInformations.isAffiner_driver = true;
    dispatch(addTravel(travelInformations));
    setHasValidateAfterAffiner(true);
  };

  useEffect(() => {
    if (auth?.info?.profile) {
      if (travelInformations != null) {
        setNewDemandRider({
          ...newDemandRider,
          rider_depAddress: travelInformations.propose_dep_rider,
          rider_destAddress: travelInformations.propose_dest_rider,
          rider_nbPlace: travelInformations.propose_nbPlace_rider,
          rider_proposedPrice: travelInformations.propose_negoPrice_rider,
          rider_duration_btw_driver:
            Math.round(auth?.info?.profile?.location?.duration) || 5,
          rider_travelDuration: travelInformations.travel_roadDurationInSecond,
          rider_travelDistance: travelInformations.travel_distanceInKm,
          rider_email: travelInformations.propose_email_rider,
          rider_firstName: travelInformations.propose_firstname_rider,
          rider_lastName: travelInformations.propose_lastname_rider,
          rider_mobile: travelInformations.propose_phone_rider,
          rider_profileImg: travelInformations.propose_profile_img_rider,
        });
        setProposedPrice(travelInformations.propose_negoPrice_rider);
      }
    }
  }, [auth?.info?.profile]);

  const renderTravelStatus = useMemo(() => {
    if (travelInformations?.status === "accepted") {
      return (
        <View
          style={{
            backgroundColor: colors.WHITE,
            justifyContent: "space-around",
            alignItems: "center",
            width: "100%",
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            shadowColor: "black",
            shadowOffset: { height: 2, width: 2 },
            shadowOpacity: 0.26,
            shadowRadius: 6,
            elevation: 6,
            paddingVertical: 15,
          }}
        >
          <View
            style={{
              width: 60,
              height: 60,
              borderRadius: 1000,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: colors.VIOLET,
              marginBottom: 20,
            }}
          >
            <SvgXml xml={juste} />
          </View>

          <View>
            <Text
              style={{
                textAlign: "center",
                color: colors.VIOLET,
                fontSize: 14,
                fontFamily: "Montserrat-Bold",
                lineHeight: 17,
                marginBottom: 20,
              }}
            >
              Tarif validé avec succès !
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              justifyContent: "center",
            }}
          >
            <TouchableOpacity
              style={{
                width: "40%",
                height: 37,
                backgroundColor: colors.WHITE,
                borderWidth: 1,
                borderColor: "#636363",
                borderRadius: 12,
                justifyContent: "center",
                alignItems: "center",
                marginHorizontal: 5,
              }}
              onPress={() => {
                setDeniedModalVisible(true);
              }}
            >
              <Text
                style={{
                  fontSize: 15,
                  lineHeight: 18,
                  textAlign: "center",
                  fontFamily: "Montserrat-Bold",
                }}
              >
                ANNULER
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flexDirection: "row",
                width: "40%",
                height: 37,
                backgroundColor: colors.VIOLET,
                borderWidth: 1,
                borderColor: "#636363",
                borderRadius: 12,
                justifyContent: "center",
                alignItems: "center",
                marginHorizontal: 5,
              }}
              onPress={() => {
                setValidationModalVisible(true);
              }}
            >
              <Text
                style={{
                  color: colors.WHITE,
                  fontFamily: "Montserrat-Bold",
                  fontSize: 14,
                  lineHeight: 18,
                  left: 10,
                }}
              >
                GO!
              </Text>
              <Feather
                name="chevrons-right"
                size={30}
                color={colors.WHITE}
                style={{ left: 35 }}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    }

    return (
      <View
        style={{
          paddingVertical: 15,
          backgroundColor: colors.WHITE,
          justifyContent: "space-around",
          alignItems: "center",
          width: "100%",
          height: 170,

          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          shadowColor: "black",
          shadowOffset: { height: 2, width: 2 },
          shadowOpacity: 0.26,
          shadowRadius: 6,
          elevation: 6,
        }}
      >
        <SvgXml xml={searchLoading} />
        <View>
          <Text
            style={{
              textAlign: "center",
              color: "#9e9e9e",
              fontSize: 14,
              fontFamily: "Montserrat-Bold",
              lineHeight: 17,
            }}
          >
            Tarif en attente de validation
          </Text>
          <Text
            style={{
              textAlign: "center",
              color: "#9e9e9e",
              fontSize: 14,
              fontFamily: "Montserrat-Bold",
              lineHeight: 17,
            }}
          >
            {" "}
            du voyageur…{" "}
          </Text>
        </View>
        <TouchableOpacity
          style={{
            width: 135,
            height: 37,
            backgroundColor: colors.WHITE,
            borderWidth: 1,
            borderColor: "#636363",
            borderRadius: 12,
            justifyContent: "center",
            alignItems: "center",
          }}
          onPress={() => {
            setDeniedModalVisible(true);
          }}
        >
          <Text
            style={{
              fontSize: 15,
              lineHeight: 18,
              textAlign: "center",
              fontFamily: "Montserrat-Bold",
            }}
          >
            REFUSER
          </Text>
        </TouchableOpacity>
      </View>
    );
  }, [travelInformations?.status]);

  const renderValidateAfterAffiner = useMemo(() => {
    if (hasValidateAfterAffiner) {
      return renderTravelStatus;
    }

    return (
      <View
        style={{
          backgroundColor: colors.WHITE,
          width: "100%",
          paddingHorizontal: 5,
          paddingVertical: 20,

          shadowColor: "black",
          shadowOffset: { height: 2, width: 2 },
          shadowOpacity: 0.26,
          shadowRadius: 6,
          elevation: 6,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        <Text
          style={{
            textAlign: "right",
            fontFamily: "Montserrat-Medium",
            width: "100%",
            fontSize: 12,
            lineHeight: 15,
          }}
        >
          AFFINER LE TARIF ?
        </Text>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
              alignItems: "center",
              flex: 2,
            }}
          >
            <SvgXml xml={travel} />
            <View
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "center",
                alignItems: "flex-start",
              }}
            >
              <View style={{ marginBottom: 10 }}>
                <Text
                  style={{
                    fontFamily: "Montserrat-Bold",
                    fontSize: 10,
                    lineHeight: 15,
                  }}
                >
                  DÉPART
                </Text>
                <Text
                  adjustsFontSizeToFit={true}
                  numberOfLines={3}
                  style={{
                    color: "#848484",
                    fontSize: 12,
                    fontFamily: "Montserrat-Medium",
                    lineHeight: 15,
                  }}
                >
                  {newDemandRider.rider_depAddress
                    ? newDemandRider.rider_depAddress
                    : "Où etes-vous ?"}
                </Text>
              </View>
              <View style={{ marginTop: 10 }}>
                <Text
                  style={{
                    fontFamily: "Montserrat-Bold",
                    fontSize: 10,
                    lineHeight: 15,
                  }}
                >
                  ARRIVÉE
                </Text>
                <Text
                  adjustsFontSizeToFit={true}
                  numberOfLines={3}
                  style={{
                    color: "#848484",
                    fontSize: 12,
                    fontFamily: "Montserrat-Medium",
                    lineHeight: 15,
                  }}
                >
                  {newDemandRider.rider_destAddress
                    ? newDemandRider.rider_destAddress
                    : "Où allez-vous ?"}
                </Text>
              </View>
            </View>
          </View>
          {/* Form */}
          <View
            style={{
              flexDirection: "column",
              flex: 1,
              justifyContent: "space-around",
              alignItems: "center",
            }}
          >
            <InputSpinner
              max={newDemandRider.rider_proposedPrice + 1}
              min={newDemandRider.rider_proposedPrice - 1}
              step={0.5}
              skin={"square"}
              showBorder={true}
              colorLeft={"transparent"}
              colorRight={"transparent"}
              // width={135}
              height={35}
              type={"real"}
              editable={false}
              color={colors.GREYD}
              inputStyle={{
                borderWidth: 0.3,
                borderBottomWidth: 0,
                borderColor: colors.GREYD,
              }}
              buttonTextColor={colors.GREYD}
              initialValue={travelInformations?.propose_negoPrice_rider || 0}
              value={proposedPrice}
              onChange={(value) => setProposedPrice(value)}
              style={{
                backgroundColor: "#fff",
                elevation: 2,
                shadowOpacity: 0.3,
                shadowRadius: 1,
                shadowOffset: {
                  height: 0,
                  width: 0,
                },
              }}
            />
            <TouchableOpacity
              onPress={handleAccepted}
              style={{
                width: "95%",
                paddingVertical: 5,
                marginTop: 20,
                marginBottom: 5,
                backgroundColor: colors.VIOLET,
                borderRadius: 8,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    color: colors.WHITE,
                    fontFamily: "Montserrat-Bold",
                    fontSize: 12,
                    lineHeight: 18,
                  }}
                >
                  ACCEPTER
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                  }}
                >
                  <Feather
                    name="chevrons-right"
                    size={24}
                    color={colors.WHITE}
                  />
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                backgroundColor: colors.WHITE,
                borderWidth: 1,
                borderColor: "#636363",
                width: "95%",
                paddingVertical: 5,
                marginTop: 10,
                marginBottom: 5,
                borderRadius: 8,
                justifyContent: "center",
                alignItems: "center",
              }}
              onPress={() => {
                setDeniedModalVisible(true);
              }}
            >
              <Text
                style={{
                  fontFamily: "Montserrat-Bold",
                  fontSize: 12,
                  lineHeight: 18,
                  textAlign: "center",
                }}
              >
                REFUSER
              </Text>
            </TouchableOpacity>
          </View>
          {/* fin Form */}
        </View>
      </View>
    );
  }, [
    hasValidateAfterAffiner,
    renderTravelStatus,
    newDemandRider?.rider_proposedPrice,
    travelInformations?.propose_negoPrice_rider,
    proposedPrice,
  ]);

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.userContainer}>
        <View style={styles.purpleUserContainer}>
          <View>
            <Text style={styles.text}>NOUVELLE DEMANDE</Text>
          </View>
        </View>
        <View style={styles.userInfos}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-around",
              paddingVertical: 10,
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Image
                source={
                  newDemandRider &&
                  newDemandRider.rider_profileImg &&
                  newDemandRider.rider_profileImg != "non"
                    ? { uri: newDemandRider.rider_profileImg }
                    : userProfil
                }
                style={{ width: 50, height: 50, borderRadius: 25 }}
              />
              <View
                style={{
                  justifyContent: "space-between",
                  height: 40,
                  marginLeft: 5,
                }}
              >
                <Text
                  style={{
                    fontSize: 12,
                    fontFamily: "Montserrat-Bold",
                    lineHeight: 15,
                  }}
                >
                  {newDemandRider.rider_firstName ||
                  newDemandRider.rider_lastName
                    ? newDemandRider.rider_firstName +
                      " " +
                      newDemandRider.rider_lastName
                    : "Juliette FERNANDEZ"}
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: "Montserrat-Medium",
                    lineHeight: 18,
                    color: "#848484",
                  }}
                >
                  {newDemandRider.rider_duration_btw_driver
                    ? "À " + newDemandRider.rider_duration_btw_driver
                    : "À 2min"}{" "}
                  min
                </Text>
              </View>
            </View>
            <View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  width: 95,
                }}
              >
                <TouchableOpacity
                  onPress={() =>
                    newDemandRider && newDemandRider != null
                      ? Linking.openURL(`sms: ${newDemandRider.rider_mobile}`)
                      : console.log(" sending sms...")
                  }
                >
                  <SvgXml xml={email} />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() =>
                    newDemandRider && newDemandRider != null
                      ? callNumber(newDemandRider.rider_mobile)
                      : console.log(" calling...")
                  }
                >
                  <SvgXml xml={tel} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.secondRow}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
              }}
            >
              <View
                style={{
                  flexDirection: "column",
                  alignItems: "center",
                  width: "50%",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <View
                    style={{
                      marginRight: 10,
                    }}
                  >
                    <SvgXml xml={euro} style={{ width: 50, height: 50 }} />
                  </View>
                  <Text
                    style={{
                      fontSize: 10,
                      lineHeight: 13,
                      fontFamily: "Montserrat-Bold",
                    }}
                  >
                    TARIF PROPOSÉ
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      fontSize: 20,
                      lineHeight: 25,
                      fontFamily: "Montserrat-Bold",
                      color: colors.VIOLET,
                      textAlign: "right",
                    }}
                  >
                    {proposedPrice ? proposedPrice + " €" : "15,30 €"}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  height: 45,
                  width: 2,
                  backgroundColor: "#D2D2D2",
                }}
              ></View>
              <View
                style={{
                  flexDirection: "column",
                  alignItems: "center",
                  width: "50%",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <SvgXml xml={userDriver} style={{ width: 50, height: 50 }} />
                  <Text
                    style={{
                      fontSize: 10,
                      lineHeight: 14,
                      fontFamily: "Montserrat-Bold",
                      paddingLeft: 4,
                    }}
                  >
                    PASSAGERS
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      fontSize: 20,
                      lineHeight: 25,
                      fontFamily: "Montserrat-Bold",
                      color: colors.VIOLET,
                      height: 22,
                    }}
                  >
                    {newDemandRider.rider_nbPlace
                      ? newDemandRider.rider_nbPlace
                      : 4}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>

        {renderValidateAfterAffiner}
      </SafeAreaView>
      <Modal
        isVisible={validationModalVisible}
        animationType="slide"
        transparent={true}
        style={{ margin: 0 }}
      >
        {travelValidated ? (
          <TravelValidatedByDriver
            setValidationModalVisible={setValidationModalVisible}
            onValidatedTravel={handleValidatedTravel}
          />
        ) : (
          <ValidationTravelModal
            validateTravel={() => {
              setTravelValidated(true);
            }}
            onClose={() => {
              setValidationModalVisible(false);
            }}
          />
        )}
      </Modal>
      <Modal
        isVisible={deniedModalVisible}
        animationType="slide"
        transparent={true}
        style={{ margin: 0 }}
      >
        <DeniedTravelByDriverModal
          denied={() => {
            onCloseModal();
            setDeniedModalVisible(false);
          }}
          onClose={() => {
            setDeniedModalVisible(false);
          }}
        />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-end",
    opacity: 1,
  },
  userContainer: {
    width: "90%",
    shadowColor: "black",
    shadowOffset: { height: 2, width: 2 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
    backgroundColor: colors.WHITE,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  purpleUserContainer: {
    width: "100%",
    height: 65,
    backgroundColor: colors.VIOLET,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    color: colors.WHITE,
    fontSize: 16,
    fontFamily: "Montserrat-Bold",
    lineHeight: 20,
    textAlign: "center",
    paddingBottom: 15,
  },
  userInfos: {
    width: "100%",
    backgroundColor: colors.WHITE,
  },
  secondRow: {
    marginVertical: 10,
  },
});

export default NewDemand;
