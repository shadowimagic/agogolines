import React, { useContext, useEffect, useState, useRef } from "react";
import { View, Text, StyleSheet, Alert, ActivityIndicator } from "react-native";

import { colors } from "../../common/theme";

import { FirebaseContext } from "../../../redux/src";
import { useSelector, useDispatch } from "react-redux";

import { SvgXml } from "react-native-svg";
import position from "../../../assets/svg/position";
import { GetDistance } from "../../../redux/src/other/GeoFunctions";
import { cloud_function_server_url, language } from "../../../config";
import DriverCancelTravel from "../../components/DriverCancelTravel";
// import { AdMobBanner, setTestDeviceIDAsync } from "expo-ads-admob";

const roundToTwo = (num) => {
  return +(Math.round(num + "e+2") + "e-2");
};

const TravelEncours = (props) => {
  const { api } = useContext(FirebaseContext);
  const { fetchOneTravel, addCreditCard, updateTravel } = api;
  const auth = useSelector((state) => state.auth);
  const travelData = useSelector((state) => state.traveldata);
  const travelInformations = travelData.travel;

  const callCharge = useRef(true);

  const mesvehiculesRedux = useSelector((state) => state.mesvehicules);
  const creditcarddata = useSelector((state) => state.creditcarddata);
  const ibanData = useSelector((state) => state.iban);
  const [showCancel, setShowCancel] = useState(false);
  const [isPaying, setIsPaying] = useState(false);
  const { selectedVehicule } = mesvehiculesRedux;
  const { cardToUse, creditcards } = creditcarddata;
  const { stripeAccountId = "acct_1DS0QPD7DjvLFFKd" } = ibanData;

  const { datarider, datadriver, navigation } = props;

  const {
    id,
    driver_email,
    driver_mobile,
    lat_dep,
    long_dep,
    driver_pushToken,
    driver_lastName,
    driver_firstName,
  } = datadriver || {};

  const {
    distance: distanceKM,
    lat_dep: rider_lat_dep,
    long_dep: rider_long_dep,
    create_date,
  } = datarider || {};

  const dispatch = useDispatch();

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      const interval = setInterval(() => {
        dispatch(fetchOneTravel(props.datadriver?.driver_id));
      }, 6000);

      return () => {
        clearInterval(interval);
      };
    }
  }, [auth.info]);

  // React.useEffect(() => {
  //   setTestDeviceIDAsync("EMULATOR");
  // }, []);

  useEffect(() => {
    if (
      travelInformations != null &&
      travelInformations.isAffiner_driver == false &&
      travelInformations.isAffiner_rider == false &&
      travelInformations.isFound == false &&
      travelInformations.status == "init"
    ) {
      //setShowCancel(true);
      props.setDisplayTravelEnCours(false);
      props.setDisplayDriverCancelTravel(true);
    }
  }, [
    showCancel,
    travelInformations,
    travelInformations.isAffiner_rider,
    travelInformations.isAffiner_driver,
    travelInformations.isFound,
    travelInformations.status,
  ]);

  useEffect(() => {
    if (travelInformations?.arriver_dest_passager == true) {
      if (travelInformations.mode_paiement === "cartecredit") {
        if (callCharge.current) {
          callCharge.current = false;
          setIsPaying(true);
          onCreateReservationCourse(travelInformations);
        }
      } else {
        dispatch(
          updateTravel({
            driver_id: travelInformations.driver_id,
            isPaid: true,
          })
        );
        props.setDisplayTravelEnCours(false);
        props.setDisplayRatingAfterTravel(true);
      }
    }
  }, [
    travelInformations?.arriver_dest_passager,
    travelInformations?.mode_paiement,
  ]);

  const updateCardChargeStatus = () => {
    const newCards = [...creditcards];
    if (cardToUse?.index > -1 && cardToUse?.firstCharge) {
      newCards[cardToUse.index] = { ...cardToUse, firstCharge: false };
      dispatch(addCreditCard(newCards));
    }
  };

  const handleEndPayment = () => {
    setIsPaying(false);
    dispatch(
      updateTravel({
        driver_id: travelInformations.driver_id,
        isPaid: true,
      })
    );
    props.setDisplayTravelEnCours(false);
    props.setDisplayRatingAfterTravel(true);
  };

  const handlePaymentResponse = (result) => {
    const {
      success,
      requires_action,
      payment_intent_client_secret,
      stripe_account_id,
    } = result?.paymentStatus || {};
    if (success) {
      Alert.alert("Success!", "Payment successful");
      // update card to use status to use card id instead of card token on next charge
      updateCardChargeStatus();
      handleEndPayment();
    } else {
      if (
        requires_action &&
        payment_intent_client_secret &&
        stripe_account_id
      ) {
        props.setDisplayTravelEnCours(false);
        const timeout = setTimeout(() => {
          navigation.navigate(
            "ThreeDSecureScreen",
            {
              data: {
                stripeAccount: stripe_account_id,
                clientSecret: payment_intent_client_secret,
              },
              callback: () => {
                Alert.alert("Success!", "Payment successful");
                updateCardChargeStatus();
                handleEndPayment();
              },
              goBackCallBack: () => {
                Alert.alert("Failed!", "Payment failed!");
                updateCardChargeStatus();
                handleEndPayment();
              },
            },
            500
          );

          clearTimeout(timeout);
        });
      } else {
        setIsPaying(false);
        Alert.alert("Failed!", "Payment failed!");
        updateCardChargeStatus();
        handleEndPayment();
      }
    }
  };

  const onCreateReservationCourse = async (travelInformation) => {
    const {
      propose_firstname_rider,
      propose_lastname_rider,
      propose_email_rider,
      rider_pushToken,
      propose_phone_rider,
      propose_negoPrice,
      travel_roadDurationText,
      rider_trip_propose_date_time,
      propose_nbPlace_rider,
      propose_dep_rider,
      propose_dest_rider,
      propose_rider_id,
      mode_paiement,
    } = travelInformation || {};
    if (selectedVehicule && cardToUse && stripeAccountId) {
      const cardIdToUse = cardToUse?.firstCharge
        ? cardToUse?.cardToken
        : cardToUse?.cardid;
      const reservationData = {
        accepter: "oui",
        accountId: stripeAccountId || "",
        chauffeurEmail: driver_email,
        chauffeurIdOneSignal: driver_pushToken,
        chauffeurkey: id,
        coursetourdeux: "non",
        coursetourun: "non",
        datejour: create_date,
        distance: `${distanceKM} km`,
        etatCourse: "oui",
        immatriculation: selectedVehicule?.carNumber,
        latdepartpassager: rider_lat_dep,
        latitudeconducteur: lat_dep,
        longdepartpassager: rider_long_dep,
        longitudeconducteur: long_dep,
        marque: selectedVehicule?.carMake,
        metrique: "", // what is this ?
        modele: selectedVehicule?.carModel,
        modepaiement: mode_paiement,
        motifAnnulation: "non",
        nombrepassager: propose_nbPlace_rider || 1,
        nomconducteur: driver_lastName,
        photoprofilchauffeur: "",
        prenomconducteur: driver_firstName,
        numdriver: 1,
        paiementagogolines: "non",
        passagerEmail: propose_email_rider,
        passagerIdOneSignal: rider_pushToken, //one signal id
        passagerNom: propose_lastname_rider,
        passagerPrenom: propose_firstname_rider,
        passagerTelephone: propose_phone_rider,
        passagerdepart: propose_dep_rider,
        passagerdestination: propose_dest_rider,
        passagerkey: propose_rider_id,
        passagerphotoprofil: "",
        cardId: cardIdToUse,
        prix: propose_negoPrice,
        recupererpassager: "non",
        fee: roundToTwo(propose_negoPrice * 0.15), // 15% of prix
        reservation: roundToTwo(propose_negoPrice * 0.15), // 15% of prix
        telephone: driver_mobile,
        terminercourse: "oui",
        time: travel_roadDurationText,
        timestamp: rider_trip_propose_date_time,
      };

      try {
        await fetch(
          `${cloud_function_server_url}/serverlessEndpoint/reservation-course`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              ...reservationData,
            }),
          }
        ).then((response) => {
          response.json().then((result) => {
            handlePaymentResponse(result);
          });
        });
      } catch (error) {
        console.log({ error });
        Alert.alert("Failed!", "Payment failed!");
        handleEndPayment();
      }
    } else {
      Alert.alert("Failed!", "Payment failed!");
      handleEndPayment();
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        {isPaying ? (
          <View
            style={{
              padding: 20,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <ActivityIndicator size={"large"} style={{ marginBottom: 20 }} />
            <Text>vous payez le voyage...</Text>
          </View>
        ) : (
          <>
            <Text
              style={{
                color: "#969696",
                fontSize: 14,
                lineHeight: 18,
                textAlign: "left",
                marginLeft: 22,
                marginTop: 15,
                fontFamily: "Montserrat-Medium",
              }}
            >
              En route vers...
            </Text>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <SvgXml
                xml={position}
                style={{ marginLeft: 22, marginTop: 15, width: 50, height: 50 }}
              />
              <View style={{ width: 350 }}>
                <Text
                  style={{
                    color: "#000",
                    fontSize: 14,
                    lineHeight: 18,
                    textAlign: "left",
                    marginLeft: 12,
                    width: "80%",
                    fontFamily: "Montserrat-Medium",
                    marginTop: 10,
                  }}
                >
                  {travelInformations?.propose_dest_rider ||
                    props.datarider.addr_dest}
                </Text>
              </View>
            </View>
          </>
        )}
      </View>

      {/* <AdMobBanner
        style={styles.bottomBanner}
        bannerSize="smartBanner"
        adUnitID="ca-app-pub-5608997176551587/4926705154"
        servePersonalizedAds // true or false
        onDidFailToReceiveAdWithError={(e) => console.log(e)}
      /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    width: "100%",
    marginBottom: 30,
  },
  bottomBanner: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
  },
  modal: {
    backgroundColor: "white",
    width: "90%",
    marginHorizontal: 15,
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
    alignSelf: "center",
    justifyContent: "center",
    padding: 10,
    marginBottom: 60,
  },
  text: {
    marginTop: 10,
    fontSize: 15,
    lineHeight: 20,
    fontFamily: "Montserrat-Bold",
    color: "#5a48cb",
  },
  secondText: {
    fontSize: 14,
    fontFamily: "Montserrat-Bold",
    color: "#9E9E9E",
    lineHeight: 20,
  },

  btnTitleStyle: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 18,
    color: "white",
    fontFamily: "Montserrat-Bold",
  },
  buttonStyle: {
    marginTop: 10,
    backgroundColor: "#5a48cb",
    borderRadius: 12,
    alignSelf: "center",
  },
  bodyTop: {
    height: "30%",
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  userImage: {
    width: 47,
    height: 47,
    marginRight: 10,
    borderRadius: 12,
  },
  userName: {
    fontWeight: "bold",
  },
  starIcon: {
    flexDirection: "row",
    marginVertical: 5,
  },
  stopPhoneIcon: {
    flexDirection: "row",
    marginLeft: "auto",
    marginRight: 10,
  },
  iconMargin: {
    marginTop: 10,
    marginRight: 10,
  },
  element: {
    flexDirection: "row",
  },
  bodyBottom: {
    backgroundColor: "white",
    height: "70%",
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  textContainer: {
    marginLeft: 5,
  },
  elementHeaderText: {
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  elementBodyText: {
    color: colors.GREYD,
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    lineHeight: 18,
  },
  elementBodyTextViolet: {
    fontWeight: "bold",
    fontSize: 20,
    lineHeight: 24,
    fontFamily: "Montserrat-Bold",
    color: "#FFB406",
  },
  bodyBottomOne: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bodyBottomTwo: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: "10%",
    marginBottom: 10,
  },
});

export default TravelEncours;
