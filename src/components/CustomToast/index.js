import React, {useEffect, useRef} from "react";

import {Animated, Text} from "react-native";
import {colors} from "../../common/theme";

export default function CustomToast(props) {
	const opacity = useRef(new Animated.Value(0)).current;

	useEffect(() => {
		Animated.sequence([
			Animated.timing(opacity, {
				toValue: 1,
				duration: 500,
				useNativeDriver: true,
			}),
			Animated.delay(2000),
			Animated.timing(opacity, {
				toValue: 0,
				duration: 500,
				useNativeDriver: true,
			}),
		]).start(() => {
			props.onHide();
		});
	}, []);

	return (
		<Animated.View
			style={{
				opacity,
				transform: [
					{
						translateY: opacity.interpolate({
							inputRange: [0, 1],
							outputRange: [-20, 0],
						}),
					},
				],
				height: 50,
				margin: 10,
				marginBottom: 5,
				backgroundColor: "white",
				padding: 10,
				borderRadius: 13,
        borderWidth: 1,
        borderLeftWidth: 5,
				borderColor: "red",
				shadowColor: "black",
				shadowOffset: {
					width: 0,
					height: 3,
				},
				shadowOpacity: 0.15,
				shadowRadius: 5,
				elevation: 6,
			}}>
			<Text style={{color: colors.VIOLET, fontFamily: "Montserrat-Medium"}}>
				{props.message}
			</Text>
		</Animated.View>
	);
}
