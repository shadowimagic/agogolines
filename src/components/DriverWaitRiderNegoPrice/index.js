import React, { useContext, useEffect, useState } from "react";
import { View, Text, Image, TouchableOpacity, Linking } from "react-native";
import { Button } from "react-native-elements";
import styles from "./styles";
import {
  FontAwesome,
  AntDesign,
  Ionicons,
  Feather,
  MaterialCommunityIcons,
  MaterialIcons,
} from "@expo/vector-icons";
import { colors } from "../../common/theme";
import car from "../../../assets/svg/car";
import user from "../../../assets/images/avatar_login.png";
import { callNumber } from "../phoneTrigger/index";
import { SvgXml } from "react-native-svg";
import email from "../../../assets/svg/email";
import tel from "../../../assets/svg/tel";
import euro from "../../../assets/svg/eurodriver";
import clock from "../../../assets/svg/clock";
import position from "../../../assets/svg/position";
import wait from "../../../assets/svg/wait";
import { FirebaseContext } from "../../../redux/src";
import { useSelector, useDispatch } from "react-redux";
import { EventRegister } from "react-native-event-listeners";
import DriverCancelTravel from "../../components/DriverCancelTravel";

const DriverWaitRiderNegoPrice = (props) => {
  const [affinerPrix, setAffinerPrix] = useState(props.datarider.prix_affiner);

  const { api } = useContext(FirebaseContext);
  const {
    updateTravel,
    fetchPoint,
    deleteTravel,
    fetchComment,
    fetchOneTravel,
  } = api;
  const auth = useSelector((state) => state.auth);

  const travelData = useSelector((state) => state.travel);
  const travelInformations = travelData.travel;

  const comments = useSelector((state) => state.comment);
  const commentInformations = comments.comment;

  const [note, setNote] = useState(0);

  const [closeCancelModal, setCloseCancelModal] = useState(false);

  const dispatch = useDispatch();

  const [showCancel, setShowCancel] = useState(false);
  const [newDemandRider, setNewDemandRider] = useState({
    driver_firstName: "",
    driver_lastName: "",
    driver_profile_img: "",
    driver_mobile: "",
    propose_tmp_btn_rider_driver: props.leTemps,
    rider_trip_distance: props.laDistance,
    propose_negoPrice: 0,
    propose_negoPrice_rider: 0,
    isAffiner_rider: false,
    isFound: false,
    status: "init",
  });

  const annulation = () => {
    EventRegister.emit("initrider", true);

    dispatch(
      updateTravel({
        propose_negoPrice: affinerPrix,
        propose_negoPrice_rider: affinerPrix,
        isAffiner_rider: false,
        driver_id: props?.datadriver?.driver_id,
        isFound: false,
      })
    );
  };

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      dispatch(fetchPoint());
    }
  }, [auth.info]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      if (travelInformations != null) {
        setNewDemandRider({
          ...newDemandRider,

          driver_firstName: travelInformations.driver_firstName,
          driver_lastName: travelInformations.driver_lastName,
          driver_profile_img: travelInformations.driver_profile_img,
          driver_mobile: travelInformations.driver_mobile,
          propose_tmp_btn_rider_driver:
            travelInformations.propose_tmp_btn_rider_driver,
          rider_trip_distance: travelInformations.rider_trip_distance,
          propose_negoPrice: travelInformations.propose_negoPrice,
          propose_negoPrice_rider: travelInformations.propose_negoPrice_rider,
          isAffiner_rider: travelInformations.isAffiner_rider,
          isFound: travelInformations.isFound,
          status: travelInformations.status,
        });
      }
    }
  }, [auth.info]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      dispatch(fetchComment(props?.datadriver?.driver_id));
    }
  }, [auth.info, props.datadriver]);

  useEffect(() => {
    if (auth.info && props.datadriver && auth.info.profile) {
      if (commentInformations) {
        const taille = commentInformations.length;
        let somme = 0;
        let moyenne = 0;

        commentInformations.forEach((element) => {
          somme = somme + element.etoile;
        });
        if (taille != 0 && somme != 0) {
          moyenne = Math.round(somme / taille);

          setNote(moyenne);
        } else {
          setNote(0);
        }
      }
    }
  }, [auth.info, props.datadriver]);

  const afficheEtoile = () => {
    //	console.log("valeur de note:",note);
    if (note === 1) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 2) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 3) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 4) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    } else if (note === 5) {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
          <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
        </>
      );
    } else {
      return (
        <>
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
          <AntDesign name="star" size={16} color={colors.GREY.primary} />
        </>
      );
    }
  };

  useEffect(() => {
    if (travelInformations && travelInformations?.isAffiner_driver) {
      props.setDisplayWaitRiderNegoPrice(false);
      props.setTravelCompleted(true);
      props.setRiderPriceDone(true);
    }
  }, [
    showCancel,
    travelInformations,
    travelInformations?.isAffiner_driver,
    travelInformations?.isFound,
    travelInformations?.status,
  ]);

  useEffect(() => {
    if (
      travelInformations != null &&
      travelInformations.isAffiner_driver == false &&
      travelInformations.isAffiner_rider == false &&
      travelInformations.isFound == false &&
      travelInformations.status == "init"
    ) {
      props.setDisplayWaitRiderNegoPrice(false);
      props.setDisplayDriverCancelTravel(true);
    }
  }, [
    showCancel,
    travelInformations,
    travelInformations?.isAffiner_rider,
    travelInformations?.isAffiner_driver,
    travelInformations?.isFound,
    travelInformations?.status,
  ]);

  useEffect(() => {
    if (props?.datadriver?.driver_id && !travelInformations) {
      dispatch(fetchOneTravel(props?.datadriver?.driver_id));
    }
  }, [props?.datadriver?.driver_id]);

  // if (showCancel) {
  //   return (
  //     <DriverCancelTravel
  //       setDisplayWaitRiderNegoPrice={props.setDisplayWaitRiderNegoPrice}
  //       datadriver={newDemandRider}
  //     />
  //   );
  // }

  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <View style={styles.bodyTop}>
          <View style={styles.userImage}>
            <Image
              source={
                newDemandRider &&
                newDemandRider.driver_profile_img &&
                newDemandRider.driver_profile_img != "non"
                  ? { uri: newDemandRider.driver_profile_img }
                  : user
              }
              style={{ width: "100%", height: "100%", borderRadius: 50 }}
            />
          </View>

          <View style={styles.userInfo}>
            <Text style={styles.userName}>
              {newDemandRider.driver_firstName || newDemandRider.driver_lastName
                ? newDemandRider.driver_firstName +
                  " " +
                  newDemandRider.driver_lastName
                : "Juliette FERNANDEZ"}
            </Text>
            <View style={styles.starIcon}>{afficheEtoile()}</View>
            <SvgXml xml={car} style={{ width: 27, height: 11 }} />
          </View>
          <View style={styles.stopPhoneIcon}>
            <TouchableOpacity
              style={styles.iconMargin}
              onPress={() =>
                newDemandRider && newDemandRider != null
                  ? Linking.openURL(`sms: ${newDemandRider.driver_mobile}`)
                  : console.log(" sending sms...")
              }
            >
              <SvgXml xml={email} style={{ width: 50, height: 50 }} />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.iconMargin}
              onPress={() =>
                newDemandRider && newDemandRider != null
                  ? callNumber(newDemandRider.driver_mobile)
                  : console.log(" calling...")
              }
            >
              <SvgXml xml={tel} style={{ width: 50, height: 50 }} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bodyBottom}>
          <View style={styles.bodyBottomOne}>
            <View style={styles.element}>
              <SvgXml xml={clock} style={{ width: 50, height: 50 }} />
              <View style={styles.textContainer}>
                <Text style={styles.elementHeaderText}>temps trajet</Text>
                <Text style={styles.elementBodyText}>
                  {newDemandRider.propose_tmp_btn_rider_driver
                    ? newDemandRider.propose_tmp_btn_rider_driver
                    : "2 mins"}
                </Text>
              </View>
            </View>
            <View style={styles.element}>
              <SvgXml xml={position} style={{ width: 50, height: 50 }} />
              <View style={styles.textContainer}>
                <Text style={styles.elementHeaderText}>distance</Text>
                <Text style={styles.elementBodyText}>
                  {newDemandRider.rider_trip_distance
                    ? newDemandRider.rider_trip_distance
                    : "1"}{" "}
                  km
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              alignItems: "center",
              justifyContent: "space-between",
              marginTop: 28,
              height: 50,
            }}
          >
            <SvgXml xml={wait} style={{ width: 50, height: 50 }} />
            <Text
              style={{
                fontFamily: "Montserrat-Bold",
                color: "#9E9E9E",
                fontSize: 14,
                lineHeight: 18,
              }}
            >
              Tarif en attente de validation
            </Text>
            <Text
              style={{
                fontFamily: "Montserrat-Bold",
                color: "#9E9E9E",
                fontSize: 14,
                lineHeight: 18,
              }}
            >
              du conducteur…
            </Text>
          </View>
          <Button
            title="annuler"
            buttonStyle={[
              styles.buttonStyle,
              { marginTop: 30, width: 150, alignSelf: "center" },
            ]}
            titleStyle={styles.btnTitleStyle}
            onPress={() => {
              props.setTravelInit(true);
              props.setDisplayWaitRiderNegoPrice(false);
              annulation();
            }}
          />
        </View>
      </View>
    </View>
  );
};

export default DriverWaitRiderNegoPrice;
