 import React, { useContext, useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Alert,
  TextInput,
} from "react-native";

import { AntDesign } from "@expo/vector-icons";
import userdriver from "../../../assets/svg/userdriver";
import euro from "../../../assets/svg/eurodriver";
import InputSpinner from "react-native-input-spinner";

import { Feather } from "@expo/vector-icons";
import { SvgXml } from "react-native-svg";
import { colors } from "./../../common/theme";
import { Icon, Button } from "react-native-elements";
import { CustomStyle } from "../../common/CustomStyle";
import eurodriver from "../../../assets/svg/eurodriver";
import { GetDistance } from "../../../redux/src/other/GeoFunctions";
import NoDriverFoundByRiderModal from "../NoDriverFoundByRiderModal";

const RiderTravelGo = ({ openModal, data, onPressDataRider, nearby, setRiderAndDriverDist }) => {
  const [affinerPrix, setAffinerPrix] = useState(data.prix);
  const [nbPassager, setNbPassager] = useState(1);

  const onPressBook = () => {
    onPressDataRider({
      nbrepassager: nbPassager,
      prixAffiner: affinerPrix || data.prix,
    });
  };

  const btnMessage = () => {
    Alert.alert("Merci de renseigner une adresse de départ et de destination");
  };

  const getAllDriverNearby = () => {
    const res = nearby.filter((obj) => obj.type == 'driver');
    const arr  = [];

    if (res.length > 1) {
      res.filter((v,i,a)=>a.findIndex(v2=>(v2.id===v.id))===i).map((obj) => {
        if (arr) {
          arr.push({
            distance: GetDistance(obj.latitude, obj.longitude, data.lat_dep, data.long_dep),
            driver_id: obj.uid,
            rider_id: data.rider_id
          })
        } else {
          if (arr.indexOf(obj.uid) === -1) {false}
        }
      })
      if (arr.length < 1) {
        console.log("NO DRIVER FOUND"); // return no driver found
      } else {
        arr.sort((a, b) =>  parseFloat(a.distance) - parseFloat(b.distance));
      }
      setRiderAndDriverDist(arr)
    } else {
      return (
        <NoDriverFoundByRiderModal />
      )
    }
     //call function that send the nearest driver TRAVEL INFO 
  } 

  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginTop: 25,
          width: "80%",
          justifyContent: "space-between",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <SvgXml xml={userdriver} style={{ height: 22, width: 18 }} />
          <Text
            style={{
              fontFamily: "Montserrat-Bold",
              fontSize: 12,
              lineHeight: 15,
              marginLeft: 5,
            }}
          >
            PASSAGERS
          </Text>
        </View>
        <View
          style={{ height: 37, flexDirection: "row", alignItems: "center" }}
        >
          <InputSpinner
            max={4}
            min={1}
            step={1}
            skin={"square"}
            showBorder={true}
            colorLeft={"transparent"}
            colorRight={"transparent"}
            width={120}
            height={37}
            editable={false}
            shadow={false}
            color={colors.GREYD}
            inputStyle={{
              borderWidth: 0.3,
              borderBottomWidth: 0,
              borderTopWidth: 0,
              borderColor: colors.GREYD,
            }}
            buttonTextColor={colors.GREYD}
            value={nbPassager}
            onChange={(value) => setNbPassager(value)}
            style={{
              backgroundColor: "#fff",

              elevation: 2,
              shadowOpacity: 0.3,
              shadowRadius: 1,
              shadowOffset: {
                height: 0,
                width: 0,
              },
            }}
          />
        </View>
      </View>

      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginTop: 25,
          width: "80%",
          justifyContent: "space-between",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <SvgXml xml={eurodriver} style={{ height: 22, width: 18 }} />
          <Text
            style={{
              fontFamily: "Montserrat-Bold",
              fontSize: 12,
              lineHeight: 15,
              marginLeft: 5,
            }}
          >
            TARIF COURSE (€)
          </Text>
        </View>
        <View
          style={{ height: 37, flexDirection: "row", alignItems: "center" }}
        >
          <Text
            style={{
              fontFamily: "Montserrat-Bold",
              fontSize: 20,
              lineHeight: 24,
              marginLeft: 5,
              color: "#5A48CB",
            }}
          >
            {data.prix && data.addr_dep != null ? data.prix + "€" : 0 + "€"}
          </Text>
        </View>
      </View>

      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginTop: 25,
          width: "80%",
          justifyContent: "space-between",
        }}
      >
        <Text
          style={{
            fontFamily: "Montserrat-Medium",
            fontSize: 12,
            lineHeight: 15,
            marginLeft: 5,
            color: "black",
          }}
        >
          AFFINER LE TARIF ?
        </Text>
      </View>

      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginTop: 7,
          width: "80%",
          justifyContent: "space-between",
        }}
      >
        <View
          style={{
            height: 37,
            flexDirection: "row",
            alignItems: "center",
            marginBottom: 40,
          }}
        >
          <InputSpinner
            max={4500}
            min={0}
            step={0.5}
            skin={"square"}
            showBorder={true}
            buttonLeftDisabled={true}
            colorLeft={"transparent"}
            colorRight={"transparent"}
            width={120}
            height={37}
            type={"real"}
            editable={false}
            color={colors.GREYD}
            inputStyle={{
              borderWidth: 0.3,
              borderBottomWidth: 0,
              borderTopWidth: 0,
              borderColor: colors.GREYD,
            }}
            borderRadius={colors.GREYD}
            buttonTextColor={colors.GREYD}
            value={data.prix && data.addr_dep != null ? data.prix : 0}
            onChange={(value) => setAffinerPrix(value)}
            style={{
              backgroundColor: "#fff",
              elevation: 2,
              shadowOpacity: 0.3,
              shadowRadius: 1,
              shadowOffset: {
                height: 0,
                width: 0,
              },
            }}
          />
        </View>

        <View
          style={{
            width: 126,
            height: 36,
            backgroundColor: "#5A48CB",
            borderRadius: 11,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginBottom: 40,
          }}
        >
          <View>
            <Button
              icon={
                <Icon
                  name="doubleright"
                  size={15}
                  color={colors.WHITE}
                  type="antdesign"
                />
              }
              iconRight
              title="GO !"
              onPress={() => {
                if (data && data.addr_dest) {
                  onPressBook();
                  openModal();
                  getAllDriverNearby();
                  console.log('find all driver currently connected')
                } else {
                  btnMessage();
                }
              }}
              titleStyle={CustomStyle.styleButtonShortCmdTitle}
              buttonStyle={CustomStyle.styleShortCmdButton}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    backgroundColor: "white",
    elevation: 1,
    padding: 10,
    height: 37,
  },
  container: {
    width: 350,
    height: 225,
    backgroundColor: "white",
    borderTopLeftRadius: 18,
    borderTopRightRadius: 18,
    alignItems: "center",
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.9,
    shadowRadius: 6,
    elevation: 6,
  },
  inputCounterStyle: {
    marginBottom: "2%",
    borderColor: colors.GREY.border,
  },
  counterStyle: {
    borderColor: colors.GREY.border,
    backgroundColor: "white",
    marginVertical: 10,
    width: "40%",
    elevation: 0,
  },
  counterBtn: {
    marginBottom: "2%",
    width: "25%",
  },
});

export default RiderTravelGo;
