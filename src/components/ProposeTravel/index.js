import React, { useContext, useRef, useState } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  ActivityIndicator,
  SafeAreaView,
  TextInput,
  FlatList,
} from "react-native";
import { Google_Map_Key, language } from "../../../config";
import { colors } from "../../common/theme";
import "react-native-get-random-values";
import travel from "../../../assets/svg/travel";
import src_dest from "../../../assets/images/src_dest.png";
import { SvgXml } from "react-native-svg";
import { CustomStyle } from "../../common/CustomStyle";

// images
import goBtn from "../../../assets/images/go_btn.png";
import { Button, Icon } from "react-native-elements";
import { FirebaseContext } from "../../../redux/src";
import useDebounce from "../../hooks/useDebounce";

// import { AdMobBanner, setTestDeviceIDAsync } from "expo-ads-admob";

const ProposeTravel = (props) => {
  const { onDepPress, onDestPress, onPressGo, isHaveDesAndDep, loading } =
    props;

    const [displayReservation, setDisplayReservation] = useState(true);
    const [searchKeyWordDepart, setSearchKeyWordDepart] = useState("");
    const [searchKeyWordDestination, setSearchKeyWordDestination] = useState("");
    const [isFocusDepart, setIsFocusDepart] = useState(false);
    const [isFocusDestination, setIsFocusDestination] = useState(false);
    const [searchResultsDepart, setSearchResultsDepart] = useState([]);
    const [searchResultsDestination, setSearchResultsDestination] = useState([]);
    const [isShowingResultsDepart, setIsShowingResultsDepart] = useState(false);
    const [RiderAndDriverDist, setRiderAndDriverDist] = useState(null);
    const [closeCancelModal, setCloseCancelModal] = useState(false);
    const [isShowingResultsDestination, setIsShowingResultsDestination] =
      useState(false);
    const [maDistance, setMaDistance] = useState(null);
    const [annuler, setAnnuler] = useState(false)
    const [pointValeur, setPointValeur] = useState(null);
    const [monTemps, setmonTemps] = useState(null);
    const refMap = useRef();
    const { api } = useContext(FirebaseContext);
    const [loadCal, setLoadCal] = useState(false);

    const latitudeDelta = 0.0222;
    const longitudeDelta = 0.0321;

  
  const [carPosition, setCarPosition] = useState({
    latitude: 0,
    longitude: 0,
    error: null,
  });
  const [depLocation, setDepLocation] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    address_name: null,
    default: true,
  });
  const [destLocation, setDestLocation] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    address_name: null,
    default: true,
  });
  const [initialRegion, setInitialRegion] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: latitudeDelta,
    longitudeDelta: longitudeDelta,
  });
  const [distance, setDistance] = useState({
    value: null,
    text: "",
  });
  const [byRoadDuration, setByRoadDuration] = useState({
    value: null,
    text: "",
  });
  const [modalVisible, setModalVisible] = useState(false);
  const [pickLocationType, setPickLocationType] = useState(null);
  const [margin, setMargin] = useState(0);

      // new google place function

  const searchLocation = async (text, type) => {
    if (type === "depart") {
      setSearchKeyWordDepart(text);
    } 
    if (type === "destination") {
      setSearchKeyWordDestination(text);
    }
  };

  useDebounce(
    async () => {
      if (isFocusDepart) {
        const response = await fetch(
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=" +
            Google_Map_Key +
            "&input=" +
            searchKeyWordDepart
        );
        const json = await response.json();

        console.log(json.predictions);

        if (json.predictions) {
          setSearchResultsDepart(json.predictions);
          setIsShowingResultsDepart(true);
          setDisplayReservation(false);
        }
      }
    },
    500,
    [searchKeyWordDepart]
  );

  useDebounce(
    async () => {
      if (isFocusDestination) {
        const response = await fetch(
          "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=" +
            Google_Map_Key +
            "&input=" +
            searchKeyWordDestination
        );
        const json = await response.json();

        if (json.predictions) {
          setSearchResultsDestination(json.predictions);
          setIsShowingResultsDestination(true);
          setDisplayReservation(false);
        }
      }
    },
    500,
    [searchKeyWordDestination]
  );

  

  const calculPosition = async (id, name, trajet) => {
    let lnlg = await fetchCoordsfromPlace(id);
    //console.log("Value of LnLG "+lnlg);
    setInitialRegion((prevState) => {
      return {
        ...prevState,
        latitude: lnlg.lat,
        longitude: lnlg.lng,
        latitudeDelta: latitudeDelta,
        longitudeDelta: longitudeDelta,
      };
    });

    if (trajet === "destination") {
      setDestLocation({
        latitude: lnlg.lat,
        longitude: lnlg.lng,
        address_name: name,
        default: false,
      });
      refMap?.current?.animateToRegion({
        ...initialRegion,
        latitude: lnlg.lat,
        longitude: lnlg.lng,
      });
    } else {
      setDepLocation({
        latitude: lnlg.lat,
        longitude: lnlg.lng,
        address_name: name,
        default: false,
      });

      refMap?.current?.animateToRegion({
        ...initialRegion,
        latitude: lnlg.lat,
        longitude: lnlg.lng,
      });
    }
  };

  return (
    // <View style={styles.container}>
    //   <View style={styles.modalView}>
    //     <SvgXml xml={srcdest} style={{ width: 50, height: 50 }} />
    //     <Text style={[styles.modalTitleText, { marginTop: 15 }]}>
    //       Bienvenue dans le tableaux de bord du Chauffeur
    //     </Text>
    //   </View>
    //   {/* <AdMobBanner
    //     style={styles.bottomBanner}
    //     bannerSize="smartBanner"
    //     adUnitID="ca-app-pub-5608997176551587/4926705154"
    //     servePersonalizedAds // true or false
    //     onDidFailToReceiveAdWithError={(e) => console.log(e)}
    //   /> */}
    // </View>
 <View style={styles.container}>
  
     <View style={styles.searchContainer}>
        
    <View style={{display:'flex', flexDirection:'column', alignItems:"center"}}>
      <Image source={src_dest} />
      <Text style={{
                  fontFamily: "Montserrat-Medium",
                  fontWeight: "bold",
                  fontSize: 18,
                  marginTop: 10,
                  textAlign:"center"
                }}> Quel trajet souhaitez vous proposer ?</Text>
    </View>
    
    

    <View
      style={{
        display:'flex', flexDirection:'row',
        marginTop:15
      }}
    >
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      >

        <SvgXml xml={travel} style={{ zIndex: 2, height: 100, width: 25 }} />
      </View>
      <SafeAreaView style={styles.searchBodyButtonsWrapper}>
        <View style={[styles.searchBodyContentButtonView]}>
          <Text style={styles.searchBodyContentTitleButton}> DEPART </Text>
          <TextInput
            placeholder="Où etes-vous ?"
            returnKeyType="search"
            style={{
              color: "#848484",
            }}
            placeholderTextColor="#848484"
            onChangeText={(text) => searchLocation(text, "depart")}
            value={searchKeyWordDepart}
            onFocus={() => setIsFocusDepart(true)}
            onBlur={() => setIsFocusDepart(false)}
          />
        </View>

        <View
          style={{
            zIndex: 2,
            height: 1,
            width: 228,
            marginVertical:12,
            backgroundColor: "#ECECEC",
          }}
        />

        <View style={[styles.searchBodyContentButtonView]}>
          <Text style={styles.searchBodyContentTitleButton}> ARRIVEE </Text>
          <TextInput
            placeholder="Où allez-vous ?"
            returnKeyType="search"
            style={{
              color: "#848484",
            }}
            placeholderTextColor="#848484"
            onChangeText={(text) => searchLocation(text, "destination")}
            value={searchKeyWordDestination}
            onFocus={() => setIsFocusDestination(true)}
            onBlur={() => setIsFocusDestination(false)}
          />
        </View>
      </SafeAreaView>
    </View>

    <View>
        <Button
          icon={
            <Icon
              name="doubleright"
              size={15}
              color={colors.WHITE}
              type="antdesign"
            />
          }
          iconRight
          title="GO !"

          onPress={()=>onPressGo()}
         
          titleStyle={CustomStyle.styleButtonShortCmdTitle}
          buttonStyle={CustomStyle.styleShortCmdButton}
        />
      </View>
  </View>
    <View
        style={{
          alignSelf: "center",
          position: "absolute",
          bottom: '20%',
        }}
      >
        
        {isShowingResultsDepart && (
          <FlatList
            data={searchResultsDepart}
            renderItem={({ item, index }) => {
              return (
                <TouchableOpacity
                  key={item.place_id}
                  style={[
                    {
                      width: "100%",
                      justifyContent: "center",
                      borderBottomColor: "#ECECEC",
                      paddingLeft: 15,
                    },
                    index === searchResultsDepart.length - 1
                      ? { borderBottomWidth: 0 }
                      : { borderBottomWidth: 1 },
                  ]}
                  onPress={() => {
                    console.log("depart");
                    setSearchKeyWordDepart(item.description);
                    setIsShowingResultsDepart(false);
                    setDisplayReservation(true);
                    calculPosition(item.place_id, item.description, "depart");
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Montserrat-Medium",
                      color: "#848484",
                      fontWeight: "bold",
                      fontSize: 14,
                      paddingBottom: 20,
                      paddingTop: 10,
                    }}
                  >
                    {item.description}
                  </Text>
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item, index) => {
              return item.place_id;
            }}
            style={styles.searchResultsContainer}
          />
        )}
        {isShowingResultsDestination && (
          <FlatList
            data={searchResultsDestination}
            renderItem={({ item, index }) => {
              return (
                <TouchableOpacity
                  key={item.place_id}
                  style={[
                    {
                      width: "100%",
                      justifyContent: "center",
                      borderBottomColor: "#ECECEC",
                      paddingLeft: 15,
                    },
                    index === searchResultsDestination.length - 1
                      ? { borderBottomWidth: 0 }
                      : { borderBottomWidth: 1 },
                  ]}
                  onPress={() => {
                    setSearchKeyWordDestination(item.description);
                    setIsShowingResultsDestination(false);
                    setDisplayReservation(true);
                    calculPosition(
                      item.place_id,
                      item.description,
                      "destination"
                    );
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Montserrat-Medium",
                      color: "#848484",
                      fontWeight: "bold",
                      fontSize: 14,
                      paddingBottom: 20,
                      paddingTop: 10,
                    }}
                  >
                    {item.description}
                  </Text>
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item, index) => {
              return item.place_id;
            }}
            style={styles.searchResultsContainer}
          />
        )}
      </View>
  </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "flex-start",
    alignItems: "center",
    position: "absolute",
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
  },

  searchContainer: {
    backgroundColor: "white",
    flexDirection: "column",
    height: 320,
    width: "90%",
    position: "absolute",
    top: 20,
    alignSelf: "center",
    // justifyContent: "center",
    paddingVertical:10,
    alignItems: "center",
    borderRadius: 13,
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 2,
    shadowRadius: 6,
    elevation: 3,
  },
  searchBodyIconWrapper: {
    backgroundColor: "red",
  },
  searchBodyButtonsWrapper: {
    height: "100%",
    width: "80%",
  },
  searchBodyButton: {
    height: 40,
    width: "100%",
    flexDirection: "column",
  },
  searchBodyContentButtonView: {
    // height: 40,
    width: "100%",
    flexDirection: "column",
  },
  searchBodyContentTitleButton: {
    // paddingBottom: 8,
    fontSize: 12,
    // lineHeight: 15,
    fontWeight:'bold',
    color: colors.BLACK,
    fontFamily: "Montserrat-Bold",
  },
  searchBodyContentLocationButton: {
    fontSize: 14,
    lineHeight: 18,
    fontFamily: "Montserrat-Medium",
    color: "rgba(151,151,151,1)",
  },
  resultItem: {
    width: "100%",
    justifyContent: "center",
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    paddingLeft: 15,
    height: 40,
    color: "rgba(151,151,151,1)",
    fontFamily: "Montserrat-Medium",
    fontSize: 14,
  },
  searchResultsContainer: {
    width: 340,
    height: 200,
    backgroundColor: "#FFFFFF",
    color: "rgba(151,151,151,1)",

    borderBottomRightRadius: 13,
    borderBottomLeftRadius: 13,
    top: 124,
    borderRadius: 13,
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 2,
    shadowRadius: 6,
    elevation: 3,
    zIndex: 20,
  },
  bottomBanner: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
  },
  modalView: {
    marginTop: 15,
    backgroundColor: "white",
    borderRadius: 13,
    paddingTop: 15,
    width: "90%",
    alignItems: "center",
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 3,
  },
  modalDivider: {
    borderBottomWidth: 1,
    borderColor: "#e1dede",
    height: 10,
    marginBottom: 10,
    width: "90%",
  },
  modalTitleText: {
    alignSelf: "center",
    color: colors.BLACK,
    fontFamily: "Montserrat-Bold",
    textAlign: "center",
    fontSize: 18,
    lineHeight: 22,
  },
  modalContentContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    alignSelf: "stretch",
  },
  modalContentButton: {
    alignSelf: "stretch",
    flex: 1,
  },
  modalContentTitleButton: {
    fontSize: 12,
    lineHeight: 15,
    color: colors.BLACK,
    fontFamily: "Montserrat-Bold",
    textTransform: "uppercase",
  },
  modalContentLocationButton: {
    fontSize: 12,
    lineHeight: 16,
    fontFamily: "Montserrat-Medium",
    color: "#848484",
  },
  goButton: {
    alignSelf: "center",
    paddingHorizontal: 50,
    height: 35,
    borderRadius: 10,
    backgroundColor: colors.VIOLET,
    marginTop: 10,
    marginBottom: 20,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default ProposeTravel;
