import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../common/theme";

const styles = StyleSheet.create({
  inputStyle: {
    height: 50,
    backgroundColor: "white",
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: {
      height: 0,
      width: 2,
    },
    shadowRadius: 6,
    shadowOpacity: 0.26,
    elevation: 3,
    padding: 10,
  },
  inputContainerStyle: {
    borderBottomWidth: 0,
  },
  labelInputStyle: {
    color: "black",
    fontSize: 13,
    fontFamily: "Montserrat-Bold",
    padding: 5,
  },
});

export default styles;
