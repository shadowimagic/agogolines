import React from "react";
import { View, Text } from "react-native";
import { InferProps } from "prop-types";
import { TextPropTypes } from "react-native";
import { Input, Button, IconProps } from "react-native-elements";
import styles from "./Agogo.style";
const AgogoInput = (IconProps: IconProps) => {
  return (
    <Input
      {...IconProps}
      inputStyle={[styles.inputStyle]}
      inputContainerStyle={styles.inputContainerStyle}
      labelStyle={styles.labelInputStyle}
    />
  );
};

export default AgogoInput;
