import React from "react";
import {View, Text, StyleSheet, TouchableOpacity, Image, TextInput} from "react-native";
import {SvgXml} from "react-native-svg";
import travel from "../../../assets/svg/travel";

import { EventRegister } from 'react-native-event-listeners';

const ModalFollowLastResearch = (props) => {

	const fermeture = () => {

		EventRegister.emit('initrider',true);
		
	};
	
	return (
		<View style={styles.container}>
			<View>
				<Text style={styles.upperText}>Souhaitez-vous poursuivre</Text>
				<Text style={styles.upperText}>votre recherche ?</Text>
			</View>
			<View style={styles.departArrContainer}>
				<SvgXml xml={travel} style={{height: "100%", marginLeft: 12}} />
				<View style={styles.textBoxContainer}>
					<View>
						<Text style={styles.mainText}>DÉPART</Text>
						<Text style={styles.wrapText} numberOfLines={1}>

						{props.data && props.data.addr_dep && props.data.addr_dep.length < 35
						? `${props.data.addr_dep}`
						: `${props.data.addr_dep.substring(0, 27)}...`}
						
						</Text>
					</View>
					<View style={styles.separator}></View>
					<View>
						<Text style={styles.mainText}>Arrivée</Text>
						<Text style={styles.wrapText} numberOfLines={1}>
							{props.data && props.data.addr_dest && props.data.addr_dest.length < 35
						? `${props.data.addr_dest}`
						: `${props.data.addr_dest.substring(0, 27)}...`}
						</Text>
					</View>
				</View>
			</View>

			<View style={styles.btnContainer}>
				<View style={styles.twoBtnWrap}>
					<TouchableOpacity
						onPress={props.onFollowTravelsModal}
						style={styles.btnVioletContainer}>
						<Text style={styles.btn}>POURSUIVRE</Text>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={() => {

							props.setDisplayModal(false);
							fermeture();
						}}
						style={styles.btnTransparentContainer}>
						<Text style={styles.btn}>ANNULER</Text>
					</TouchableOpacity>
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#000",
		opacity: 0.8,
	},
	upperText: {
		fontFamily: "Montserrat-Bold",
		color: "white",
		fontSize: 18,
		lineHeight: 22,
		textAlign: "center",
	},
	departArrContainer: {
		backgroundColor: "#FFFEFE",
		width: 335,
		height: 120,
		marginTop: 20,
		borderRadius: 13,
		flexDirection: "row",
		alignItems: "center",
	},
	textBoxContainer: {
		flexDirection: "column",
		height: "100%",
		justifyContent: "space-around",
		marginLeft: 13,
	},
	separator: {
		width: "100%",
		borderWidth: 1,
		borderColor: "#ECECEC",
	},
	mainText: {
		fontFamily: "Montserrat-Bold",
		fontSize: 12,
		lineHeight: 15,
		textTransform: "uppercase",
		textAlign: "left",
	},
	wrapText: {
		fontFamily: "Montserrat-Medium",
		fontSize: 14,
		lineHeight: 18,
		color: "#848484",
		textAlign: "left",
	},
	btnContainer: {
		marginTop: 40,
	},
	twoBtnWrap: {
		flexDirection: "column",
	},
	btnVioletContainer: {
		backgroundColor: "#5A48CB",
		width: 200,
		height: 40,
		alignItems: "center",
		justifyContent: "center",
		borderRadius: 12,
		marginBottom: 12,
	},
	btnTransparentContainer: {
		width: 200,
		height: 40,
		alignItems: "center",
		justifyContent: "center",
		borderWidth: 1,
		borderRadius: 12,
		borderColor: "#FFF",
	},
	btn: {
		fontFamily: "Montserrat-Bold",
		fontSize: 12,
		lineHeight: 15,
		color: "#FFF",
		textAlign: "center",
	},
});

export default ModalFollowLastResearch;
