import React, { useContext, useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  SafeAreaView,
  StyleSheet,
  TextInput,
} from "react-native";
import { Button } from "react-native-elements";
import { AirbnbRating } from "react-native-ratings";

import { colors } from "../../common/theme";
import user from "../../../assets/images/avatar_login.png";

import { FirebaseContext } from "../../../redux/src";
import { useSelector, useDispatch } from "react-redux";

import { SvgXml } from "react-native-svg";
import travel from "../../../assets/svg/travel";
import smallCar from "../../../assets/svg/car";

import { EventRegister } from "react-native-event-listeners";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { removeData } from "../../functions/AsyncStorageFunctions";

const RatingAfterTravel = (props) => {
  const auth = useSelector((state) => state.auth);
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;
  const dispatch = useDispatch();
  const { api } = useContext(FirebaseContext);
  const { addComment, fetchOneTravel, deleteTravel, updateProfile } = api;

  const [commentaire, setCommentaire] = useState("");
  const [etoile, setEtoile] = useState(0);

  useEffect(() => {
    if (auth.info && props.datarider && auth.info.profile) {
      dispatch(fetchOneTravel(props?.datadriver?.driver_id));
    }
  }, [auth.info, props.datarider]);

  const onSubmit = async () => {
    let driverId = props?.datadriver?.driver_id
      ? props?.datadriver?.driver_id
      : null;

    if (travelInformations && travelInformations.propose_negoPrice) {
      let pointrider = Math.ceil(travelInformations.propose_negoPrice);

      if (pointrider >= 1) {
        if (pointrider > 10) pointrider = 10;

        let updateData = {
          point: pointrider,
        };

        dispatch(updateProfile(auth.info, updateData));
      }
    }

    if (driverId && travelInformations) {
      let commentaireData = null;

      if (commentaire != "") {
        commentaireData = {
          dateEtHeure: new Date().getTime(),
          commentaire: commentaire || "Parfait",
          etoile: etoile,
          idDriver: props?.datadriver?.driver_id,
          idRider: props.datarider.rider_id,
          depart: props.datarider.addr_dep,
          destination: props.datarider.addr_dest,
        };
      }

      dispatch(addComment(commentaireData, driverId));
    }

    props.setDisplayRatingAfterTravel(false);
    props.setTravelInit(true);
    await removeData("travelId");
    await removeData("cardToUse");
    EventRegister.emit("initrider", true);
  };

  return (
    <KeyboardAwareScrollView
      extraHeight={200}
      scrollEnabled={false}
      contentContainerStyle={{ height: "100%" }}
    >
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <View style={styles.modal}>
            <View
              style={{
                alignSelf: "center",
                width: 80,
                height: 80,
                position: "absolute",
                top: -40,
              }}
            >
              <Image
                source={
                  props.datadriver && props?.datadriver?.driver_profile_img
                    ? { uri: props?.datadriver?.driver_profile_img }
                    : user
                }
                style={{ width: 80, height: 80, borderRadius: 40 }}
              />
            </View>

            <Text
              style={{
                color: "#000",
                textAlign: "center",
                fontFamily: "Montserrat-Bold",
                textTransform: "uppercase",
                fontSize: 14,
                lineHeight: 19,
                marginTop: 20,
              }}
            >
              {props?.datadriver?.driver_firstName}{" "}
              {props?.datadriver?.driver_lastName}
            </Text>

            <View
              style={{
                alignItems: "center",
                marginTop: 5,
                marginBottom: 5,
              }}
            >
              <SvgXml xml={smallCar} />
            </View>

            <View
              style={{
                alignSelf: "center",
                paddingHorizontal: 15,
                alignItems: "center",
                justifyContent: "space-between",
                flexDirection: "row",
                marginTop: 5,
              }}
            >
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <SvgXml xml={travel} />
              </View>
              <View
                style={{
                  flex: 8,
                  paddingHorizontal: 5,
                  justifyContent: "space-between",
                }}
              >
                <View style={{ marginBottom: 5 }}>
                  <Text
                    style={{
                      color: "black",
                      fontFamily: "Montserrat-Bold",
                      fontSize: 12,
                      lineHeight: 15,
                    }}
                  >
                    Départ
                  </Text>
                  <Text
                    style={{
                      color: "#848484",
                      fontFamily: "Montserrat-Medium",
                      fontSize: 14,
                      lineHeight: 18,
                    }}
                    numberOfLines={3}
                  >
                    {props.datarider.addr_dep}
                  </Text>
                </View>

                <View
                  style={{
                    width: "100%",
                    height: 1,
                    backgroundColor: "#ECECEC",
                  }}
                ></View>

                <View style={{ marginTop: 5 }}>
                  <Text
                    style={{
                      color: "black",
                      fontFamily: "Montserrat-Bold",
                      fontSize: 12,
                      lineHeight: 15,
                    }}
                  >
                    Arrivée
                  </Text>
                  <Text
                    style={{
                      color: "#848484",
                      fontFamily: "Montserrat-Medium",
                      fontSize: 14,
                      lineHeight: 18,
                    }}
                    numberOfLines={3}
                  >
                    {props.datarider.addr_dest}
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                width: "85%",
                height: 1,
                marginVertical: 10,
                backgroundColor: "#DEDDDD",
                alignSelf: "center",
              }}
            ></View>

            <View style={{ alignSelf: "center", padding: 5 }}>
              <Text
                style={{
                  fontFamily: "Montserrat-Bold",
                  fontSize: 18,
                  lineHeight: 22,
                  textAlign: "center",
                }}
              >
                Comment était votre trajet
              </Text>
              <Text
                style={{
                  fontFamily: "Montserrat-Bold",
                  fontSize: 18,
                  lineHeight: 22,
                  textAlign: "center",
                }}
              >
                avec {props?.datadriver?.driver_firstName} ?
              </Text>
              <Text
                style={{
                  fontFamily: "Montserrat-Medium",
                  fontSize: 14,
                  lineHeight: 18,
                  textAlign: "center",
                  color: "#848484",
                  marginTop: 10,
                }}
              >
                Merci de noter votre conducteur
              </Text>
              <Text
                style={{
                  fontFamily: "Montserrat-Medium",
                  fontSize: 14,
                  lineHeight: 18,
                  textAlign: "center",
                  color: "#848484",
                }}
              >
                et de nous aider à améliorer nos services.
              </Text>
              <AirbnbRating
                starContainerStyle={{ marginTop: -20 }}
                count={5}
                reviews={[]}
                defaultRating={5}
                size={30}
                onFinishRating={(rating) => setEtoile(rating)}
              />

              <TextInput
                placeholder="Laisser un commentaire"
                style={styles.input}
                multiline={true}
                numberOfLines={5}
                textAlignVertical="top"
                value={commentaire}
                onChangeText={(text) => setCommentaire(text)}
              />
              <Button
                title="ENVOYER"
                buttonStyle={styles.buttonStyle}
                titleStyle={styles.btnTitleStyle}
                onPress={onSubmit}
              />
            </View>
          </View>
        </View>
      </SafeAreaView>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    backgroundColor: "white",
  },
  modal: {
    backgroundColor: "white",
    width: "90%",
    paddingVertical: 20,
    borderRadius: 13,

    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
    justifyContent: "center",
  },
  text: {
    marginTop: 10,
    fontSize: 15,
    lineHeight: 20,
    fontFamily: "Montserrat-Bold",
    color: "#5a48cb",
  },
  secondText: {
    fontSize: 14,
    fontFamily: "Montserrat-Bold",
    color: "#9E9E9E",
    lineHeight: 20,
  },

  btnTitleStyle: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 18,
    color: "white",
    fontFamily: "Montserrat-Bold",
  },
  buttonStyle: {
    marginTop: 15,
    backgroundColor: "#5a48cb",
    borderRadius: 12,
    alignSelf: "center",
  },
  bodyTop: {
    height: "30%",
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  userImage: {
    width: 47,
    height: 47,
    marginRight: 10,
    borderRadius: 12,
  },
  userName: {
    fontWeight: "bold",
  },
  starIcon: {
    flexDirection: "row",
    marginVertical: 5,
  },
  stopPhoneIcon: {
    flexDirection: "row",
    marginLeft: "auto",
    marginRight: 10,
  },
  iconMargin: {
    marginTop: 10,
    marginRight: 10,
  },
  element: {
    flexDirection: "row",
  },
  bodyBottom: {
    backgroundColor: "white",
    height: "70%",
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  textContainer: {
    marginLeft: 5,
  },
  elementHeaderText: {
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  elementBodyText: {
    color: colors.GREYD,
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    lineHeight: 18,
  },
  elementBodyTextViolet: {
    fontWeight: "bold",
    fontSize: 20,
    lineHeight: 24,
    fontFamily: "Montserrat-Bold",
    color: "#FFB406",
  },
  bodyBottomOne: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bodyBottomTwo: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: "10%",
    marginBottom: 10,
  },
  input: {
    borderRadius: 10,
    color: colors.BLACK,
    paddingHorizontal: 10,
    paddingVertical: 10,
    fontFamily: "Montserrat-Medium",
    fontSize: 15,
    height: 100,
    width: 300,
    marginTop: 15,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowRadius: 8,
    shadowOpacity: 0.8,
    borderColor: "red",
    backgroundColor: "white",
    shadowColor: "#979797",
    elevation: 2,
    alignSelf: "center",
  },
});

export default RatingAfterTravel;
