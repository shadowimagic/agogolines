import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

import { FontAwesome } from "@expo/vector-icons";

const NoDriverFound = (props) => {
  return (
    <View style={styles.container}>
      <View
        style={{
          width: 350,
          height: 200,
          backgroundColor: "white",
          borderRadius: 15,
        }}
      >
        <View style={{ marginTop: 20, alignItems: "center" }}>
          <FontAwesome name="search-plus" size={30} color="red" />
        </View>
        <View
          style={{
            marginTop: 10,
            alignItems: "center",
          }}
        >
          <Text
            style={{
              color: "#9a9a9a",
              fontFamily: "Montserrat-Bold",
              fontSize: 15,
              lineHeight: 18,
              textAlign: "center",
            }}
          >
            Aucun conducteur n’est disponible
          </Text>
          <Text
            style={{
              color: "#9a9a9a",
              fontFamily: "Montserrat-Bold",
              fontSize: 15,
              lineHeight: 18,
              textAlign: "center",
            }}
          >
            actuellement
          </Text>
        </View>
        <View
          style={{
            marginTop: 10,
            alignItems: "center",
          }}
        >
          <Text
            style={{
              color: "#9a9a9a",
              fontFamily: "Montserrat-Medium",
              fontSize: 15,
              lineHeight: 18,
              textAlign: "center",
            }}
          >
            Veuillez réessayer ultérieurement.
          </Text>
        </View>

        <View style={{ alignItems: "center", marginTop: 10 }}>
          <TouchableOpacity
            style={{
              marginTop: 10,
              alignItems: "center",
              width: 125,
              borderRadius: 12,
              backgroundColor: "#5a48cb",
            }}
            onPress={props.onCloseModal}
          >
            <Text
              style={{
                color: "white",
                fontSize: 15,
                fontFamily: "Montserrat-Bold",
                textAlign: "center",
                lineHeight: 18,
                paddingVertical: 10,
              }}
            >
              RÉESSAYER
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-end",
    opacity: 1,
  },
});

export default NoDriverFound;
