import React, { useContext } from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet } from "react-native";
import { colors } from "../../common/theme";

import { SvgXml } from "react-native-svg";

import cancel from "../../../assets/svg/cancel";
import riderImg from "../../../assets/images/avatar_login.png";
import { FirebaseContext } from "../../../redux/src";
import { useDispatch, useSelector } from "react-redux";

const DriverCancelTravelInProcess = (props) => {
  const { closeDriverCancelTravelInProcess, openRouteConfirm } = props;
  const dispatch = useDispatch();

  const { api } = useContext(FirebaseContext);
  const { addTravel } = api || {};

  const travels = useSelector((state) => state.travel);

  const travelInformations = travels.travel;

  const onPressConfirm = () => {
    travelInformations.status = "init";
    travelInformations.isFound = false;
    travelInformations.arriver_dest_passager = false;
    travelInformations.arriver_vers_passager = false;

    travelInformations.isAffiner_rider = false;
    travelInformations.isAffiner_driver = false;

    dispatch(addTravel(travelInformations));
    closeDriverCancelTravelInProcess();
    openRouteConfirm();
  };

  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            marginLeft: 13,
            marginTop: 10,
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <SvgXml
              xml={cancel}
              style={{ marginRight: -15, elevation: 0.1, zIndex: 2 }}
            />

            <Image
              source={
                travelInformations &&
                travelInformations.propose_profile_img_rider &&
                travelInformations.propose_profile_img_rider != "non"
                  ? { uri: travelInformations.propose_profile_img_rider }
                  : riderImg
              }
              style={{ width: 47, height: 47, borderRadius: 50 }}
            />
          </View>
          <View style={{ flexDirection: "column" }}>
            <Text
              style={{
                color: "black",
                fontSize: 14,
                lineHeight: 18,
                textAlign: "left",
                marginLeft: 15,
                fontFamily: "Montserrat-Bold",
              }}
            >
              {travelInformations
                ? travelInformations.propose_firstname_rider +
                  "" +
                  travelInformations.propose_lastname_rider
                : null}
            </Text>
            <Text
              style={{
                color: "red",
                fontSize: 14,
                lineHeight: 18,
                textAlign: "left",
                marginLeft: 15,
                fontFamily: "Montserrat-Bold",
              }}
            >
              vient d'annuler son trajet
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            marginTop: 8,
            marginLeft: 13,
            alignItems: "center",
            alignSelf: "center",
          }}
        >
          <TouchableOpacity
            style={{
              width: 117,
              height: 36,
              borderRadius: 11,
              backgroundColor: "#5A48CB",
              justifyContent: "center",
            }}
            onPress={onPressConfirm}
          >
            <Text
              style={{
                fontFamily: "Montserrat-Bold",
                color: "white",
                lineHeight: 18,
                textAlign: "center",
              }}
            >
              OK
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    width: "100%",
    marginBottom: 30,
  },
  modal: {
    backgroundColor: "white",
    height: 110,
    width: 380,
    marginHorizontal: 15,
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
    alignSelf: "center",
    //justifyContent: 'center',
  },
  text: {
    marginTop: 10,
    fontSize: 15,
    lineHeight: 20,
    fontFamily: "Montserrat-Bold",
    color: "#5a48cb",
  },
  secondText: {
    fontSize: 14,
    fontFamily: "Montserrat-Bold",
    color: "#9E9E9E",
    lineHeight: 20,
  },

  btnTitleStyle: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 18,
    color: "white",
    fontFamily: "Montserrat-Bold",
  },
  buttonStyle: {
    marginTop: 10,
    backgroundColor: "#5a48cb",
    borderRadius: 12,
    alignSelf: "center",
  },
  bodyTop: {
    height: "30%",
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  userImage: {
    width: 47,
    height: 47,
    marginRight: 10,
    borderRadius: 12,
  },
  userName: {
    fontWeight: "bold",
  },
  starIcon: {
    flexDirection: "row",
    marginVertical: 5,
  },
  stopPhoneIcon: {
    flexDirection: "row",
    marginLeft: "auto",
    marginRight: 10,
  },
  iconMargin: {
    marginTop: 10,
    marginRight: 10,
  },
  element: {
    flexDirection: "row",
  },
  bodyBottom: {
    backgroundColor: "white",
    height: "70%",
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  textContainer: {
    marginLeft: 5,
  },
  elementHeaderText: {
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  elementBodyText: {
    color: colors.GREYD,
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    lineHeight: 18,
  },
  elementBodyTextViolet: {
    fontWeight: "bold",
    fontSize: 20,
    lineHeight: 24,
    fontFamily: "Montserrat-Bold",
    color: "#FFB406",
  },
  bodyBottomOne: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bodyBottomTwo: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: "10%",
    marginBottom: 10,
  },
});

export default DriverCancelTravelInProcess;
