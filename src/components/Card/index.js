import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native";

//FontAwesome
import { FontAwesome } from "@expo/vector-icons";

const Card = ({ data, cardImage, tickStyle, defaut, vert, onPressCard }) => {
  return (
    <TouchableOpacity
      style={{
        flexDirection: "row",
        justifyContent: "space-around",
        marginTop: 5,
        borderWidth: 1,
        borderColor: vert || "#000",
        borderRadius: 10,
        width: 270,
        height: 45,
      }}
      onPress={onPressCard}
    >
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-around",
          width: 180,
        }}
      >
        <Image source={cardImage} />
        <Text
          style={{
            fontSize: 12,
            color: "#979797",
          }}
        >
          **** **** ****{data.numerocarte}
        </Text>
      </View>
      <View
        style={{
          alignSelf: "center",
        }}
      >
        <Image source={tickStyle} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textLabel: {
    fontSize: 14,
    lineHeight: 18,
    color: "#979797",
    fontFamily: "Montserrat-Medium",
    marginTop: 16,
    marginBottom: 8,
  },

  bankDefaultCard: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 270,
    borderRadius: 10,
    shadowRadius: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: "rgba(137,137,137,0.5)",
    paddingTop: 25,
    paddingBottom: 15,
    backgroundColor: "white",
    shadowColor: "rgba(137,137,137,0.5)",
    shadowOpacity: 0.8,
    elevation: 5,
  },
  bankOptionalCard: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 270,
    borderRadius: 10,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: "rgba(137,137,137,0.5)",
    paddingTop: 25,
    paddingBottom: 15,
    backgroundColor: "white",
  },
  separator: {
    borderBottomWidth: 1,
    marginVertical: 25,
    borderBottomColor: "#CFCFCF",
  },
});

export default Card;
