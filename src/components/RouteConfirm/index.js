import React, { useContext, useEffect, useState, useRef } from "react";
import { View, Text } from "react-native";
import { Button } from "react-native-elements";
import popuptick from "../../../assets/svg/popuptick";
import DeleteTravel from "../DeleteTravelModal";
import styles from "./styles";
import NoRiderFoundByDriverModal from "./../NoRiderFoundByDriverModal/index";
import Modal from "react-native-modal";

import { FirebaseContext } from "../../../redux/src";
import { useDispatch, useSelector } from "react-redux";
import { SvgXml } from "react-native-svg";

// import { AdMobBanner, setTestDeviceIDAsync } from "expo-ads-admob";

const RouteConfirm = (props) => {
  const { closeRouteConfirm, openDriverGoingToRider, isVisible, reInitTravel, onDestPress } =
    props;

  const [confirmDeleteTravel, setConfirmDeleteTravel] = useState(false);
  const [newDemandRider, setNewDemandRider] = useState(false);

  const firstCheck = useRef(true);

  const { api } = useContext(FirebaseContext);
  const { deleteTravel } = api || {};
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;
  const dispatch = useDispatch();

  // useEffect(() => {
  //   setTestDeviceIDAsync("EMULATOR");
  // }, []);

  const handleCancelTravel = () => {
    setConfirmDeleteTravel(true);
  };

  const onDeleteTravel = () => {
    setConfirmDeleteTravel(false);
    closeRouteConfirm();
    reInitTravel(true);
  };

  const openNewDemand = () => setNewDemandRider(true);
  const closeNewDemand = () => setNewDemandRider(false);

  const onValidatedTravel = () => {
    closeNewDemand();
    closeRouteConfirm();
    openDriverGoingToRider();
  };

  const handleNotFound = () => {
    closeNewDemand();
    closeRouteConfirm();
    reInitTravel(true);
  };

  useEffect(() => {
    if (
      travelInformations?.isAffiner_rider &&
      travelInformations?.isFound &&
      !newDemandRider &&
      firstCheck.current
    ) {
      openNewDemand();
      firstCheck.current = false;
    }
    if (
      !travelInformations?.isAffiner_rider &&
      !travelInformations?.isFound &&
      !firstCheck.current
    ) {
      firstCheck.current = true;
    }
  }, [
    newDemandRider,
    travelInformations?.isAffiner_rider,
    travelInformations?.isFound,
  ]);
  useEffect(() => {
    if (!travelInformations) {
      firstCheck.current = true;
    }
  }, [travelInformations]);

  useEffect(() => {
    if (travelInformations != null) {
      const timer = setInterval(() => {
        let times1 = parseInt(travelInformations.create_time, 10);
        let times2 = parseInt(new Date().getTime(), 10);
        let date1 = new Date(times1);
        let date2 = new Date(times2);
        let diff = (date2 - date1) / 1000;
        diff /= 60;
        let datadiff = Math.abs(Math.round(diff));

        if (datadiff && datadiff > 120) {
          if (travelInformations != null) {
            dispatch(deleteTravel(travelInformations, "oui"));
            reInitTravel();
          }
        }
      }, 12000);
      return () => clearInterval(timer);
    }
  }, []);

  const renderContent = () => {
    if (confirmDeleteTravel) {
      return <DeleteTravel onDeleteTravel={onDeleteTravel} />;
    }

    if (newDemandRider) {
      return (
        <NoRiderFoundByDriverModal
          onValidatedTravel={onValidatedTravel}
          handleNotFound={handleNotFound}
          closeNewDemand={closeNewDemand}
          onDestPress={onDestPress}
        />
      );
    }

    return (
      <View style={styles.container}>
        <View style={styles.modal}>
          <View style={styles.tick}>
            <SvgXml xml={popuptick} />
          </View>
          <View style={styles.header}>
            <Text style={styles.headerText}>
              Nouveau trajet disponible !
            </Text>
          </View>
          <View style={styles.middle}>
            <Text style={styles.middleText}>
              Nous recupérons les informations du passager vers votre destination...
            </Text>
          </View>
          <View style={styles.btnContainer}>
            <Button
              title="annuler mon trajet"
              buttonStyle={styles.buttonStyle}
              titleStyle={styles.btnTitleStyle}
              onPress={handleCancelTravel}
            />
          </View>
        </View>
        {/* <AdMobBanner
          style={styles.bottomBanner}
          bannerSize="smartBanner"
          adUnitID="ca-app-pub-5608997176551587/4926705154"
          servePersonalizedAds // true or false
          onDidFailToReceiveAdWithError={(e) => console.log(e)}
        /> */}
      </View>
    );
  };

  return (
    <Modal
      isVisible={isVisible}
      animationType="slide"
      transparent={true}
      backdropOpacity={0}
      coverScreen={false}
      style={{ margin: 0 }}
    >
      {renderContent()}
    </Modal>
  );
};

export default RouteConfirm;
