import { StyleSheet } from 'react-native';
import { colors } from '../../common/theme';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},	
	bottomBanner: {
		position: "absolute",
		alignSelf: "center",
		bottom: 0
	  },
	tick: {
		alignItems: 'center',
		marginVertical: 20,
	},
	header: {
		alignItems: 'center',
		marginHorizontal: 15,
		marginVertical: 10,
	},
	middle: {
		alignItems: 'center',
		marginHorizontal: 10,
		marginVertical: 10,
	},
	headerText: {
		fontSize: 18,
		color: colors.VIOLET,
		textAlign: 'center',
		textTransform: 'uppercase',
		fontFamily: 'Montserrat-Light',
		fontWeight: 'bold',
	},
	middleText: {
		fontFamily: 'Montserrat-Medium',
		fontWeight: 'bold',
		fontSize: 15,
		color: colors.GREYD,
		textAlign: 'center',
	},
	modal: {
		backgroundColor: 'white',
		height: '50%',
		marginHorizontal: 15,
		borderRadius: 13,
		shadowColor: 'black',
		shadowOffset: {height: 2, width: 0},
		shadowOpacity: 0.26,
		shadowRadius: 6,
		elevation: 6
	},
	btnContainer: {
		alignItems: 'center',
		marginTop: '2%',
		marginBottom: '10%'
	},
	btnTitleStyle: {
		textAlign: 'center',
		textTransform: 'uppercase',
		padding: 16,
		color: 'black'
	},
	buttonStyle: {
		backgroundColor: 'transparent',
		borderColor: 'black',
		borderWidth: 2,
		borderRadius: 13
	}
});

export default styles;