import React, { useContext, useEffect, useState } from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import { Button } from "react-native-elements";

import { SvgXml } from "react-native-svg";
import juste from "../../../assets/svg/reservConf";

import { FirebaseContext } from "../../../redux/src";
import { useSelector, useDispatch } from "react-redux";

import DriverCancelTravel from "../../components/DriverCancelTravel";

const TravelConfirm = (props) => {
  const { api } = useContext(FirebaseContext);
  const { fetchOneTravel, fetchUser } = api;
  const auth = useSelector((state) => state.auth);
  const travelData = useSelector((state) => state.travel);
  const donner = travelData.travel;

  const [showCancel, setShowCancel] = useState(false);

  const [closeCancelModal, setDisplayWaitRiderNegoPrice] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      if (
        donner != null &&
        donner.isFound == false &&
        donner.status == "init"
      ) {
        //setShowCancel(true);
        props.setDisplayDriverCancelTravel(true);
        props.setTravelAccepted(false);
      }
    }
  }, [auth.info, donner, showCancel]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      const interval = setInterval(() => {
        dispatch(fetchUser());
        dispatch(fetchOneTravel(props?.datadriver?.driver_id));
      }, 8000);

      return () => {
        clearInterval(interval);
      };
    }
  }, [auth.info]);

  /**if (showCancel){
	
		return   <DriverCancelTravel setDisplayWaitRiderNegoPrice={setDisplayWaitRiderNegoPrice} datadriver={donner}/>; 
	}**/

  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <View
          style={{
            width: 60,
            height: 60,
            backgroundColor: "#5a48cb",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 50,
            marginTop: 15,
          }}
        >
          <SvgXml xml={juste} style={{ width: 58, height: 58 }} />
        </View>
        <Text style={styles.text}>CONFIRMÉE</Text>
        <Text style={[styles.secondText, { marginTop: 10 }]}>
          Votre réservation est confirmée.
        </Text>
        <Text style={styles.secondText}>
          {/*Votre conducteur arrivera dansdonner && donner.propose_tmp_btn_rider_driver ? donner.propose_tmp_btn_rider_driver: props?.datadriver?.propose_tmp_btn_rider_driver*/}
          Votre conducteur arrivera dans{" "}
          {Math.round(auth?.info?.profile?.location?.duration) || 5}min.
        </Text>
        <Button
          title="ok"
          buttonStyle={styles.buttonStyle}
          titleStyle={styles.btnTitleStyle}
          onPress={props.onCloseModal}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    width: "100%",
  },
  modal: {
    backgroundColor: "white",
    height: 230,
    marginHorizontal: 15,
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
    alignItems: "center",
    // justifyContent: 'center',
  },
  text: {
    marginTop: 10,
    fontSize: 15,
    lineHeight: 20,
    fontFamily: "Montserrat-Bold",
    color: "#5a48cb",
  },
  secondText: {
    fontSize: 15,
    fontFamily: "Montserrat-Medium",
    color: "#898989",
    lineHeight: 20,
  },

  btnTitleStyle: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 18,
    color: "white",
    fontFamily: "Montserrat-Bold",
  },
  buttonStyle: {
    marginTop: 10,
    backgroundColor: "#5a48cb",
    borderRadius: 12,
  },
});

export default TravelConfirm;
