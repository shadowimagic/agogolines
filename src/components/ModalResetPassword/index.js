import React, {useState, useContext, useEffect} from "react";
import {
  View,
  StyleSheet,
  Text,
  Alert,
  TextInput,
  TouchableOpacity,
  Platform,
} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {FirebaseContext} from "../../../redux/src";
import {language} from "../../../config";


const ModalResetPassword = (props) => {

  const [email , setEmail] =useState("");
  const [fermer, setFermer] =useState(false);

  const {api} = useContext(FirebaseContext);
	const {sendResetMail} = api;
  const dispatch = useDispatch();

  useEffect(() => {
    if (fermer  == true ){

      if (email !=""){
       
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if(re.test(email)){
        

     Alert.alert(
       language.forgot_password_link,
       language.forgot_password_confirm,
       [
         {text: language.cancel, style: "cancel"},
         {
           text: language.ok,
           onPress: () => {
             dispatch(sendResetMail(email));
             props.closeModal();
           },
         },
       ],
       {cancelable: true}
     );

    }else{

     Alert.alert(language.alert,language.proper_email);
    }
      
       

      }else{
        Alert.alert(language.alert,language.proper_email);
        setFermer(false)
      }
     
    }
 

}, [fermer]);

  const onPresSave = () => {
   
    setFermer(true)
  }

  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#000",
        opacity: 0.9,
      }}
    >
      <View style={styles.container}>
        <Text
          style={{
            textAlign: "center",
            fontFamily: "Montserrat-Bold",
            fontSize: 15,
            marginTop: 10,
          }}
        >
          Merci d'entrer votre adresse email pour réinitialiser votre mot de
          passe
        </Text>
        <TextInput 
        
        style={styles.textInput} 
        onChangeText={text => {
          setEmail(text);
      }}
        
        />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
            marginHorizontal: 25,
          }}
        >
          <TouchableOpacity
            onPress={props.closeModal}
            style={{
              width: 100,
              height: 50,
              borderColor: "black",
              borderWidth: 1,
              justifyContent: "center",
              alignItems: "center",
              borderRadius: 12,
              marginTop: 20,
            }}
          >
            <Text
              style={{
                fontFamily: "Montserrat-Bold",
                color: "black",
                fontSize: 15,
              }}
            >
              Annuler
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
           
            onPress={() => {onPresSave()}}
            style={{
              width: 100,
              height: 50,
              backgroundColor: "#5A48CB",
              justifyContent: "center",
              alignItems: "center",
              borderRadius: 12,
              marginTop: 20,
              marginLeft: 10,
            }}
          >
            <Text
              style={{
                fontFamily: "Montserrat-Bold",
                color: "white",
                fontSize: 15,
              }}
            >
              Envoyer
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "95%",
    height: 250,
    backgroundColor: "white",
    alignSelf: "center",
    justifyContent: "space-around",
    flexDirection: "column",
    shadowColor: "black",
    shadowOffset: {
      height: 0,
      width: 2,
    },
    shadowRadius: 6,
    shadowOpacity: 0.26,
    elevation: 3,
    borderRadius: 12,
  },
  textInput: {
    height: 50,
    width: "90%",
    backgroundColor: "white",
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: {
      height: 0,
      width: 2,
    },
    shadowRadius: 6,
    shadowOpacity: 0.26,
    elevation: 3,
    padding: 10,
    alignSelf: "center",
    marginTop: 20,
  },
});

export default ModalResetPassword;
