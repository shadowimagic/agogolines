import React, {useState, useEffect, useContext} from "react";
import {View, Text, Image, TouchableOpacity, Linking} from "react-native";
import {Button} from "react-native-elements";
import styles from "./styles";
import {
	FontAwesome,
	AntDesign,
	Ionicons,
	Feather,
	MaterialCommunityIcons,
	MaterialIcons,
} from "@expo/vector-icons";
import {colors} from "../../common/theme";
import car from "../../../assets/svg/car";
import user from "../../../assets/images/avatar_login.png";
import {callNumber} from "../phoneTrigger/index";
import {SvgXml} from "react-native-svg";
import email from "../../../assets/svg/email";
import tel from "../../../assets/svg/tel";
import euro from "../../../assets/svg/eurodriver";
import clock from "../../../assets/svg/clock";
import position from "../../../assets/svg/position";
import InputSpinner from "react-native-input-spinner";
import {
	Alert
} from "react-native";

import {FirebaseContext} from "../../../redux/src";
import {useSelector, useDispatch} from "react-redux";
import { EventRegister } from 'react-native-event-listeners';

const NegoPrixDriver = (props) => {
	
	let prixTotal = props.datarider.prix_affiner;
	const [affinerPrix, setAffinerPrix] = useState(props.datarider.prix_affiner);

	const {api} = useContext(FirebaseContext);
	const {updateTravel,fetchComment} = api;
	const auth = useSelector((state) => state.auth);

	const comments = useSelector(state => state.comment);
	const commentInformations =comments.comment;

	const [note, setNote] = useState(0);
	const dispatch = useDispatch();

	const onPressValider = () => {
	
		dispatch(
			updateTravel({
				propose_negoPrice: affinerPrix,
				propose_negoPrice_rider: affinerPrix,
				driver_id: props?.datadriver?.driver_id,
				mode_paiement:props.modepaiement.donner,
				isFound: true,
				isAffiner_rider: true,
				isAffiner_driver: false
			})
		);
	};



	
	useEffect(() => {
		if (auth.info && auth.info.profile) {
		
			dispatch(fetchComment(props?.datadriver?.driver_id));

		}
	}, [auth.info,props.datadriver]);
           

	useEffect(() => {
		if (auth.info && props.datadriver && auth.info.profile) {

	
			
			if (commentInformations){
				
				const taille = commentInformations.length;
				let somme =0;
				let moyenne =0;

				commentInformations.forEach(element => {

                    somme = somme +element.etoile;
					
				});
                if (taille!=0 && somme !=0){
					moyenne = Math.round(somme /taille);
				
					setNote(moyenne);
				}else{
					setNote(0);
				}
				

			}
			
		}
	}, [auth.info, props.datadriver]);

	const afficheEtoile = () => {

		//console.log("valeur de note:",note);
	   if (note ===1){
		   return (
			   <>
			   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
			   <AntDesign name="star" size={16} color={colors.GREY.primary} />
			   <AntDesign name="star" size={16} color={colors.GREY.primary} />
			   <AntDesign name="star" size={16} color={colors.GREY.primary} />
			   <AntDesign name="star" size={16} color={colors.GREY.primary} />
			   </>
		   );
	   }else if (note ===2){
			
		   return (
			  <>
			   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
			   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
			   <AntDesign name="star" size={16} color={colors.GREY.primary} />
			   <AntDesign name="star" size={16} color={colors.GREY.primary} />
			   <AntDesign name="star" size={16} color={colors.GREY.primary} />
			   </>
		   );
	   }else if (note ===3){
		   return (
		   <>
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.GREY.primary} />
		   <AntDesign name="star" size={16} color={colors.GREY.primary} />
		   </>);

	   }else if (note ===4){
		   return (
		   <>
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.GREY.primary} />
		   </>);

	   }else if (note ===5){
		   return (
		   <>
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   <AntDesign name="star" size={16} color={colors.YELLOW.primary} />
		   </>);
	   }else{
		   
		   return (
		   <>
		   <AntDesign name="star" size={16} color={colors.GREY.primary} />
		   <AntDesign name="star" size={16} color={colors.GREY.primary} />
		   <AntDesign name="star" size={16} color={colors.GREY.primary} />
		   <AntDesign name="star" size={16} color={colors.GREY.primary} />
		   <AntDesign name="star" size={16} color={colors.GREY.primary} />
		   </>);
	   }
   }


	const annulation = () => {

		EventRegister.emit('initrider',true);
		
		dispatch(
			updateTravel({
				propose_negoPrice: affinerPrix,
				propose_negoPrice_rider: affinerPrix,
				isAffiner_rider: false,
				driver_id: props?.datadriver?.driver_id,
				isFound: false,
			})
		);
	};

	return (
		<View style={styles.container}>
			<View style={styles.modal}>
				<View style={styles.bodyTop}>
					<View style={styles.userImage}>
						<Image
							source={
								props.datadriver && props?.datadriver?.driver_profile_img
									? {uri: props?.datadriver?.driver_profile_img}
									: user
							}
							style={{width: "100%", height: "100%", borderRadius: 50}}
						/>
					</View>

					<View style={styles.userInfo}>
						<Text style={styles.userName}>
							{props?.datadriver?.driver_firstName} {props?.datadriver?.driver_lastName}
						</Text>
						<View style={styles.starIcon}>
					    	{
							afficheEtoile()
							}
						</View>
						<SvgXml xml={car} style={{width: 27, height: 11}} />
					</View>
					<View style={styles.stopPhoneIcon}>
						<TouchableOpacity
							style={styles.iconMargin}
							onPress={() =>
								Linking.openURL(`sms: ${props?.datadriver?.driver_mobile}`)
							}>
							<SvgXml xml={email} style={{width: 50, height: 50}} />
						</TouchableOpacity>
						<TouchableOpacity
							style={styles.iconMargin}
							onPress={() => {
								callNumber(props?.datadriver?.driver_mobile);
							}}>
							<SvgXml xml={tel} style={{width: 50, height: 50}} />
						</TouchableOpacity>
					</View>
				</View>
				<View style={styles.bodyBottom}>
					<View style={styles.bodyBottomOne}>
						<View style={styles.element}>
							<SvgXml xml={clock} style={{width: 50, height: 50}} />
							<View style={styles.textContainer}>
								<Text style={styles.elementHeaderText}>temps trajet</Text>
								<Text style={styles.elementBodyText}>
									{props.datarider.durer} min
								</Text>
							</View>
						</View>
						<View style={styles.element}>
							<SvgXml xml={position} style={{width: 50, height: 50}} />
							<View style={styles.textContainer}>
								<Text style={styles.elementHeaderText}>distance</Text>
								<Text style={styles.elementBodyText}>
									{props.datarider.distance} Km
								</Text>
							</View>
						</View>
					</View>
					<View style={styles.bodyBottomTwo}>
						<View style={styles.element}>
							<View
								style={{
									width: 25,
									height: 25,
									borderRadius: 20,
									alignItems: "center",
									justifyContent: "center",
								}}>
								<SvgXml xml={euro} style={{width: 50, height: 50}} />
							</View>
							<View style={styles.textContainer}>
								<Text style={styles.elementHeaderText}>tarif validé</Text>
								<Text style={styles.elementBodyTextViolet}>
									{props.datarider.prix_affiner} €
								</Text>
							</View>
						</View>
						<View style={{height: 37, flexDirection: "column", alignItems: "center"}}>
							<Text
								style={{
									marginBottom: 7,
									fontFamily: "Montserrat-Medium",
									fontSize: 12,
									lineHeight: 15,
								}}>
								AFFINER LE TARIF ?
							</Text>
							<InputSpinner
								max={1000}
								min={1}
								step={0.5}
								skin={"square"}
								buttonLeftDisabled ={true}
								showBorder={true}
								colorLeft={"transparent"}
								colorRight={"transparent"}
								width={120}
								height={37}
								editable={false}
								shadow={false}
								color={colors.GREYD}
								type={"real"}
								inputStyle={{
									borderWidth: 0.3,
									borderBottomWidth: 0,
									borderTopWidth: 0,
									borderColor: colors.GREYD,
								}}
								buttonTextColor={colors.GREYD}
								value={prixTotal}
								onChange={(value) => setAffinerPrix(value)}
								style={{
									backgroundColor: "#fff",

									elevation: 2,
									shadowOpacity: 0.3,
									shadowRadius: 1,
									shadowOffset: {
										height: 0,
										width: 0,
									},
								}}
							/>
						</View>
					</View>
					<View
						style={{
							flexDirection: "row",
							alignItems: "center",
							marginTop: 21,
							justifyContent: "space-around",
						}}>
						<Button
							title="annuler"
							buttonStyle={styles.buttonStyle}
							titleStyle={styles.btnTitleStyle}
							onPress={() => {
								
								props.setDisplayTravelConfirm(false);
								annulation();
							}}
						/>
						<Button
							title="valider"
							buttonStyle={styles.buttonStyleViolet}
							titleStyle={styles.btnTitleStyleViolet}
							icon={
								<MaterialCommunityIcons
									name="chevron-double-right"
									size={24}
									color="white"
								/>
							}
							iconPosition="right"
							onPress={() => {
								onPressValider();
								props.setDisplayTravelConfirm(false);
								props.setDisplayWaitRiderNegoPrice(true);
							}}
						/>
					</View>
				</View>
			</View>
		</View>
	);
};

export default NegoPrixDriver;
