import React, { useContext, useEffect, useState } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Linking,
  StyleSheet,
} from "react-native";
import { colors } from "../../common/theme";
import Modal from "react-native-modal";
import { SvgXml } from "react-native-svg";
import tel from "../../../assets/svg/tel";
import email from "../../../assets/svg/email";
import riderImg from "../../../assets/images/avatar_login.png";
import CancelReasonModal from "../CancelReasonModal";
import { useDispatch, useSelector } from "react-redux";
import { FirebaseContext } from "../../../redux/src";
import { callNumber } from "./../phoneTrigger/index";

const DriverGoingToRider = (props) => {
  const {
    closeDriverGoingToRider,
    openRouteConfirm,
    openConfirmPickUpRider,
    openDriverCancelTravelInProcess,
  } = props;
  const { api } = useContext(FirebaseContext);
  const { addTravel } = api || {};
  const auth = useSelector((state) => state.auth);
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;

  const [isVisibleCancelReason, setIsVisibleCancelReason] = useState(false);

  const dispatch = useDispatch();

  const openCancelReason = () => setIsVisibleCancelReason(true);
  const closeCancelReason = () => setIsVisibleCancelReason(false);

  const onSelectedCancelReason = () => {
    closeCancelReason();
    closeDriverGoingToRider();
    openRouteConfirm();
  };

  useEffect(() => {
    if (auth?.info?.profile) {
      if (
        !travelInformations?.isAffiner_rider &&
        !travelInformations?.isFound
      ) {
        closeDriverGoingToRider();
        openDriverCancelTravelInProcess();
      }
    }
  }, [
    auth?.info?.profile,
    travelInformations?.isFound,
    travelInformations?.isAffiner_rider,
  ]);

  const onReachRider = () => {
    if (travelInformations) {
      travelInformations.arriver_vers_passager = true;
      travelInformations.arriver_vers_passager_at = new Date().getTime();
      dispatch(addTravel(travelInformations));
    }

    closeDriverGoingToRider();
    openConfirmPickUpRider();
  };

  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-around",
          }}
        >
          <View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: 10,
              }}
            >
              <Image
                source={
                  travelInformations &&
                  travelInformations != null &&
                  travelInformations.propose_profile_img_rider &&
                  travelInformations.propose_profile_img_rider != "non"
                    ? { uri: travelInformations.propose_profile_img_rider }
                    : riderImg
                }
                style={{ width: 40, height: 40, borderRadius: 25 }}
              />
              <View style={{ flexDirection: "column" }}>
                <Text
                  style={{
                    color: "#969696",
                    fontSize: 14,
                    lineHeight: 18,
                    textAlign: "left",
                    marginLeft: 15,
                    fontFamily: "Montserrat-Medium",
                  }}
                >
                  En route vers...
                </Text>
                <Text
                  style={{
                    color: "black",
                    fontSize: 14,
                    lineHeight: 18,
                    textAlign: "left",
                    marginLeft: 15,
                    fontFamily: "Montserrat-Bold",
                  }}
                >
                  {travelInformations && travelInformations != null
                    ? travelInformations.propose_firstname_rider +
                      " " +
                      travelInformations.propose_lastname_rider
                    : "Martin LEMOINE"}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: "row",
                height: 58,
                alignItems: "center",
              }}
            >
              <TouchableOpacity
                onPress={openCancelReason}
                style={{
                  width: 117,
                  height: 36,
                  borderRadius: 11,
                  borderWidth: 1,
                  borderColor: "black",
                  justifyContent: "center",
                  marginRight: 5,
                }}
              >
                <Text
                  style={{
                    fontFamily: "Montserrat-Bold",
                    color: "black",
                    lineHeight: 18,
                    textAlign: "center",
                  }}
                >
                  ANNULER
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={onReachRider}
                style={{
                  width: 117,
                  height: 36,
                  borderRadius: 11,
                  backgroundColor: "#5A48CB",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    fontFamily: "Montserrat-Bold",
                    color: "white",
                    lineHeight: 18,
                    textAlign: "center",
                  }}
                >
                  ARRIVÉ
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flexDirection: "column",
              marginVertical: -2,
              height: "100%",
              justifyContent: "space-around",
            }}
          >
            <TouchableOpacity
              onPress={() =>
                travelInformations && travelInformations != null
                  ? callNumber(travelInformations.propose_phone_rider)
                  : console.log(" calling...")
              }
            >
              <SvgXml xml={tel} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                travelInformations && travelInformations != null
                  ? Linking.openURL(
                      `sms: ${travelInformations.propose_phone_rider}`
                    )
                  : console.log(" sending sms...")
              }
            >
              <SvgXml xml={email} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <Modal
        isVisible={isVisibleCancelReason}
        animationType="slide"
        onBackButtonPress={closeCancelReason}
        transparent={true}
        style={{ margin: 0, backgroundColor: "pink" }}
      >
        <CancelReasonModal
          closeCancelReason={closeCancelReason}
          onSelectedCancelReason={onSelectedCancelReason}
        />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    marginBottom: 10,
    position: "absolute",
    bottom: 50,
    padding: 10,
  },
  modal: {
    backgroundColor: "white",
    height: "100%",
    width: "100%",
    marginHorizontal: 15,
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
    alignSelf: "center",
    padding: 10,
  },
  text: {
    marginTop: 10,
    fontSize: 15,
    lineHeight: 20,
    fontFamily: "Montserrat-Bold",
    color: "#5a48cb",
  },
  secondText: {
    fontSize: 14,
    fontFamily: "Montserrat-Bold",
    color: "#9E9E9E",
    lineHeight: 20,
  },

  btnTitleStyle: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 18,
    color: "white",
    fontFamily: "Montserrat-Bold",
  },
  buttonStyle: {
    marginTop: 10,
    backgroundColor: "#5a48cb",
    borderRadius: 12,
    alignSelf: "center",
  },
  bodyTop: {
    height: "30%",
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  userImage: {
    width: 47,
    height: 47,
    marginRight: 10,
    borderRadius: 12,
  },
  userName: {
    fontWeight: "bold",
  },
  starIcon: {
    flexDirection: "row",
    marginVertical: 5,
  },
  stopPhoneIcon: {
    flexDirection: "row",
    marginLeft: "auto",
    marginRight: 10,
  },
  iconMargin: {
    marginTop: 10,
    marginRight: 10,
  },
  element: {
    flexDirection: "row",
  },
  bodyBottom: {
    backgroundColor: "white",
    height: "70%",
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  textContainer: {
    marginLeft: 5,
  },
  elementHeaderText: {
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  elementBodyText: {
    color: colors.GREYD,
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    lineHeight: 18,
  },
  elementBodyTextViolet: {
    fontWeight: "bold",
    fontSize: 20,
    lineHeight: 24,
    fontFamily: "Montserrat-Bold",
    color: "#FFB406",
  },
  bodyBottomOne: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bodyBottomTwo: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: "10%",
    marginBottom: 10,
  },
});

export default DriverGoingToRider;
