import React from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import { SvgXml } from "react-native-svg";
import confirm from "../../../assets/svg/confirm";
import juste from "../../../assets/svg/juste";
import { colors } from "../../common/theme";
import { useSelector } from "react-redux";
import { wazeRouting } from "../../functions/WazeFunctions";

const TravelValidatedByDriver = (props) => {
  const { onValidatedTravel } = props;

  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;

  const openWazeRouting = () => {
    if (travelInformations) {
      const longitude = travelInformations?.rider_long_dep;
      const latitude = travelInformations?.rider_lat_dep;
      wazeRouting({
        longitude,
        latitude,
      });
    }
  };

  const nextGoingToRider = () => {
    onValidatedTravel();
  };

  const onRouting = () => openWazeRouting();

  return (
    <View style={styles.container}>
      <View style={styles.headerImage}>
        <SvgXml xml={confirm} />
        <SvgXml xml={juste} style={{ position: "absolute", top: 15 }} />
      </View>
      <Text style={styles.headerText}> course acceptée </Text>
      <View style={styles.body}>
        <Text style={styles.bodyText}>
          {" "}
          Vous pouvez lancer la navigation et aller{" "}
        </Text>
        <Text style={styles.bodyText}> récupérer votre voyageur. </Text>
      </View>
      <TouchableOpacity style={styles.footerButton} onPress={onRouting}>
        <Text style={styles.buttonText}> naviguer </Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.footerButton} onPress={nextGoingToRider}>
        <Text style={styles.buttonText}> Ok! </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  blurBackground: {
    flex: 1,
    borderWidth: 2,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(248,248,248,0.2)",
  },
  container: {
    alignItems: "center",
    justifyContent: "space-evenly",
    alignSelf: "center",
    backgroundColor: "white",
    height: "35%",
    width: "90%",
    borderRadius: 11,
    shadowColor: "rgba(146,146,146,0.5)",
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowRadius: 8,
    shadowOpacity: 0.5,
    elevation: 3,
  },
  headerImage: {
    alignItems: "center",
    justifyContent: "flex-start",
  },
  headerText: {
    textTransform: "uppercase",
    textAlign: "center",
    color: colors.VIOLET,
    fontSize: 15,
    lineHeight: 18,
    fontFamily: "Montserrat-Bold",
  },
  body: {
    justifyContent: "flex-start",
    alignItems: "center",
  },
  bodyText: {
    textAlign: "center",
    color: "#898989",
    fontSize: 15,
    lineHeight: 18,
    fontFamily: "Montserrat-Medium",
  },
  footerButton: {
    width: "40%",
    height: 40,
    backgroundColor: colors.VIOLET,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 11,
  },
  buttonText: {
    textTransform: "uppercase",
    textAlign: "center",
    color: "white",
    fontSize: 14,
    lineHeight: 17,
    fontFamily: "Montserrat-Bold",
  },
});

export default TravelValidatedByDriver;
