import React, {useState} from "react";
import {View, StyleSheet, Text} from "react-native";
import {Button} from "react-native-elements";
import {colors} from "../../common/theme";
import {SvgXml} from "react-native-svg";
import src_dest from "../../../assets/svg/src_dest";

const ValidationTravelModal = (props) => {
	return (
		<View style={styles.screen}>
			<SvgXml xml={src_dest} />
			<View style={styles.textContainer}>
				<Text style={styles.textTitle}> Êtes-vous sûr(e) de vouloir </Text>
				<Text style={styles.textTitle}> accepter cette course ? </Text>
			</View>
			<View style={styles.validateButtonWrapper}>
				<View style={styles.btnContainer}>
					<Button
						title={"oui"}
						buttonStyle={styles.uploadBtnStyle}
						titleStyle={styles.btnTitleStyle}
						onPress={props.validateTravel}
					/>
				</View>
			</View>
			<View style={styles.cancelBtnWrapper}>
				<View style={styles.btnContainer}>
					<Button
						title={"annuler"}
						buttonStyle={styles.cancelBtnStyle}
						titleStyle={styles.btnTitleStyle}
						onPress={props.onClose}
					/>
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		backgroundColor: "black",
		opacity: 0.83,
		alignItems: "center",
		justifyContent: "center",
	},
	textContainer: {
		marginBottom: 50,
		marginTop: 25,
	},
	validateButtonWrapper: {
		width: "100%",
		alignItems: "center",
	},
	cancelBtnWrapper: {
		marginTop: 10,
		width: "100%",
		alignItems: "center",
	},
	btnContainer: {
		width: "60%",
	},
	uploadBtnStyle: {
		textAlign: "center",
		borderRadius: 13,
		backgroundColor: colors.VIOLET,
		paddingVertical: 10,
		paddingHorizontal: 20,
	},
	cancelBtnStyle: {
		textAlign: "center",
		borderRadius: 13,
		backgroundColor: "transparent",
		paddingVertical: 10,
		paddingHorizontal: 20,
		borderColor: "white",
		borderWidth: 1,
	},
	btnTitleStyle: {
		textTransform: "uppercase",
		fontFamily: "Montserrat-Bold",
		fontSize: 12,
		lineHeight: 15,
		color: "white",
	},
	textTitle: {
		textTransform: "uppercase",
		fontFamily: "Montserrat-Bold",
		fontSize: 18,
		lineHeight: 22,
		textAlign: "center",
		color: "white",
	},
});

export default ValidationTravelModal;
