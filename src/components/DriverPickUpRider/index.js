import React, { useContext, useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Linking,
  StyleSheet,
} from "react-native";
import { Button } from "react-native-elements";
import { colors } from "../../common/theme";
import user from "../../../assets/images/avatar_login.png";
import { callNumber } from "../phoneTrigger/index";
import email from "../../../assets/svg/email";
import tel from "../../../assets/svg/tel";

import { FirebaseContext } from "../../../redux/src";
import { useSelector, useDispatch } from "react-redux";

import { SvgXml } from "react-native-svg";
import car from "../../../assets/svg/bigCar";
// Functions
import { getData } from "../../functions/AsyncStorageFunctions";

// import { AdMobBanner, setTestDeviceIDAsync } from "expo-ads-admob";

const DriverPickUpRider = (props) => {
  const { api } = useContext(FirebaseContext);
  const {
    fetchOneTravel,
    fetchDriverVehicules,
    updateSelectedVehicule,
    getStripeAccountId,
    chooseCreditCard,
  } = api;
  const auth = useSelector((state) => state.auth);
  const travelData = useSelector((state) => state.traveldata);
  const mesvehiculesRedux = useSelector((state) => state.mesvehicules);
  const creditcarddata = useSelector((state) => state.creditcarddata);
  const { cardToUse, creditcards } = creditcarddata;

  const { driverVehicules = [], selectedVehicule } = mesvehiculesRedux || {};

  const donner = travelData.travel;
  const dispatch = useDispatch();

  const [isReadyToEnRoute, setIsReadyToEnRoute] = useState(false);

  // React.useEffect(() => {
  //   setTestDeviceIDAsync("EMULATOR");
  // }, []);
  useEffect(() => {
    if (auth.info && auth.info.profile) {
      if (donner != null) {
        const interval = setInterval(() => {
          dispatch(fetchOneTravel(props?.datadriver?.driver_id));
        }, 8000);

        return () => {
          clearInterval(interval);
        };
      }
    }
  }, [auth.info]);

  useEffect(() => {
    if (donner?.car_id && driverVehicules?.length) {
      const selectedCar = driverVehicules.find(
        (item) => item.carKey === donner?.car_id
      );
      selectedCar && dispatch(updateSelectedVehicule(selectedCar));
    }
  }, [donner, driverVehicules]);

  useEffect(() => {
    if (props.datadriver?.driver_id) {
      dispatch(fetchDriverVehicules(props.datadriver?.driver_id));
      dispatch(getStripeAccountId(props.datadriver?.driver_id));
    }
  }, [props.datadriver?.driver_id]);

  useEffect(() => {
    if (auth.info && auth.info.profile) {
      if (
        donner != null &&
        donner.isFound == false &&
        donner.status == "init"
      ) {
        //	setShowCancel(true);
        props.setDisplayDriverCancelTravel(true);
        props.setDisplayDriverPURider(false);
      }
    }
  }, [auth.info, donner]);

  const getCardToUse = async () => {
    const cardId = await getData("cardToUse");
    if (cardId) {
      const currentCardToUse = creditcards.find(
        (item) => item.cardid === cardId
      );
      dispatch(chooseCreditCard(currentCardToUse));
    }
  };

  useEffect(() => {
    if (selectedVehicule) {
      if (donner?.mode_paiement !== "cartecredit") {
        return setIsReadyToEnRoute(true);
      }
      if (cardToUse) {
        return setIsReadyToEnRoute(true);
      }

      getCardToUse();
    }
  }, [selectedVehicule, cardToUse]);

  /*if (showCancel){

    return <DriverCancelTravel setDisplayWaitRiderNegoPrice={setDisplayWaitRiderNegoPrice} datadriver={donner} />;
  }*/

  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <View
          style={{
            alignItems: "center",
            marginTop: 24,
            marginBottom: 5,
          }}
        >
          <SvgXml xml={car} />
        </View>
        <View style={{ alignSelf: "center" }}>
          <Text style={[styles.secondText, { marginTop: 10 }]}>
            Votre conducteur est arrivé
          </Text>
          <Text style={[styles.secondText, { textAlign: "center" }]}>
            et vous attend !
          </Text>
        </View>
        <View
          style={{
            width: "95%",
            height: 0.5,
            marginVertical: 10,
            backgroundColor: "#DEDDDD",
            alignSelf: "center",
          }}
        ></View>

        <View style={styles.bodyTop}>
          <View style={styles.userImage}>
            <Image
              source={
                props.datadriver && props?.datadriver?.driver_profile_img
                  ? { uri: props?.datadriver?.driver_profile_img }
                  : user
              }
              style={{ width: 47, height: 47, borderRadius: 50 }}
            />
          </View>

          <View style={{ justifyContent: "center" }}>
            <Text style={styles.userName}>
              {props?.datadriver?.driver_firstName}{" "}
              {props?.datadriver?.driver_lastName}
            </Text>
          </View>
          <View style={styles.stopPhoneIcon}>
            <TouchableOpacity
              style={styles.iconMargin}
              onPress={() =>
                Linking.openURL(`sms: ${props?.datadriver?.driver_mobile}`)
              }
            >
              <SvgXml xml={email} style={{ width: 50, height: 50 }} />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.iconMargin}
              onPress={() => {
                callNumber(props?.datadriver?.driver_mobile);
              }}
            >
              <SvgXml xml={tel} style={{ width: 50, height: 50 }} />
            </TouchableOpacity>
          </View>
        </View>
        <Button
          title="EN ROUTE !"
          buttonStyle={styles.buttonStyle}
          titleStyle={styles.btnTitleStyle}
          onPress={props.validateModal}
          loading={!isReadyToEnRoute}
          disabled={!isReadyToEnRoute}
        />
      </View>
      {/* 
      <AdMobBanner
        style={styles.bottomBanner}
        bannerSize="smartBanner"
        adUnitID="ca-app-pub-5608997176551587/4926705154"
        servePersonalizedAds // true or false
        onDidFailToReceiveAdWithError={(e) => console.log(e)}
      /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    width: "100%",
  },
  bottomBanner: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
  },
  modal: {
    backgroundColor: "white",
    height: 265,
    width: 350,
    marginHorizontal: 15,
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
    alignSelf: "center",
    //justifyContent: 'center',
  },
  text: {
    marginTop: 10,
    fontSize: 15,
    lineHeight: 20,
    fontFamily: "Montserrat-Bold",
    color: "#5a48cb",
  },
  secondText: {
    fontSize: 14,
    fontFamily: "Montserrat-Bold",
    color: "#9E9E9E",
    lineHeight: 20,
  },

  btnTitleStyle: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 18,
    color: "white",
    fontFamily: "Montserrat-Bold",
  },
  buttonStyle: {
    marginTop: 10,
    backgroundColor: "#5a48cb",
    borderRadius: 12,
    alignSelf: "center",
    width: "50%",
  },
  bodyTop: {
    height: "30%",
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  userImage: {
    width: 47,
    height: 47,
    marginRight: 10,
    borderRadius: 12,
  },
  userName: {
    fontWeight: "bold",
  },
  starIcon: {
    flexDirection: "row",
    marginVertical: 5,
  },
  stopPhoneIcon: {
    flexDirection: "row",
    marginLeft: "auto",
  },
  iconMargin: {
    marginTop: 10,
    marginRight: 10,
  },
  element: {
    flexDirection: "row",
  },
  bodyBottom: {
    backgroundColor: "white",
    height: "70%",
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  textContainer: {
    marginLeft: 5,
  },
  elementHeaderText: {
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  elementBodyText: {
    color: colors.GREYD,
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    lineHeight: 18,
  },
  elementBodyTextViolet: {
    fontWeight: "bold",
    fontSize: 20,
    lineHeight: 24,
    fontFamily: "Montserrat-Bold",
    color: "#FFB406",
  },
  bodyBottomOne: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bodyBottomTwo: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: "10%",
    marginBottom: 10,
  },
});

export default DriverPickUpRider;
