import React, { useEffect, useState, useRef, useContext } from "react";
import { View, StyleSheet, Text } from "react-native";
import { Button, Image } from "react-native-elements";
import { colors } from "../../common/theme";
import { SvgXml } from "react-native-svg";
import cancel from "../../../assets/svg/cancel";
import profileImg from "../../../assets/images/cancel_profile_img.png";
import { FirebaseContext } from "../../../redux/src";
import { useSelector, useDispatch } from "react-redux";
import { EventRegister } from "react-native-event-listeners";

function DriverCancelTravel(props) {
  const [isClose, setIsClose] = useState(true);
  const { api } = useContext(FirebaseContext);
  const { fetchOneTravel, updateTravel } = api;
  const auth = useSelector((state) => state.auth);
  const travelData = useSelector((state) => state.traveldata);
  const donner = travelData.travel;
  const dispatch = useDispatch();

  useEffect(() => {
    if (auth?.info?.profile && props?.datadriver?.driver_id) {
      dispatch(fetchOneTravel(props?.datadriver?.driver_id));
    }
  }, [auth?.info?.profile, props?.datadriver?.driver_id]);

  const annulation = () => {
    EventRegister.emit("initrider", true);
    props.setDisplayDriverCancelTravel(false);
    if (props.datadriver && props?.datadriver?.driver_id) {
      dispatch(
        updateTravel({
          isAffiner_rider: false,
          driver_id: props?.datadriver?.driver_id,
          isFound: false,
        })
      );
    }
    setIsClose(true);
  };

  return isClose ? (
    <View style={styles.container}>
      <View style={styles.modalView}>
        <View style={styles.modalHeader}>
          <View style={{ flexDirection: "row", position: "absolute" }}>
            <Image
              source={
                donner && donner.driver_profile_img
                  ? { uri: donner.driver_profile_img }
                  : profileImg
              }
              style={{ width: 47, height: 47, borderRadius: 50 }}
            />
          </View>
          <SvgXml xml={cancel} style={{ width: 47, height: 47 }} />
        </View>
        <Text style={[styles.modalTitleText, { marginTop: 15 }]}>
          {props?.datadriver?.driver_firstName}{" "}
          {props?.datadriver?.driver_lastName}
        </Text>

        <Text
          style={[
            styles.modalTitleText,
            { marginBottom: 15, fontFamily: "Montserrat-Medium" },
          ]}
        >
          vient d'annuler son trajet
        </Text>

        <Button
          title="ok"
          titleStyle={styles.modalFooterButtonTitle}
          buttonStyle={styles.modalFooterButton}
          containerStyle={styles.modalFooterButtonContainer}
          onPress={() => {
            annulation();
          }}
        />
      </View>
    </View>
  ) : null;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  modalView: {
    backgroundColor: "#FFFFFF",
    borderTopLeftRadius: 13,
    borderTopRightRadius: 13,
    paddingTop: 15,
    height: "33%",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    shadowOpacity: 0.8,
    elevation: 3,
  },
  modalHeader: {
    flexDirection: "row-reverse",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginLeft: 20,
  },
  modalDivider: {
    borderBottomColor: colors.BLACK,
    borderBottomWidth: 1,
    borderColor: "#E1DEDE",
    borderStyle: "solid",
    height: 10,
    marginBottom: 10,
    width: "90%",
  },
  modalTitleText: {
    alignSelf: "center",
    color: "#9A9A9A",
    fontFamily: "Montserrat-Bold",
    textAlign: "center",
    fontSize: 14,
    lineHeight: 18,
  },
  modalContentContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  modalFooterButtonTitle: {
    color: colors.WHITE,
    fontSize: 16,
    fontFamily: "Montserrat-Bold",
    textTransform: "uppercase",
    textAlign: "center",
  },
  modalFooterButtonContainer: {
    borderRadius: 13,
    width: "50%",
  },
  modalFooterButton: {
    borderRadius: 13,
    backgroundColor: colors.VIOLET,
  },
});

export default DriverCancelTravel;
