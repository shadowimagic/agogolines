import React, {
	useState,
	useEffect,
	useRef,
	useContext,
	createRef,
} from 'react';
import {
	View,
	Text,
	Dimensions,
	TouchableOpacity,
	ScrollView,
	KeyboardAvoidingView,
	TouchableWithoutFeedback,
	Image,
	ActivityIndicator,
	Platform,
	Alert,
	SafeAreaView
} from 'react-native';
import Background from '../Background';
import { Icon, Button, Header, Input } from 'react-native-elements';
import ActionSheet from 'react-native-actions-sheet';
import { colors } from '../../common/theme';
var { height } = Dimensions.get('window');
import {
	language,
	countries,
	default_country_code,
	features,
} from '../../../config';
import RadioForm from 'react-native-simple-radio-button';
import RNPickerSelect from 'react-native-picker-select';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import { CustomStyle } from '../../common/CustomStyle';
import InputSpinner from 'react-native-input-spinner';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

export default function Registration(props) {
	const [state, setState] = useState({
		usertype: 'rider',
		firstName: '',
		lastName: '',
		profile_image: null,
		email: '',
		mobile: '',
		referralId: '',
		vehicleNumber: '',
		cniRectoImage: null,
		cniVersoImage: null,
		carteGriseImage: null,
		licenseImage: null,
		numero: '',
		vehicleMake: '',
		vehicleModel: '',
		annee: '',
		couleur: '',
		vehicleNumber: '',
		nombreplace: '',
		countryCode: '+' + default_country_code.phone,
		carType: props.cars && props.cars.length > 0 ? props.cars[0].value : '',
		bankAccount: '',
		bankCode: '',
		bankName: '',
		verificationCode: '',
		verificationId: '',
		other_info: '',
		password: '',
	});
	const [role, setRole] = useState(0);
	const [capturedImage, setCapturedImage] = useState(null);
	const [confirmpassword, setConfirmPassword] = useState('');
	const [countryCode, setCountryCode] = useState(
		'+' + default_country_code.phone
	);
	const [mobileWithoutCountry, setMobileWithoutCountry] = useState('');
	const [loader, setLoader] = useState(false);
	const actionSheetRef = useRef(null);

	const radio_props = [
		{ label: language.no, value: 0 },
		{ label: language.yes, value: 1 },
	];

	const formatCountries = () => {
		let arr = [];
		for (let i = 0; i < countries.length; i++) {
			arr.push({
				label: countries[i].label + ' (+' + countries[i].phone + ')',
				value: '+' + countries[i].phone,
				key: countries[i].code,
			});
		}
		return arr;
	};

	showActionSheet = () => {
		actionSheetRef.current?.setModalVisible(true);
	};

	uploadImage = () => {
		return (
			<ActionSheet ref={actionSheetRef}>
				<TouchableOpacity
					style={{
						width: '90%',
						alignSelf: 'center',
						paddingLeft: 20,
						paddingRight: 20,
						borderColor: colors.GREY.iconPrimary,
						borderBottomWidth: 1,
						height: 60,
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						_pickImage(ImagePicker.launchCameraAsync);
					}}
				>
					<Text
						style={{
							color: colors.BLUE.greenish_blue,
							fontWeight: 'bold',
						}}
					>
						Camera
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					style={{
						width: '90%',
						alignSelf: 'center',
						paddingLeft: 20,
						paddingRight: 20,
						borderBottomWidth: 1,
						borderColor: colors.GREY.iconPrimary,
						height: 60,
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						_pickImage(ImagePicker.launchImageLibraryAsync);
					}}
				>
					<Text
						style={{
							color: colors.BLUE.greenish_blue,
							fontWeight: 'bold',
						}}
					>
						Media Library
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					style={{
						width: '90%',
						alignSelf: 'center',
						paddingLeft: 20,
						paddingRight: 20,
						height: 50,
						alignItems: 'center',
						justifyContent: 'center',
					}}
					onPress={() => {
						actionSheetRef.current?.setModalVisible(false);
					}}
				>
					<Text style={{ color: 'red', fontWeight: 'bold' }}>
						Cancel
					</Text>
				</TouchableOpacity>
			</ActionSheet>
		);
	};

	_pickImage = async (res) => {
		var pickFrom = res;
		const { status } = await Permissions.askAsync(
			Permissions.CAMERA,
			Permissions.MEDIA_LIBRARY
		);

		if (status == 'granted') {
			setLoader(true);
			let result = await pickFrom({
				allowsEditing: true,
				aspect: [3, 3],
				base64: true,
			});
			actionSheetRef.current?.setModalVisible(false);
			if (!result.cancelled) {
				let data = 'data:image/jpeg;base64,' + result.base64;
				setCapturedImage(result.uri);
				const blob = await new Promise((resolve, reject) => {
					const xhr = new XMLHttpRequest();
					xhr.onload = function () {
						resolve(xhr.response);
					};
					xhr.onerror = function () {
						Alert.alert(
							language.alert,
							language.image_upload_error
						);
						setLoader(false);
					};
					xhr.responseType = 'blob';
					xhr.open(
						'GET',
						Platform.OS == 'ios' ? data : result.uri,
						true
					);
					xhr.send(null);
				});
				if (blob) {
					setState({ ...state, profile_image: blob });
				}
				setLoader(false);
			} else {
				setLoader(false);
			}
		}
	};

	//upload cancel
	cancelPhoto = () => {
		setCapturedImage(null);
	};

	const setUserType = (value) => {
		if (value == 0) {
			setState({ ...state, usertype: 'rider' });
		} else {
			setState({ ...state, usertype: 'driver' });
		}
	};

	validateMobile = () => {
		let mobileValid = true;
		if (mobileWithoutCountry.length < 6) {
			mobileValid = false;
			Alert.alert(language.alert, language.mobile_no_blank_error);
		}
		return mobileValid;
	};

	validatePassword = (complexity) => {
		let passwordValid = true;
		const regx1 = /^([a-zA-Z0-9@*#]{8,15})$/;
		const regx2 =
			/(?=^.{6,10}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$/;
		if (complexity == 'any') {
			passwordValid = state.password.length >= 1;
			if (!passwordValid) {
				Alert.alert(language.alert, language.password_blank_messege);
			}
		} else if (complexity == 'alphanumeric') {
			passwordValid = regx1.test(state.password);
			if (!passwordValid) {
				Alert.alert(
					language.alert,
					language.password_alphaNumeric_check
				);
			}
		} else if (complexity == 'complex') {
			passwordValid = regx2.test(password);
			if (!passwordValid) {
				Alert.alert(language.alert, language.password_complexity_check);
			}
		} else if (state.password != confirmpassword) {
			passwordValid = false;
			if (!passwordValid) {
				Alert.alert(
					language.alert,
					language.confirm_password_not_match_err
				);
			}
		}
		return passwordValid;
	};

	/*const onPressRegisterRideNext = async () => {
  
        const { onPressNextRide } = props;

        onPressNextRide(state);
    }*/

	//register button press for validation
	onPressRegister = () => {
		const { onPressRegister } = props;

		const re =
			/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (re.test(state.email)) {
			if (state.usertype == 'driver' || state.usertype == 'rider') {
				if (state.firstName.length > 0 && state.lastName.length > 0) {
					if (validatePassword('alphanumeric')) {
						if (validateMobile()) {
							onPressRegister(state);
						} else {
							Alert.alert(
								language.alert,
								language.mobile_no_blank_error
							);
						}
					}
				} else {
					Alert.alert(language.alert, language.proper_input_name);
				}
			} else {
				Alert.alert(language.alert, language.proper_input_vehicleno);
			}
		} else {
			Alert.alert(language.alert, language.proper_email);
		}
	};

	return (
		<KeyboardAwareScrollView
      		extraHeight={200}
      		enableOnAndroid
      		scrollEnabled={false}
      		contentContainerStyle={{ height: "100%" }}
    	>
      		<SafeAreaView style={{ flex: 1 }}>
					<Background>
						<Header
							backgroundColor={colors.TRANSPARENT}
							leftComponent={{}}
							containerStyle={styles.headerContainerStyle}
							innerContainerStyles={styles.headerInnerContainer}
						/>
						<ScrollView
							style={styles.scrollViewStyle}
							showsVerticalScrollIndicator={false}
						>
							{uploadImage()}

							<View style={styles.containerStyle}>
								<Text style={styles.headerStyle}>
									{language.registration_title}
								</Text>

								<View style={styles.viewStyle}>
									<View style={styles.imageParentView}>
										<View style={styles.imageViewStyle}>
											{loader ? (
												<View
													style={[
														styles.loadingcontainer,
														styles.horizontal,
													]}
												>
													<ActivityIndicator
														size='large'
														color={colors.BLUE.secondary}
													/>
												</View>
											) : (
												<TouchableOpacity onPress={cancelPhoto}>
													<Image
														source={
															capturedImage
																? { uri: capturedImage }
																: require('../../../assets/images/userprofil.png')
														}
														style={{
															borderRadius: 65 / 2,
															width: 65,
															height: 65,
														}}
													/>
												</TouchableOpacity>
											)}
										</View>
									</View>

									<Button
										icon={
											<Icon
												name='picture'
												size={15}
												color={colors.GREYD}
												type='antdesign'
											/>
										}
										title={' ' + language.ajouter_photo}
										type='outline'
										buttonStyle={styles.button}
										titleStyle={styles.texte}
										onPress={showActionSheet}
									/>
								</View>

								<View style={CustomStyle.textInputContainerStyle}>
									<Input
										editable={true}
										label={language.first_name_placeholder + ' *'}
										value={state.firstName}
										keyboardType={'email-address'}
										inputStyle={CustomStyle.inputTextStyle}
										onChangeText={(text) => {
											setState({ ...state, firstName: text });
										}}
										inputContainerStyle={
											CustomStyle.inputContainerStyle
										}
										labelStyle={CustomStyle.labelInputStyle}
									/>
								</View>

								<View style={CustomStyle.textInputContainerStyle}>
									<Input
										editable={true}
										underlineColorAndroid={colors.TRANSPARENT}
										label={language.last_name_placeholder + ' *'}
										value={state.lastName}
										keyboardType={'email-address'}
										inputStyle={CustomStyle.inputTextStyle}
										onChangeText={(text) => {
											setState({ ...state, lastName: text });
										}}
										inputContainerStyle={
											CustomStyle.inputContainerStyle
										}
										labelStyle={CustomStyle.labelInputStyle}
									/>
								</View>
								<View style={CustomStyle.textInputContainerStyle}>
									<Input
										underlineColorAndroid={colors.TRANSPARENT}
										label={language.email_placeholder + ' *'}
										value={state.email}
										keyboardType={'email-address'}
										inputStyle={CustomStyle.inputTextStyle}
										onChangeText={(text) => {
											setState({ ...state, email: text });
										}}
										inputContainerStyle={
											CustomStyle.inputContainerStyle
										}
										labelStyle={CustomStyle.labelInputStyle}
									/>
								</View>
								<View style={CustomStyle.textInputContainerStyle}>
									<Input
										underlineColorAndroid={colors.TRANSPARENT}
										label={language.password_placeholder + ' *'}
										value={state.password}
										inputStyle={CustomStyle.inputTextStyle}
										onChangeText={(text) =>
											setState({ ...state, password: text })
										}
										inputContainerStyle={
											CustomStyle.inputContainerStyle
										}
										secureTextEntry={true}
										labelStyle={CustomStyle.labelInputStyle}
									/>
								</View>
								<View style={CustomStyle.textInputContainerStyle}>
									<Input
										underlineColorAndroid={colors.TRANSPARENT}
										label={language.confirm_password_placeholder + ' *'}
										value={confirmpassword}
										inputStyle={CustomStyle.inputTextStyle}
										onChangeText={(text) => setConfirmPassword(text)}
										inputContainerStyle={
											CustomStyle.inputContainerStyle
										}
										secureTextEntry={true}
										labelStyle={CustomStyle.labelInputStyle}
									/>
								</View>
								{/*

									<View style={[CustomStyle.textInputContainerStyle,{marginBottom:10}]}>
									
										<RNPickerSelect
											placeholder={{ label: language.select_country, value: language.select_country }}
											value={countryCode}
											useNativeAndroidPickerStyle={true}
											style={{
												inputIOS: styles.pickerStyle,
												inputAndroid: styles.pickerStyle,
											}}
											onValueChange={
												(text) => {
													setCountryCode(text);                                
													let formattedNum = mobileWithoutCountry.replace(/ /g, '');
													formattedNum = text + formattedNum.replace(/-/g, '');
													setState({ ...state, countryCode: formattedNum })
												
												}
											}
											items={formatCountries()}
											disabled={features.AllowCountrySelection ? false : true}
										/>
									</View>

									*/}

								<View style={CustomStyle.phonetextInputContainerStyle}>
									<Input
										leftIcon={
											<Image
												source={require('../../../assets/images/contact.jpg')}
												style={styles.ImageStyle}
											/>
										}
										underlineColorAndroid={colors.TRANSPARENT}
										label={language.mobile_no_placeholder + ' *'}
										labelStyle={CustomStyle.labelInputStyle}
										leftIconContainerStyle={
											CustomStyle.labelInputPhoneStyle
										}
										value={mobileWithoutCountry}
										keyboardType={'number-pad'}
										inputStyle={CustomStyle.inputTextPhoneStyle}
										onChangeText={(text) => {
											setMobileWithoutCountry(text);
											let formattedNum = text.replace(/ /g, '');
											let simplenumber = formattedNum.replace(
												/-/g,
												''
											);
											formattedNum = countryCode + simplenumber;
											setState({
												...state,
												mobile: formattedNum,
												numero: simplenumber,
											});
										}}
										inputContainerStyle={
											CustomStyle.inputContainerStyle
										}
									/>
								</View>
								<View style={CustomStyle.textInputContainerStyle}>
									<Input
										editable={true}
										underlineColorAndroid={colors.TRANSPARENT}
										label={language.code_parrainage}
										value={state.referralId}
										inputStyle={CustomStyle.inputTextStyle}
										onChangeText={(text) => {
											setState({ ...state, referralId: text });
										}}
										inputContainerStyle={
											CustomStyle.inputContainerStyle
										}
										labelStyle={CustomStyle.labelInputStyle}
									/>
								</View>
								<View style={CustomStyle.textInputContainerStyle}>
									<Text
										style={{
											marginLeft: 20,
											marginTop: 0,
											color: colors.BLACK,
											fontWeight: 'bold',
										}}
									>
										{language.register_as_driver}
									</Text>
								</View>
								<View style={CustomStyle.textInputContainerStyle}>
									<RadioForm
										radio_props={radio_props}
										initial={role}
										formHorizontal={true}
										labelHorizontal={true}
										buttonColor={colors.BLACK}
										labelColor={colors.BLACK}
										style={{ marginLeft: 20 }}
										labelStyle={{ marginRight: 20 }}
										selectedButtonColor={colors.BLACK}
										selectedLabelColor={colors.BLACK}
										onPress={(value) => {
											setRole(value);
											setUserType(value);
										}}
									/>
								</View>

								<View style={styles.buttonContainer}>
									<Button
										onPress={onPressRegister}
										title={language.suivant}
										loading={props.loading}
										titleStyle={CustomStyle.styleButtonTitle}
										buttonStyle={CustomStyle.styleButton}
									/>
								</View>

								<View style={styles.textContainerStyle}>
									<Text style={styles.vousNavezPasEnco}>
										{' '}
										{language.deja_compte_create}{' '}
									</Text>
								</View>
								<View style={styles.inscriptionContainerStyle}>
									<Text
										onPress={props.onPressBack}
										style={styles.inscrivezVous}
									>
										{language.connexion_link}
									</Text>
								</View>

								<View style={styles.gapView} />
							</View>
						</ScrollView>
					</Background>
			</SafeAreaView>
		</KeyboardAwareScrollView>
	);
}

const styles = {
	headerContainerStyle: {
		backgroundColor: colors.TRANSPARENT,
		borderBottomWidth: 0,
		marginTop: 0,
	},
	headerInnerContainer: {
		marginLeft: 10,
		marginRight: 10,
	},

	iconContainer: {
		paddingBottom: 20,
	},
	gapView: {
		height: 40,
		width: '100%',
	},
	buttonContainer: {
		justifyContent: 'center',
		marginLeft: 20,
		marginRight: 20,
		paddingLeft: 15,
		paddingRight: 15,
		paddingTop: 10,
	},
	labelInputStyle: {
		color: 'rgba(0,0,0,1)',
		fontSize: 13,
		fontWeight: 'bold',
		fontFamily: 'Montserrat-Bold',
		padding: 5,
	},

	pickerStyle: {
		width: 200,
		fontSize: 15,
		height: 40,
		marginLeft: Platform.OS == 'ios' ? 20 : 10,
		marginTop: Platform.OS == 'ios' ? 0 : 10,
		borderBottomWidth: 1,
		borderBottomColor: colors.WHITE,
		color: colors.GREYD,
	},

	errorMessageStyle: {
		fontSize: 12,
		fontWeight: 'bold',
		marginLeft: 0,
	},
	containerStyle: {
		flexDirection: 'column',
		marginTop: 20,
	},
	form: {
		flex: 1,
	},
	logo: {
		width: '100%',
		justifyContent: 'flex-start',
		marginTop: 10,
		alignItems: 'center',
	},
	scrollViewStyle: {
		height: height,
	},
	headerStyle: {
		fontSize: 25,
		fontWeight: '300',
		fontFamily: 'Montserrat-Bold',
		textAlign: 'center',
		flexDirection: 'row',
		color: colors.VIOLET,
		marginTop: 0,
	},

	textStyle: {
		fontSize: 13,
		flexDirection: 'row',
		color: colors.BLACK,
		marginTop: 0,
	},

	capturePhoto: {
		width: '80%',
		alignSelf: 'center',
		flexDirection: 'column',
		justifyContent: 'center',
		borderRadius: 10,
		backgroundColor: colors.WHITE,
		marginLeft: 20,
		marginRight: 20,
		paddingTop: 15,
		paddingBottom: 10,
		marginTop: 15,
	},
	capturePhotoTitle: {
		color: colors.BLACK,
		fontSize: 14,
		textAlign: 'center',
		paddingBottom: 15,
	},
	errorPhotoTitle: {
		color: colors.RED,
		fontSize: 13,
		textAlign: 'center',
		paddingBottom: 15,
	},
	photoResult: {
		alignSelf: 'center',
		flexDirection: 'column',
		justifyContent: 'center',
		borderRadius: 10,
		marginLeft: 20,
		marginRight: 20,
		paddingTop: 15,
		paddingBottom: 10,
		marginTop: 15,
		width: '80%',
		height: height / 4,
	},
	imagePosition: {
		position: 'relative',
	},
	photoClick: {
		paddingRight: 48,
		position: 'absolute',
		zIndex: 1,
		marginTop: 18,
		alignSelf: 'flex-end',
	},
	capturePicClick: {
		backgroundColor: colors.WHITE,
		flexDirection: 'row',
		position: 'relative',
		zIndex: 1,
	},
	imageStyle: {
		width: 30,
		height: height / 15,
	},
	flexView1: {
		flex: 12,
	},
	imageFixStyle: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	imageStyle2: {
		width: 150,
		height: height / 15,
	},
	myView: {
		flex: 2,
		height: 50,
		width: 1,
		alignItems: 'center',
	},
	myView1: {
		height: height / 18,
		width: 1.5,
		backgroundColor: colors.GREY.btnSecondary,
		alignItems: 'center',
		marginTop: 10,
	},
	myView2: {
		flex: 20,
		alignItems: 'center',
		justifyContent: 'center',
	},
	myView3: {
		flex: 2.2,
		alignItems: 'center',
		justifyContent: 'center',
	},
	textStyle: {
		color: colors.GREY.btnPrimary,
		fontFamily: 'Roboto-Bold',
		fontSize: 13,
	},
	viewStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 13,
	},
	imageParentView: {
		borderRadius: 65 / 2,
		width: 65,
		height: 65,
		backgroundColor: colors.GREY.secondary,
		justifyContent: 'center',
		alignItems: 'center',
	},
	imageViewStyle: {
		borderRadius: 65 / 2,
		width: 65,
		height: 65,
		backgroundColor: colors.WHITE,
		justifyContent: 'center',
		alignItems: 'center',
	},
	loadingcontainer: {
		flex: 1,
		justifyContent: 'center',
	},
	horizontal: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		padding: 10,
	},
	button: {
		margin: 10,
		borderRadius: 21,
		width: 196,
		height: 37,
		justifyContent: 'space-evenly',
		borderWidth: 1,
		backgroundColor: 'transparent',
		borderColor: colors.GREYD,
	},
	texte: {
		backgroundColor: 'transparent',
		color: colors.GREYD,
		fontSize: 12,
		fontWeight: 'bold',
		height: 16,
		width: 152,
		fontFamily: 'Montserrat-Bold',
	},

	ImageStyle: {
		padding: 15,
		margin: 15,
		height: 35,
		width: 40,
		resizeMode: 'stretch',
		alignItems: 'center',
	},
	textContainerStyle: {
		alignItems: 'center',
		textAlign: 'center',
		marginLeft: 20,
		marginRight: 20,
		paddingLeft: 5,
		paddingRight: 15,
		paddingTop: 10,
	},
	inscrivezVous: {
		backgroundColor: 'transparent',
		color: 'rgba(0,0,0,1)',
		fontSize: 14,
		fontFamily: 'Montserrat-Medium',
		textDecorationLine: 'underline',
		paddingLeft: 40,
		paddingRight: 30,
	},
	vousNavezPasEnco: {
		backgroundColor: 'transparent',
		color: 'rgba(90,72,203,1)',
		fontSize: 14,
		fontFamily: 'Montserrat-Medium',
		paddingLeft: 20,
		paddingRight: 20,
	},
	inscriptionContainerStyle: {
		alignItems: 'center',
		textAlign: 'center',
		marginLeft: 20,
		marginRight: 20,
		paddingLeft: 5,
		paddingRight: 15,
		paddingTop: 20,
	},
};
