import { StyleSheet } from "react-native";
import { colors } from "../../common/theme";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  modal: {
    backgroundColor: colors.VIOLET,
    height: "42%",
    width: "80%",
    marginHorizontal: 15,
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
  },
  header: {
    height: "15%",
    alignItems: "center",
    justifyContent: "center",
  },
  headerText: {
    color: "white",
    fontSize: 20,
    fontFamily: "Montserrat-Medium",
    textTransform: "uppercase",
  },
  bodyWrapper: {
    borderRadius: 13,
    backgroundColor: "white",
  },
  bodyTop: {
    height: "80%",
    backgroundColor: "white",
    borderRadius: 13,
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.26,
    shadowRadius: 6,
    elevation: 6,
  },
  elView: {
    flexDirection: "row",
    marginHorizontal: 10,
    marginVertical: 10,
  },
  textWrapper: {
    borderBottomWidth: 1,
    borderColor: colors.GREY.border,
    marginBottom: 5,
  },
  textContainer: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    justifyContent: "space-between",
  },
  elKey: {
    // textTransform: 'uppercase',
    marginBottom: 2,
    fontSize: 12,
    lineHeight: 15,
    color: colors.BLACK,
    fontFamily: "Montserrat-Bold",
    textTransform: "uppercase",
  },
  elValue: {
    marginBottom: 2,
    fontSize: 14,
    lineHeight: 18,
    fontFamily: "Montserrat-Medium",
    color: "#848484",
  },
  btnContainer: {
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "flex-end",
    width: "90%",
    flex: 1,
    // marginTop: 10,
    marginBottom: 10,
    borderTopWidth: 1,
    borderTopColor: "#e1dede",
  },
  btnContainer1: {
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "flex-end",
    width: "90%",
    flex: 1,
    //marginTop: 10,
    //marginBottom: 2,
    //borderTopWidth: 1,
    borderTopColor: "#e1dede",
  },
  btnTitleStyle: {
    textAlign: "center",
    textTransform: "uppercase",
    padding: 8,
    color: "black",
  },
  buttonStyle: {
    backgroundColor: "transparent",
    borderColor: "black",
    borderWidth: 2,
    marginBottom: 2,
    borderRadius: 13,
  },
  bodyBottom: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 15,
    height: "20%",
    alignItems: "center",
  },
  bottomBanner: {
    position: "absolute",
    alignSelf: "center",
    bottom: 0,
  },
  todayText: {
    color: colors.GREYD,
    fontSize: 18,
  },
  routeText: {
    color: colors.GREYD,
    fontSize: 14,
  },
  routeTextValue: {
    color: colors.VIOLET,
    fontWeight: "bold",
  },
});

export default styles;
