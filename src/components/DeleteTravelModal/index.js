import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";
import { Button } from "react-native-elements";
import { useSelector } from "react-redux";
import styles from "./styles";
import { SvgXml } from "react-native-svg";
import travel from "../../../assets/svg/travel";

// import { AdMobBanner, setTestDeviceIDAsync } from "expo-ads-admob";

const DeleteTravel = (props) => {
  const { onDeleteTravel } = props;
  const auth = useSelector((state) => state.auth);
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;
  const [adresseArrive, setAdresseArrive] = useState("");
  const [adresseDepart, setAdresseDepart] = useState("");

  // React.useEffect(() => {
  //   setTestDeviceIDAsync("EMULATOR");
  // }, []);

  useEffect(() => {
    if (travelInformations) {
      !adresseDepart && setAdresseDepart(travelInformations.addr_dep);
      !adresseArrive && setAdresseArrive(travelInformations.addr_dest);
    }
    if (auth.info && auth.info.profile.dateTrajet === "init") {
      auth.info.profile.trajet = 0;
    }
  }, [travelInformations]);

  const confirmDeleteTravel = () => {
    onDeleteTravel();
  };
  return (
    <View style={styles.container}>
      <View style={styles.modal}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Trajet qui était proposé !</Text>
        </View>
        <View style={styles.bodyWrapper}>
          <View style={styles.bodyTop}>
            <View style={styles.elView}>
              <View>
                {/* <Image source={departdestination} /> */}
                <SvgXml xml={travel} />
              </View>
              <View style={styles.textContainer}>
                <View style={[styles.textWrapper, { paddingBottom: 10 }]}>
                  <Text style={styles.elKey}>départ</Text>
                  <Text style={styles.elValue}>
                    {travelInformations &&
                    travelInformations.addr_dep?.length < 20
                      ? `${travelInformations?.addr_dep}`
                      : `${travelInformations?.addr_dep?.substring(0, 20)}...`}
                  </Text>
                </View>
                <View style={[styles.textWrapper, { borderBottomWidth: 0 }]}>
                  <Text style={styles.elKey}>arrivée</Text>
                  <Text style={styles.elValue}>
                    {travelInformations &&
                    travelInformations.addr_dest?.length < 20
                      ? `${travelInformations?.addr_dest}`
                      : `${travelInformations?.addr_dest?.substring(0, 20)}...`}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.btnContainer1}>
              <Button
                title="Merci "
                buttonStyle={styles.buttonStyle}
                titleStyle={styles.btnTitleStyle}
                onPress={confirmDeleteTravel}
              />
            </View>
          </View>
          <View style={styles.bodyBottom}>
            <Text style={styles.todayText}>Aujourd'hui</Text>
          </View>
        </View>
      </View>

      {/* <AdMobBanner
        style={styles.bottomBanner}
        bannerSize="smartBanner"
        adUnitID="ca-app-pub-5608997176551587/4926705154"
        servePersonalizedAds
        onDidFailToReceiveAdWithError={(e) => console.log(e)}
      /> */}
    </View>
  );
};

export default DeleteTravel;
