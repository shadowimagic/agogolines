import React, { useEffect, useMemo } from "react";
import { View, StyleSheet, Text } from "react-native";
import { Button, Image } from "react-native-elements";
import { colors } from "../../common/theme";
import { useDispatch, useSelector } from "react-redux";
import NewDemand from "./../NewDemand/index";

import { Audio } from "expo-av";

function NoRiderFoundByDriverModal(props) {
  const { onValidatedTravel, handleNotFound, closeNewDemand, onDestPress } = props;

  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;

  const dispatch = useDispatch();
  const sound = React.useRef(new Audio.Sound());

  const showProposeTravel = () => {
    handleNotFound();
  };

  React.useEffect(() => {
    let unsubscribe = sound.current;

    return () => unsubscribe.unloadAsync();
  }, []);

  const LoadAudio = async () => {
    const checkLoading = await sound.current.getStatusAsync();
    // Get Loading Status

    if (checkLoading.isLoaded === false) {
      try {
        const result = await sound.current.loadAsync(
          require("../../../assets/sounds/car_horn.mp3"),
          {},
          true
        );
        if (result.isLoaded === false) {
          console.log("Error in Loading Audio");
        } else {
          await playSound();
        }
      } catch (error) {
        console.log("Error in Loading Audio");
      }
    } else {
      console.log("Error in Loading Audio");
    }
  };

  const playSound = async () => {
    try {
      const result = await sound.current.getStatusAsync();
      // Get Loading Status

      if (result.isLoaded) {
        if (result.isPlaying === false) {
          sound.current.playAsync();
        }
      } else {
        await LoadAudio();
      }
    } catch (error) {
      console.log("Error in Playing Audio", error);
    }
  };

  useEffect(() => {
    if (travelInformations != null) {
      const timer = setInterval(() => {
        let times1 = parseInt(travelInformations.create_time, 10);
        let times2 = parseInt(new Date().getTime(), 10);
        let date1 = new Date(times1);
        let date2 = new Date(times2);

        let diff = (date2 - date1) / 1000;
        diff /= 60;
        let datadiff = Math.abs(Math.round(diff));
        if (datadiff && datadiff > 120) {
          dispatch(deleteTravel(travelInformations, "oui"));
          handleNotFound();
        }
      }, 12000);
      return () => clearInterval(timer);
    }
  }, []);

  const showNewDemand = useMemo(() => {
    return (
      travelInformations &&
      travelInformations.status != "terminated" &&
      travelInformations.status != "denied" &&
      travelInformations.isFound &&
      travelInformations.isAffiner_rider
    );
  }, [
    travelInformations?.status,
    travelInformations?.isFound,
    travelInformations?.isAffiner_rider,
  ]);

  if (showNewDemand) {
    // TODO: turn on sound
    if (travelInformations.isAffiner_driver == false) playSound();

    return (
      <NewDemand
        onValidatedTravel={onValidatedTravel}
        closeNewDemand={closeNewDemand}
        onDestPress={onDestPress}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.modalView}>
        <View style={styles.modalHeader}>
          <Image
            source={require("../../../assets/images/not-found.png")}
            resizeMode="contain"
            style={{ width: 35, height: 35 }}
          />
        </View>
        <Text style={[styles.modalTitleText, { marginTop: 15 }]}>
          Aucun voyageur trouvé vers votre
        </Text>
        <Text style={[styles.modalTitleText, { marginBottom: 5 }]}>
          destination.
        </Text>
        <Text
          style={[styles.modalTitleText, { fontFamily: "Montserrat-Medium" }]}
        >
          Vous pouvez réessayer ultérieurement ou
        </Text>
        <Text
          style={[
            styles.modalTitleText,
            { marginBottom: 15, fontFamily: "Montserrat-Medium" },
          ]}
        >
          proposer un autre trajet.
        </Text>

        <Button
          title="ok"
          titleStyle={styles.modalFooterButtonTitle}
          buttonStyle={styles.modalFooterButton}
          containerStyle={styles.modalFooterButtonContainer}
          onPress={showProposeTravel}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  modalView: {
    backgroundColor: "#FFFEFE",
    borderTopLeftRadius: 13,
    borderTopRightRadius: 13,
    paddingTop: 15,
    height: "33%",
    width: "90%",
    alignItems: "center",
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 3,
  },
  modalHeader: {
    flexDirection: "row-reverse",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  modalDivider: {
    borderBottomColor: colors.BLACK,
    borderBottomWidth: 1,
    borderColor: "#E1DEDE",
    borderStyle: "solid",
    height: 10,
    marginBottom: 10,
    width: "90%",
  },
  modalTitleText: {
    alignSelf: "center",
    color: "#9A9A9A",
    fontFamily: "Montserrat-Bold",
    textAlign: "center",
    fontSize: 14,
    lineHeight: 18,
  },
  modalContentContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  modalFooterButtonTitle: {
    color: colors.WHITE,
    fontSize: 16,
    fontFamily: "Montserrat-Bold",
    textTransform: "uppercase",
    textAlign: "center",
  },
  modalFooterButtonContainer: {
    borderRadius: 13,
    width: "35%",
  },
  modalFooterButton: {
    borderRadius: 13,
    backgroundColor: colors.VIOLET,
  },
});

export default NoRiderFoundByDriverModal;
