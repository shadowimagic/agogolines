import React, { useEffect } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, Linking } from "react-native";

//colors
import { colors } from "../../common/theme";
import { useSelector } from "react-redux";
import riderImg from "../../../assets/images/avatar_login.png";
import coins from "../../../assets/svg/grosCoins";
//Constants
import Constants from "expo-constants";

import { SvgXml } from "react-native-svg";

const DriverValidatePaymentMethod = (props) => {
  const { openDropDownRiderByDriver, closeDriverValidatePaymentMethod } = props;

  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;

  const onValidatedPaymentMethod = () => {
    closeDriverValidatePaymentMethod();
    openDropDownRiderByDriver();
  };

  useEffect(() => {
    console.log("mount DriverValidatePaymentMethod");

    return () => {
      console.log("unmount DriverValidatePaymentMethod");
    };
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.headerDocsContainer}>
        <Image
          source={
            travelInformations &&
            travelInformations?.propose_profile_img_rider &&
            travelInformations?.propose_profile_img_rider != "non"
              ? { uri: travelInformations?.propose_profile_img_rider }
              : riderImg
          }
          style={{ width: 60, height: 60, borderRadius: 50 }}
        />
        <Text style={styles.headerText}>
          {travelInformations && travelInformations != null
            ? travelInformations?.propose_firstname_rider +
              " " +
              travelInformations?.propose_lastname_rider
            : "Martin LEMOINE"}
        </Text>
      </View>

      <View style={styles.scrollViewContainer}>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text
            style={[
              styles.profileText,
              {
                fontSize: 16,
                lineHeight: 19,
                color: colors.VIOLET,
                textAlign: "center",
              },
            ]}
          >
            a choisi le mode de paiement
          </Text>
          <Text
            style={[
              styles.profileText,
              {
                fontSize: 16,
                lineHeight: 19,
                color: colors.VIOLET,
                textAlign: "center",
              },
            ]}
          >
            {" "}
            suivant :
          </Text>
        </View>
        <View
          style={{
            marginTop: 7,
            alignItems: "center",
            justifyContent: "center",
            alignSelf: "center",
            shadowColor: "black",
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.23,
            shadowRadius: 2.62,
            elevation: 4,
            backgroundColor: "white",
            width: 350,
            height: 70,
            borderRadius: 13,
            flexDirection: "row",
          }}
        >
          <SvgXml xml={coins} />
          {travelInformations &&
            travelInformations?.mode_paiement === "especes" && (
              <Text
                style={{
                  color: "#979797",
                  fontSize: 18,
                  lineHeight: 22,
                  marginLeft: 10,
                }}
              >
                Espèces
              </Text>
            )}

          {travelInformations &&
            travelInformations?.mode_paiement === "cartecredit" && (
              <Text
                style={{
                  color: "#979797",
                  fontSize: 18,
                  lineHeight: 22,
                  marginLeft: 10,
                }}
              >
                Carte de crédit
              </Text>
            )}

          {travelInformations && travelInformations?.mode_paiement === "bonus" && (
            <Text
              style={{
                color: "#979797",
                fontSize: 18,
                lineHeight: 22,
                marginLeft: 10,
              }}
            >
              Bonus
            </Text>
          )}
        </View>

        {travelInformations?.mode_paiement === "especes" && (
          <View style={{ marginTop: 10 }}>
            <Text
              style={{
                color: "#979797",
                fontSize: 18,
                lineHeight: 22,
                textAlign: "center",
              }}
            >
              Pensez à lui demander de vous
            </Text>
            <Text
              style={{
                color: "#979797",
                fontSize: 18,
                lineHeight: 22,
                textAlign: "center",
              }}
            >
              reverser:{" "}
            </Text>
          </View>
        )}

        {travelInformations?.mode_paiement === "bonus" && (
          <View style={{ marginTop: 10 }}>
            <Text
              style={{
                color: "#979797",
                fontSize: 18,
                lineHeight: 22,
                textAlign: "center",
              }}
            >
              La différence sera payé par Agogolines
            </Text>
            <Text
              style={{
                color: "#979797",
                fontSize: 18,
                lineHeight: 22,
                textAlign: "center",
              }}
            >
              reverser:{" "}
            </Text>
          </View>
        )}

        <View style={{ marginTop: 10 }}>
          <Text
            style={{
              color: colors.VIOLET,
              fontSize: 25,
              lineHeight: 30,
              textAlign: "center",
              fontFamily: "Montserrat-Bold",
            }}
          >
            {travelInformations && travelInformations != null
              ? travelInformations?.propose_negoPrice
              : "0"}{" "}
            €
          </Text>
        </View>

        {travelInformations?.mode_paiement === "bonus" && (
          <TouchableOpacity
            onPress={onValidatedPaymentMethod}
            style={{
              width: 282,
              height: 55,
              marginTop: 7,
              alignSelf: "center",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 13,
              backgroundColor: colors.VIOLET,
            }}
          >
            <Text
              style={[
                styles.profileText,
                {
                  fontSize: 20,
                  lineHeight: 24,
                  color: "white",
                },
              ]}
            >
              Ok
            </Text>
          </TouchableOpacity>
        )}

        {travelInformations?.mode_paiement === "cartecredit" && (
          <TouchableOpacity
            onPress={onValidatedPaymentMethod}
            style={{
              width: 282,
              height: 55,
              marginTop: 7,
              alignSelf: "center",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 13,
              backgroundColor: colors.VIOLET,
            }}
          >
            <Text
              style={[
                styles.profileText,
                {
                  fontSize: 20,
                  lineHeight: 24,
                  color: "white",
                },
              ]}
            >
              Ok
            </Text>
          </TouchableOpacity>
        )}

        {travelInformations?.mode_paiement === "especes" && (
          <TouchableOpacity
            onPress={onValidatedPaymentMethod}
            style={{
              width: 282,
              height: 55,
              marginTop: 7,
              alignSelf: "center",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 13,
              backgroundColor: colors.VIOLET,
            }}
          >
            <Text
              style={[
                styles.profileText,
                {
                  fontSize: 19,
                  lineHeight: 24,
                  color: "white",
                },
              ]}
            >
              Ok, c’est fait !
            </Text>
          </TouchableOpacity>
        )}

        {travelInformations?.mode_paiement === "especes" && (
          <TouchableOpacity
            onPress={() => Linking.openURL('mailto:support@agogolines.com')}
            style={{
              width: 282,
              height: 55,
              marginTop: 7,
              alignSelf: "center",
              alignItems: "center",
              justifyContent: "center",
              borderColor: "black",
              borderWidth: 1,
              borderRadius: 13,
            }}
          >
            <Text
              style={[
                styles.profileText,
                {
                  fontSize: 19,
                  lineHeight: 24,
                  color: "black",
                },
              ]}
            >
              Signaler un problème
            </Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: Constants.statusBarHeight,
    backgroundColor: colors.TRANSPARENT,
  },
  headerDocsContainer: {
    backgroundColor: colors.VIOLET,
    height: "40%",
    paddingHorizontal: "8%",
    flexBasis: "33%",
    justifyContent: "center",
    alignItems: "center",
  },

  headerText: {
    color: colors.WHITE,
    fontFamily: "Montserrat-Bold",
    fontSize: 25,
    lineHeight: 30,
  },

  profileText: {
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
    lineHeight: 19,
    color: colors.BLACK,
  },

  boxUnderHeaderText: {
    width: 350,
    height: 75,
    backgroundColor: colors.WHITE,
    position: "absolute",
    alignSelf: "center",
    top: "31%",
    shadowColor: "black",
    shadowOffset: { height: 2, width: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 6,
    elevation: 8,
    borderRadius: 12,
    justifyContent: "space-around",
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 13,
  },

  scrollViewContainer: {
    backgroundColor: colors.WHITE,
    position: "absolute",
    width: "100%",
    height: "80%",
    top: "26%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: "5%",
    paddingTop: 20,
    /* backgroundColor: colors.WHITE,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingTop: 20, */
  },

  originDestination: {
    marginBottom: 12,
  },

  titleOr: {
    width: 110,
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    lineHeight: 15,
    color: "#848484",
  },
  titleDes: {
    width: 95,
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    lineHeight: 15,
    color: "#848484",
  },

  date: {
    width: 110,
    height: 30,
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    lineHeight: 15,
    color: "#848484",
    textAlign: "right",
  },

  price: {
    color: colors.VIOLET,
    width: 110,
    height: 30,
    fontFamily: "Montserrat-Bold",
    fontSize: 14,
    lineHeight: 18,
    textAlign: "right",
  },
});

export default DriverValidatePaymentMethod;
