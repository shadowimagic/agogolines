import React, { useState } from "react";
import { View, StyleSheet, Text } from "react-native";
import { Button, Image } from "react-native-elements";
import { colors } from "../../common/theme";
import { useSelector } from "react-redux";

function DeniedTravelByRiderModal(props) {
  const travels = useSelector((state) => state.travel);
  const travelInformations = travels.travel;
  const [isVisible, setIsVisible] = useState(true);

  if (isVisible) {
    return (
      <View style={styles.container}>
        <View style={styles.modalView}>
          <View style={styles.modalHeader}>
            <Image
              source={require("../../../assets/images/cancel_profile_img.png")}
              resizeMode="contain"
              style={{ width: 40, height: 40 }}
              containerStyle={{ left: 5 }}
            />
            <Image
              source={require("../../../assets/images/cancel.png")}
              resizeMode="contain"
              style={{ width: 40, height: 40 }}
              containerStyle={{ right: 5 }}
            />
          </View>
          <Text style={[styles.modalTitleText, { marginTop: 15 }]}>
            {travelInformations &&
              travelInformations.propose_firstname_rider +
                " " +
                travelInformations.propose_lastname_rider}
          </Text>
          <Text style={[styles.modalTitleText, { marginBottom: 15 }]}>
            vient d’annuler son voyage !
          </Text>

          <Button
            title="ok"
            titleStyle={styles.modalFooterButtonTitle}
            buttonStyle={styles.modalFooterButton}
            containerStyle={styles.modalFooterButtonContainer}
            onPress={() => {
              setIsVisible(false);
              //IF ANOTHER DRIVER EXIST SEND ANOTHER REQUEST 
            }}
          />
        </View>
      </View>
    );
  } else {
    return null;
  }
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  modalView: {
    backgroundColor: "#FFFEFE",
    borderTopLeftRadius: 13,
    borderTopRightRadius: 13,
    paddingTop: 15,
    height: "30%",
    width: "90%",
    alignItems: "center",
    shadowColor: "rgba(175,174,174,0.5)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 3,
  },
  modalHeader: {
    flexDirection: "row-reverse",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  modalTitleText: {
    alignSelf: "center",
    color: colors.BLACK,
    fontFamily: "Montserrat-Bold",
    textAlign: "center",
    fontSize: 14,
    lineHeight: 18,
  },
  modalFooterButtonTitle: {
    color: colors.WHITE,
    fontSize: 16,
    fontFamily: "Montserrat-Bold",
    textTransform: "uppercase",
    textAlign: "center",
  },
  modalFooterButtonContainer: {
    borderRadius: 13,
    width: "33%",
  },
  modalFooterButton: {
    borderRadius: 13,
    backgroundColor: colors.VIOLET,
  },
});

export default DeniedTravelByRiderModal;
