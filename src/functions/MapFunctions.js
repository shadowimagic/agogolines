// Converts numeric degrees to radians
const toRadians = (value) => {
  return (value * Math.PI) / 180;
};

//This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
export const calculateCrowFlies = (lat1, lon1, lat2, lon2) => {
  const R = 6371; // km, Radius of Earth

  // convert coordinate to radians
  const diffLat = toRadians(lat2 - lat1);
  const diffLong = toRadians(lon2 - lon1);
  const latitude1 = toRadians(lat1);
  const latitude2 = toRadians(lat2);

  // Haversine Formula
  const a =
    Math.sin(diffLat / 2) * Math.sin(diffLat / 2) +
    Math.sin(diffLong / 2) *
      Math.sin(diffLong / 2) *
      Math.cos(latitude1) *
      Math.cos(latitude2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  // Distance (m) R * c was km / * 1000 convert to m
  return R * c * 1000;
};

export const findDirection = (dep, dest) => {
  let direction = "";
  let difx = dep.latitude - dest.latitude;
  let dify = dep.longitude - dest.longitude;
  let angle = (Math.atan2(difx, dify) * 180) / Math.PI;
  if (angle > -22.5 && angle < 22.5) {
    direction = "N";
  }
  if (angle > 22.5 && angle < 67.5) {
    //direction = 'NE';
    direction = "N";
  }
  if (angle > 67.5 && angle < 112.5) {
    direction = "E";
  }
  if (angle > 112.5 && angle < 157.5) {
    //direction = 'SE';
    direction = "S";
  }
  if (angle < -157.5 || angle > 157.5) {
    direction = "S";
  }
  if (angle < -112.5 && angle > -157.5) {
    //direction ='SW';
    direction = "S";
  }
  if (angle < -67.5 && angle > -112.5) {
    direction = "W";
  }
  if (angle < -22.5 && angle > -67.5) {
    //direction = 'NW';
    direction = "N";
  }
  return direction;
};
