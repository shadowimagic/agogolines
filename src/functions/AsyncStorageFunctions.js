import AsyncStorage from "@react-native-async-storage/async-storage";

export const setData = async (key, data) => {
  await AsyncStorage.setItem(key, data);
};

export const getData = async (key) => {
  const result = await AsyncStorage.getItem(key);
  if (result) {
    return result;
  }
  return null;
};

export const removeData = async (key) => {
  await AsyncStorage.removeItem(key);
};
