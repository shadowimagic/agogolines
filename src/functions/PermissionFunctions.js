import * as ImagePicker from "expo-image-picker";

export const getLibraryPermission = async () => {
  const permission = await ImagePicker.getMediaLibraryPermissionsAsync();
  if (!permission.granted) {
    const requestPermission =
      await ImagePicker.requestMediaLibraryPermissionsAsync();
    return requestPermission.granted;
  }

  return true;
};

export const getCameraPermission = async () => {
  const permission = await ImagePicker.getCameraPermissionsAsync();
  if (!permission.granted) {
    const requestPermission = await ImagePicker.requestCameraPermissionsAsync();
    return requestPermission.granted;
  }

  return true;
};
