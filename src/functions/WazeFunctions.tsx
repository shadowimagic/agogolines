import { Linking, Platform } from "react-native";

type WazeParams = {
  value: string;
  type:
    | "search"
    | "navigate"
    | "zoom"
    | "coordinate"
    | "favorite"
    | "start"
    | "end";
};

type Coordinate = {
  longitude: number | string;
  latitude: number | string;
};

const wazeStoreLink =
  Platform.OS === "android"
    ? "market://details?id=com.waze"
    : "http://itunes.apple.com/us/app/id323229106";

const openWazeQuery = async (params: WazeParams[]) => {
  let query = "ul?";
  params.forEach((param) => {
    switch (param.type) {
      case "search":
        query += `&q=${encodeURIComponent(param.value)}`;
        break;
      case "favorite":
        query += `&favorite=${param.value}`;
        break;
      case "navigate":
        query += `&navigate=${param.value}`;
        break;
      case "coordinate":
        query += `&ll=${encodeURIComponent(param.value)}`;
        break;
      case "start":
        query += `&ll=${encodeURIComponent(param.value)}`;
        break;
      case "end":
        query += `&ll=${encodeURIComponent(param.value)}`;
        break;
      case "zoom":
        query += `&z=${param.value}`;
        break;
      default:
        return query;
    }
  });
  const formattedQuery = query.replaceAll("&&", "&").replaceAll("?&", "?");
  const wazeLink = `https://waze.com/${formattedQuery}`;
  if (await Linking.canOpenURL("waze://")) {
    Linking.openURL(wazeLink);
  } else {
    Linking.openURL(wazeStoreLink);
  }
};

const wazeRouting = (destination: Coordinate) => {
  const queryDestination: WazeParams = {
    type: "end",
    value: `${destination.latitude},${destination.longitude}`,
  };

  const params = [
    queryDestination,
    { type: "navigate", value: "yes" },
    { type: "zoom", value: "17" },
  ] as WazeParams[];

  openWazeQuery(params);
};

export { openWazeQuery, WazeParams, wazeRouting };
