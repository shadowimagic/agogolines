import {StyleSheet} from 'react-native';
import { colors } from './theme';
import { Dimensions } from 'react-native';

export const CustomStyle = StyleSheet.create({

  /* Register Screen */
  // Header style
  headerContainerStyle: {
    marginHorizontal: '20%',
    marginVertical: 35
  },

  headerContainerIcon: {
    width: '100%',
    marginBottom: 20, 
    justifyContent: 'center', 
    alignItems: 'center'
  },

  headerTitle: {
    textAlign: 'center', 
    width: '100%',
    height: 41,
    fontFamily: 'Montserrat-Medium',
    color: colors.VIOLET,
    fontSize: 25,
    lineHeight: 30
  },

  icon: {
    width: 82,
    height: 82
  },

  ButtonAjContainer: {
    justifyContent: 'center', 
    alignItems: 'center',
  },

  buttonAjImage: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#979797',
    borderRadius: 21,
    paddingVertical: 10,
  },

  textAjPhoto: {
    paddingLeft: 10,
    fontSize: 13,
    color: "#979797",
    lineHeight: 16,
    textTransform: 'uppercase',
    fontFamily: 'Montserrat-Bold',
    height: 16
  },

  /* fin Header style */


  //form style
  labelInputStyle:{
    color: "rgba(0,0,0,1)",
    fontSize: 13,
    width:263,
    height:27,
    fontWeight:"bold",
    fontFamily: "Montserrat-Bold",
    padding:5,
    marginVertical: 5
  },

  textInputContainerStyle: {
    flexDirection: 'column',
    alignItems: "center",
    marginLeft: 20,
    marginRight: 20,
    paddingLeft: 5,
    paddingRight:15,
  },

  inputTextStyle: {
    fontSize: 13,
    padding:15,
    borderRadius: 13,
    height: 50,
    width: 315.5,
    marginBottom: 10,
    shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    backgroundColor: "#fff",
  },

  errorInputText: {
    color: colors.RED,
    fontFamily: 'Montserrat-Medium',
  },
  
  champOb: {
    fontFamily: 'Montserrat-Medium',
    color: '#969696',
    fontSize: 14,
    lineHeight: 17
  },

  textSuiv: {
    color: colors.WHITE,
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
    textTransform: 'uppercase'
  },

  /* fin form */

  /* footer */
  textAvDejCompte: {
    fontSize: 14, 
    lineHeight: 17, 
    color: colors.VIOLET, 
    fontFamily: 'Montserrat-Medium'
  },

  textConn: {
    borderBottomWidth: 1
  },

  /* fin footer */
  /* Register Screen */


    /* phoneMessage Screen */
    modalContainer: { 
      width: Dimensions.get('window').width - 20,
      height: 284,
      borderRadius: 13,
      backgroundColor: colors.WHITE,
      paddingHorizontal: 20,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },

    modalHeaderText:{
        fontSize: 15,
        lineHeight: 18,
        fontFamily: 'Montserrat-Bold',
        alignItems: 'center',
        marginTop: 25,
      },

    textInputCode: {
      padding:15,
      borderRadius: 8,
      height: 50,
      width: 166,
      marginVertical: 40,
      shadowColor: "#000",
      elevation: 5,
      backgroundColor: "#fff",
    },

    /* fin phoneMessage screen*/

    /* congratRegisScreen*/
    modalCompteCree: {
      width: Dimensions.get('window').width - 20,
      height: 445,
      borderRadius: 13,
      backgroundColor: colors.WHITE,
      paddingHorizontal: 20,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },

    comment: {
      color: '#939393',
      fontFamily: 'Montserrat-Medium',
      fontSize: 14,
      lineHeight: 17,
    },

    textFelicitation: {
      height: 29,
      color: colors.VIOLET,
      fontFamily: 'Montserrat-Bold',
      fontSize: 15,
      lineHeight: 18,
      marginTop: 20
    },

    buttonOk: {
      fontFamily: 'Montserrat-Bold',
      color: colors.WHITE,
      paddingHorizontal: 50,
    },

    /* fin congratRegisScreen*/

  // container InputText
    phonetextInputContainerStyle: {
      flexDirection: 'row',
    },

  // InputText style

  /*  inputContainerStyle: {
    borderBottomWidth: 1,
    borderBottomColor: colors.WHITE,
  }, */

    labelInputphStyle:{
      color: "rgba(0,0,0,1)",
      fontSize: 13,
      width:263,
      height:27,
      fontWeight:"bold",
      fontFamily: "Montserrat-Bold",
    },

    labelInputPhoneStyle:{
      backgroundColor:colors.WHITE,
      borderBottomLeftRadius: 13,
      borderTopLeftRadius:13,
      borderBottomWidth: 1,
      borderBottomColor: colors.WHITE,
      paddingLeft:13,
      height: 49.50,
      width: 50, 
    },

    labelInputSmallStyle:{
      color: "rgba(0,0,0,1)",
      fontSize: 13,
      fontFamily: "Montserrat-Bold",
      textTransform:"lowercase",
      padding:5
    },

    labelCarteStyle:{
      color: colors.GREYD,
      fontSize: 13,
      fontFamily: "Montserrat-Bold",
      textTransform:"capitalize",
      padding:5
    },

    labelCarteContainerStyle: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginLeft: 90,
      marginRight: 70,
      paddingTop:2,
      },

    buttonSuiv: {
      fontFamily: 'Montserrat-Bold',
      fontSize: 16,
      lineHeight: 20,
    },

    buttonSuivContainer: {
      width: 200,
      justifyContent: 'center', 
      alignItems: 'center',
    },

      inputTextPhoneStyle: {
        fontSize: 13,
        marginLeft: 0,
        paddingLeft:5,
        borderBottomRightRadius: 13,
        borderTopRightRadius:13,
        height: 49.18,
        width: 315.5,
       
        shadowColor: "rgba(197,197,197,0.5)",
        shadowOffset: {
        height: 0,
        width: 0
        },
        shadowRadius: 8,
        shadowOpacity: 1,
        backgroundColor: "rgba(255,255,255,1)"
        },

      inputFr: {
        marginLeft: 5,
        fontSize: 13,
        padding:15,
        borderRadius: 13,
        height: 49.18,
        shadowColor: "rgba(197,197,197,0.5)",
        shadowOffset: {
        height: 0,
        width: 0
        },
        shadowRadius: 8,
        shadowOpacity: 1,
        backgroundColor: "rgba(255,255,255,1)",        
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
        borderRadius: 21
      },


    // Button style
        
    styleButtonShortTitle: {
        backgroundColor: "transparent",
        color: colors.WHITE,
        fontSize: 16,
        fontWeight:"bold",
        fontFamily: "Montserrat-Bold",
    },
    styleShortButton:{
          width: 125,
          height: 49,
          margin: 10,
          padding:5,
          borderRadius: 30,
          backgroundColor: "transparent",
          backgroundColor: colors.VIOLET,
          borderRadius: 13,

    },
    // short button commande
    styleShortCmdButton:{
      width: 126.44,
      height: 36.74,
      margin: 10,
      padding:5,
      borderRadius:11,
      backgroundColor: "transparent",
      backgroundColor: colors.VIOLET,
      borderRadius: 13,
    },

    // Short texte commande
    styleButtonShortCmdTitle: {
      backgroundColor: "transparent",
      color: colors.WHITE,
      fontSize: 16,
      fontWeight:"bold",
      padding: 35,
      fontFamily: "Montserrat-Bold",
    },
      styleButton: {
        height: 49.18,
        width: 315.5,
        marginLeft: 0,
        paddingLeft:5,
        borderRadius: 13,
        backgroundColor: "transparent",
        backgroundColor: colors.VIOLET,
      },
      styleButtonPaiement: {
        height: 41,
        width: 268,
        marginLeft: 0,
        paddingLeft:5,
        borderRadius: 11,
        backgroundColor: "transparent",
        backgroundColor: colors.VIOLET,
        justifyContent:'space-evenly'  
      },
      styleButtonTitle:{
        backgroundColor: "transparent",
        color: colors.WHITE,
        fontSize: 16,
        fontWeight:"bold",
        fontFamily: "Montserrat-Bold",
    },
    styleButtonPaiementTitle:{
      backgroundColor: "transparent",
      color: colors.WHITE,
      fontSize: 12,
      fontWeight:"bold",
      fontFamily: "Montserrat-Bold",
  },

    styleButtonPhotoTitle: {
      color: colors.WHITE,
      fontSize: 12,
      fontWeight:"bold",
      fontFamily: "Montserrat-Bold",
  },

  
  styleShortButtonPhoto:{
        height: 49,
        width:200,
        padding:15,
        borderWidth: 1,
        borderColor:colors.VIOLET,
        backgroundColor: colors.VIOLET,
        borderRadius: 11

  },
      buttonOnlineStyle: {
        height: 49,
        width:200,
        padding:15,
        borderRadius: 11,
        backgroundColor: "transparent",
        borderColor:colors.WHITE
        
      }
      ,
      // Loading style
      loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom:40
      },

      // commande passager

      passagerContainerStyle: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        },
    
        

        /* headerInnerContainer: {
            marginLeft: 10,
            marginRight: 10
        }, */
        textMenuStyle: {
          backgroundColor: "transparent",
          textAlign: "center",
          color: "rgba(255,255,255,1)",
          fontSize: 15,
          fontFamily: "Montserrat-Bold",
          textTransform:'uppercase'
        },  

    // input text card
    inputTextCardStyle: {
      fontSize: 13,
      marginLeft: 0,
      padding:15,
      borderRadius: 9,
      height: 45,
      width: 301,
      backgroundColor: "rgba(255,255,255,1)",
      borderColor:"#979797",
      borderWidth: 1,
      },

      labelInputCardStyle:{
        color: "rgba(0,0,0,1)",
        fontSize: 14,
        fontWeight:"bold",
        fontFamily: "Montserrat-Bold",
        paddingBottom:10
        
        
      },
      //Button d'enregistrement
      styleButtonEngTitle: {
        backgroundColor: "transparent",
        color: colors.WHITE,
        fontSize: 12,
        fontWeight:"bold",
        fontFamily: "Montserrat-Bold",
    },
    styleEngButton:{
      width: 170,
      height: 41,
      margin: 10,
      padding:5,
      borderRadius: 11,
      backgroundColor: "transparent",
      backgroundColor: colors.VIOLET,
    },
  });