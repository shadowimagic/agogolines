import { DependencyList, EffectCallback, useEffect } from "react";

import { debounce } from "lodash";

export default function useDebounce(
  effect: EffectCallback,
  delay: number,
  dependencies: DependencyList | undefined
): void {
  useEffect(() => {
    const deb = debounce(() => {
      effect();
    }, delay);
    deb();
    return deb.cancel;
  }, dependencies); // eslint-disable-line react-hooks/exhaustive-deps
}
