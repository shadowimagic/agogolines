import React from "react";

import { Dimensions, ScrollView, View } from "react-native";
import PropTypes, { InferProps } from "prop-types";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
  Platform,
  SafeAreaView,
  ViewPropTypes,
} from "react-native";
type Props = {
  children?: React.ReactNode;
};
export function DismissAvoidKeyboard({
  children,
  width,
  height,
}: InferProps<typeof DismissAvoidKeyboard.prototypes>) {
  return Platform.OS === "web" ? (
    <View
      style={{
        flex: 1,
        height: height,
        width: width,
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      {children}
    </View>
  ) : Platform.OS === "android" ? (
    <KeyboardAwareScrollView
      extraHeight={200}
      enableOnAndroid
      scrollEnabled={false}
    >
      <SafeAreaView style={{ flex: 1 }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          {children}
        </TouchableWithoutFeedback>
      </SafeAreaView>
    </KeyboardAwareScrollView>
  ) : (
    <KeyboardAwareScrollView
      extraHeight={200}
      enableOnAndroid
      scrollEnabled={false}
      contentContainerStyle={{ height: "100%" }}
    >
      <SafeAreaView style={{ flex: 1 }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          {children}
        </TouchableWithoutFeedback>
      </SafeAreaView>
    </KeyboardAwareScrollView>
  );
}

DismissAvoidKeyboard.prototypes = {
  width: PropTypes.any,
  height: PropTypes.any,
  children: PropTypes.element.isRequired,
};

DismissAvoidKeyboard.defaultProps = {
  height: Dimensions.get("window").height,
  width: Dimensions.get("window").width,
};
