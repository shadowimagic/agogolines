
import "react-native-gesture-handler";
// enableScreens();

import React, { useEffect } from "react";
import AppContainer from "./src/navigation/AppNavigator";
import * as Notifications from "expo-notifications";
// import { enableScreens } from "react-native-screens";
// import { LogBox } from "react-native";
import { Provider } from "react-redux";
import { FirebaseProvider, store } from "./redux/src";
import AppCommon from "./AppCommon";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: false,
  }),
});

export default function App() {
  useEffect(() => {
    // LogBox.ignoreAllLogs(true);
  }, []);

  return (
    <Provider store={store}>
      <FirebaseProvider>
        <AppCommon>
          <AppContainer />
        </AppCommon>
      </FirebaseProvider>
    </Provider>
  );
}