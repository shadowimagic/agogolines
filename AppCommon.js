import { useState, useContext, useEffect, useRef } from "react";
import { store, FirebaseContext } from "./redux/src";
import { useSelector, useDispatch } from "react-redux";
import * as TaskManager from "expo-task-manager";
// import { /* PERMISSIONS, */ request, RESULTS, check } from "react-native-permissions";
import { Audio } from "expo-av";
import { AppState, Platform } from "react-native";
import Geolocation from "react-native-geolocation-service";

const LOCATION_TASK_NAME = "background-location-task";

TaskManager.defineTask(LOCATION_TASK_NAME, ({ data: { locations }, error }) => {
  if (error) {
    console.log("Task Error");
    return;
  }
  if (locations.length > 0) {
    let location = locations[locations.length - 1];
    try {
      if (store.getState().auth.info && store.getState().auth.info.uid) {
        store.dispatch({
          type: "UPDATE_GPS_LOCATION",
          payload: {
            lat: location.coords.latitude,
            lng: location.coords.longitude,
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
  }
});

export default function AppCommon({ children }) {
  const { api } = useContext(FirebaseContext);
  const auth = useSelector((state) => state.auth);
  const tasks = useSelector((state) => state.taskdata.tasks);
  const settings = useSelector((state) => state.settingsdata.settings);
  const [sound, setSound] = useState();
  const dispatch = useDispatch();

  const appState = useRef(AppState.currentState);

  useEffect(() => {
    if (auth?.info?.profile?.usertype == "driver") {
      if (tasks && tasks.length > 0) {
        playSound();
      } else {
        stopPlaying();
      }
    }
  }, [auth?.info?.profile?.usertype, tasks]);

  useEffect(() => {
    if (settings) {
      loadSound();
    }
  }, [settings]);

  const loadSound = async () => {
    Audio.setAudioModeAsync({
      // allowsRecordingIOS: false,
      staysActiveInBackground: true,
      // interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DUCK_OTHERS,
      // playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
      useNativeControls: false,
    });

    const { sound } = await Audio.Sound.createAsync(
      settings.CarHornRepeat
        ? require("./assets/sounds/car_horn_gap.mp3")
        : require("./assets/sounds/car_horn.mp3")
    );
    sound.setIsLoopingAsync(settings.CarHornRepeat);
    setSound(sound);
  };

  const playSound = async () => {
    sound.playAsync();
  };

  const stopPlaying = async () => {
    if (sound) {
      sound.stopAsync();
    }
  };
  // const getBGPermission = async () => {
  //   const { status, granted, canAskAgain } =
  //     await Location.getBackgroundPermissionsAsync();
  //   if (granted) {
  //     return true;
  //   }
  //   if (!canAskAgain) {
  //     return false;
  //   }
  //   if (status !== "granted") {
  //     try {
  //       const { granted: requestGranted } =
  //         await Location.requestBackgroundPermissionsAsync();
  //       return requestGranted;
  //     } catch (error) {
  //       return false;
  //     }
  //   }
  //   return false;
  // };

  // const getFGPermission = async () => {
  //   let keyPermission =
  //     Platform.OS === "ios"
  //       ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
  //       : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
  //   const status = await check(keyPermission);

  //   switch (status) {
  //     case RESULTS.GRANTED:
  //       return true;
  //     case RESULTS.UNAVAILABLE:
  //     case RESULTS.BLOCKED:
  //       return false;
  //     case RESULTS.DENIED:
  //       request(keyPermission)
  //         .then((res) => {
  //           if (res === RESULTS.GRANTED) {
  //             return true;
  //           }
  //           return false;
  //         })
  //         .catch((error) => {
  //           console.log({ error });
  //           return false;
  //         });
  //     default:
  //       return false;
  //   }
  // };

  // const checkPermission = async () => {
  //   const foreGroundStatus = await getFGPermission();
  //   store.dispatch({
  //     type: "UPDATE_FOREGROUND_LOCATION",
  //     payload: foreGroundStatus ? "granted" : "denied",
  //   });
  // };

  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      // checkPermission();
    }
    appState.current = nextAppState;
  };

  useEffect(() => {
    if (api) {
      dispatch(api.fetchUser());
      dispatch(api.fetchCarTypes());
      dispatch(api.fetchSettings());
    }
  }, [api]);

  useEffect(() => {
    // checkPermission();

    AppState.addEventListener("change", _handleAppStateChange);
    return () => {
      AppState.removeEventListener("change", _handleAppStateChange);
      Geolocation.stopObserving();
    };
  }, []);

  return children;
}
