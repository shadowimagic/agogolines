export const features = {
    AllowCriticalEditsAdmin: true,
    AllowCountrySelection: true,
    WebsitePagesEnabled: false,
    MobileLoginEnabled: true,
    FacebookLoginEnabled: false,
    AppleLoginEnabled: false
};