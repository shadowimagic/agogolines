export const MainConfig = {
    AppDetails: {
        app_name: "agogolines-mobile",
        app_description: "Agogolines pour le covoiturage",
        app_identifier: "com.app.agogolines",
        ios_app_version: "1.0.0",
        android_app_version: 1
    },
    FirebaseConfig: {
        apiKey: "AIzaSyAb_F9h9rlUoKkn1tdTuglpZX9sTzzfzzY",
        authDomain: "agogolines-8bc39.firebaseapp.com",
        databaseURL: "https://agogolines-8bc39.firebaseio.com",
        projectId: "agogolines-8bc39",
        storageBucket: "agogolines-8bc39.appspot.com",
        messagingSenderId: "998867161341",
        appId: "1:998867161341:web:1403d398f6f15104b1ee94"


    },
    Google_Map_Key: "AIzaSyAQX2iFEPh2ZaX5ks8JHbHpzc5K8GD6G08",
    facebookAppId: "973091322846524",
    PurchaseDetails: {
        CodeCanyon_Purchase_Code: "",
        Buyer_Email_Address: ""
    }
}