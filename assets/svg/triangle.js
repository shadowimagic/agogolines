const triangle = `<svg width="10px" height="10px" viewBox="0 0 10 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<title>Triangle</title>
<g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="Menu-profil-voyageur" transform="translate(-282.000000, -400.000000)" fill="#5A48CB">
        <g id="Menu" transform="translate(0.000000, 86.000000)">
            <g id="Points" transform="translate(57.000000, 267.000000)">
                <polygon id="Triangle" points="230 47 235 57 225 57"></polygon>
            </g>
        </g>
    </g>
</g>
</svg>`;

export default triangle;
