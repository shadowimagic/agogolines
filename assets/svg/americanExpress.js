const american = `<?xml version="1.0" encoding="utf-8"?>
<svg width="35px" height="24px" viewBox="0 0 35 24" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
  <path d="M35 21.9428C35 23.0789 34.0737 24 32.9311 24L2.0689 24C0.92627 24.0001 0 23.0789 0 21.9428L0 2.05718C0 0.921061 0.92627 0 2.0689 0L32.9311 0C34.0738 0 35 0.921061 35 2.05718L35 21.9428L35 21.9428Z" id="Path" fill="#306FC5" stroke="none" />
</svg>`

export default american;