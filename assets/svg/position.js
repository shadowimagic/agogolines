const position = `<?xml version="1.0" encoding="utf-8"?>
<svg width="35px" height="35px" viewBox="0 0 20 30" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
  <g id="Icon" transform="translate(1 1)">
    <path d="M22 10.6364C22 18.9091 11 26 11 26C11 26 0 18.9091 0 10.6364C9.05265e-08 4.76206 4.92487 -2.62416e-16 11 0C17.0751 2.62416e-16 22 4.76206 22 10.6364L22 10.6364Z" transform="translate(-0.32258064 -0.016129032)" id="Shape" fill="none" fill-rule="evenodd" stroke="#5A48CB" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    <path d="M3.5 7C5.433 7 7 5.433 7 3.5C7 1.567 5.433 0 3.5 0C1.567 0 0 1.567 0 3.5C0 5.433 1.567 7 3.5 7Z" transform="translate(6.677419 6.983871)" id="Oval" fill="none" fill-rule="evenodd" stroke="#5A48CB" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
  </g>
</svg>`;

export default position;