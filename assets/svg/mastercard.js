const mastercard = `<?xml version="1.0" encoding="utf-8"?>
<svg width="30px" height="19px" viewBox="0 0 30 19" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
  <g id="mastercard">
    <path d="M9.33333 18.6667C14.488 18.6667 18.6667 14.488 18.6667 9.33333C18.6667 4.17868 14.488 0 9.33333 0C4.17868 0 0 4.17868 0 9.33333C0 14.488 4.17868 18.6667 9.33333 18.6667Z" id="Oval" fill="#EE0005" stroke="none" />
    <path d="M9.33333 18.6667C14.488 18.6667 18.6667 14.488 18.6667 9.33333C18.6667 4.17868 14.488 0 9.33333 0C4.17868 0 0 4.17868 0 9.33333C0 14.488 4.17868 18.6667 9.33333 18.6667Z" transform="translate(11.333333 0)" id="Oval" fill="#F9A000" stroke="none" />
    <path d="M0 7.33333C0 10.3048 1.43281 12.9525 3.66667 14.6667C5.90045 12.9524 7.33333 10.3048 7.33333 7.33333C7.33333 4.3619 5.90053 1.71421 3.66667 0C1.43288 1.71435 0 4.36197 0 7.33333Z" transform="translate(11.333333 2)" id="Path" fill="#FF6300" stroke="none" />
  </g>
</svg>`

export default mastercard;