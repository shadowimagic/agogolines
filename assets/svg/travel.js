const travel =`<svg width="50px" height="120px" viewBox="0 0 40 107" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<title>Group 3</title>
<defs>
    <circle id="path-1" cx="13" cy="13" r="13"></circle>
    <filter x="-42.3%" y="-38.5%" width="184.6%" height="184.6%" filterUnits="objectBoundingBox" id="filter-2">
        <feOffset dx="0" dy="1" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="3.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feComposite in="shadowBlurOuter1" in2="SourceAlpha" operator="out" result="shadowBlurOuter1"></feComposite>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.240439248 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
    </filter>
    <path d="M10.5,0 C4.71026873,0 0,4.71148621 0,10.5026573 C0,17.6896817 9.39647139,28.2406536 9.79653449,28.6863025 C10.172305,29.1049339 10.8283745,29.1041976 11.2034655,28.6863025 C11.6035286,28.2406536 21,17.6896817 21,10.5026573 C21,4.71148621 16.2896746,0 10.5,0 Z M10.5000295,16 C7.46726658,16 5,13.5327009 5,10.4999705 C5,7.46724013 7.46732553,5 10.5000295,5 C13.5327334,5 16,7.46729909 16,10.5000295 C16,13.5327599 13.5327334,16 10.5000295,16 Z" id="path-3"></path>
    <filter x="-57.1%" y="-37.9%" width="214.3%" height="182.8%" filterUnits="objectBoundingBox" id="filter-4">
        <feMorphology radius="1" operator="dilate" in="SourceAlpha" result="shadowSpreadOuter1"></feMorphology>
        <feOffset dx="0" dy="1" in="shadowSpreadOuter1" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="3.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feComposite in="shadowBlurOuter1" in2="SourceAlpha" operator="out" result="shadowBlurOuter1"></feComposite>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.24 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
    </filter>
</defs>
<g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="Barre-départ-arrivée2" transform="translate(-4.000000, -4.000000)">
        <g id="Group" transform="translate(11.000000, 10.000000)">
            <g id="Link" transform="translate(12.000000, 25.000000)" stroke="#5A48CB" stroke-linecap="square" stroke-width="2">
                <line x1="0.5" y1="0.5" x2="0.5" y2="3.5" id="Line-2"></line>
                <line x1="0.5" y1="7.5" x2="0.5" y2="10.5" id="Line-2-Copy"></line>
                <line x1="0.5" y1="14.5" x2="0.5" y2="17.5" id="Line-2-Copy-2"></line>
                <line x1="0.5" y1="21.5" x2="0.5" y2="24.5" id="Line-2-Copy-3"></line>
                <line x1="0.5" y1="28.5" x2="0.5" y2="31.5" id="Line-2-Copy-4"></line>
                <line x1="0.5" y1="35.5" x2="0.5" y2="38.5" id="Line-2-Copy-5"></line>
            </g>
            <g id="Oval-Copy-2">
                <use fill="black" fill-opacity="1" filter="url(#filter-2)" xlink:href="#path-1"></use>
                <circle stroke="#FFFFFF" stroke-width="2" stroke-linejoin="square" fill="#B2B2B2" fill-rule="evenodd" cx="13" cy="13" r="12"></circle>
            </g>
            <g id="pin" transform="translate(2.000000, 63.000000)" fill-rule="nonzero">
                <g id="Shape">
                    <use fill="black" fill-opacity="1" filter="url(#filter-4)" xlink:href="#path-3"></use>
                    <use stroke="#FFFFFF" stroke-width="2" fill="#5A48CB" xlink:href="#path-3"></use>
                </g>
            </g>
        </g>
    </g>
</g>
</svg>`;

export default travel;
