const confirm = `<?xml version="1.0" encoding="utf-8"?>
<svg width="60px" height="60px" viewBox="0 0 58 58" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
  <path d="M29 58C45.0163 58 58 45.0163 58 29C58 12.9837 45.0163 0 29 0C12.9837 0 0 12.9837 0 29C0 45.0163 12.9837 58 29 58Z" id="Oval" fill="#5A48CB" fill-rule="evenodd" stroke="none" />
</svg>`;

export default confirm;
