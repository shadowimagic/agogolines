 const pointBleue = `<svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<title>Oval Copy 4</title>
<g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="Menu-profil-voyageur" transform="translate(-169.000000, -384.000000)" fill="#5A48CB">
        <g id="Menu" transform="translate(0.000000, 86.000000)">
            <g id="Points" transform="translate(57.000000, 267.000000)">
                <circle id="Oval-Copy-4" cx="120" cy="39" r="8"></circle>
            </g>
        </g>
    </g>
</g>
</svg>`;

export default pointBleue;