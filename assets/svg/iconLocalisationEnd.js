const iconLocalisationEnd = `<?xml version="1.0" encoding="utf-8"?>
<svg viewBox="0 0 35 47" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
  <defs>
    <filter filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB" id="filter_1">
      <feFlood flood-opacity="0" result="BackgroundImageFix" />
      <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
      <feOffset dx="0" dy="1" />
      <feGaussianBlur stdDeviation="3.5" />
      <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.23921569 0" />
      <feBlend mode="normal" in2="BackgroundImageFix" result="effect0_dropShadow" />
      <feBlend mode="normal" in="SourceGraphic" in2="effect0_dropShadow" result="shape" />
    </filter>
  </defs>
  <g id="pin" transform="translate(1 1)">
    <path d="M16.5 0C7.40185 0 0 7.31093 0 16.2972C0 27.4495 14.7659 43.8217 15.3946 44.5132C15.9851 45.1628 17.016 45.1617 17.6054 44.5132C18.2341 43.8217 33 27.4495 33 16.2972C32.9998 7.31093 25.5981 0 16.5 0ZM16.5 24.8276C11.7343 24.8276 7.85714 20.999 7.85714 16.2931C7.85714 11.5871 11.7344 7.75862 16.5 7.75862C21.2657 7.75862 25.1429 11.5872 25.1429 16.2931C25.1429 20.9991 21.2657 24.8276 16.5 24.8276Z" id="Shape" fill="#5A48CB" stroke="#FFFFFF" stroke-width="2" filter="url(#filter_1)" />
  </g>
</svg>`;
export default iconLocalisationEnd;
