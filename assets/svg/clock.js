const clock = `<?xml version="1.0" encoding="utf-8"?>
<svg width="28px" height="28px" viewBox="0 0 28 28" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
  <g id="Icon" transform="translate(1 1)">
    <path d="M13 26C20.1797 26 26 20.1797 26 13C26 5.8203 20.1797 0 13 0C5.8203 0 0 5.8203 0 13C0 20.1797 5.8203 26 13 26Z" transform="translate(0.22580644 -0.016129032)" id="Oval" fill="none" fill-rule="evenodd" stroke="#5A48CB" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
    <path d="M0 0L0 8L4 12" transform="translate(13.225806 4.983871)" id="Shape" fill="none" fill-rule="evenodd" stroke="#5A48CB" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
  </g>
</svg>`

export default clock;