const rondpoint = `
<?xml version="1.0" encoding="UTF-8"?>
<svg width="99px" height="99px" viewBox="0 0 99 99" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <title>Group</title>
    <defs>
        <circle id="path-1" cx="38.5" cy="38.5" r="38.5"></circle>
        <filter x="-21.4%" y="-21.4%" width="142.9%" height="142.9%" filterUnits="objectBoundingBox" id="filter-2">
            <feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
            <feGaussianBlur stdDeviation="5.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
            <feComposite in="shadowBlurOuter1" in2="SourceAlpha" operator="out" result="shadowBlurOuter1"></feComposite>
            <feColorMatrix values="0 0 0 0 0.362969882   0 0 0 0 0.362969882   0 0 0 0 0.362969882  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
        </filter>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Navigation" transform="translate(-44.000000, -564.000000)">
            <g id="Group" transform="translate(55.000000, 575.000000)">
                <g id="Oval">
                    <use fill="black" fill-opacity="1" filter="url(#filter-2)" xlink:href="#path-1"></use>
                    <circle stroke="#FFFFFF" stroke-width="5" stroke-linejoin="square" fill="#5A48CB" fill-rule="evenodd" cx="38.5" cy="38.5" r="36"></circle>
                </g>
                <path d="M52.6662141,23.3353661 C52.3385989,23.0068997 51.8450052,22.9094493 51.4175288,23.0887306 L14.7007109,38.4803268 C14.2439083,38.6717989 13.9625294,39.1363453 14.0040431,39.6303783 C14.0455567,40.1244113 14.4005176,40.5353181 14.8828378,40.6477023 L30.9595186,44.3931032 C31.2197979,44.4537525 31.4234053,44.6566535 31.4851807,44.9169275 L35.329346,61.1211942 C35.4434514,61.6022745 35.8537893,61.9554273 36.346088,61.9961142 C36.3776993,61.9987047 36.4091583,62 36.4404649,62 C36.8966582,62 37.3132421,61.7266209 37.4923222,61.3000183 L52.9107258,24.5859157 C53.0904152,24.1580179 52.9938293,23.6638325 52.6662141,23.3353661 Z" id="Path" fill="#FFFFFF" fill-rule="nonzero"></path>
            </g>
        </g>
    </g>
</svg>
`;

export default rondpoint;
