const iconLocalisationStart = `<?xml version="1.0" encoding="utf-8"?>
<svg viewBox="0 0 40 40" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
  <defs>
    <filter filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB" id="filter_1">
      <feFlood flood-opacity="0" result="BackgroundImageFix" />
      <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
      <feOffset dx="0" dy="1" />
      <feGaussianBlur stdDeviation="3.5" />
      <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.23921569 0" />
      <feBlend mode="normal" in2="BackgroundImageFix" result="effect0_dropShadow" />
      <feBlend mode="normal" in="SourceGraphic" in2="effect0_dropShadow" result="shape" />
    </filter>
    <path d="M20 40C31.0457 40 40 31.0457 40 20C40 8.9543 31.0457 0 20 0C8.9543 0 0 8.9543 0 20C0 31.0457 8.9543 40 20 40Z" id="path_1" />
    <clipPath id="clip_1">
      <use xlink:href="#path_1" />
    </clipPath>
  </defs>
  <g id="icone-localisation">
    <g id="Oval" filter="url(#filter_1)">
      <use stroke="none" fill="#BDBDBD" xlink:href="#path_1" fill-rule="evenodd" />
      <g clip-path="url(#clip_1)">
        <use xlink:href="#path_1" fill="none" stroke="#FFFFFF" stroke-width="6" />
      </g>
    </g>
    <path d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z" transform="translate(12 12)" id="Oval-Copy" fill="#FFFFFF" fill-rule="evenodd" stroke="none" />
  </g>
</svg>`;
export default iconLocalisationStart;